(ns de.uni-stuttgart.iaas.ipsm.execution-environment-integrator.docker-compose
  (:require [taoensso.timbre :as timbre]
            [environ.core :refer [env]]
            [postal.core :as postal]
            [clojure.java.io :as io]
            [de.uni-stuttgart.iaas.ipsm.protocols.core :as p]
            [puppetlabs.trapperkeeper.services :as services]
            [puppetlabs.trapperkeeper.core :as tk]
            [clojure.string :as s]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.schema :as ipsm-schema]
            [clj-yaml.core :as yaml]
            [de.uni-stuttgart.iaas.ipsm.protocols.core :as protos]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.domain-language :as domain-language]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.transformations :as transformations]
            [de.uni-stuttgart.iaas.ipsm.utils.conversions :as conversions]
            [clojure.java.shell :as shell]
            [de.uni-stuttgart.iaas.ipsm.integrator-client.core :as integrator]
            [clj-http.client :as http]
            [de.uni-stuttgart.iaas.ipsm.utils.xml :as xml-utils])
  (:import [javax.xml.namespace QName])
  (:gen-class))

(timbre/refer-timbre)



(def os-type (s/replace (shell/sh "uname" "-s") "\n" ""))
(def os-arch (s/replace (shell/sh "uname" "-m") "\n" ""))
;; (def docker-compose-dl-url (str (s/replace (constants/property :docker-compose-bin-uri)
;;                                            (constants/property :version-key)
;;                                            (constants/property :docker-compose-version))
;;                                 "-" os-type "-" os-arch))

(defn- force-create-dir!
  [dir]
  (if (.exists dir)
    (if (.isDirectory dir)
      nil
      (do
        (io/delete-file dir)
        (.mkdir dir)))
    (.mkdir dir)))

(defn- create-execution-dir!
  [m]
  {:pre [(:execution-dir-path m)
         (:execution-path m)]}
  (force-create-dir! (io/file (:execution-path m)))
  (force-create-dir! (io/file (:execution-dir-path m))))


(defn- download-docker-bin!
  [m]
  {:pre [(:executable-path m) (:executable-name m)]}
  (when-not (.exists (io/file (str (:executable-path m) (:executable-name m))))
    (debug "Downloading from the following url" (:docker-compose-dl-url m))
    (io/copy (io/input-stream (:docker-compose-dl-url m)
                              )
             (io/file (str (:executable-path m) (:executable-name m))))
    (shell/sh "chmod" "+x" (str (:executable-path m) (:executable-name m)))))

(defn- add-process-counter-file-path
  [m]
  {:pre [(:process-counter-file-name m)
         (:execution-dir-path m)]}
  (debug "Adding process counter file path")
  (assoc m :process-counter-file-path (str (:execution-dir-path m) (:process-counter-file-name m))))



(defn- add-execution-dir-path
  [m]
  {:pre [(:target-resource m)
         (:redo/type (:target-resource m))
         (:execution-path m)]}
  (debug "Adding execution path" (:execution-path m))
  (assoc m :execution-dir-path (if (.endsWith (:execution-path m) "/")
                                 (str (:execution-path m) (conversions/winery-encode (:redo/type (:target-resource m))) "/")
                                 (str (:execution-path m) "/" (conversions/winery-encode (:redo/type (:target-resource m))) "/"))))

(defn- add-execution-dir-path-exists?
  [m]
  {:pre [(:execution-dir-path m)]}
  (debug "Adding execution path" m)
  (assoc m :execution-dir-path-exists? (.exists (io/file (:execution-dir-path m)))))

(defn- read-process-counter
  [m]
  {:pre [(:process-counter-file-path m)]}
  (read-string (slurp (io/file (:process-counter-file-path m)))))

(defn- increment-counter!
  [m]
  {:pre [(:process-counter-file-path m)]}
  (debug "Incrementing counter" m)
  (if (.exists (io/file (:process-counter-file-path m)))
    (spit (:process-counter-file-path m) (str (inc (read-process-counter m))))
    (spit (:process-counter-file-path m) "1")))


(defn- decrement-counter!
  [m]
  (debug "Counter will be decremented!!")
  (if (.exists (io/file (:process-counter-file-path m)))
    (spit (:process-counter-file-path m) (dec (read-process-counter m)))
    (spit (:process-counter-file-path m) "0")))


(defn- pass-resource-instance!
  [m]
  {:pre [(:callback m) (:resulting-resource m)]}
  (.onSuccess (:callback m) [] "upcoming"))

(defn- add-resulting-instance-definition
  [m]
  {:pre [(:target-resource m)]}
  (assoc m :resulting-resource (:target-resource m)))

(defn- create-and-pass-resource-instance!
  [m]
  {:pre [(:callback m)]}
  (debug "Creating an passing callback" m)
  (.completeProcessExecutionSuccessfully
   (:current-obj m)
   (java.net.URI.
    (or (:resource-uri m)
        "http://www.example.org/missing-uri/error/"))
   (:callback m)))

(defn- create-and-pass-error!
  [m]
  {:pre [(:callback m) (:current-obj m)]}
  (debug "Passing error to the callback" (or (:err (:compose-up-res m)) "Resource could not be engaged!!"))
  (.completeProcessExecutionwithError (:current-obj m) (or (:err (:compose-up-res m)) "Resource could not be engaged!!") (:callback m)))


(defn- add-projects-api-uri
  [m]
  {:pre [(:resource-uri m)
         (:projects-api-path m)]
   :post [(:target-api-uri %)]}
  (debug "Adding project api uri" (str (:resource-uri m)
                                       (:projects-api-path m)))
  (assoc m :target-api-uri (str (:resource-uri m)
                                (:projects-api-path m))))

(defn- add-redmine-project-post-body
  [m]
  {:pre [(:project-name m)
         (:project-id m)]}
  (assoc m :post-body {:project
                       {:name (:project-name m)
                        :identifier (:project-id m)
                        :is_public true
                        :enabled_module_names ["issue_tracking"
                                               "news"
                                               "wiki"
                                               "time_tracking"
                                               "calendar"]}}))

(defn- post-to-redmine!
  [m]
  {:pre [(:target-api-uri m)
         (:post-body m)
         (:redmine-username m)
         (:redmine-password m)]}
  (debug "Posting to redmine"  (cheshire.core/generate-string (:post-body m)))
  (debug (try
           (http/post (:target-api-uri m)
                         {:body (cheshire.core/generate-string (:post-body m))
                          :content-type :json
                          :accept :json
                          :basic-auth [(:redmine-username m) (:redmine-password m)]
                          :debug true})
           (catch Exception e
             (do
               (debug "Failure at request will retry " (:retry m) " "  e)
               (Thread/sleep 10000)
               (if (or (not (:retry m)) (< (:retry m) 100))
                 (post-to-redmine! (update-in m [:retry] #(or (and % (inc %)) 1)))
                 (debug "Retry max has been reached stopping")))))))


(defn- add-project-name
  [m]
  {:pre [(:process-id m)]}
  (assoc m :project-name (:redo/name (:process-id m))))


(defn- add-project-id
  [m]
  {:pre [(:process-id m)]}
  (assoc m :project-id (conversions/get-unique-id-from-entity-identity (:process-id m))))

(defn- add-process-id
  [m]
  {:pre [(:redo/process-id (:parameters m))]
   :post [(:process-id %)]}
  (debug "Adding process id" (:redo/process-id (:parameters m)))
  (assoc m :process-id (:redo/process-id (:parameters m))))

(defn- post-process-allocation!
  [m]
  (debug "Post processing allocation before passing that" (:redo/name (:target-resource m)))
  (cond
    (= (:redo/type (:target-resource m)) "{http://www.iaas.uni-stuttgart.de/ipsm/domains/docker-compose/metadata-based}redmine") ;; post process redmine initalization!
    (-> m
        add-process-id
        add-project-name
        add-project-id
        add-projects-api-uri
        add-redmine-project-post-body
        post-to-redmine!)
    :default
    m))


(defn- post-process-docker-up!
  [m]
  {:pre [(:compose-up-res m) (:exit (:compose-up-res m))]}
  (debug "Post processing docker up result without any pause")
  (if (= (:exit (:compose-up-res m)) 0)
    (do
      (increment-counter! m)
      (create-and-pass-resource-instance! m)
      (post-process-allocation! m))
    (create-and-pass-error! m)))


(defn- execute-docker-compose-up!
  [m]
  {:pre [(:execution-dir-path m)
         (:executable-name m)]}
  (info "Execution docker compose up!!!")
  (-> m
      (assoc :compose-up-res
             (if (:exec-env m)
               (shell/sh (:executable-name m) "up" "-d" "--no-recreate" :dir (:execution-dir-path m)
                         :env (:exec-env m))
               (shell/sh (:executable-name m) "up" "-d" "--no-recreate" :dir (:execution-dir-path m))))
      post-process-docker-up!))


(defn- execute-docker-compose-stop!
  [m]
  {:pre [(:execution-dir-path m)
         (:executable-name m)
         (:exec-env m)]}
  (info "Execution docker compose stop!!!" m)
  (-> m
      (assoc :compose-up-res
             (if (:exec-env m)
               (shell/sh (:executable-name m) "stop" :dir (:execution-dir-path m)
                         :env (:exec-env m))
               (shell/sh (:executable-name m) "stop"  :dir (:execution-dir-path m))))
      (debug)))


(defn- add-labels
  [m]
  {:pre [(:target-resource-runnable m)]}
  (debug "Adding labels")
  (assoc m :labels (:labels (yaml/parse-string (:target-resource-runnable m)))))

(defn- remove-labels
  [m]
  {:pre [(:target-resource-runnable m)]}
  (debug "Removing labels")
  (assoc m :target-resource-runnable
         (yaml/generate-string (dissoc (yaml/parse-string (:target-resource-runnable m)) :labels))))


(defn- remove-labels-from-stream
  [m]
  {:pre [(:target-resource-runnable m)]}
  (debug "Removing labels from stream")
  (-> m
      add-labels
      remove-labels))

(defn- copy-runnable-into-execution-dir!
  [m]
  (debug "Copying runnable into execution dir ")
  (io/copy (:target-resource-runnable m) (io/file (:execution-dir-path m) "docker-compose.yml")))



(defn- add-resource-uri-from-docker-compose-labels
  [m]
  {:pre [(:target-resource-runnable m)
         (:resource-uri-label-key m)]}
  {:pre [(:labels m)]}
  (debug "Adding resource ur from docker compose labels" )
  (let [label-key (keyword (:resource-uri-label-key m))]
    (->> m
         :labels
         label-key
         (assoc m :resource-uri))))


(defn- prepare-for-cmd-exec
  [m]
  (-> m
      add-execution-dir-path
      remove-labels-from-stream
      add-resource-uri-from-docker-compose-labels
      add-process-counter-file-path))

(defn- prepare-docker-compose!
  [m]
  (doto (-> m
            prepare-for-cmd-exec)
    download-docker-bin!
    create-execution-dir!
    copy-runnable-into-execution-dir!))

(defn- apply-and-add-into-map-if-not-nil
  [m key val desired-fn]
  (if val
    (assoc m key (desired-fn val))
    m))

(defn- init-deployable
  [m]
  (debug "Resource will be initialized")
  (doto (-> m
            prepare-for-cmd-exec)
    prepare-docker-compose!
    execute-docker-compose-up!))


(defn- add-identity-from-type
  [m]
  {:pre [(:type m)]}
  (assoc m :resource-id (.substring (re-find #"--.*--" (.getLocalPart (:type m)))
                                    2
                                    (- (.length (re-find #"--.*--" (.getLocalPart (:type m)))) 2))))

(defn- add-name-from-type
  [m]
  {:pre [(:type m)]}
  (debug "Local part is:" (.getLocalPart (:type m)))
  (assoc m :resource-name (first (.split (.getLocalPart (:type m)) "--"))))

(defn- add-last-name-from-type
  [m]
  {:pre [(:resource-name m)]}
  (assoc m :last-name (last (.split (:resource-name m) " "))))

(defn- add-first-name-from-type
  [m]
  {:pre [(:resource-name m)]}
  (assoc m :first-name (clojure.string/join " " (drop-last (.split (:resource-name m) " ")))))

(defn- add-login-name-from-id
  [m]
  {:pre [(:resource-id m)]}
  (assoc m :login-name (:resource-id m)))

(defn- add-resource-type
  [m]
  (debug "Adding resource type" (:redo/type (:redo/relationship-source-resource (:parameters m))))
  (assoc m :type (QName/valueOf (:redo/type (:redo/relationship-source-resource (:parameters m))))))

(defn- add-redmine-uri
  [m]
  (assoc m :redmine-uri (:redo/instance-uri (:redo/relationship-target-resource-instance-descriptor (:parameters m)))))

(defn- add-redmine-users-uri
  [m]
  {:pre [(:redmine-uri m)
         (:users-path m)]}
  (assoc m :redmine-users-uri (str (:redmine-uri m) (:users-path m))))

(defn- add-redmine-user-membership-uri
  [m]
  (assoc m :redmine-user-membership-uri (str (:redmine-uri m) "projects/" (:project-id m) "/memberships.json")))

(defn- add-redmine-user-password
  [m]
  (assoc m :password (conversions/create-unique-id-from-str (:resource-id m))))

(defn- add-user-creation-body
  [m]
  (assoc m :user-creation-body {:user {:login (:login-name m)
                                       :firstname (:first-name m)
                                       :lastname (:last-name m)
                                       :mail (:resource-id m)
                                       :password (:password m)
                                       :must_change_password true
                                       }}))

(defn- post-creation-message!
  [m]
  (debug "Posting creation" m)
  (when-not (:user-exists? m)
    (post-to-redmine! (clojure.set/rename-keys m {:user-creation-body :post-body :redmine-users-uri
                                                  :target-api-uri}))))


(defn- add-items-from-redmine
  [m]
  (assoc m :items (cheshire.core/parse-string (:body (http/get (:target-api-uri m)
                                                               {:accept :json
                                                                :basic-auth [(:redmine-username m) (:redmine-password m)]
                                                                :debug true}))
                                              true)))

(defn- add-existing-users
  [m]
  (assoc m :existing-users (-> (clojure.set/rename-keys m {:redmine-users-uri :target-api-uri})
                               add-items-from-redmine
                               :items
                               :users)))


(defn- add-user-exists?
  [m]
  {:pre [(:login-name m)]}
  (assoc m :user-exists? (->> m
                              add-existing-users
                              :existing-users
                              (filterv #(= (:login-name m) (:login %)))
                              seq
                              first)))


(defn- add-user-membership-message
  [m]
  {:pre [(:redmine-user-id m)]}
  (assoc m :user-membership-message {:membership {:user_id (:redmine-user-id m)
                                                  :role_ids (:role-ids m)}}))

(defn- add-user-id
  [m]
  (if (:user-exists? m)
    (assoc m :redmine-user-id (:id (:user-exists? m)))
    (error "User has not been created previously!!!")))


(defn- post-membership-message!
  [m]
  (post-to-redmine! (clojure.set/rename-keys m {:user-membership-message :post-body
                                                :redmine-user-membership-uri
                                                :target-api-uri})))




(defn- add-password
  [m]
  (debug "Adding password")
  (assoc m :password (:smtp-password m)))

(defn- add-from-address
  [m]
  (debug "Adding from address host")
  (assoc m :from (:smtp-from m)))

(defn- add-from-user
  [m]
  (debug "Adding from user")
  (assoc m :user (:smtp-user m)))


(defn- add-information-body
  [m]
  {:pre [(:resource-name m)
         (:process-id m)
         (:redmine-uri m)
         (:login-name m)]
   :post [(:message-body %)]}
  (debug "Adding invitation message body")
  (assoc m :message-body (str "Dear " (:resource-name m) ",\n\n"
                              "Thank you for your participation in: " (:redo/name (:process-id m)) "\n\n"
                              "Please login to: " (:redmine-uri m)  "\n\n"
                              "Using: \n\n"
                              "User name:" (:login-name m) "\n\n"
                              "Password:" (:password m)"\n\n"
                              "Yours Sincerly,\n\n"
                              "CoAct Informer Service\n\n")))


(defn- send-email!
  [m]
  {:pre [(:smtp-host m)
         (:user m)
         (:password m)
         (:from m)
         (:resource-id m)
         (:message-subject m)
         (:message-body m)]}
  (debug "Sending email!"
         {:host (:smtp-host m)
          :user (:user m)
          :pass (:password m)
          :ssl true}
         {:from (:from m)
          :to (:resource-id m)
          :subject (:message-subject m)
          :body (:message-body m)})
  (debug (postal/send-message {:host (:smtp-host m)
                               :user (:user m)
                               :pass (:password m)
                               :ssl true}
                              {:from (:from m)
                               :to (:resource-id m)
                               :subject (:message-subject m)
                               :body (:message-body m)})))


(defn- add-message-subject
  [m]
  (assoc m :message-subject (str "[Information] Your Account Is Ready:" (.getLocalPart (:target-resource-type m)))))


(defn- inform-user!
  [m]
  (doto (-> m
            add-password
            add-from-user
            add-from-address
            add-message-subject
            add-information-body)
    send-email!))




(defn- create-and-post-membership-message!
  [m]
  (doto
      (-> m
          add-user-exists?
          add-user-id
          add-user-membership-message)
    post-membership-message!))


(defn- establish-relationship-redmine
  [m]
  (debug "Establishing relationship redmine")
  (doto
      (-> m
          add-process-id
          add-project-id
          add-resource-type
          add-identity-from-type
          add-name-from-type
          add-last-name-from-type
          add-first-name-from-type
          add-login-name-from-id
          add-redmine-uri
          add-redmine-users-uri
          add-redmine-user-membership-uri
          add-redmine-user-password
          add-user-creation-body
          add-user-exists?)
    post-creation-message!
    create-and-post-membership-message!
    inform-user!))


(defn- add-target-resource-type
  [m]
  (debug "Adding resource type" (:redo/type (:redo/relationship-target-resource (:parameters m))))
  (assoc m :target-resource-type (QName/valueOf (:redo/type (:redo/relationship-target-resource (:parameters m))))))

(defn- establish-manages-relationship
  [m]
  (debug "Establishing relationship based on target resource type")
  (let [target-resource-type (:target-resource-type (add-target-resource-type m))]
    (cond->> m
      :default add-target-resource-type
      (= (.getLocalPart target-resource-type) "redmine") establish-relationship-redmine
      (not (= (.getLocalPart target-resource-type) "redmine")) (debug "Unsupported type" (.getLocalPart target-resource-type)))))



(defn- release-or-decrement-counter!
  [m]
  (let [counter (read-process-counter m)]
    (debug "Counter is" counter)
    (if (> counter 1)
      (decrement-counter! m)
      (if (= counter 1)
        (do
          (execute-docker-compose-stop! m)
          (decrement-counter! m))))))

(defn- release-deployable
  [m]
  (debug "Resource will be released")
  (doto (-> m
            prepare-for-cmd-exec)
    prepare-docker-compose!
    release-or-decrement-counter!))

(gen-class :name "uni_stuttgart.ipsm.execution_environment_integrator.AcquireResourceOperation"
           :extends uni_stuttgart.ipsm.protocols.integration.operations.lifecycle.AcquireResourceOperation
           :prefix "acquire-resources-")

(defn acquire-resources-executeOperation
  [this target-resource-runnable-container parameters callback]
  (init-deployable {:target-resource (transformations/get-map-of-jaxb-type-obj
                                      (.getTargetModel (first target-resource-runnable-container)))
                    :target-resource-runnable (slurp (.getDeployable (first target-resource-runnable-container)))
                    :callback callback
                    :current-obj this}))

(gen-class :name "uni_stuttgart.ipsm.execution_environment_integrator.AcquireRelationshipOperation"
           :extends uni_stuttgart.ipsm.protocols.integration.operations.lifecycle.AcquireRelationshipOperation
           :prefix "acquire-relationships-")

(defn acquire-relationships-getSupportedTargetRelationships [_] [])

(defn acquire-relationships-getSupportedSourceRelationships [_] [])

(defn acquire-relationships-getSupportedTargetDomains [_] [])

(defn acquire-relationships-getSupportedSourceDomains [_] [])

(defn acquire-relationships-executeOperation
  [_ target-resource-runnable-container parameters callback]
  (init-deployable target-resource-runnable-container parameters callback))

(gen-class :name "uni_stuttgart.ipsm.execution_environment_integrator.ReleaseResourceOperation"
           :extends uni_stuttgart.ipsm.protocols.integration.operations.lifecycle.ReleaseResourceOperation
           :prefix "release-resources-")

(defn release-resources-getSupportedResources [_] nil)

(defn release-resources-executeOperation
  [this target-resource-runnable-container parameters callback]
  (debug "Executing release operation")
  (release-deployable {:target-resource (transformations/get-map-of-jaxb-type-obj
                                         (.getTargetModel (first target-resource-runnable-container)))
                       :target-resource-runnable (slurp (.getDeployable (first target-resource-runnable-container)))
                       :callback callback
                       :current-obj this}))

(gen-class :name "uni_stuttgart.ipsm.execution_environment_integrator.ReleaseResourceOperation"
           :extends uni_stuttgart.ipsm.protocols.integration.operations.lifecycle.StoreResourceOperation
           :prefix "store-resources-")

(defn store-resources-getSupportedResources [_] nil)


(defn store-resources-executeOperation
  [_ target-resource-runnable-container parameters callback]
  (init-deployable target-resource-runnable-container parameters callback))



(deftype DockerExecutionEnvironmentMetadata [m]
  uni_stuttgart.ipsm.protocols.integration.IntegratorMetadata
  (getName [_] (:name m))
  (getTargetNamespace [_] (java.net.URI. (:domain-uri m)))
  (getUri [_] (java.net.URI. (:uri m))))


(defn acquire-resource-operation
  [m]
  {:pre [(:domain-uri m)]}
  (proxy [uni_stuttgart.ipsm.protocols.integration.operations.lifecycle.AcquireResourceOperation]
      []
    (getSupportedResources [] nil)
    (getSupportedDomains [] [(:domain-uri m)])
    (executeOperation [target-resource-runnable-container parameters callback]
      (debug "Executing acquire operation" (transformations/get-map-of-jaxb-type-obj
                                            (.getTargetModel (first target-resource-runnable-container))))
      (init-deployable (into m {:target-resource (transformations/get-map-of-jaxb-type-obj
                                                  (.getTargetModel (first target-resource-runnable-container)))
                                :target-resource-runnable (slurp (.getDeployable (first target-resource-runnable-container)))
                                :parameters parameters
                                :callback callback
                                :current-obj this})))))

(defn acquire-relationship-operation
  [m]
  {:pre [(:domain-uri m)]}
  (proxy [uni_stuttgart.ipsm.protocols.integration.operations.lifecycle.AcquireRelationshipOperation]
      []
    (getSupportedTargetRelationships [] [(str (QName. (:domain-uri m) "uses"))
                                         (str (QName. (:domain-uri m) "manages"))])
    (getSupportedSourceRelationships [] [])
    (getSupportedTargetDomains [] [])
    (getSupportedSourceDomains [] [])
    (executeOperation [target-resource-runnable-container parameters callback]
      (do
        (let [target-relationship (transformations/get-map-of-jaxb-type-obj
                                   (.getTargetModel (first target-resource-runnable-container)))]
          (debug "Establishing relationship" target-relationship)
          (establish-manages-relationship
           (into m {:target-relationship target-relationship
                    :parameters parameters
                    :role-ids (if (= "{http://www.iaas.uni-stuttgart.de/ipsm/domains/docker-compose/metadata-based}manages"
                                     (:redo/type target-relationship))
                                [(:manager-role-id m)]
                                [(:developer-role-id m)])
                    :callback callback
                    :current-obj this})))))))



(defn release-operation
  [m]
  {:pre [(:domain-uri m)]}
  (proxy [uni_stuttgart.ipsm.protocols.integration.operations.lifecycle.ReleaseResourceOperation]
      []
    (getSupportedResources [] nil)
    (getSupportedDomains [] [(:domain-uri m)])
    (executeOperation [target-resource-runnable-container parameters callback]
      (debug "Executing release operation")
      (release-deployable (into m {:target-resource (transformations/get-map-of-jaxb-type-obj
                                                     (.getTargetModel (first target-resource-runnable-container)))
                                   :target-resource-runnable (slurp (.getDeployable (first target-resource-runnable-container)))
                                   :callback callback
                                   :current-obj this})))))

(defn store-operation
  [m]
  {:pre [(:domain-uri m)]}
  (proxy [uni_stuttgart.ipsm.protocols.integration.operations.lifecycle.StoreResourceOperation]
      []
    (getSupportedResources [] nil)
    (getSupportedDomains [] [(:domain-uri m)])
    (executeOperation [target-resource-runnable-container parameters callback]
      (init-deployable target-resource-runnable-container parameters callback))))

(defn run-dip
  [m]
  (integrator/register-execution-environment-integrator! [(store-operation m)
                                                          (release-operation m)
                                                          (acquire-resource-operation m)
                                                          (acquire-relationship-operation m)]
                                                         (DockerExecutionEnvironmentMetadata. m)
                                                         m))



(tk/defservice DockerComposeService
  p/DSOPDockerCompose
  [;; [:WebserverService add-war-handler]
   [:ConfigService get-config]
   ToscaPersistenceProtocol]
  (init [this context] context)
  (start [this context]
         (info "Starting docker compose domain-specific operation provider")
         (assoc context :dsop-docker-compose-map (-> (:docker-compose-execution-config (get-config))
                                                     (assoc :tosca-persistence (services/get-service this :ToscaPersistenceProtocol))
                                                     (assoc :resources ipsm-schema/resources)
                                                     run-dip)))
  (stop [this context]
        (info "Shutting docker compose domain-specific operation provider")
        (integrator/stop-execution-environment-integrator-handlers! context)
        context))
