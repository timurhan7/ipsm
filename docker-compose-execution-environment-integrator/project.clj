(defproject de.uni-stuttgart.iaas.ipsm/docker-compose-execution-environment-integrator "0.0.1-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [puppetlabs/trapperkeeper "1.5.3"]
                 [com.taoensso/timbre "4.10.0"]
                 [com.draines/postal "2.0.2"]
                 [de.uni-stuttgart.iaas.ipsm/integrator-client "0.0.3-SNAPSHOT"]
                 [midje "1.8.2"]
                 [org.clojure/core.async "0.4.474"]
                 [circleci/clj-yaml "0.5.6"]
                 [de.uni-stuttgart.iaas.ipsm/commons "0.0.2-SNAPSHOT"]
                 [de.uni-stuttgart.iaas.ipsm/protocols "0.5.3-SNAPSHOT"]
                 [environ "1.0.0"]
                 [clj-http "3.7.0"]
                 [com.github.docker-java/docker-java "3.0.13"]
                 [uni-stuttgart.ipsm/tosca-persistence "0.0.1-SNAPSHOT"]]
  :plugins [[lein-environ "1.0.0"]
            [lein-modules "0.3.11"]]
  :source-paths ["src/main/clj"]
  :resource-paths ["src/main/resources"]
  :repl-options {:init-ns user}
  :aliases {"tk" ["trampoline" "run" "--config" "dev-resources/config.conf"]}
  :main puppetlabs.trapperkeeper.main)
