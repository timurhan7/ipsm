(ns de.uni-stuttgart.iaas.ipsm.providers.winery
  (:require [taoensso.timbre :as t]
            [de.uni-stuttgart.iaas.ipsm.utils.constants :as c]
            [de.uni-stuttgart.iaas.ipsm.utils.winery :as w]
            [de.uni-stuttgart.iaas.ipsm.utils.io :as io-utils]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.domain-language :as dl]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.transformations :as tx]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.tosca :as to]
            [clj-http.client :as http]
            [clojure.java.io :as io]
            [cheshire.core :as j]
            [de.uni-stuttgart.iaas.ipsm.utils.xml :as xu]))

(t/refer-timbre)

(def default-winery-paths {:node-type "nodetypes/"
                           :capability-type "capabilitytypes/"
                           :relationship-type "relationshiptypes/"
                           :artifact-type "artifacttypes/"
                           :artifact-template "artifacttemplates/"
                           :service-template "servicetemplates/"
                           :node-type-implementation "nodetypeimplementations/"
                           :relationship-type-implementation "relationshiptypeimplementations/"})

(def ^:private initiation-map {:winery-uri (c/property :winery-uri)
                               :winery-path-mappings (or (some-> "entity-path-mappings.edn" io-utils/read-resource read-string)
                                                         default-winery-paths)})



(defn- add-subsitute-if-possible
  [m]
  {:pre [(:service-template m)]}
  (if (:substituable-node-type (:service-template m))
    (debug "TODO"))
  m)

(defn- add-converted-if-needed
  [m]
  (if (:substitution-node-type m)
    (clojure.set/rename-keys m {:substitution-node-type :node-type})
    (assoc m :node-type
           (dl/resource-definition {:name (:id (:service-template m))
                                    :target-namespace (:target-namespace (:service-template m))
                                    :abstract "no"
                                    :final "yes"}))))

(defn- add-converted-service-templates
  "Converts a service template into a node type definition. Uses
  subsitutableNodeType field where applciable"
  [m]
  (-> m
      add-subsitute-if-possible
      add-converted-if-needed))


(defn- add-available-resources
  "Gets the available resource maps"
  [m]
  (->> (assoc m :entity-data {:entity-type :service-template})
       w/get-entity-type-from-winery
       :types
       (mapv #(-> (add-converted-service-templates (assoc m :service-template %))
                  :node-type))
       (assoc m :node-types)))

(defn- merge-node-types-into-types
  [m]
  (debug "Merging types" m)
  (update-in m [:types] into (:node-types m)))


(defn list-domain []
  (or (some->> (assoc initiation-map :entity-types [:relationship-type :capability-type])
               add-available-resources
               w/get-entity-types-from-winery
               merge-node-types-into-types
               :types
               seq
               (apply tx/get-definitions-obj-with-types))
      (org.oasis_open.docs.tosca.ns._2011._12.Definitions.)))


(defn create-deployable-for-type
  [m]
  (->> m
       w/add-csar-stream
       :csar-stream))


(deftype CSARDeployable [t]
  uni_stuttgart.ipsm.protocols.integration.Deployable
  (getTargetRuntime[this] (c/property :target-ee))
  (getDeployable [this] (create-deployable-for-type
                         (-> (assoc initiation-map :entity-data {:entity-type :service-template})
                             (assoc-in [:entity-data :target-namespace] (.getNamespaceURI t))
                             (assoc-in [:entity-data :id] (.getLocalPart t))))))

(deftype WineryResourceProviderOperations []
  uni_stuttgart.ipsm.protocols.integration.DomainManagerOperations
  (listDomain [_] (list-domain))
  (getTargetExecutionEnvironment [this resource-type] (c/property :target-ee))
  (getDeployable [_ resource-type intentions] (create-deployable-for-type resource-type)))


(deftype WineryResourceProviderMetadata []
  uni_stuttgart.ipsm.protocols.integration.IntegratorMetadata
  (getName [_] (c/property :name))
  (getTargetNamespace [_] (c/property :domain-uri))
  (getUri [_] (java.net.URI. (c/property :rest-url))))
