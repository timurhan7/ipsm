#!/bin/bash

filePath=$1
packageName=$2


mvn org.apache.maven.plugins:maven-install-plugin:2.5.2:install-file -Dfile=$filePath -DgroupId=de.uni-stuttgart.iaas.ipsm -DartifactId=$packageName -Dversion=0.0.1-SNAPSHOT -Dpackaging=jar -DlocalRepositoryPath=../coact/repo
