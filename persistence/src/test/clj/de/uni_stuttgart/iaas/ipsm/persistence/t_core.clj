(ns de.uni-stuttgart.iaas.ipsm.persistence.t-core
  (:require [de.uni-stuttgart.iaas.ipsm.persistence.core :as persistence-core]
            [taoensso.timbre :as timbre]))

(timbre/refer-timbre)
