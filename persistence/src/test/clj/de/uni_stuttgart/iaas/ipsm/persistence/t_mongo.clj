(ns de.uni-stuttgart.iaas.ipsm.persistence.t-mongo
  (:require [de.uni-stuttgart.iaas.ipsm.persistence.mongo :as mongo]
            [clojure.test :as clj-test]
            [taoensso.timbre :as timbre]))

(timbre/refer-timbre)

(def test-data [{:redo/entity-type :redo/resource-driven-process-definition-test
                 :redo/initializable-entity-definition
                 {:redo/identifiable-entity-definition
                  {:redo/entity-identity {:redo/name "Test Name"
                                          :redo/target-namespace "Test Namespace"}}}}
                {:redo/entity-type :redo/resource-driven-process-definition-test
                 :redo/initializable-entity-definition
                 {:redo/identifiable-entity-definition
                  {:redo/entity-identity {:redo/name "Test Name 2"
                                          :redo/target-namespace "Test Namespace"}}}}
                {:redo/entity-type :redo/context-definition-test
                 :redo/identifiable-entity-definition
                 {:redo/entity-identity
                  {:redo/name "Test Name" :redo/target-namespace "Test Namespace"}}}])



(defn fixture-setup
  [t-fn]
  (debug "Setting up the test")
  (mapv (fn [e] (mongo/add-to-collection! (:redo/entity-type e) e)) test-data)
  (t-fn)
  (mapv (fn [e] (mongo/remove-obj-by-id! (:redo/entity-type e) e)) test-data))


(clj-test/use-fixtures :once fixture-setup)

(defn is-subset-of?
  [m1 m2]
  (and (every? (set (keys m1)) (keys m2))  ;; subset on keys
       (every? #(= (m1 %)(m2 %)) (keys m2)))   ;; on that subset all the same values
  )


(clj-test/deftest get-by-id
  (clj-test/is (is-subset-of?
                (mongo/get-by-id
                 :redo/resource-driven-process-definition-test
                 {:redo/initializable-entity-definition
                  {:redo/identifiable-entity-definition
                   {:redo/entity-identity {:redo/name "Test Name"
                                           :redo/target-namespace "Test Namespace"}}}})
                {:redo/entity-type :redo/resource-driven-process-definition-test
                 :redo/initializable-entity-definition
                 {:redo/identifiable-entity-definition
                  {:redo/entity-identity {:redo/name "Test Name"
                                          :redo/target-namespace "Test Namespace"}}}}

                ))
  (clj-test/is (is-subset-of?
                (mongo/get-by-id
                 :redo/resource-driven-process-definition-test
                 {:redo/initializable-entity-definition
                  {:redo/identifiable-entity-definition
                   {:redo/entity-identity {:redo/name "Test Name 2"
                                           :redo/target-namespace "Test Namespace"}}}})
                {:redo/entity-type :redo/resource-driven-process-definition-test
                 :redo/initializable-entity-definition
                 {:redo/identifiable-entity-definition
                  {:redo/entity-identity {:redo/name "Test Name 2"
                                          :redo/target-namespace "Test Namespace"}}}}

                ))
  (clj-test/is (is-subset-of?
                (mongo/get-by-id
                 :redo/context-definition-test
                 {:redo/entity-type :redo/context-definition-test
                  :redo/identifiable-entity-definition
                  {:redo/entity-identity
                   {:redo/name "Test Name" :redo/target-namespace "Test Namespace"}}})
                {:redo/entity-type :redo/context-definition-test
                 :redo/identifiable-entity-definition
                 {:redo/entity-identity
                  {:redo/name "Test Name" :redo/target-namespace "Test Namespace"}}})))






(clj-test/run-tests 'de.uni-stuttgart.iaas.ipsm.persistence.t-mongo)
