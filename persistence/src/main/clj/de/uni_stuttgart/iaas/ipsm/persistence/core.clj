(ns de.uni-stuttgart.iaas.ipsm.persistence.core
  (:require [taoensso.timbre :as timbre]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.schema :as sc]
            [de.uni-stuttgart.iaas.ipsm.utils.conversions :as cv]
            [clojure.spec.alpha :as sp]
            [de.uni-stuttgart.iaas.ipsm.persistence.mongo :as mongo]))

(timbre/refer-timbre)

(defn get-entity-id
  [m]
  (->> (assoc m :resources (or (:resources m) sc/resources))
       sc/add-entity-identity-of-entity-data
       :redo/entity-identity
       cv/get-unique-id-from-entity-identity))

(defn add-entity-id-into-entity-data
  [m]
  (->> m
       get-entity-id
       (assoc-in m [:entity-data :_id])))

(defn add-entity!
  [m]
  {:pre [(:redo/entity-type (:entity-data m))]}
  (mongo/add-to-collection! (:redo/entity-type (:entity-data m))
                            (-> m add-entity-id-into-entity-data :entity-data)))



(defn get-entity
  [m]
  (if (sp/valid? :redo/entity-query (:entity-data m))
    (mongo/get-by-id
     (:redo/entity-type (:entity-data m))
     (-> m
         add-entity-id-into-entity-data
         :entity-data))
    (error "Faulty query" (sp/explain :redo/entity-query (:entity-data m)))))



(defn get-all-entities
  [m]
  ;; {:pre [(:redo/entity-type (:entity-data m))]}
  (debug "Getting all entities")
  (if (seq (dissoc (:entity-data m) :redo/entity-type))
    (mongo/get-all
     (:redo/entity-type (:entity-data m)) (:entity-data m))
    (mongo/get-all
     (:redo/entity-type (:entity-data m)))))

(defn update-entity-by-id!
  [m]
  {:pre [(:redo/entity-type (:entity-data m))]}
  (mongo/update-obj-by-id!
   (:redo/entity-type (:entity-data m))
   (add-entity-id-into-entity-data (:entity-data m))
   (add-entity-id-into-entity-data (:entity-data m))))

(defn delete-entity-by-id!
  [m]
  {:pre [(:redo/entity-type (:entity-data m)) ]}
  (debug "Removing entity " (:redo/entity-identity (:entity-data m)))
  (debug "Execution completed!" (mongo/remove-obj-by-id!
                                 (:redo/entity-type (:entity-data m))
                                 (-> m add-entity-id-into-entity-data :entity-data (select-keys [:_id])))))


(defn import-organizational-definitions!
  [m]
  (if (->> m
           :entity-data
           (sp/valid? :redo/organizational-definitions))
    (mapv
     #(if (% (:entity-data m))
        (mapv
         (fn [e] (do
                   (add-entity! (assoc-in m [:entity-data] e))))
         (% (:entity-data m))))
     [:redo/organizational-capabilities
      :redo/organizational-intentions
      :redo/organizational-processes
      :redo/organizational-contexts])
    (error "Bad formatted organizational definition"
           (->> m
                :entity-data
                (sp/explain-str :redo/organizational-definitions)))))
