(ns de.uni-stuttgart.iaas.ipsm.persistence.mongo
  (:require [de.uni-stuttgart.iaas.ipsm.utils.constants :as constants]
            [de.uni-stuttgart.iaas.ipsm.utils.conversions :as con]
            [environ.core :refer [env]]
            [somnium.congomongo :as new-mongo-client]
            [clojure.string :as s]
            [clojure.walk :as clj-walk]
            [taoensso.timbre :as timbre])
  (:import [com.mongodb MongoOptions ServerAddress]
           [org.bson.types ObjectId]))

(timbre/refer-timbre)

;;(let [conn (mg/connect {:host "db.megacorp.internal" :port 7878})])
;; localhost, default port

;; try to get possible uris from environment

(defn get-db-uri
  []
  (info "Getting the URI option from the environment: " (env :mongolab-uri) " or " (env :mongolab-uri))
  (or (env :mongolab-uri) (env :mongosoup-url)))

(defn- connect
  ([] (new-mongo-client/make-connection "mydb"))
  ([host] (new-mongo-client/make-connection "mydb" {:host host}))
  ([host port] (new-mongo-client/make-connection "mydb" {:host host :port port})))

(defn- get-db
  []
  (info "Datastore connection and instance will be created...")
  (if (get-db-uri)
    {:conn (new-mongo-client/make-connection (get-db-uri))}
    (let [host (constants/property "db-host")
          port (constants/property "db-port")
          conn (if (and host port)
                   (new-mongo-client/make-connection "mydb" {:host host :port port})
                   (if host
                     (new-mongo-client/make-connection "mydb" {:host host})
                     (new-mongo-client/make-connection "mydb")))]
         {:conn conn})))


(defn- convert-id-to-objectid
  "convert id to object id if its a string"
  [str-id]
  (if (instance? String str-id)
      (ObjectId. str-id)
      str-id))

(defn- swap-id
  "swap id if exists otherweise add one"
  [col old-key new-key]
  (if (contains? col old-key)
    (let [value (get col old-key)]
      (merge (dissoc col old-key) {new-key (convert-id-to-objectid value)}))
    (merge col {new-key (ObjectId.)})))

(defn- val-to-str
  "Converts the given val to string in a map. Val is identified by the
  given keyword key"
  [m key]
  (assoc m key (str (key m))))

(defn- swap-id-for-mongo
  [item]
  ;; (println (str ((keyword "_id") item) " " ))
  (if (or (:id item) (:_id item))
    (if (contains? item (keyword "_id"))
      (val-to-str (swap-id item (keyword "_id") (keyword constants/id-tag)) (keyword constants/id-tag))
      (swap-id item (keyword constants/id-tag) (keyword "_id")))
    nil))

(defn- run-command
  [m]
  {:pre [(:command m)
         (:conn m)]}
  (-> m
      (assoc :response ((:command m) (:conn m)))
      (assoc :dissconnect-res (new-mongo-client/close-connection (:conn m)))
      :response) ;; execute command using db

  )


(defn map-key->encoded-map-key
  [m]
  (->> m
       (mapv #(vector (-> (first %)
                          ((fn[v] (if (keyword? v) (name v) v)))
                          (s/replace #"\." "&#46;")
                          ((fn[v] (cond (and (keyword? (first %))
                                             (namespace (first %)))
                                        (keyword (namespace (first %)) v)
                                        (keyword? (first %));;option 2
                                        (keyword v)
                                        :default
                                        v))))
                      (cond
                        (map? (second %))
                        (map-key->encoded-map-key (second %))
                        (vector? (second %))
                        (mapv (fn [e] (map-key->encoded-map-key e)) (second %))
                        :else
                        (second %))))
       (into {})))


(defn encoded-map-key->map-key
  [m]
  (->> m
       (mapv #(vector (-> (first %)
                          ((fn[v] (if (keyword? v) (name v) v)))
                          (s/replace #"&#46;" ".")
                          ((fn[v] (cond (and (keyword? (first %))
                                             (namespace (first %)))
                                        (keyword (namespace (first %)) v)
                                        (keyword? (first %));;option 2
                                        (keyword v)
                                        :default
                                        v))))
                      (cond
                        (map? (second %))
                        (encoded-map-key->map-key (second %))
                        (vector? (second %))
                        (mapv (fn [e] (encoded-map-key->map-key e)) (second %))
                        (= (first %) :redo/entity-type)
                        (keyword (second %))
                        :else
                        (second %))))
       (into {})))

(defn- post-process-entity
  [m]
  (-> m
      ;; swap-id-for-mongo
      (dissoc :_id)
      (dissoc :id)
      encoded-map-key->map-key))

(defn- pre-process-entity
  [m]
  (-> m
      (dissoc :entity-type)
      (dissoc :redo/entity-type)
      map-key->encoded-map-key))




(defn get-all
  ([collection]
   (-> (get-db)
       (assoc :command
              #(mapv
                (fn [e] (post-process-entity (assoc e :redo/entity-type collection)))
                (new-mongo-client/with-mongo % (new-mongo-client/fetch collection))))
       run-command))
  ([collection specs]
   (-> (get-db)
       (assoc :collection collection)
       (assoc :command
              #(mapv
                (fn [e] (post-process-entity (assoc e :redo/entity-type collection)))
                (new-mongo-client/with-mongo % (new-mongo-client/fetch collection :where (-> specs
                                                                                             (dissoc :entity-type)
                                                                                             (dissoc :redo/entity-type))))))
       run-command)))


(defn get-by-id
  [collection id]
  (-> (get-db)
      (assoc :collection collection)
      (assoc :command
             #(-> %
                  (new-mongo-client/with-mongo
                    (new-mongo-client/fetch collection :where (-> id
                                                                  (select-keys [:_id]))))
                  first
                  post-process-entity
                  (assoc :redo/entity-type collection)))
      run-command))

(defn add-to-collection!
  [collection item]
  (-> (get-db)
      (assoc :collection collection)
      (assoc :command
             #(-> (new-mongo-client/with-mongo % (new-mongo-client/update! collection (pre-process-entity item) (pre-process-entity item)))
                  ))
      run-command))

(defn remove-obj-by-id!
  [collection oid]
  (-> (get-db)
      (assoc :collection collection)
      (assoc :command
             #(new-mongo-client/with-mongo % (new-mongo-client/destroy! collection (pre-process-entity oid))))
      run-command))

(defn update-obj-by-id!
  [collection oid item]
  (-> (get-db)
      (assoc :collection collection)
      (assoc :command
             #(new-mongo-client/with-mongo % (new-mongo-client/update! collection (convert-id-to-objectid oid) (pre-process-entity item))))
      run-command))

