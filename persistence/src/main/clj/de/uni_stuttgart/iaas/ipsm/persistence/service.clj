(ns de.uni-stuttgart.iaas.ipsm.persistence.service
  (:require [taoensso.timbre :as timbre]
            [de.uni-stuttgart.iaas.ipsm.protocols.core :as protocols]
            [de.uni-stuttgart.iaas.ipsm.persistence.core :as c]
            [puppetlabs.trapperkeeper.core :as trapperkeeper]))

(timbre/refer-timbre)


(trapperkeeper/defservice Persistor
  protocols/PersistenceProtocol
  []
  (init [this context]
        (info "Initializing persistence service")
        context)
  (start [this context]
         (info "Starting persistence service")
         context)
  (stop [this context]
        (info "Shutting down persistence service")
        context)
  (add-entity! [this entity]
               (c/add-entity! {:entity-data entity}))
  (get-entity [this entity] (c/get-entity {:entity-data entity}))
  (get-all-entities [this entity] (c/get-all-entities {:entity-data entity}))
  (update-entity-by-id! [this entity] (c/update-entity-by-id! {:entity-data entity}))
  (delete-entity-by-id! [this entity] (c/delete-entity-by-id! {:entity-data entity}))
  (import-organizational-definitions! [this entity] (c/import-organizational-definitions! {:entity-data entity}))
  (export-organizational-definitions [this] (error "not yet implemented")))


;; (c/add-entity! {:entity-data {:redo/entity-type :redo/resource-driven-process-definition, :redo/initializable-entity-definition {:redo/identifiable-entity-definition {:redo/entity-identity {:redo/name "-------A", :redo/target-namespace "LOLAsdfsdf [This is a namespace] MY TARGET NAMESPACES"}}}, :id "3f90adb3ee52118b10e0df47"}})

;; (c/get-entity {:entity-data {:redo/entity-type :redo/resource-driven-process-definition, :redo/initializable-entity-definition {:redo/identifiable-entity-definition {:redo/entity-identity {:redo/name "-------A", :redo/target-namespace "LOLAsdfsdf [This is a namespace] MY TARGET NAMESPACES"}}}}})

;; (mapv
;;  #(do
;;     (debug "Created ID" (str (:_id %)))
;;     (debug "Created ID" %)
;;     (de.uni-stuttgart.iaas.ipsm.persistence.mongo/remove-obj-by-id!
;;      :redo/resource-driven-process-definition
;;      (dissoc (assoc % :id (str (:_id %))) :redo/entity-type)))
;;  (c/get-all-entities {:entity-data {:redo/entity-type :redo/resource-driven-process-definition}}))

;; (de.uni-stuttgart.iaas.ipsm.persistence.mongo/remove-obj-by-id!
;;  :redo/resource-driven-process-definition
;;  {:redo/entity-type :redo/resource-driven-process-definition})

;; (count (debug (c/get-all-entities {:entity-data {:redo/entity-type :redo/resource-driven-process-definition}})))

;; {:redo/initializable-entity-definition #:redo{:identifiable-entity-definition #:redo{:entity-identity #:redo{:target-namespace "LOLAsdfsdf [This is a namespace] MY TARGET NAMESPACES",}}}}
