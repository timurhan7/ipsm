(ns de.uni-stuttgart.iaas.ipsm.execution-environment-integrator.t-elgg
  (:require [de.uni-stuttgart.iaas.ipsm.execution-environment-integrator.elgg :as elgg]
            [clojure.test :as test]
            [de.uni-stuttgart.iaas.ipsm.utils.constants :as constants]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.schema :as redo-schema]
            [taoensso.timbre :as timbre])
  (:import [javax.xml.namespace QName]))

(timbre/refer-timbre)

(def sample-data {:target-resource {}

                  :target-resource-runnable {:salt "", :prev_last_login 0, :banned "no", :email "timurhan.s@gmail.com", :prev_last_action 1512257580, :admin "no", :password "", :name "lalala", :last_login 1512257579, :username "lalala", :password_hash "$2y$10$3dDnGFVRuxi./9ApwVfHjejC6pWDZH/fUvyAevXL3pmEDnvoa36H2", :language "en", :last_action 1512257887, :guid 49}
                  :resource-driven-process-definition {:redo/resource-model {:redo/model-uri "http://localhost:8080/winery-ipsm/servicetemplates/http%253A%252F%252Fwww.example.org%252FTOSCA%252FPHPApp/Bug_Fixing_Process/"}, :redo/initializable-entity-definition {:redo/identifiable-entity-definition {:redo/entity-identity {:redo/name "Bug Fixing Process", :redo/target-namespace "http://www.example.org/TOSCA/PHPApp"}}}, :redo/entity-type :redo/resource-driven-process-definition}
                  :resources redo-schema/resources
                  :message-subject (:allocation-subject (:en (constants/property :texts)))
                  :callback ""})


(test/deftest send-email
  (test/is (let [init-deployable #'elgg/init-deployable
                 result-map (init-deployable sample-data)
                 inquery-id (:inquery-id result-map)
                 handle-response! #'elgg/handle-response!]
             (handle-response! inquery-id true true))))
