(ns de.uni-stuttgart.iaas.ipsm.execution-environment-integrator.elgg
  (:require [taoensso.timbre :as timbre]
            [environ.core :refer [env]]
            [clojure.java.io :as io]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.schema :as redo-schema]
            [de.uni-stuttgart.iaas.ipsm.protocols.core :as p]
            [puppetlabs.trapperkeeper.services :as services]
            [ring.util.response :as resp]
            [puppetlabs.trapperkeeper.core :as tk]
            [clojure.string :as s]
            [hiccup.core :as hiccup]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.schema :as ipsm-schema]
            [de.uni-stuttgart.iaas.ipsm.protocols.core :as protos]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.domain-language :as domain-language]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.transformations :as transformations]
            [de.uni-stuttgart.iaas.ipsm.utils.conversions :as conversions]
            [de.uni-stuttgart.iaas.ipsm.utils.constants :as constants]
            [de.uni-stuttgart.iaas.ipsm.integrator-client.core :as integrator]
            [clj-http.client :as http]
            [compojure.core :as compojure]
            [compojure.route :as route]
            [postal.core :as postal]
            [ring.adapter.jetty :as jetty]
            [ring.middleware.params :as m-params]
            [de.uni-stuttgart.iaas.ipsm.utils.xml :as xml-utils])
  (:import [javax.xml.namespace QName])
  (:gen-class))

(timbre/refer-timbre)

(defonce request-map (atom {}))
(defonce user-map (atom {}))
(defonce handler-state (atom {}))


(defn- call-callback-handler!
  [m]
  {:pre [(or (= (:invitation-accepted? m) true)
             (= (:invitation-accepted? m) false)
             (:current-obj m))]}
  (if (:invitation-accepted? m)
    (do (.completeProcessExecutionSuccessfully
         (:current-obj m)
         (java.net.URI.
          (or (:resource-uri m)
              "http://www.example.org/missing-uri/error/"))
         (:callback m))
        (assoc m :response (resp/response (hiccup/html [:body [:div
                                                               [:h3 "Thank you for your reponse, we recieved and recoreded it. Please check the web-site of the process to continue your your."]]]))))
    (do (.completeProcessExecutionwithError
         (:current-obj m)
         (or (:err (:compose-up-res m)) "Resource could not be engaged! The invitation was rejected.")
         (:callback m))
        (assoc m :response (resp/response (hiccup/html [:body [:div
                                                               [:h3 "Thank you for your reponse, we recieved and recoreded it."]]]))))))

(defn- add-from-request-map
  [m]
  {:pre [(:inquery-id m)]}
  (get @request-map (keyword (:inquery-id m))))

(defn- remove-from-request-map!
  [m]
  {:pre [(:inquery-id m)]}
  (swap! request-map dissoc (keyword (:inquery-id m)))
  m)


(defn- add-user-to-user-map
  [m]
  {:pre [(:internal-user-id m)]}
  (swap! user-map assoc (keyword (:internal-user-id m)) m))

(defn- handle-response!
  [query-id response accepted]
  (debug "Request map is" query-id response accepted (add-from-request-map {:inquery-id query-id}))
  (let [request-map (add-from-request-map {:inquery-id query-id})]
    (cond-> request-map
      (not request-map) ("No such process!!!!")
      (and response accepted) (assoc :invitation-accepted? true)
      (and response (not accepted)) (assoc :invitation-accepted? true)
      response call-callback-handler!
      response remove-from-request-map!
      (not response) (assoc :response (hiccup/html [:body [:div
                                                           [:h3 "Malformed request. Please use one of the addresses that was provided to you in the invitation e-mail!"]]]))
      :default :response
      :default (#(do (debug "Response map is:" %) %)))))

(defn add-routes
  [m]
  {:pre [(:handler-path m)]
   :post [(:routes %)]}
  (assoc m :routes (compojure/routes
                    (compojure/GET (str "/" (:handler-path m) "/:query-id") [query-id response accepted]
                                   (handle-response! query-id response accepted))
                    (route/not-found "<h1>Page not found</h1>"))))


(defn run-response-handler!
  [m]
  {:pre [(:routes m)
         (:response-handler-port m)]}
  (-> m
      :routes
      m-params/wrap-params
      (jetty/run-jetty  {:port (:response-handler-port m)
                         :join? false})))

(defn- add-into-request-map!
  [m]
  {:pre [(:inquery-id m)]}
  (swap! request-map assoc (keyword (:inquery-id m)) m))


(defn- send-request-to-the-user!
  [m]
  {:pre [(:recipient m) (:smp)]})


(defn- add-invitation-hiccup
  [m]
  {:pre [(:name m) (:confirmation-address m) (:rejection-address m)]}
  (assoc m :body-hiccup []))

(defn- add-termination-hiccup
  [m]
  {:pre [(:name m) (:confirmation-address m) (:rejection-address m)]}
  (assoc m :body-hiccup []))


(defn- add-body-html-from-hiccup
  [m]
  {:pre [(:body-hiccup m)]}
  (assoc m :body-html (hiccup/html (:body-hiccup m))))



(defn- send-email!
  [m]
  {:pre [(:smtp-host m)
         (:user m)
         (:password m)
         (:from m)
         (:recipient-address m)
         (:message-subject m)
         (:message-body m)]}
  (debug "Sending email!"
         {:host (:smtp-host m)
          :user (:user m)
          :pass (:password m)
          :tls true}
         {:from (:from m)
          :to (:recipient-address m)
          :subject (:message-subject m)
          :body (:message-body m)})
  (debug (postal/send-message {:host (:smtp-host m)
                               :user (:user m)
                               :pass (:password m)
                               :ssl true}
                              {:from (:from m)
                               :to (:recipient-address m)
                               :subject (:message-subject m)
                               :body (:message-body m)})))
;; (postal/send-message {:host "smtp.gmail.com", :user "coact.informer", :pass "Start1234!", :ssl true} {:from "CoAct Informer Service <coact.informer@gmail.com>", :to "Oliver Kopp <timurhan.s@gmail.com>", :subject "Invitation for Participating in a Resource-Driven Process", :body "Dear Oliver Kopp,\n\nWe kindly invite you tou participate in the process with the name: Bug Fixing Process\n\nIf you accept our invitation, please click here: http://localhost:9113/invitations/041ddc490b27a7b1e1421e21?response=true&accepted=true\n\nIn case of a rejection, please click here:http://localhost:9113/invitations/041ddc490b27a7b1e1421e21?response=true&accepted=false\n\nPlease consider this invitaion after an hour as invalid.\n\nYours Sincerly,\n\nCoAct Informer Service\n\n"})
(defn- add-invitation-message-body
  [m]
  {:pre [(:recipient-name m)
         (:process-id m)
         (:confirmation-address m)
         (:rejection-address m)]
   :post [(:message-body %)]}
  (debug "Adding invitation message body")
  (assoc m :message-body (str "Dear " (:recipient-name m) ",\n\n"
                              "We kindly invite you tou participate in the process with the name: " (:redo/name (:process-id m)) "\n\n"
                              "If you accept our invitation, please click here: " (:confirmation-address m) "\n\n"
                              "In case of a rejection, please click here:" (:rejection-address m) "\n\n"
                              "Please consider this invitaion after an hour as invalid.\n\n"
                              "Yours Sincerly,\n\n"
                              "CoAct Informer Service\n\n")))






(defn- add-recipient-address
  [m]
  {:pre [(:recipient-name m)
         (:recipient-email m)]
   :post [(:recipient-address %)]}
  (debug "Adding recipient address")
  (assoc m :recipient-address (str (:recipient-name m) " <" (:recipient-email m) ">")))


(defn- add-resource-uri
  [m]
  {:pre [(:user-profile-uri m)]
   :post [(:resource-uri %)]}
  (debug "Adding recipient address")
  (assoc m :resource-uri (:user-profile-uri m)))


(defn- add-recipient-email
  [m]
  {:pre [(:target-resource-runnable m)]}
  (debug "Adding recipient name")
  (assoc m :recipient-email (:email (:target-resource-runnable m))))

(defn- add-recipient-name
  [m]
  {:pre [(:target-resource-runnable m)]}
  (debug "Adding recipient name")
  (assoc m :recipient-name (:name (:target-resource-runnable m))))


(defn- add-request-id
  [m]
  {:pre [(:process-id m)]
   :post [(:request-id %)]}
  (assoc m :request-id (conversions/create-unique-id-from-str (conversions/create-unique-id) (conversions/get-unique-id-from-entity-identity (:process-id m)))))

(defn- add-confirmation-address
  [m]
  {:pre [(:query-path m)
         (:server-url m)]
   :post [(:confirmation-address %)]}
  (debug "Adding confirmation")
  (assoc m :confirmation-address (str (:server-url m) (:query-path m) "?response=true&accepted=true")))

(defn- add-rejection-address
  [m]
  {:pre [(:query-path m)
         (:server-url m)]
   :post [(:rejection-address %)]}
  (assoc m :rejection-address (str (:server-url m) (:query-path m)  "?response=true&accepted=false")))

(defn- add-response-handler-path
  [m]
  {:pre [(:handler-path m)
         (:inquery-id m)]}
  (debug "Adding reponse handler path")
  (assoc m :query-path (str (:handler-path m) "/" (:inquery-id m))))


(defn- add-inquery-id
  [m]
  {:pre [(:process-id m)]}
  (debug "Adding inquery id")
  (assoc m :inquery-id (conversions/create-unique-id-from-str (conversions/create-unique-id) (conversions/get-unique-id-from-entity-identity (:process-id m)))))

(defn- add-process-id
  [m]
  {:pre [(:resource-driven-process-definition m)]
   :post [(:process-id %)]}
  (debug "Adding process id" (keys m))
  (assoc m :process-id (-> m
                           (assoc :entity-data (:resource-driven-process-definition m))
                           redo-schema/add-entity-identity-of-entity-data
                           :redo/entity-identity)))

(defn- add-user-name
  [m]
  {:pre [(:target-resource-runnable m)]
   :post [(:username %)]}
  (assoc m :username (:username (:target-resource-runnable m))))

(defn- add-user-profile-uri
  [m]
  {:pre [(:username m)
         (:elgg-profile-path m)]
   :post [(:user-profile-uri %)]}
  (assoc m :user-profile-uri (str (:elgg-profile-path m) (:username m))))

(defn- add-internal-user-id
  [m]
  {:pre [(:user-profile-uri m)]
   :post [(:internal-user-id %)]}
  (assoc m :internal-user-id (conversions/create-unique-id-from-str (:user-profile-uri m))))



(defn- add-map-of-target-runnable
  [m]
  (assoc m :target-resource-runnable (read-string (:target-resource-runnable m))))

(defn- prepare-for-initialization
  [m]
  (-> m
      add-map-of-target-runnable
      add-process-id
      add-inquery-id
      add-response-handler-path
      add-rejection-address
      add-confirmation-address
      add-recipient-email
      add-recipient-name
      add-recipient-address
      add-user-name
      add-user-profile-uri
      add-internal-user-id
      add-resource-uri
      add-invitation-message-body))


(defn- init-deployable
  [m]
  (debug "Resource will be initialized")
  (doto (prepare-for-initialization m)
    add-into-request-map!
    send-email!))



(defn- release-deployable
  [m]
  (debug "Resource will be released")
  )

(gen-class :name "uni_stuttgart.ipsm.execution_environment_integrator.AcquireResourceOperation"
           :extends uni_stuttgart.ipsm.protocols.integration.operations.lifecycle.AcquireResourceOperation
           :prefix "acquire-resources-")




(deftype ElggExecutionEnvironmentMetadata [m]
  uni_stuttgart.ipsm.protocols.integration.IntegratorMetadata
  (getName [_] (constants/property :name))
  (getTargetNamespace [_] (java.net.URI. (:domain-uri m)))
  (getUri [_] (java.net.URI. (constants/property :uri))))


(defn acquire-resource-operation
  [m]
  {:pre [(:domain-uri m)
         (:allocation-subject (:en (:texts m)))]}
 (proxy [uni_stuttgart.ipsm.protocols.integration.operations.lifecycle.AcquireResourceOperation]
     []
   (getSupportedResources [] nil)
   (getSupportedDomains [] [(:domain-uri m)])
   (executeOperation [target-resource-runnable-container parameters callback]
     (debug "Executing acquire operation" (transformations/get-map-of-jaxb-type-obj
                                           (.getTargetModel (first target-resource-runnable-container))))
     (init-deployable (into m {:target-resource (transformations/get-map-of-jaxb-type-obj
                                                 (.getTargetModel (first target-resource-runnable-container)))
                               :target-resource-runnable (slurp (.getDeployable (first target-resource-runnable-container)))
                               :resource-driven-process-definition (:redo/resource-driven-process-definition parameters)
                               :resources redo-schema/resources
                               :message-subject (:allocation-subject (:en (:texts m)))
                               :callback callback
                               :current-obj this})))))




(defn release-operation
  [m]
  {:pre [(:domain-uri m)]}
 (proxy [uni_stuttgart.ipsm.protocols.integration.operations.lifecycle.ReleaseResourceOperation]
     []
   (getSupportedResources [] nil)
   (getSupportedDomains [] [(:domain-uri m)])
   (executeOperation [target-resource-runnable-container parameters callback]
     (debug "Executing release operation")
     (release-deployable (into m {:target-resource (transformations/get-map-of-jaxb-type-obj
                                                    (.getTargetModel (first target-resource-runnable-container)))
                                  :target-resource-runnable (slurp (.getDeployable (first target-resource-runnable-container)))
                                  :resources redo-schema/resources
                                  :callback callback
                                  :current-obj this})))))




(defn prepare-and-run-response-handler!
  [m]
  {:pre [(:response-handler-port m)]}
  (swap! handler-state assoc :server (-> m
                                         add-routes
                                         run-response-handler!)))


(defn run-dip
  [m]
  (prepare-and-run-response-handler! m)
  (integrator/register-execution-environment-integrator! [(acquire-resource-operation m)
                                                          (release-operation m)]
                                                         (ElggExecutionEnvironmentMetadata. m)
                                                         m))
;; (run-dip {})


(defn- stop-handler
  []
  (.stop (get @handler-state :server)))

(tk/defservice ElggService
  p/DSOPElgg
  [;; [:WebserverService add-war-handler]
   [:ConfigService get-config]
   ToscaPersistenceProtocol]
  (init [this context] context)
  (start [this context]
         (info "Starting docker compose domain-specific operation provider")
         (assoc context :dsop-docker-compose-map (-> (:elgg-dsop-config (get-config))
                                                     (assoc :tosca-persistence (services/get-service this :ToscaPersistenceProtocol))
                                                     (assoc :resources ipsm-schema/resources)
                                                     run-dip)))
  (stop [this context]
        (info "Shutting docker compose domain-specific operation provider")
        (integrator/stop-execution-environment-integrator-handlers! context)
        (stop-handler)
        context))
