(defproject de.uni-stuttgart.iaas.ipsm/elgg-domain-specific-operation-provider "0.0.1-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [com.draines/postal "2.0.2"]
                 [puppetlabs/trapperkeeper "1.5.3"]
                 [com.taoensso/timbre "4.10.0"]
                 [de.uni-stuttgart.iaas.ipsm/integrator-client "0.0.3-SNAPSHOT"]
                 [javax.servlet/servlet-api "2.5"]
                 [compojure "1.6.0"]
                 [org.clojure/core.async "0.4.474"]
                 [de.uni-stuttgart.iaas.ipsm/commons "0.0.2-SNAPSHOT"]
                 [de.uni-stuttgart.iaas.ipsm/protocols "0.5.3-SNAPSHOT"]
                 [environ "1.0.0"]
                 [hiccup "1.0.5"]
                 [uni-stuttgart.ipsm/tosca-persistence "0.0.1-SNAPSHOT"]]
  :plugins [[lein-environ "1.0.0"]
            [lein-modules "0.3.11"]]
  :source-paths ["src/main/clj"]
  :resource-paths ["src/main/resources"]
  :repl-options {:init-ns user}
  :aliases {"tk" ["trampoline" "run" "--config" "dev-resources/config.conf"]}
  :main puppetlabs.trapperkeeper.main)
