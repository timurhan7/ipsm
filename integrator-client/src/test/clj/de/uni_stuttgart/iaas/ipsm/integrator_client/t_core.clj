(ns de.uni-stuttgart.iaas.ipsm.integrator-client.t-core
  (:require [de.uni-stuttgart.iaas.ipsm.integrator-client.core :as c])
  (:use [midje.sweet]
        [midje.util :only [testable-privates]]))


(testable-privates de.uni-stuttgart.iaas.ipsm.integrator-client.core
                   add-class-loader
                   add-integrator-metadata
                   add-all-eei-operations
                   add-all-domain-manager-operations
                   add-integrator-metadata
                   get-config
                   register-domain-managers!)


(fact "client core adds class loaders"
      (-> {} add-class-loader) => (contains {:class-loader anything}))

(fact "client core adds integrator metadata"
      (-> (get-config) add-integrator-metadata) => (contains {:metadata anything}))

(fact "creates a single client"
      (c/create-single-client-using-all-configs! nil) => {:rest-service anything})

(fact "stop rest service of the client"
      (c/stop-rest-service! nil) => {:rest-service nil})
