(ns de.uni-stuttgart.iaas.ipsm.integrator-client.domain-manager
  (:require [taoensso.timbre :as timbre]
            [langohr.core      :as rmq]
            [langohr.channel   :as lch]
            [langohr.queue     :as lq]
            [langohr.consumers :as lc]
            [langohr.basic     :as lb]
            [de.uni-stuttgart.iaas.ipsm.integrator-client.deployment-handler :as deployment-handler]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.transformations :as transformations]
            [de.uni-stuttgart.iaas.ipsm.utils.conversions :as co]
            [de.uni-stuttgart.iaas.ipsm.integrator-client.commons :as c]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.domain-language :as domain-language]
            [de.uni-stuttgart.iaas.ipsm.utils.communications :as communications]
            [de.uni-stuttgart.iaas.ipsm.utils.io :as ipsm-io])
  (:import [java.net URI]
           [org.w3c.dom Document]
           [javax.xml.namespace QName]))


(timbre/refer-timbre)

(defn- transform-to-exchange-format
  [m]
  (-> m
      (assoc :response-data (apply transformations/get-definitions-xml-with-types (:types m)))))

(defn- add-definitions-map
  [m]
  (->> m
       :response-data
       transformations/add-definitions-map))

(defn- convert-deployables-to-internal-format
  [m]
  (->> m
       :response-data
       (assoc m :defs-obj)
       transformations/add-definitions-map
       transformations/add-types-of-defs-map
       transformations/add-imports-of-definitions))

(defn- add-type-maps
  [m]
  {:pre [(:types m)]}
  (debug "Type maps to be returned are" (:types m))
  (assoc m :response-data {:types (:types m)}))

(defn- add-target-namespace
  [m]
  {:pre [(:types m)]}
  (update-in m [:response-data] into {:target-namespace (str (.getTargetNamespace (:metadata m)))}))

(defn- add-response-format
  [m]
  {:pre [(:response-data m)]}
  (assoc m :response-format (if (string? (:response-data m)) :xml :edn)))


(defn- add-life-cycle-interface
  [m]
  {:pre [(:node-type m) (:life-cycle-interface m)]}
  (update-in m [:node-type] #(domain-language/resource-definition
                              (into % {:interfaces
                                       {:interface [(:life-cycle-interface m)]}}))))

(defn- add-life-cycle-interfaces
  [m]
  {:pre [(:types m)]}
  (debug "Adding lifecycles for" (:types m))
  (->> m
       :types
       (mapv #(if (domain-language/resource-definition? %)
                (-> m
                    (assoc :node-type %)
                    add-life-cycle-interface
                    :node-type)
                %))
       (assoc m :types)))


(defn- add-import-location
  [m]
  {:pre [(:namespace m) (:uri m)]}
  (str (if (.endsWith (str (:uri m)) "/")
         (str (:uri m))
         (str (:uri m) "/"))
       (:import-path m)
       (co/winery-encode (:namespace m))))

(defn- append-import
  "Adds import handler url into this in case no location was provided"
  [m]
  (update-in m [:import] #(if-not (:location %)
                            (->> (assoc m :namespace (:namespace m))
                                 add-import-location
                                 :location
                                 (assoc % :lcoation))
                            %)))


(defn- append-imports
  [m]
  (->> m
       :imports
       (mapv #(-> (assoc m :import %)
                  append-import
                  :import))
       (mapv #(domain-language/def-import %))
       (assoc-in m [:response-data :imports])))

(defn- add-entity-identity
  [m]
  (assoc-in m [:response-data :redo/entity-identity]  {:redo/name (str (.getName (:metadata m)))
                                                       :redo/target-namespace (str (.getTargetNamespace (:metadata m)))}))

(defn- add-entity-type
  [m]
  (assoc-in m [:response-data :redo/entity-type]  :redo/domain-manager-message))


(defn- post-process-response-data
  [m]
  (debug "Post process response" m)
  (-> m
      convert-deployables-to-internal-format
      deployment-handler/enrich-with-deployment-data
      ;; c/add-life-cycle-interface-name

      ;; add-life-cycle-interfaces
      add-type-maps
      add-response-format
      add-entity-identity
      add-entity-type
      append-imports
      (#(do (debug "After post process" %) %))))


(defn- add-response-data-list-domain
  [m]
  {:pre [(:manager m)]}
  (assoc m :response-data (.listDomain (:manager m))))


(defn- create-response-data
  [request-map]
  (assoc request-map :response-data
         (case (:request-type (:message-payload request-map))
           :list-domain (.listDomain (:manager request-map))
           :resource (.getResource (:manager request-map)
                                   (QName. (:resource-name (:message-payload request-map))))
           "default" (do
                       (error "Error in the request type")
                       "Error in the request type"))))

(defn- add-target-queue-from-message-type
  [request-map]
  (debug "Adding target queue for publishing the response to a resource request...")
  (debug "Request map is" request-map)
  ;(debug "Request payload" (:message-payload request-map))
  (debug "Request type" (:request-type (:message-payload request-map)))
  (assoc request-map
         :queue-name
         (case (:request-type (:message-payload request-map))
           :list-domain (communications/get-queue-name :resource-list-queue)
           :resource (communications/get-queue-name :resource-queue)
           "default" (do
                       (error "Error in the request type")
                       "Error in the request type"))))

(defn- add-list-queue-as-queue-name
  [request-map]
  (debug "Adding target queue for publishing the response to a resource request..." (communications/get-queue-name :resource-list-queue))
  (assoc request-map :queue-name (communications/get-queue-name :resource-list-queue)))

(defn- process-and-send-response-data-if-not-nil!
  [m]
  {:pre [(:metadata m)]}
  (debug "Generated definitions are" (:response-data m))
  (if (:response-data m)
    (-> m
        post-process-response-data
        communications/create-response
        communications/publish-message!)
    (warn "No definitions (resources, relationships, or capabilities) are provided by this domain manager!!" (.getTargetNamespace (:manager m)))))



(defn register-domain-manager!
  [m]
  {:pre [(:metadata m)]}
  (doto (-> m
            communications/add-connection-data
            add-response-data-list-domain
            add-list-queue-as-queue-name
            post-process-response-data
            communications/create-response)
    communications/publish-message!))

(defn add-handler-resource-aggregator-request
  [listener-map]
  (assoc listener-map :handler
         (fn [ch {:keys [content-type delivery-tag type] :as meta} ^bytes payload]
           (info "Message has been received! " (String. payload "UTF-8"))
           (doto (-> listener-map
                     (communications/add-edn-message-body payload)
                     add-target-queue-from-message-type
                     create-response-data)
             process-and-send-response-data-if-not-nil!))))


(defn- add-provider-metadata
  [communication-map provider metadata]
  (assoc communication-map :provider provider :metadata metadata))
;;
(defn add-new-listener!
  "Adds a new listener to the main queue."
  [m]
  (-> m
      communications/add-connection-data
      (assoc :queue-name (str (:target-queue m) (.getTargetNamespace (:manager m))))
      (assoc :binding (:target-queue m))
      communications/declare-queue-or-exchange!
      communications/bind-queue-to-exchange!
      add-handler-resource-aggregator-request
      communications/subscribe-to-a-queue!))



(defn- add-namespace-from-request
  [m]
  (debug "namespace will be added to the map" m)
  (assoc m :namespace (co/winery-decode (:namespace (:params (:request (:request m)))))))

(defn- add-import-ns->import-stream
  [m]
  (assoc m :import-stream (some->> m
                                   :manager
                                   .getImports
                                   (filterv #(= (:namespace m)
                                                (.getNamespace %)))
                                   .getImport)))

(defn- add-import
  [m]
  (-> m
      add-namespace-from-request
      add-import-ns->import-stream))

(defn- handle-import-get
  [m]
  (fn [request]
    (debug "Request has been received." request)
    (some-> m
            (assoc :request request)
            add-import
            :import-stream)))


(defn- add-imports-rest-handler-config
  [m]
  {:pre [(:import-path m)]}
  (update-in m [:resource-config :route-configs] into [{:resource-path (str "/" (:import-path m) ":namespace")
                                                        :available-media-types ["application/xml"]
                                                        :allowed-methods [:get]
                                                        :handle-ok (handle-import-get m)
                                                        :respond-with-entity? true
                                                        :handle-created #(:response-stream %)}]))


(defn add-rest-handler-config
  "Adds needed rest handler configuration using deployment handlers config"
  [m]
  (-> m
      deployment-handler/add-deployment-rest-handler-config))

(defn create-handlers
  [m]
  (-> m
      (assoc :resource-config {:route-configs []})
      deployment-handler/add-deployment-rest-handler-config
      c/add-host
      c/add-port
      communications/add-routes
      communications/run-server-detached!))
