(ns de.uni-stuttgart.iaas.ipsm.integrator-client.commons
  (:require [taoensso.timbre :as timbre]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.domain-language :as domain-language])
  (:import [java.net URI]))

(timbre/refer-timbre)


(defn add-host
  [m]
  {:pre [(:uri m)]}
  (assoc m :host (.getHost (:uri m))))

(defn add-port
  [m]
  {:pre [(:uri m)]}
  (assoc m :port (.getPort (:uri m))))


(defn add-uri
  [m]
  {:pre [(:metadata m)]}
  (assoc m :uri (or (.getUri (:metadata m)) (URI. "http://localhost:8111"))))

(defn add-uri-path
  [m path k]
  {:pre [(:uri m)]}
  (assoc m k (str (.toString (:uri m)) path)))


(defn add-life-cycle-interface-name
  [m]
  {:pre [(:metadata m)
         (:life-cycle-interface-postfix m)]}
  (if (.endsWith (.getTargetNamespace (:metadata m)) "/")
    (assoc m :interface-name (str (.getTargetNamespace (:metadata m)) (:life-cycle-interface-postfix m)))
    (assoc m :interface-name (str (.getTargetNamespace (:metadata m)) "/" (:life-cycle-interface-postfix m)))))


(defn add-life-cycle-interface-with-custom-operations
  [m]
  {:pre [(:interface-name m) (:interface-operations m)]}
  (assoc m :life-cycle-interface
         (domain-language/interface {:redo/name (:interface-name m)
                                     :redo/operation (:interface-operations m)})))

(defn add-life-cycle-interface
  [m]
  {:pre [(:interface-name m)]}
  (assoc m :life-cycle-interface
         (domain-language/interface {:redo/name (:interface-name m)
                                     :redo/operation [(domain-language/operation {:name "engage"})
                                                 (domain-language/operation {:name "release"})
                                                 (domain-language/operation {:name "store"})]})))




(defn add-artifact-type-uri
  [m]
  (assoc m :artifact-type-uri "http://www.uni-stuttgart.de/ipsm/artifact-types/"))

(defn- create-artifact-type
  [m]
  {:pre [(:artifact-type-name m)
         (:artifact-type-uri m)]}
  (domain-language/artifact-type {:redo/name (:artifact-type-name m)
                                  :redo/abstract "no"
                                  :redo/final "yes"
                                  :redo/target-namespace (:artifact-type-uri m)}))

(defn add-artifact-type
  [m]
  (assoc m :artifact-type (create-artifact-type m)))
