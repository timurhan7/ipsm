(ns de.uni-stuttgart.iaas.ipsm.integrator-client.execution-environment-integrator
  (:require [taoensso.timbre :as timbre]
            [langohr.core      :as rmq]
            [langohr.channel   :as lch]
            [langohr.queue     :as lq]
            [langohr.consumers :as lc]
            [langohr.basic     :as lb]
            [clojure.java.io :as io]
            [de.uni-stuttgart.iaas.ipsm.integrator-client.commons :as c]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.transformations :as transformations]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.domain-language :as domain-language]
            [de.uni-stuttgart.iaas.ipsm.integrator-client.deployment-handler :as deployment-handler]
            [de.uni-stuttgart.iaas.ipsm.utils.conversions :as conversions]
            [clj-http.client :as http]
            [de.uni-stuttgart.iaas.ipsm.utils.communications :as communications])
  (:import [org.w3c.dom Document]
           [javax.xml.namespace QName]
           [uni_stuttgart.ipsm.protocols.integration.operations ResourceOperationRealization RelationshipOperationRealization]))

(timbre/refer-timbre)


(defonce ^:private rest-service (atom {}))


(defn- post-process-response-data
  [m]
  (-> m
      deployment-handler/enrich-with-deployment-data))


(defn- create-response-data
  [request-map]
  (assoc request-map :response-data
         (String. (case (:request-type (:message-payload request-map))
                    :list-resources (.listDomain (:manager request-map))
                    :resource (.getResource (:manager request-map)
                                            (QName. (:resource-name (:message-payload request-map))))
                    "default" (do
                                (error "Error in the request type")
                                "Error in the request type")) "UTF-8")))

(defn- add-target-queue-from-message-type
  [request-map]
  (debug "Adding target queue for publishing the response to a resource request...")
  (debug "Request map is")
                                        ;(debug "Request payload" (:message-payload request-map))
  (debug "Request type" (:request-type (:message-payload request-map)))
  (assoc request-map
         :queue-name
         (case (:request-type (:message-payload request-map))
           :list-resources (communications/get-queue-name :resource-list-queue)
           :resource (communications/get-queue-name :resource-queue)
           "default" (do
                       (error "Error in the request type")
                       "Error in the request type"))))

(defn add-handler-resource-aggregator-request
  [listener-map]
  (assoc listener-map :handler
         (fn [ch {:keys [content-type delivery-tag type] :as meta} ^bytes payload]
           (info "Message has been received! " (String. payload "UTF-8"))
           (info "Now it will be parsed")
           (-> listener-map
               (communications/add-edn-message-body payload)
               add-target-queue-from-message-type
               create-response-data
               post-process-response-data
               communications/create-response
               communications/publish-message!))))


(defn- add-provider-metadata
  [communication-map provider metadata]
  (assoc communication-map :provider provider :metadata metadata))

(defn- add-host
  [m]
  {:pre [(:metadata m)]}
  (assoc m :host (or (.getHost (:metadata m)) "localhost")))

(defn- add-port
  [m]
  {:pre [(:metadata m)]}
  (assoc m :port (or (Integer. (re-find #"\d+" (.getPort (:metadata m)))) 8111)))


(defn- add-deployable-download-path
  [m]
  (when (not (:download-path m)) (warn "Download path is missing! Default will be used"))
  (let [download-path (or (:download-path m) "runnables/")]
      (assoc m :download-path (if (.endsWith download-path "/")
                                download-path
                                (str download-path "/")))))

(defn- add-runnable-path
  [m]
  (assoc m :runnable-path
         (str (:download-path m)
              (:runnable-name m))))

(defn- add-runnable-exists?
  [m]
  {:pre [(:target-entity m)]}
  (assoc m :runnable-exits? (.exists
                             (io/file
                              (:runnable-path m)))))

(defn- create-download-path-if-needed!
  [m]
  {:pre [(:target-entity m)]}
  (debug "Downloading the runnable:" (:download-path m))
  (if (.exists (io/file (:download-path m)))
    (when-not (.isDirectory (io/file (:download-path m)))
      (io/delete-file (io/file (:download-path m)))
      (.mkdir (io/file (:download-path m))))
    (.mkdir (io/file (:download-path m)))))

(defn- add-deployable-file
  [m]
  {:pre [(:runnable-path m)]}
  (debug "Adding deployable file")
  (assoc m :deployable-file (:runnable-path m)))


(defn- download-runnable-if-needed!
  [m]
  (debug "Runnable will be downloaded!")
  (if (= (:redo/entity-type (:target-entity m))
         :redo/node-template)
    (when-not (:runnable-exits? m)
      (-> m
          add-deployable-file
          (clojure.set/rename-keys {:resource-driven-process-definition :entity-data})
          domain-language/add-deployable-uri-of-target-resource
          domain-language/add-main-intention
          communications/download-deployable!))
    m))


(deftype RunnableContainerImpl [m]
  uni_stuttgart.ipsm.protocols.integration.operations.RunnableContainer
  (getTargetModel [_] (do
                        (debug "Getting target model")
                        (:tosca-obj m)))
  (getDeployable [_] (io/input-stream (:runnable-path m))))


(defn- java-props->clojure-props
  [props]
  (mapv
   #(into {} %)
   (into [] props)))

(defn- create-domain-specific-propeties
  [clj-props]
  (mapv
   #(str "ResourceInstanceProperty" "Describes of instance a resource definition" (first %))
   clj-props))

(defn- java-props->domain-props
  [props]
  (debug "Properties will be converted")
  (mapv
   (into {} props)));; make sure that properties are associative




(defn- add-final-model-state
  [m]
  {:pre [(:target-entity m) (:instance-location m) (:instance-state m)]}
  (debug "Final model state is" (:instance-state m))
  (assoc m :final-model-state
         (-> m
             (assoc-in [:target-entity :any/any 0] (-> m
                                                         :target-entity
                                                         :redo/any
                                                         (get 0)
                                                         (into {:redo/instance-state (:instance-state m)
                                                                :redo/instance-uri (:instance-location m)
                                                                :redo/start-time (conversions/get-current-time-in-str)})
                                                         domain-language/instance-descriptor-element))
             :target-entity
             domain-language/node-template)))



(defn- post-back-result!
  [m]
  {:pre [(:callback-address m) (:form-params m)]}
  (debug "Posting back the result with the following map")
  (debug (http/post (:callback-address m)
                    {:content-type "application/edn"
                     :form-params {:operation-response (:form-params m)}})))





(defn- create-final-model-state
  [m properties instance-location instance-state]
  (-> m
      (assoc :instance-properties properties)
      (assoc :instance-location instance-location)
      (assoc :instance-state instance-state)
      add-final-model-state))


(defn- add-form-paramters
  [m]
  {:pre [(:output-parameters m)]}
  (assoc m :form-params (-> m
                            :output-parameters
                            transformations/get-map-of-jaxb-type-obj
                            (assoc :redo/process-id (:process-id m))
                            (assoc :redo/target-resource (:target-entity m))
                            (assoc :redo/operation-type (:operation-type m))
                            (assoc :redo/instance-descriptor (:instance-descriptor m))
                            (assoc :redo/resource-driven-process-definition (:resource-driven-process-definition m))
                            domain-language/operation-message)))



(defn- create-and-post-back-result!
  [m]
  (doto (-> m
            add-form-paramters)
    post-back-result!))

(deftype OperationCallbackImpl [m]
  uni_stuttgart.ipsm.protocols.integration.operations.OperationCallback
  (onSuccess [_ output-parameters]
    (create-and-post-back-result! (assoc m :output-parameters output-parameters)))
  (onError [_ output-parameters]
    (create-and-post-back-result! (assoc m :output-parameters output-parameters))))



(defn- add-request-params
  [m]
  {:pre [(:message m)]
   :post [(:params %)]}
  (debug "Adding request params")
  (assoc m :params (:params (:request (:message m)))))

(defn- add-operation-request
  [m]
  (if (or (get (:params m) "operation-request") (get (:params m) :operation-request))
    (assoc m :operation-request (read-string (or (get (:params m) "operation-request")
                                                 (get (:params m) :operation-request))))
    (error "Missing operation request")))


(defn- add-callback-address
  [m]
  (debug "Adding callback")
  (if (or (get (:params m) "callback-address") (get (:params m) :callback-address))
    (assoc m :callback-address (or (get (:params m) "callback-address")
                                   (get (:params m) :callback-address)))
    (error "Missing callback address")))

(defn- add-process-id
  [m]
  (debug "Adding process id")
  (if (:redo/process-id (:operation-request m))
    (assoc m :process-id (:redo/process-id (:operation-request m)))
    (error "Missing process id!!!")))

(defn- add-informal-process-instance
  [m]
  (debug "Adding process instance")
  (if (:redo/resource-driven-process-definition (:operation-request m))
    (assoc m :resource-driven-process-definition (:redo/resource-driven-process-definition (:operation-request m)))
    (do
      (debug "Missing process instance!!!")
      m)))

(defn- add-target-resource
  [m]
  (debug "Target entity is" (or (:redo/target-resource (:operation-request m))
                                (:redo/target-relationship (:operation-request m))))
  (if (or (:redo/target-resource (:operation-request m))
          (:redo/target-relationship (:operation-request m)))
    (assoc m :target-entity (or (:redo/target-resource (:operation-request m))
                                  (:redo/target-relationship (:operation-request m))))
    (error "Missing target resource")))

(defn- add-instance-descriptor
  [m]
  (debug "Adding isntance descriptor" (:redo/instance-descriptor (:operation-request m)))
  (if (:redo/instance-descriptor (:operation-request m))
    (assoc m :instance-descriptor (:redo/instance-descriptor (:operation-request m)))
    (do
      (error "Missing instance descriptor")
      m)))

(defn- add-postconditioner-relationships
  [m]
  (if (:redo/postconditioner-relationships (:operation-request m))
    (assoc m :postconditioner-relationships (:redo/postconditioner-relationships (:operation-request m)))
    (do
      (debug "No postconditioner relationships have been sent")
      (assoc m :postconditioner-relationships []))))

(defn- add-preconditioner-relationships
  [m]
  (if (:preconditioner-relationship (:redo/preconditioner-relationships (:operation-request m)))
    (assoc m :preconditioner-relationships (:redo/preconditioner-relationship (:redo/preconditioner-relationships (:operation-request m))))
    (do
      (debug "No preconditioner relationships have been sent")
      (assoc m :preconditioner-relationships []))))

(defn- add-dependent-resources
  [m]
  (if (:dependents (:operation-request m))
    (assoc m :dependents (:redo/dependents (:operation-request m)))
    (do
      (debug "No dependent resources have been sent")
      (assoc m :dependents []))))

(defn- add-dependency-resources
  [m]
  (if (:dependency (:dependencies (:operation-request m)))
    (assoc m :dependencies (:dependency (:redo/dependencies (:operation-request m))))
    (do
      (debug "No dependent resources have been sent")
      (assoc m :dependencies []))))

(defn- add-runnable-name
  [m]
  {:pre [(:target-entity m)]}
  (assoc m :runnable-name (:redo/id (:target-entity m))))



(defn- return-unsupported-operation
  [m]
  {:pre [(:operation-type m)]}
  #(hash-map :status 501
             :body (str "Provided operation is not supported by this domain manager"
                        "Operation type:" (:operation-type m)
                        "Domain manager type:" )))

(defmulti dispatch-operation! :operation-type)


(defn- register-operation
  [m]
  {:pre [(:operation-realization m)]}
  (debug "Registring operation" (keys m) m)
  (defmethod dispatch-operation!
    (-> m
        :operation-realization
        .getOperationDefinition
        .getName)
    [im]
    (debug "Executing dispatch!" (:redo/operation-type (:operation-request im)))
    (.executeOperation (:operation-realization m)
                       [(RunnableContainerImpl. im)]
                       (:operation-request im)
                       (OperationCallbackImpl. im))))




(defn register-operations!
  [m]
  {:pre [(:available-operations m)]}
  (->> m
       :available-operations
       (into [])
       (mapv #(register-operation (assoc m :operation-realization %))))
  m)



(defn- add-operation-type
  [m]
  (if (:redo/operation-type (:operation-request m))
    (assoc m :operation-type (:redo/operation-type (:operation-request m)))
    (error "Missing process operation type!!!")))




(defn- add-tosca-obj
  [m]
  {:pre [(:target-entity m)]
   :post [(:tosca-obj %)]}
  (debug "Adding tosca object" (:target-entity m))
  (assoc m :tosca-obj (if (= (:redo/entity-type (:target-entity m))
                             :redo/node-template)
                        (transformations/get-node-template-of-map
                         (assoc m :entity-data (:target-entity m)))
                        (transformations/get-relationship-template-of-map
                         (assoc m :entity-data (:target-entity m))))))


(defn- run-dispatch!
  [m]
  (future (doto (-> m
                    add-process-id
                    add-informal-process-instance
                    add-callback-address
                    add-target-resource
                    add-instance-descriptor
                    add-preconditioner-relationships
                    add-postconditioner-relationships
                    add-dependent-resources
                    add-runnable-name
                    add-deployable-download-path
                    add-runnable-path
                    add-tosca-obj
                    add-runnable-exists?)
            create-download-path-if-needed!
            download-runnable-if-needed!
            dispatch-operation!))
  "OK")


(defn- post-handler!
  [m]
  (fn [request]
    (debug "Request has been received!!")
    (debug "now it will be parsed with the following map!!")
    (-> m
        (assoc :message request)
        add-request-params
        add-operation-request
        add-operation-type
        run-dispatch!)))

(defn- return-knowledge-resource!
  [request]
  (debug "Request has been received!!")
  ())

(defn- deletion-handler!
  [request]
  (debug "Request has been received!!"))


(def handler-path "resources/")


;; instead of id namespace name pair should be used
(defn rest-handler-config
  [m]
  {:pre [(:operations-handler-path m)]}
  [{:resource-path (str "/" (:operations-handler-path m) ":namespace/:name")
    :available-media-types ["application/json"]
    :allowed-methods [:delete :get :post]
    :handle-ok return-knowledge-resource!
    :post! (post-handler! m)
    :delete! deletion-handler!}])

(defn add-rest-handler-config
  [m]
  (update-in m [:resource-config :route-configs] into (rest-handler-config m)))


(defn start-rest-handler!
  "Adds a new listener to the main queue."
  [m]
  (debug "Adding rest handler" (:uri m))
  (->> m
       add-rest-handler-config
       communications/add-routes
       communications/run-server-detached!
       (swap! rest-service assoc :rest-config)))

(defn stop-rest-handler!
  "Adds a new listener to the main queue."
  [m]
  (debug "Adding rest handler")
  (->> @rest-service
       :rest-config
       .stop
       (swap! rest-service assoc :rest-config)))

(defn register-execution-environment!
  [m]
  {:pre [(communications/get-queue-name :ee-announcement-queue) (:metadata m) (:execution-environment-integrator m)]}
  (doto (-> m
            (assoc :queue-name (communications/get-queue-name :ee-announcement-queue))
            (assoc :request {:request-type :register-ee
                             :correlation-id (conversions/create-unique-id)
                             :register-ee (:execution-environment-integrator m)}))
    communications/publish-message!))

(defn- add-interfaces
  [m]
  {:pre [(:life-cycle-interface m)]}
  (assoc m :eei-interfaces [(:life-cycle-interface m)]))

(defn- add-execution-environment-integrator-name
  [m]
  {:pre [(:metadata m)]}
  (assoc m :eei-name (str (.getName (:metadata m)))))

(defn- add-execution-environment-integrator-target-namespace
  [m]
  {:pre [(:metadata m)]}
  (assoc m :eei-target-namespace (str (.getTargetNamespace (:metadata m)))))

(defn- add-execution-environment-integrator-uri
  [m]
  {:pre [(:handler-path m)]}
  (assoc m :eei-uri (:handler-path m)))

(defn- add-execution-environment-integrator-id
  [m]
  {:pre [(:eei-name m) (:eei-uri m)]}
  (assoc m :eei-id (conversions/create-unique-id-from-str (:eei-name m) (:eei-uri m))))

(defn- add-execution-environment-integrator
  [m]
  {:pre [(:eei-name m) (:eei-id m) (or (:resource-specific-type-groups m) (:domain-specific-type-groups m)) (:eei-uri m)]
   :post [(:execution-environment-integrator %)]}
  (assoc m :execution-environment-integrator (domain-language/execution-environment-integrator
                                              {:redo/entity-identity {:redo/name (:eei-name m)
                                                                      :redo/target-namespace (:eei-target-namespace m)}
                                               :redo/id (:eei-id m)
                                               :redo/resource-specific-type-groups (:resource-specific-type-groups m)
                                               :redo/domain-specific-type-groups (:domain-specific-type-groups m)
                                               :redo/artifact-type (:artifact-type m)
                                               :redo/uri (:eei-uri m)})))


(defn- operation-realization->interface-operation
  [m]
  {:pre [(:operation-realization m)]}
  (debug "INTERFACE IS" (domain-language/operation (transformations/get-map-of-jaxb-type-obj
                                                    (.getOperationDefinition (:operation-realization m)))))
  (domain-language/operation (transformations/get-map-of-jaxb-type-obj
                              (.getOperationDefinition (:operation-realization m)))))



(defn- add-interface-operations
  [m]
  {:pre [(:available-operations m)]
   :post [(:interface-operations %)]}
  (->> m
       :available-operations
       (mapv #(operation-realization->interface-operation (assoc m :operation-realization %)))
       (assoc m :interface-operations)))


(defn- type-group-name
  [o]
  (keyword (conversions/winery-encode (.getInterfaceName o))))

(defn- add-interface-groups
  [m]
  {:pre [(:available-operations m)]
   :post [(:interface-groups %)]}
  (->> m
       :available-operations
       (mapv type-group-name)
       distinct
       (mapv #(vector % {}))
       (into {})
       (assoc m :interface-groups)))

(defn- add-resource-specific-type-groups
  [m]
  {:pre [(:interface-groups m)]
   :post [(:resource-specific-type-groups %)]}
  (->> m
       :available-operations
       (mapcat #(or
                 (and
                  (instance? ResourceOperationRealization %)
                  (seq (.getSupportedResources %)))
                 (and
                  (instance? RelationshipOperationRealization %)
                  (seq (-> () (into (.getSupportedSourceRelationships %)) (into (.getSupportedTargetRelationships %)))))
                 []))
       (mapv str)
       distinct
       (mapv conversions/winery-encode)
       (mapv keyword)
       (mapv #(vector % (:interface-groups m)))
       (into {})
       (assoc m :resource-specific-type-groups)))


(defn- add-domain-specific-type-groups
  [m]
  {:pre [(:interface-groups m)]
   :post [(:domain-specific-type-groups %)]}
  (->> m
       :available-operations
       (mapcat #(or
                 (and
                  (instance? ResourceOperationRealization %)
                  (seq (.getSupportedDomains %)))
                 (and
                  (instance? RelationshipOperationRealization %)
                  (seq (-> () (into (.getSupportedSourceDomains %)) (into (.getSupportedTargetDomains %)))))
                 []))
       (mapv str)
       distinct
       (mapv conversions/winery-encode)
       (mapv keyword)
       (mapv #(vector % (:interface-groups m)))
       (into {})
       (assoc m :domain-specific-type-groups)))


(defn- add-new-operation-into-interface
  [m]
  (debug "Adding new operation into interface" (:target-interface m))
  (if (and (:target-interface m) (seq (:target-interface m)))
    (domain-language/interface
     (update-in (:target-interface m) [:operation]
                #(into [] (into % [(transformations/get-map-of-jaxb-type-obj
                                    (.getOperationDefinition (:target-operation m)))]))))
    (domain-language/interface
     {:name (.getInterfaceName (:target-operation m))
      :operation [(transformations/get-map-of-jaxb-type-obj
                   (.getOperationDefinition (:target-operation m)))]})))

(defn- operation-is-supported?
  [m]
  {:pre [(:target-operation m)
         (:type-collection-fn m)
         (:type-group m)]}
  (or (nil? ((:type-collection-fn m) (:target-operation m)))
      ((into #{} (->> ((:type-collection-fn m) (:target-operation m))
                      (mapv str)
                      (mapv conversions/winery-encode)
                      (mapv keyword)))
       (first (:type-group m)))))



(defn- add-resource-operation
  [m]
  {:pre [(:target-operation m)
         (:type-group m)]}
  (debug "Type group is" (:type-group m))
  (if (operation-is-supported? (assoc m :type-collection-fn (memfn getSupportedResources)))
    (update-in m [:type-group 1 (type-group-name (:target-operation m)) :interface]
               #(add-new-operation-into-interface (assoc m :target-interface %)))))

(defn- add-relationship-operation
  [m]
  {:pre [(:target-operation m)
         (:type-group m)]}
  (cond-> m
    (operation-is-supported? (assoc m :type-collection-fn (memfn getSupportedSourceRelationships)))
    (update-in [:type-group 1 (type-group-name (:target-operation m)) :source-interfaces]
               #(add-new-operation-into-interface (assoc m :target-interface %)))

    (operation-is-supported? (assoc m :type-collection-fn (memfn getSupportedTargetRelationships)))
    (update-in [:type-group 1 (type-group-name (:target-operation m)) :target-interfaces]
               #(add-new-operation-into-interface (assoc m :target-interface %)))))


(defn- add-resource-domain-operation
  [m]
  {:pre [(:target-operation m)
         (:type-group m)]}
  (debug "Adding resource operation")
  (if (operation-is-supported? (assoc m :type-collection-fn (memfn getSupportedDomains)))
    (update-in m [:type-group 1 (type-group-name (:target-operation m)) :interfaces]
               #(add-new-operation-into-interface (assoc m :target-interface %)))))

(defn- add-relationship-domain-operation
  [m]
  {:pre [(:target-operation m)
         (:type-group m)]}
  (cond-> m
    (operation-is-supported? (assoc m :type-collection-fn (memfn getSupportedRelationshipDomains)))
    (update-in [:type-group 1 (type-group-name (:target-operation m)) :source-interfaces]
               #(add-new-operation-into-interface (assoc m :target-interface %)))

    (operation-is-supported? (assoc m :type-collection-fn (memfn getSupportedTargetRelationshipDomains)))
    (update-in [:type-group 1 (type-group-name (:target-operation m)) :target-interfaces]
               #(add-new-operation-into-interface (assoc m :target-interface %)))))


(defn- add-operation-into-a-type-group
  [m]
  {:pre [(:target-operation m)]}
  (cond-> m
    (instance? ResourceOperationRealization (:target-operation m)) add-resource-operation
    (instance? RelationshipOperationRealization (:target-operation m)) add-relationship-operation))


(defn- add-interface-operations-for-each-type-group
  [m]
  {:pre [(:available-operations m)
         (:type-group m)]}
  (debug "Add interface operations for each type group")
  (let [operation-count (count (:available-operations m))]
    (loop [index 0
           type-group (:type-group m)]
      (if (< index operation-count)
        (recur (inc index)
               (-> m
                   (assoc :target-operation (get (:available-operations m) index))
                   (assoc :type-group type-group)
                   add-operation-into-a-type-group
                   :type-group))
        (assoc m :type-group type-group)))))



(defn- add-interface-operations-for-each-resource-specific-type
  [m]
  {:pre [(:resource-specific-type-groups m)]}
  (->> m
       :resource-specific-type-groups
       (mapv #(-> m
                  (assoc :type-group %)
                  add-interface-operations-for-each-type-group
                  :type-group))
       (into {})
       (assoc m :resource-specific-type-groups)))


(defn- add-interface-operations-for-each-domain-specific-type
  [m]
  {:pre [(:domain-specific-type-groups m)]}
  (debug "Adding interface ops")
  (->> m
       :domain-specific-type-groups
       (mapv #(-> m
                  (assoc :type-group %)
                  add-interface-operations-for-each-type-group
                  :type-group))
       (into {})
       (assoc m :domain-specific-type-groups)))


(defn- interface-maps->interface-vector-for-type-group
  [m]
  {:pre [(:interface-map m)]
   :post [(:interface-vector %)]}
  (->> m
       :interface-map
       (mapv second)
       (assoc m :interface-vector)))

;; TODO check values of type group values
(defn- interface-maps->interface-vector-for-type-groups
  [m]
  {:pre [(:type-groups m)]}
  (->> m
       :type-groups
       (mapv #(->> %
                   second
                   (assoc m :interface-map)
                   interface-maps->interface-vector-for-type-group
                   :interface-vector
                   (hash-map :interfaces)
                   (vector (first %))))
       (into {})
       (assoc m :type-groups)))


(defn- interface-maps->interface-vector-for-both-type-groups
  [m]
  {:pre [(:domain-specific-type-groups m)
         (:resource-specific-type-groups m)]}
  (-> m
      (assoc :domain-specific-type-groups (-> m
                                              (assoc :type-groups (:domain-specific-type-groups m))
                                              interface-maps->interface-vector-for-type-groups
                                              :type-groups))
      (assoc :resource-specific-type-groups (-> m
                                                (assoc :type-groups (:resource-specific-type-groups m))
                                                interface-maps->interface-vector-for-type-groups
                                                :type-groups))))


(defn create-add-execution-environment-integrator
  [m]
  {:post [(:execution-environment-integrator %)]}
  (debug "Creating EEI with the map")
  (->> m
       add-interface-operations
       add-interface-groups
       add-resource-specific-type-groups
       add-domain-specific-type-groups
       ; interface-maps->interface-vector-for-both-type-groups
       add-interface-operations-for-each-resource-specific-type
       add-interface-operations-for-each-domain-specific-type
       add-execution-environment-integrator-name
       add-execution-environment-integrator-target-namespace
       add-execution-environment-integrator-uri
       add-execution-environment-integrator-id
       c/add-artifact-type-uri
       c/add-artifact-type
       add-execution-environment-integrator
       :execution-environment-integrator
       (assoc m :execution-environment-integrator)))
