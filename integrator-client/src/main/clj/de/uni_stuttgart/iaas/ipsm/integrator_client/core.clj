(ns de.uni-stuttgart.iaas.ipsm.integrator-client.core
  (:require [de.uni-stuttgart.iaas.ipsm.integrator-client.domain-manager :as dm]
            [taoensso.timbre :as timbre]
            [de.uni-stuttgart.iaas.ipsm.utils.communications :as com]
            [clojure.spec.alpha :as sc]
            [de.uni-stuttgart.iaas.ipsm.integrator-client.commons :as c]
            [de.uni-stuttgart.iaas.ipsm.utils.constants :as con]
            [de.uni-stuttgart.iaas.ipsm.integrator-client.execution-environment-integrator :as eei])
  (:import [com.google.common.reflect ClassPath]
           [java.net URI]
           [uni_stuttgart.ipsm.protocols.integration.operations OperationRealization]
           [uni_stuttgart.ipsm.protocols.integration DomainManagerOperations])
  (:gen-class
   :prefix "provider-factory-"
   :methods [[registerDomainManager [uni_stuttgart.ipsm.protocols.integration.DomainManagerOperations uni_stuttgart.ipsm.protocols.integration.IntegratorMetadata] void]
             [registerExecutionEnvironmentIntegrator [java.util.List
                                                      uni_stuttgart.ipsm.protocols.integration.IntegratorMetadata] void]]
   :name de.uni_stuttgart.iaas.ipsm.ResourceProvider))

(timbre/refer-timbre)


(def config (atom {:operations-handler-path "resources/"
                   :deployable-path "deployables/"}))

(sc/def ::config-schema (sc/keys :opt-un [::operations-handler-path
                                          ::dm-operation-packages
                                          ::deployable-path
                                          ::eei-operation-packages]))

(sc/def ::operations-handler-path string?)
(sc/def ::deployable-path string?)

(sc/def ::eei-operation-packages (sc/coll-of string?))
(sc/def ::dm-operation-packages (sc/coll-of string?))

(def state (atom {:rest-service nil}))

(defn get-config
  []
  (swap! config into (con/properties-file)))

(defn register-domain-manager-and-start-handlers!
  "Creates one single Domain Manager registers it starts its
  handlers. Respective DomainManagerOperations and IntegratorMetadata
  must be provided."
  [^uni_stuttgart.ipsm.protocols.integration.DomainManagerOperations manager
   ^uni_stuttgart.ipsm.protocols.integration.IntegratorMetadata metadata]
  (doto (-> (into {:manager manager :metadata metadata} (get-config))
            (assoc :target-queue (com/get-queue-name :resource-requestor-queue))
            (assoc :message-type "resource.response")
            c/add-uri)
    dm/register-domain-manager!
    dm/create-handlers))

(defn register-domain-manager!
  "Creates one single Domain Manager registers it."
  [^uni_stuttgart.ipsm.protocols.integration.DomainManagerOperations manager
   ^uni_stuttgart.ipsm.protocols.integration.IntegratorMetadata metadata]
  (doto (-> (into {:manager manager :metadata metadata} (get-config))
            (assoc :target-queue (com/get-queue-name :resource-requestor-queue))
            (assoc :message-type "resource.response")
            c/add-uri)
    dm/register-domain-manager!))

(defn provider-factory-registerDomainManager
  "Creates one single Domain Manager registers it starts its
  handlers. Respective DomainManagerOperations and IntegratorMetadata
  must be provided."
  [this
   manager
   metadata]
  "Registers a provider to rabbit and executes its functions when needed."
  (register-domain-manager-and-start-handlers! manager metadata))



(defn register-execution-environment-integrator!
  "Registers a provider to rabbit and executes its functions when needed."
  [^java.util.List availableOperations
   ^uni_stuttgart.ipsm.protocols.integration.IntegratorMetadata metadata
   m]
  (doto (-> (into {:available-operations availableOperations :metadata metadata} (get-config))
            (into m)
            c/add-uri
            (c/add-uri-path (:operations-handler-path @config) :handler-path)
            c/add-port
            c/add-host
            com/add-connection-data
            eei/create-add-execution-environment-integrator)
    (debug "will be added and registered...")
    eei/register-execution-environment!
    eei/register-operations!
    eei/start-rest-handler!))


(defn stop-execution-environment-integrator-handlers!
  "Stops a provider to rabbit and executes its functions when needed."
  [m]
  (eei/stop-rest-handler! m))



(defn provider-factory-registerExecutionEnvironmentIntegrator
  "Registers a provider to rabbit and executes its functions when needed."
  [this
   ^java.util.List availableOperations
   ^uni_stuttgart.ipsm.protocols.integration.IntegratorMetadata metadata]
  (register-execution-environment-integrator! availableOperations metadata))


(defn- add-class-loader
  [m]
  (assoc m :class-loader (java.lang.ClassLoader/getSystemClassLoader)))


(defn- add-list-of-class-names-for-target-package
  [m]
  {:pre [(:class-loader m)
         (:target-package m)]}
  (->> (some-> (ClassPath/from (:class-loader m)) (.getTopLevelClasses (:target-package m)) vec)
       (mapv #(.getName %))

       (assoc m :list-of-class-names)))



(defn- add-all-eei-operations
  [m]
  (assoc m :eei-operations
         (some->> m
                  :eei-operation-packages
                  (mapv #(-> m
                             (assoc :target-package %)
                             add-list-of-class-names-for-target-package
                             :list-of-class-names))
                  flatten
                  seq
                  vec
                  (mapv #(resolve (symbol %))) ;; convert class string names into class objects
                  (filterv #(.isAssignableFrom OperationRealization %)) ;; check if the classes extend the desired operations otherwise ignore them
                  (mapv #(clojure.lang.Reflector/invokeConstructor % (into-array [])))))) ;; create operations


(defn- add-all-dm-operations
  [m]
  (assoc m :dm-operations
         (some->> m
                  :dm-operation-packages
                  (mapv #(-> m
                             (assoc :target-package %)
                             add-list-of-class-names-for-target-package
                             :list-of-class-names))
                  flatten
                  seq
                  vec
                  (mapv #(resolve (symbol %))) ;; convert class string names into class objects
                  (filterv #(.isAssignableFrom DomainManagerOperations %)) ;; check if the classes extend the desired operations otherwise ignore them
                  (mapv #(clojure.lang.Reflector/invokeConstructor % (into-array []))))))

(deftype IntegratorClientMetadata [m]
  uni_stuttgart.ipsm.protocols.integration.IntegratorMetadata
  (getName [_] (m :name))
  (getTargetNamespace [_] (URI. (m :domain-uri)))
  (getUri [_] (java.net.URI. (m :uri))))


(defn- add-integrator-metadata
  [m]
  (assoc m :metadata (IntegratorClientMetadata. m)))

(defn- register-domain-managers!
  [m]
  (some->> m
           :dm-operations
           (mapv #(-> m (assoc :manager %) dm/register-domain-manager!))))

(defn create-single-client-using-all-configs!
  "Register all specified domain managers, i.e.,
  DomainManagerOperations, and ExecutionEnvironmentIntegrator
  operations inside the given configuration files, update them and
  create REST services."
  [&args]
  (swap! state assoc :rest-service
         (com/run-server-detached! (doto (-> (sc/conform ::config-schema (get-config))
                                             add-class-loader
                                             add-integrator-metadata
                                             add-all-eei-operations
                                             add-all-dm-operations
                                             add-integrator-metadata
                                             c/add-uri
                                             (c/add-uri-path (:handler-path @config) :handler-path)
                                             com/add-connection-data
                                             (clojure.set/rename-keys {:eei-operations :available-operations})
                                             eei/create-add-execution-environment-integrator
                                             (assoc :target-queue (com/get-queue-name :resource-requestor-queue))
                                             (assoc :message-type "resource.response")
                                             c/add-port
                                             c/add-host
                                             dm/add-rest-handler-config
                                             eei/add-rest-handler-config
                                             com/add-routes)
                                     (debug)
                                     register-domain-managers!
                                     eei/register-execution-environment!
                                     eei/register-operations!))))
(defn stop-rest-service!
  [m]
  (swap! state assoc :rest-service (.stop (:rest-service @state))))
