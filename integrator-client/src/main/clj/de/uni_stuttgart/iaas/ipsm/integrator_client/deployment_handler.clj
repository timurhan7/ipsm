(ns de.uni-stuttgart.iaas.ipsm.integrator-client.deployment-handler
  (:require [taoensso.timbre :as timbre]
            [de.uni-stuttgart.iaas.ipsm.utils.conversions :as conversions] ;
            [de.uni-stuttgart.iaas.ipsm.utils.communications :as communications]
            [de.uni-stuttgart.iaas.ipsm.integrator-client.commons :as c]
            [clojure.string :as s]
            [de.uni-stuttgart.iaas.ipsm.protocols.core :as protocols]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.domain-language :as domain-language]
            [de.uni-stuttgart.iaas.ipsm.utils.constants :as constants]))


(timbre/refer-timbre)

(def deployable-path "deployables/")
(def access-protocol-prefix  "http://")

(defn- add-intention-from-request
  [m]
  (debug "Adding intentions to the map" (:target-intention (:params (:request (:request m)))))
  (assoc m :target-intention (:target-intention (:params (:request (:request m))))))

(defn- add-deployable
  [m]
  (debug "Deployable will be added to the map" m)
  (assoc m :deployable
         ;; nil is given for the type of the deployable a dirty short cut
         (.getDeployable (:manager m) (:type m) nil (:intention m))))

(defn- add-id-from-request
  [m]
  (debug "Id will be added to the map" (javax.xml.namespace.QName.
                                        (conversions/winery-decode (:type (:params (:request (:request m)))))
                                        (conversions/winery-decode (:id (:params (:request (:request m)))))))
  (assoc m :type (javax.xml.namespace.QName.
                  (conversions/winery-decode (:type (:params (:request (:request m)))))
                  (conversions/winery-decode (:id (:params (:request (:request m))))))))


(defn- return-deployable
  [m]
  (-> m
      add-intention-from-request
      add-id-from-request
      add-deployable
      :deployable))



(defn- add-node-types
  [m]
  (debug "Types are" (m :types))
  (->> m
       :types
       (filterv domain-language/resource-definition?)
       (assoc m :node-types)))

(defn- add-relationship-types
  [m]
  (debug "Types are" (m :types))
  (->> m
       :types
       (filterv domain-language/relationship-type?)
       (assoc m :relationship-types)))

(defn- create-artifact-type-name
  [m]
  (assoc m :artifact-type-name (constants/property :deployable-artifact-type-name)))



(defn- get-template-id-for-node-type
  [m]
  {:pre [(:artifact-template-name m)]}
  (conversions/create-unique-id-from-str (:artifact-template-name m)))



(defn- create-node-type-implementation
  [m]
  {:pre [(:tosca-type m)
         (:artifact-type m)
         (:artifact-template m)
         (:manager m)]}
  (debug "Artifact type" (:artifact-type m))
  (debug "Creating node type implementation" {:redo/name "ipsm-deployable"
                                              :redo/artifact-type (str (javax.xml.namespace.QName. (:redo/target-namespace (:artifact-type m))
                                                                                                   (:redo/name (:artifact-type m))))
                                              :redo/artifact-ref (str (javax.xml.namespace.QName. (str (.getTargetNamespace (:manager m)))
                                                                                                  (:redo/id (:artifact-template m))))})
  (domain-language/node-type-implementation
   {:redo/name (str (:redo/name (:tosca-type m)) "-node-type-implementation")
    :redo/target-namespace (str (.getTargetNamespace (:manager m)))
    :q/node-type (str (javax.xml.namespace.QName.
                       (:redo/target-namespace (:tosca-type m))
                       (:redo/name (:tosca-type m))))
    :redo/abstract "no"
    :redo/final "yes"
    :redo/deployment-artifacts {:redo/deployment-artifact [(domain-language/deployment-artifact {:redo/name "ipsm-deployable"
                                                                                                 :redo/artifact-type (str (javax.xml.namespace.QName. (:redo/target-namespace (:artifact-type m))
                                                                                                                                            (:redo/name (:artifact-type m))))
                                                                                                 :redo/artifact-ref (str (javax.xml.namespace.QName. (str (.getTargetNamespace (:manager m)))
                                                                                                                                                     (:redo/id (:artifact-template m))))})]}}))


(defn- create-relationship-type-implementation
  [m]
  {:pre [(:tosca-type m)
         (:artifact-type m)
         (:artifact-template m)
         (:manager m)]}
  (debug "Artifact type" (:artifact-type m))
  (debug "Creating node type implementation" {:redo/name "ipsm-deployable"
                                              :redo/artifact-type (str (javax.xml.namespace.QName. (:redo/target-namespace (:artifact-type m))
                                                                                                   (:redo/name (:artifact-type m))))
                                              :redo/artifact-ref (str (javax.xml.namespace.QName. (str (.getTargetNamespace (:manager m)))
                                                                                                  (:redo/id (:artifact-template m))))})
  (domain-language/relationship-type-implementation
   {:redo/name (str (:redo/name (:tosca-type m)) "-relationship-type-implementation")
    :redo/target-namespace (str (.getTargetNamespace (:manager m)))
    :q/relationship-type (str (javax.xml.namespace.QName.
                       (:redo/target-namespace (:tosca-type m))
                       (:redo/name (:tosca-type m))))
    :redo/abstract "no"
    :redo/final "yes"}))

(defn- add-node-type-implementation
  [m]
  (debug "Adding node type implementation" m)
  (assoc m :type-implementation (create-node-type-implementation m)))

(defn- add-relationship-type-implementation
  [m]
  (debug "Adding relationship type implementation" m)
  (assoc m :type-implementation (create-relationship-type-implementation m)))

(defn- merge-new-types-together
  [m]
  {:pre [(:artifact-template m) (:type-implementation m)]}
  (debug "Merging types together" [(:artifact-template m) (:type-implementation m)])
  (assoc m :new-types [(:artifact-template m) (:type-implementation m)]))


(defn- merge-new-relationship-types-together
  [m]
  {:pre [(:artifact-template m) (:relationship-type-implementation m)]}
  (assoc m :new-types [(:artifact-template m) (:relationship-type-implementation m)]))

(defn- add-artifact-template-name
  [m]
  {:pre [(:tosca-type m)]}
  (assoc m :artifact-template-name (str (:redo/name (:tosca-type m)) "-deployable")))

(defn- add-artifact-template-id
  [m]
  (assoc m :artifact-template-id (get-template-id-for-node-type m)))

(defn- add-artifact-template-type
  [m]
  {:pre [(:artifact-type m)]}
  (assoc m :type (str (javax.xml.namespace.QName. (:redo/target-namespace (:artifact-type m))
                                                  (:redo/name (:artifact-type m))))))

(defn- add-artifact-reference-uri-path
  [m]
  {:pre [(:tosca-type m)]}
  (debug "Node type is" (:tosca-type m))
  (assoc m :artifact-reference-uri-path (str (javax.xml.namespace.QName. (:redo/target-namespace (:tosca-type m))
                                                                         (:redo/name (:tosca-type m))))))

(defn- add-node-type-implementations
  "Adds node type implementations after adding artifact types and templates."
  [m]
  {:pre [(:node-types m)]
   :post [(:node-type-implementations %)]}
  (debug "Adding node type implementations" m)
  (->> m
       :node-types
       (mapv #(->> (assoc m :tosca-type %)
                   add-artifact-template-name
                   add-artifact-template-id
                   add-artifact-template-type
                   add-artifact-reference-uri-path
                   domain-language/add-artifact-template
                   add-node-type-implementation
                   merge-new-types-together
                   :new-types))
       flatten
       vec
       (assoc m :node-type-implementations)))

(defn- add-relationship-type-implementations
  "Adds node type implementations after adding artifact types and templates."
  [m]
  (debug "Adding relationship type implementations" m)
  (if (:relationship-types m)
    (->> m
         :relationship-types
         (mapv #(->> (assoc m :tosca-type %)
                     add-artifact-template-name
                     add-artifact-template-id
                     add-artifact-template-type
                     add-artifact-reference-uri-path
                     domain-language/add-artifact-template
                     add-relationship-type-implementation
                     :type-implementation
                     vector))
         flatten
         vec
         (assoc m :relationship-type-implementations))
    m))

(defn- add-artifact-type-into-types
  [m]
  (->> m
       :artifact-type
       vector
       (into (:types m))
       (assoc m :types)))

(defn- add-node-type-implementations-into-types
  [m]
  (->> m
       :node-type-implementations
       (into (:types m))
       (assoc m :types)))

(defn- add-relationship-type-implementations-into-types
  [m]
  (if (:relationship-type-implementations m)
    (->> m
         :relationship-type-implementations
         (into (:types m))
         (assoc m :types))
    m))

(defn- add-uri
  [m]
  {:pre [(:metadata m)]}
  (assoc m :uri (.toString (.getUri (:metadata m)))))

(defn- add-access-protocol-prefix
  [m]
  (assoc m :access-protocol-prefix access-protocol-prefix))

(defn- add-deployable-path
  [m]
  (if (:deployable-path m)
    m
    (assoc m :deployable-path deployable-path)))

(defn- replace-and-merge-new-types
  [m]
  {:pre [(:types m) (:new-types m)]
   :post [(:merged-types %)]}
  (->> m
       :types
       (remove domain-language/resource-definition?)
       vec
       (into (:new-types m))
       (assoc m :merged-types)))

(defn enrich-with-deployment-data
  "m is a map that contains a :response-data field, :manager,
  and :metadata. Manager is a DomainManagerOperations realization and
  Metadata is a IntegratorMetadata"
  [m]
  (->> m
       add-node-types
       add-relationship-types
       add-uri
       add-deployable-path
       add-access-protocol-prefix
       create-artifact-type-name
       c/add-artifact-type-uri
       c/add-artifact-type
       add-node-type-implementations
       (#(do (debug "After adding node type defs" %) %))
       add-relationship-type-implementations
       add-artifact-type-into-types
       (#(do (debug "After adding artifact types" %) %))
       add-node-type-implementations-into-types
       add-relationship-type-implementations-into-types
       :types
       (#(do (debug "Resultiny types are" %) %))
       (assoc m :new-types)
       replace-and-merge-new-types
       :merged-types
       (assoc m :types)))


(defn- handle-deployable-post
  [m]
  (fn [request]
    (debug "Request has been recieved." request)
    (let [media-type (get-in request [:representation :media-type])]
      (condp = media-type
        constants/deployable-media-type
        (let [return-val (return-deployable (assoc m :request request))
              return-stream (.getDeployable return-val)]
          (debug "Return stream is" return-stream)
          (if return-stream
            (do
              (debug "A deployable will be returned" return-stream)
              {:response-stream return-stream})
            "<html><h1>Deployable could not be generated</h1></html>"))
        "text/html" "<html><h1>You requested HTML</h1></html>"))))



(defn- add-deployable-media-type-if-not-exists
  [m]
  (if (:deployable-media-type m)
    m
    (assoc m :deployable-media-type constants/deployable-media-type)))


(defn- add-deployable-route-configs
  [m]
  {:pre [(:deployable-path m)
         (:deployable-media-type m)]}
  (update-in m [:resource-config :route-configs] into [{:resource-path (str "/" (:deployable-path m) ":type/:id")
                                                      :available-media-types [(:deployable-media-type m)]
                                                      :allowed-methods [:post]
                                                      :post! (handle-deployable-post m)
                                                      :respond-with-entity? true
                                                      :handle-created #(:response-stream %)}]))

(defn add-deployment-rest-handler-config
  [m]
  (-> m
      add-deployable-media-type-if-not-exists
      add-deployable-route-configs))
