(ns uni-stuttgart.ipsm.tosca-persistence.service
  (:require [taoensso.timbre :as ti]
            [de.uni-stuttgart.iaas.ipsm.protocols.core :as p]
            [de.uni-stuttgart.iaas.ipsm.utils.constants :as c]
            [de.uni-stuttgart.iaas.ipsm.utils.communications :as com]
            [clj-http.client :as http]
            [clojure.java.io :as io]
            [dire.core :as d]
            [de.uni-stuttgart.iaas.ipsm.utils.winery :as i]
            [de.uni-stuttgart.iaas.ipsm.utils.io :as io-utils]
            [puppetlabs.trapperkeeper.core :as tk])
  (:import [org.eclipse.winery.common Util]))

(ti/refer-timbre)




(def default-winery-paths {:node-type "nodetypes/"
                           :relationship-type "relationshiptypes/"
                           :artifact-type "artifacttypes/"
                           :artifact-template "artifacttemplates/"
                           :service-template "servicetemplates/"
                           :node-type-implementation "nodetypeimplementations/"
                           :relationship-type-implementation "relationshiptypeimplementations/"})

(def ^:private initiation-map {:war-path "winary.war"
                               :war-download-uri "https://www.dropbox.com/s/fkv8tx2fwncvio0/winery.war?dl=1"
                               :winery-path-mappings (or (some-> "entity-path-mappings.edn" io-utils/read-resource read-string)
                                                         default-winery-paths)})

(defn- add-repo-available?
  [m]
  (debug "Winery at the uri will be checked" (:winery-uri m))
  (if (:winery-uri m)
    (do (http/head (:winery-uri m))
        (assoc m :repo-available? true))
    (assoc m :repo-available? false)))

(d/with-handler! #'add-repo-available?
  java.lang.Exception
  (fn [e m & args]
    (error "Connection to winery has failed!!! " (:winery-uri m))
    (assoc m :repo-available? false)))

(defn- stop-on-unavailable-repo!
  [m]
  (if (:repo-available? m)
    (do (info "Winery is available proceeding")
        m)
    (error "Winery is not available! Please start it under the following uri" (:winery-uri m))))

(defn- add-war-available?
  [m]
  {:pre [(:war-path m)]}
  (assoc m :war-exists? (.exists? (io/file (:war-path m)))))

(defn- download-if-winery-war-not-available!
  [m]
  {:pre [(:war-path m)
         (:war-exists? m)
         (:war-download-uri m)]}
  (io/copy (:war-download-uri m) (io/file (:war-path m))))

(defn- create-tosca-entity-using-winery
  [m]
  (some-> m
          add-repo-available?
          stop-on-unavailable-repo!
          i/create-entity-in-winery!))


(defn- delete-single-entity-on-winery!
  [m]
  (-> m
      add-repo-available?
      stop-on-unavailable-repo!
      i/delete-single-entity-from-winery!))


(tk/defservice WineryPersistence
  p/ToscaPersistenceProtocol
  [;; [:WebserverService add-war-handler]
   [:ConfigService get-config]]
  (init [this context]
        ;; (info "Initializing winery persistence service" (get-config))
        ;; (debug "Context" context)
        ;; (debug "This" this)
        ;; (-> (get-config)
        ;;     :winery-config
        ;;     com/prepare-war-for-deployment!)
        ;; (debug "War path" (-> (get-config) :winery-config :war-path))
        ;; (debug "Context path" (-> (get-config) :winery-config :context-path))
        ;; (add-war-handler "winery.war" "/winery")
        ;; (debug "Addition has been completed")
        context)
  (start [this context]
         (info "Starting winery persistence service" (get-config))
         context)
  (stop [this context]
        (info "Shutting down winery persistence service")
        context)
  (get-uri-for-added-service-template [_ m]
                                      (doto (some-> (into m initiation-map)
                                                    (into (:winery-config (get-config)))
                                                    add-repo-available?
                                                    stop-on-unavailable-repo!)
                                        i/upload-definitions-map-into-winery!))

  (get-service-template-on-location [_ m]
                                    (debug "Returning the service template definitions of the provided service template URI, :service-template-uri field is necessary")
                                    (-> m
                                        i/add-resolved-xml-of-service-template))

  (add-domain-manager-data! [_ m]
                            (debug "Adding domain manager data" m)
                            (some-> (into {:entity-data m} initiation-map)
                                    (into (:winery-config (get-config)))
                                    add-repo-available?
                                    stop-on-unavailable-repo!
                                    i/add-domain-manager-data-into-winery!))
  (get-tosca-entities [_ m]
                      (debug "Getting tosca entities" m)
                      (some-> (into m initiation-map)
                              (into (:winery-config (get-config)))
                              add-repo-available?
                              stop-on-unavailable-repo!
                              i/get-entity-types-from-winery
                              :types))
  (get-tosca-entity [_ m]
                    (debug "Getting tosca entity" m)
                    (some-> m
                            (into initiation-map)
                            (into (:winery-config (get-config)))
                            add-repo-available?
                            stop-on-unavailable-repo!
                            i/get-entity-type-from-winery))
  (delete-tosca-entities! [_ m]
                          (debug "Removeing TOSCA entities" m)
                          (some-> (into m initiation-map)
                                  (into (:winery-config (get-config)))
                                  add-repo-available?
                                  stop-on-unavailable-repo!
                                  i/delete-entity-types-from-winery!))
  (delete-tosca-entity! [_ m]
                        (debug "Deleting a single entity" m)
                        (some-> (into m initiation-map)
                                (into (:winery-config (get-config)))
                                i/add-tosca-id-into-entity-data
                                delete-single-entity-on-winery!))
  (add-tosca-entity! [_ m]
                     (error "Adding entity is not yet implemented!")
                     "Adding entity is not yet implemented!")
  (create-tosca-entity! [_ m]
                        (debug "Adding entity" {:entity-data m})
                        (->
                         (into m initiation-map)
                         (into (:winery-config (get-config)))
                         create-tosca-entity-using-winery))
  (get-entity-id [_ m]
                 (some-> (into m initiation-map)
                         (into (:winery-config (get-config)))
                         i/add-tosca-id-into-entity-data))
  (get-entity-url [_ m]
                  (some-> (into m initiation-map)
                          (into (:winery-config (get-config)))
                          i/add-tosca-id-into-entity-data
                          i/get-entity-winery-url))
  (add-type-implementation [_ m]
                           (debug "Returns the implementation of a given" (:implementation-type m))
                           (some-> (into m initiation-map)
                                   (into (:winery-config (get-config)))
                                   (assoc :entity-data {:redo/entity-type (:implementation-type m)})
                                   (#(do (debug "Calling winery repo")%))
                                   i/add-matching-type-implementation))
  (add-artifact-template [_ m]
                           (debug "Returns the implementation of a given" (:implementation-type m))
                           (some-> (into m initiation-map)
                                   (into (:winery-config (get-config)))
                                   (assoc :entity-data {:redo/entity-type :artifact-template})
                                   i/add-matching-artifact-template)))
