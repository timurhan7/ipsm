
package de.uni_stuttgart.iaas.ipsm.executorprocess.data;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.uni_stuttgart.iaas.ipsm.executorprocess.data package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _InformalProcessData_QNAME = new QName("http://iaas.uni-stuttgart.de/ipsm/ExecutorProcess/Data", "InformalProcessData");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.uni_stuttgart.iaas.ipsm.executorprocess.data
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TInformalProcessData }
     * 
     */
    public TInformalProcessData createTInformalProcessData() {
        return new TInformalProcessData();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TInformalProcessData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://iaas.uni-stuttgart.de/ipsm/ExecutorProcess/Data", name = "InformalProcessData")
    public JAXBElement<TInformalProcessData> createInformalProcessData(TInformalProcessData value) {
        return new JAXBElement<TInformalProcessData>(_InformalProcessData_QNAME, TInformalProcessData.class, null, value);
    }

}
