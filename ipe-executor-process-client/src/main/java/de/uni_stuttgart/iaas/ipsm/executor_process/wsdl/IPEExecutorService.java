package de.uni_stuttgart.iaas.ipsm.executor_process.wsdl;

import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * This class was generated by Apache CXF 2.2.9
 * Thu Oct 15 14:40:23 CEST 2015
 * Generated source version: 2.2.9
 * 
 */
 
@WebService(targetNamespace = "http://iaas.uni-stuttgart.de/ipsm/executor-process/wsdl", name = "IPEExecutorService")
@XmlSeeAlso({ObjectFactory.class, de.uni_stuttgart.iaas.ipsm.executorprocess.data.ObjectFactory.class})
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface IPEExecutorService {

    @WebMethod(action = "http://iaas.uni-stuttgart.de/ipsm/executor-process/wsdl/terminateExecutionWithError")
    @Oneway
    public void terminateExecutionWithError(
        @WebParam(partName = "errorDefinition", name = "errorDefinition")
        de.uni_stuttgart.iaas.ipsm.executorprocess.data.TInformalProcessData errorDefinition
    );

    @WebMethod(action = "http://iaas.uni-stuttgart.de/ipsm/executor-process/wsdl/initiate")
    @Oneway
    public void initiate(
        @WebParam(partName = "initializationData", name = "initializationData")
        de.uni_stuttgart.iaas.ipsm.executorprocess.data.TInformalProcessData initializationData
    );

    @WebMethod(action = "http://iaas.uni-stuttgart.de/ipsm/executor-process/wsdl/engagementSuccessful")
    @Oneway
    public void engagementSuccessful(
        @WebParam(partName = "ipeModelInstance", name = "ipeModelInstance")
        de.uni_stuttgart.iaas.ipsm.executorprocess.data.TInformalProcessData ipeModelInstance
    );

    @WebMethod(action = "http://iaas.uni-stuttgart.de/ipsm/executor-process/wsdl/updateIPEProcess")
    @Oneway
    public void updateIPEProcess(
        @WebParam(partName = "newProcess", name = "newProcess")
        de.uni_stuttgart.iaas.ipsm.executorprocess.data.TInformalProcessData newProcess
    );

    @WebMethod(action = "http://iaas.uni-stuttgart.de/ipsm/executor-process/wsdl/terminateSuccessfully")
    @Oneway
    public void terminateSuccessfully(
        @WebParam(partName = "inputResource", name = "inputResource")
        de.uni_stuttgart.iaas.ipsm.executorprocess.data.TInformalProcessData inputResource
    );

    @WebMethod(action = "http://iaas.uni-stuttgart.de/ipsm/executor-process/wsdl/initializationFailed")
    @Oneway
    public void initializationFailed(
        @WebParam(partName = "initializationFailedRequest", name = "initializationFailedRequest")
        de.uni_stuttgart.iaas.ipsm.executorprocess.data.TInformalProcessData initializationFailedRequest
    );

    @WebMethod(action = "http://iaas.uni-stuttgart.de/ipsm/executor-process/wsdl/terminateExecution")
    @Oneway
    public void terminateExecution(
        @WebParam(partName = "terminateExecutionRequest", name = "terminateExecutionRequest")
        de.uni_stuttgart.iaas.ipsm.executorprocess.data.TInformalProcessData terminateExecutionRequest
    );
}
