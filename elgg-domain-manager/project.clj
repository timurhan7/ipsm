(defproject elgg-domain-manager "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [compojure "1.6.0"]
                 [com.taoensso/carmine "2.16.0"]
                 [com.taoensso/timbre "4.10.0"]
                 [ring "1.6.2"]
                 [mysql/mysql-connector-java "6.0.6"]
                 [korma "0.4.3"]
                 [ring/ring-defaults "0.3.1"]
                 [de.uni-stuttgart.iaas.ipsm/commons "0.0.2-SNAPSHOT"]
                 [de.uni-stuttgart.iaas.ipsm/protocols "0.5.3-SNAPSHOT"]
                 [de.uni-stuttgart.iaas.ipsm/integrator-client "0.0.3-SNAPSHOT"]
                 [ring-middleware-format "0.7.2"]]
  :plugins [[lein-ring "0.9.7"]]
  :main elgg-domain-manager.handler
  :ring {:handler elgg-domain-manager.handler/app}
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring/ring-mock "0.3.0"]]}})
