(ns elgg-domain-manager.handler
  (:require [compojure.core :refer :all]
            [taoensso.carmine :as car :refer (wcar)]
            [ring.middleware.format :refer [wrap-restful-format]]
            [compojure.route :as route]
            [clojure.java.io :as io]
            [taoensso.timbre :as timbre]
            [de.uni-stuttgart.iaas.ipsm.protocols.core :as protos]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.domain-language :as domain-language]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.transformations :as transformations]
            [de.uni-stuttgart.iaas.ipsm.utils.io :as ipsm-io]
            [de.uni-stuttgart.iaas.ipsm.utils.constants :as constants]
            [de.uni-stuttgart.iaas.ipsm.utils.conversions :as conversions]
            [de.uni-stuttgart.iaas.ipsm.integrator-client.core :as integrator]
            [ring.adapter.jetty :as ring-adapter]
            [korma.db :as db]
            [korma.core :as db-cont]
            [ring.middleware.defaults :refer [wrap-defaults api-defaults]])
  (:import [java.net URI])
  (:gen-class))

(timbre/refer-timbre)


(db/defdb db (db/mysql {:db (constants/property :db)
                        :host (constants/property :db-host)
                        :port (constants/property :db-port)
                        :user (constants/property :db-user-name)
                        :password (constants/property :db-password)}))

(db-cont/defentity users_entity)




(def redis-conn {:pool {} :spec {}})
(defmacro wcar* [& body] `(car/wcar redis-conn ~@body))


(defn- add-human-resource-from-params
  [m]
  (debug "Adding human resource from params" m)
  (->> m
       :request
       :params
       :messageContent
       (assoc m :resource-map)))

(defn- add-changed-resource-name
  [m]
  (->> m
       :resource-map
       (#(clojure.set/rename-keys % {:displayName :name}))
       (assoc-in m [:resource-map])))

(defn- add-resource-id
  [m]
  {:pre [(:resource-map m)]}
  (debug "Adding resource id" (->> m
                                   :resource-map
                                   :resourceUri
                                   conversions/create-unique-id-from-str))
  (->> m
       :resource-map
       :resourceUri
       conversions/create-unique-id-from-str
       (assoc-in m [:resource-map :id])))

;; (persist-recieved-human-resource! {:request {:params {:messageContent {:resourceUri "name"}}}})


(defn- add-resource-into-redis!
  [m]
  {:pre [(:resource-map m)]}
  (debug "Adding resoruce in to redis!")
  (let [resources (wcar* (car/get "human-resources"))]
    (wcar*
     (car/set "human-resources" (assoc-in (or resources {})
                                          [(keyword (:id (:resource-map m)))]
                                          (:resource-map m))))))

(defn- add-human-resources-from-db
  [m]
  {:post [(:human-resources %)]}
  (let [resources (db-cont/select users_entity)]
    (assoc m :human-resources (or resources {}))))


(defn- persist-recieved-human-resource!
  [m]
  (doto (-> m
            add-human-resource-from-params
            add-resource-id)
    add-resource-into-redis!))


(defroutes app-routes
  (GET "/human-resources" [] #(do
                                (debug %1)
                                (-> {} add-human-resources-from-db :human-resources)))
  (PUT "/human-resources" [] #(do
                                (debug %1)
                                (-> {} (assoc :request %1) persist-recieved-human-resource!)))
  (route/not-found "Not Found :-/"))

(def app
  (wrap-defaults app-routes api-defaults))


(defn- add-entity
  [m]
  {:pre [(:resource-definition m)]}
  (debug "Adding enttiy" m)
  (assoc m :entity-data (domain-language/resource-definition
                         {:redo/name (str (:name (:resource-definition m)) "--" (:email (:resource-definition m)) "--")
                          :redo/abstract "no"
                          :redo/final "yes"
                          :redo/target-namespace (constants/property :domain-uri)})))

(defn run-server
  []
  (-> app
      (wrap-restful-format)
      (ring-adapter/run-jetty {:port 3131 :join? false})))


(defn- add-identity-from-type
  [m]
  {:pre [(:type m)]}
  (assoc m :resource-id (.substring (re-find #"--.*--" (.getLocalPart (:type m)))
                                    2
                                    (- (.length (re-find #"--.*--" (.getLocalPart (:type m)))) 2))))


(defn- add-matching-resource-map
  [m]
  {:pre [(:resource-id m)
         (:human-resources m)]}
  (assoc m :resource-map (some->> m
                                  :human-resources
                                  (filterv #(= (:email %) (:resource-id m)))
                                  first)))

;; https://stackoverflow.com/questions/38283891/how-to-wrap-a-string-in-an-input-stream
(defn- string->stream
  ([s] (string->stream s "UTF-8"))
  ([s encoding]
   (-> s
       (.getBytes encoding)
       (java.io.ByteArrayInputStream.))))

(defn- create-deployable
  [t]
  (debug "Type is " t)
  (->> {:type t}
       add-identity-from-type
       add-human-resources-from-db
       add-matching-resource-map
       :resource-map
       str
       string->stream))

(defn- add-resource-types
  [m]
  (->> m
       add-human-resources-from-db
       :human-resources
       (filterv #(and (= (:admin %) "no")
                     (= (:banned %) "no")))
       (mapv #(-> m (assoc :resource-definition %) add-entity :entity-data))

       (assoc m :resource-types)))

(defn- get-domain-resource-definitions
  []
  (some->> {}
           add-resource-types
           :resource-types
           seq
           (filterv identity)
           (apply transformations/get-definitions-obj-with-types)))
;; NAME IS FALSE

(deftype ElggDeployable [t]
  uni_stuttgart.ipsm.protocols.integration.Deployable
  (getTargetRuntime[this] (constants/property :target-ee))
  (getDeployable [this] (io/input-stream (create-deployable t))))



(deftype ElggManagerMetadata []
  uni_stuttgart.ipsm.protocols.integration.IntegratorMetadata
  (getName [_] (constants/property :name))
  (getTargetNamespace [_] (URI. (constants/property :domain-uri)))
  (getUri [_] (URI. (constants/property :uri))))


(deftype ElggDomainManagerOperations []
  uni_stuttgart.ipsm.protocols.integration.DomainManagerOperations
  (listDomain [this] (elgg-domain-manager.handler/get-domain-resource-definitions))
  (getTargetNamespace [this] (URI. (constants/property :domain-uri)))
  (getDeployable [this resource-type deployable-type intentions] (ElggDeployable. resource-type))
  (listDeployablesOfType [this resourceType] [(constants/property :elgg-deployable-type)]))


;; (def returned-val (integrator/register-domain-manager-and-start-handlers! (ElggDomainManagerOperations.) (ElggManagerMetadata. )))

(defn -main
  []
  (integrator/register-domain-manager-and-start-handlers! (ElggDomainManagerOperations.) (ElggManagerMetadata. )))

;; (def returned-val (integrator/register-domain-manager! (ElggDomainManagerOperations.) (ElggManagerMetadata. )))

;; (def server (run-server))
;; (.serverl server)
