(ns de.uni-stuttgart.iaas.ipsm.providers.docker-compose
  (:require [taoensso.timbre :as timbre]
            [clojure.string :as s]
            [clojure.java.io :as io]
            [clj-yaml.core :as yaml]
            [de.uni-stuttgart.iaas.ipsm.protocols.core :as protos]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.domain-language :as domain-language]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.transformations :as transformations]
            [de.uni-stuttgart.iaas.ipsm.utils.io :as ipsm-io]
            [de.uni-stuttgart.iaas.ipsm.utils.constants :as constants]
            [de.uni-stuttgart.iaas.ipsm.utils.conversions :as conversions]
            [de.uni-stuttgart.iaas.ipsm.integrator-client.core :as integrator]
            [clj-http.client :as http])
  (:import [java.net URI]
           [javax.xml.namespace QName])
  (:gen-class))

(timbre/refer-timbre)

(def repository
  (if (constants/property :repository-path)
    (do
      (info "Repository path is" (constants/property :repository-path))
      (info "Default path might be used if not set")
      (io/file (constants/property :repository-path)))
    (error "Missing variable!!!! REPOSITORY_PATH must be defined during execution.")))

(defn- get-available-docker-compose-configs
  []
  (if (.exists repository)
    (if (.isDirectory repository)
      (mapv
       #(do
          (debug "Reading" %)
          (debug "Type is" (s/replace (.getName %) ".yml" ""))
          (assoc (yaml/parse-string (slurp %)) :type (s/replace (.getName %) ".yml" "")))
       (filter
        #(and (not (.isDirectory %)) (.endsWith (.getName %) ".yml"))
        (file-seq repository))))
    (do
      (.mkdir repository)
      [])))

;(seq (:labels (yaml/parse-string (slurp "/home/timur/docker-compose-repository/mediawiki.yml"))))
;(.getName (second (file-seq repository)))
(defn- add-available-docker-compose-configs
  [m]
  (assoc m :docker-compose-configs (get-available-docker-compose-configs)))

(def property-mappings (read-string (ipsm-io/read-resource "property-mappings.edn")))


(defn add-type
  [m]
  {:pre [(:source-definition m) (:type (:source-definition m))]}
  (debug "Type will be added to the following map" m)
  (-> m
      (assoc-in [:resource-definition :type] (constants/property :domain-uri))))

(defn add-description
  [m]
  (-> m
      (assoc :description "Descriptions is not yet supported for this retrieval service")))




(defn- merge-metadata-based-on-defs
  [m]
  {:pre [(:resource-definition m) (:source-definition m)]}
  (debug "Merging metadata based on type")
  (update-in m [:resource-definition] merge
             (into {} (filterv
                       seq
                       (mapv
                        #(if ((first %) (:labels (:source-definition m)))
                           [(second %) ((first %) (:labels (:source-definition m)))])
                        property-mappings)))))
(defn- add-entity
  [m]
  {:pre [(:resource-definition m)]}
  (assoc m :entity-data (domain-language/resource-definition
                         {:redo/name (:name (:resource-definition m))
                          :redo/abstract "no"
                          :redo/final "yes"
                          :redo/target-namespace (constants/property :domain-uri)})))



(defn- add-entity-map
  [m]
  (update-in m [:resource-definition] #(hash-map :entity-map %)))

(defn- docker-compose-config->resource-definition
  [m]
  (debug m)
  (debug (:labels m))
  (if (:labels m)
    (-> {:resource-definition {}  :source-definition m}
        add-type
        merge-metadata-based-on-defs
        add-entity
        :entity-data)))

(defn- docker-compose-configs->resource-definitions
  [m]
  (debug "Following map will be converted to resource definitions" m)
  (mapv
   docker-compose-config->resource-definition
   (:docker-compose-configs m)))

(defn- add-definitions
  [m]
  (assoc m :definitions (docker-compose-configs->resource-definitions m)))


(defn- get-relationship-types
  [m]
  [(domain-language/relationship-definition
    {:redo/name "manages"
     :redo/abstract "no"
     :redo/final "yes"
     ;; :redo/valid-target {:redo/type-ref (str (QName. (constants/property :domain-uri) "redmine"))} ;; a relationship targetgetin redmine
     ;:redo/valid-source {:redo/type-ref (str (QName. (constants/property :human-resources-domain-uri) "*"))} ;; a relationship targetgetin all human resources
     :redo/target-namespace (constants/property :domain-uri)})
   (domain-language/relationship-definition
    {:redo/name "uses"
     :redo/abstract "no"
     :redo/final "yes"
     ; :redo/valid-target {:redo/type-ref (str (QName. (constants/property :domain-uri) "*"))} ;; a relationship targetgetin redmine
     ;:redo/valid-source {:redo/type-ref (str (QName. (constants/property :human-resources-domain-uri) "*"))} ;; a relationship targetgetin all human resources
     :redo/target-namespace (constants/property :domain-uri)})])

(defn- append-with-relationships
  [rv]
  (into rv (get-relationship-types rv)))

(defn list-domain
  []
  (-> {}
      add-available-docker-compose-configs
      docker-compose-configs->resource-definitions
      append-with-relationships))

(defn get-domain-resource-definitions
  []
  (some->> (list-domain)
           seq
           (filterv identity)
           (apply transformations/get-definitions-obj-with-types)))

(defn check-nil-or-empty?
  [input]
  (or (nil? input) (= "" input)
     (and (vector? input) (empty? input))
     (and (seq? input) (empty? input))))


(defn apply-or-nil
  [f s]
  (debug "Function" f) s
  (debug "will be executed on" s)
  (if (check-nil-or-empty? s)
    nil
    (f s)))

(defn- get-bytes
  [s]
  (if s
    (.getBytes s)
    s))

(defn get-domain-entity-map
  [type]
  (->> (list-domain)
       (filterv #(= (:type (:entity-map %)) type))))



(defn read-deployable-metadata-edn
  [enrichment-data-str]
  (read-string enrichment-data-str))


(defn- append-relationship-resources
  [relationships-v k m]
  (mapv
   (fn [r] (update-in r [k] #(vec (concat (:id (:entity-map m)) %))))
   relationships-v))


(defn- append-sources
  [relationships deployable-map]
  (append-relationship-resources relationships :source-resources deployable-map))

(defn- append-targets
  [relationships deployable-map]
  (append-relationship-resources relationships :target-resources deployable-map))


(defn- concat-if-not-empty
  [v1 v2]
  (if (empty? v2)
    v1
    (vec (concat v1 v2))))

(defn- assoc-if-not-nil
  [v index val]
  (if val
    (assoc v index val)
    v))


(defn- remove-domain-uri-from-type
  [m]
  {:pre [(:type m)]}
  (debug m)
  (assoc m :type (.getLocalPart (:type m))))

(defn- add-file-path
  [m]
  {:pre [(:type m)]}
  (assoc m :file-path (if (.endsWith (constants/property :repository-path) "/")
                        (str (constants/property :repository-path) (:type m) ".yml")
                        (str (constants/property :repository-path) "/" (:type m) ".yml"))))

(defn- add-file-stream
           [m]
           {:pre [(:file-path m)]}
           (assoc m :file-stream (io/input-stream (io/file (:file-path m)))))

(defn- create-deployable
  [t]
  (debug "Creating deployable" t)
  (-> {:type t}
      remove-domain-uri-from-type
      add-file-path
      add-file-stream
      :file-stream))

(deftype DockerComposeDeployable [t]
  uni_stuttgart.ipsm.protocols.integration.Deployable
  (getTargetRuntime[this] (constants/property :target-ee))
  (getDeployable [this] (io/input-stream (create-deployable t))))



(deftype DockerComposeManagerMetadata []
  uni_stuttgart.ipsm.protocols.integration.IntegratorMetadata
  (getName [_] (constants/property :name))
  (getTargetNamespace [_] (URI. (constants/property :domain-uri)))
  (getUri [_] (URI. (constants/property :uri))))



(deftype DockerComposeDomainManagerOperations []
  uni_stuttgart.ipsm.protocols.integration.DomainManagerOperations
  (listDomain [this] (de.uni-stuttgart.iaas.ipsm.providers.docker-compose/get-domain-resource-definitions))
  (getTargetNamespace [this] (URI. (constants/property :domain-uri)))
  (getDeployable [this resource-type deployable-type intentions] (DockerComposeDeployable. resource-type))
  (listDeployablesOfType [this resourceRelationshipType] [(constants/property :docker-compose-deployable-type)]))


(defn -main
  []
  (integrator/register-domain-manager-and-start-handlers! (DockerComposeDomainManagerOperations.)
                                                          (DockerComposeManagerMetadata. )))



;; (def returned-val (integrator/register-domain-manager-and-start-handlers! (DockerComposeDomainManagerOperations.)
;;                                                                           (DockerComposeManagerMetadata. )))
;; (integrator/stop-rest-service! returned-val)
;; (integrator/register-domain-manager! (DockerComposeDomainManagerOperations.) (DockerComposeManagerMetadata. ))
