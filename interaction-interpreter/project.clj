(defproject de.uni-stuttgart.iaas.ipsm/interaction-interpreter "0.0.1-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [clj-http "1.0.1"]
                 [cheshire "5.4.0"]
                 [com.taoensso/timbre "4.7.4"]
                 [de.uni-stuttgart.iaas.ipsm/commons "0.0.2-SNAPSHOT"]
                 [de.uni-stuttgart.iaas.ipsm/interaction-services "0.0.1-SNAPSHOT"]]
  :source-paths ["src/main/clj"])
