(def ks-version "1.0.0")
(def tk-version "1.5.3")

(defproject de.uni-stuttgart.iaas.ipsm/resource-organizer "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [compojure "1.4.0"]
                 [dire "0.5.3"]
                 [speclj "3.3.2"]
                 [org.clojure/core.async "0.4.474"]
                 [com.taoensso/timbre "4.7.4"]
                 [puppetlabs/trapperkeeper ~tk-version]
                 [com.novemberain/langohr "3.4.1"]
                 [environ "1.0.0"]
                 [overtone/at-at "1.2.0"]
                 [uni-stuttgart.ipsm/tosca-persistence "0.0.1-SNAPSHOT"]
                 [de.uni-stuttgart.iaas.ipsm/persistence "0.0.1-SNAPSHOT"]
                 [de.uni-stuttgart.iaas.ipsm/protocols "0.5.3-SNAPSHOT"]]
  :repl-options {:init-ns user}
  :aliases {"tk" ["trampoline" "run" "--config" "dev-resources/config.conf"]}
  :main puppetlabs.trapperkeeper.main
  :source-paths ["src/main/clj"]
  :plugins [[lein-environ "1.0.0"]]
  :resource-paths ["src/main/resources"])
