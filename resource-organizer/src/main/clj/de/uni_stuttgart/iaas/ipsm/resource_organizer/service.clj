(ns de.uni-stuttgart.iaas.ipsm.resource-organizer.service
  (:require [de.uni-stuttgart.iaas.ipsm.protocols.core :as protocols]
            [de.uni-stuttgart.iaas.ipsm.resource-organizer.core :as core]
            [de.uni-stuttgart.iaas.ipsm.utils.communications :as communications]
            [environ.core :refer [env]]
            [clojure.spec.alpha :as sc]
            [puppetlabs.trapperkeeper.core :as trapperkeeper]
            [puppetlabs.trapperkeeper.services :as services]
            [overtone.at-at :as at]
            [taoensso.timbre :as timbre
             :refer (log  trace  debug  info  warn  error  fatal  report
                          logf tracef debugf infof warnf errorf fatalf reportf
                          spy logged-future with-log-level with-logging-config
                          sometimes)]))




(sc/def ::service-suffix string?)
(sc/def ::service-name string?)
(sc/def ::polling-period integer?)
(sc/def ::access-protocol-prefix string?)
(sc/def ::exchanges (sc/coll-of (sc/keys :req-un [::queue-name])))

(sc/def ::initialization-data (sc/keys :req-un [::service-suffix
                                                ::service-name
                                                ::polling-period
                                                ::polling-pool
                                                ::access-protocol-prefix
                                                ::exchanges]))

(def initialization-data
  {:service-name "Resource organizer"
   :service-suffix "-aggregator"
   :polling-period 50000
   :default-target-namespace "http://www.uni-stuttgart.de/ipsm/resource-organizer"
   :polling-pool (at/mk-pool)
   :access-protocol-prefix "http://"
   :exchanges ;; changing the order can break things
   [{:queue-name (communications/get-queue-name :resource-list-queue)}
    {:queue-name (communications/get-queue-name :resource-queue)
     :handler (fn [ch {:keys [content-type delivery-tag type] :as meta} ^bytes payload]
                (info "Message has been received!"))}
    {:queue-name (communications/get-queue-name :ee-announcement-queue)}
    {:queue-name (communications/get-queue-name :resource-requestor-queue)}]})





(trapperkeeper/defservice ResourceOrganizer
  protocols/ResourceOrganizer
  []
  (init [this context]
        (info "Initializing" (:service-name initialization-data))
        (core/init-service initialization-data)
        context)
  (start [this context]
         (info "Starting" (:service-name initialization-data))
         (as-> (sc/conform ::initialization-data initialization-data) d
           (assoc d :tosca-persistence (services/get-service this :ToscaPersistenceProtocol))
           (assoc d :persistence (services/get-service this :PersistenceProtocol))
           (update-in d [:exchanges 0] #(assoc % :handler (core/aggregate-response d)))
           (update-in d [:exchanges 2] #(assoc % :handler (core/handle-execution-environment-message d)))
           (core/start-service d))
         context)
  (stop [this context]
        (info "Shutting down" (:service-name initialization-data))
        (core/stop-services initialization-data)
        context)
  (get-domain-manager-data [this]
                           (protocols/get-all-entities (services/get-service this :PersistenceProtocol) :dm-data)))
