{:dev {:env {}
       :source-paths ["dev"]
       :dependencies [[puppetlabs/trapperkeeper "1.5.2" :classifier "test" :scope "test"]
                      [midje "1.8.2"]
                      [clj-http "2.0.1"]]}}
