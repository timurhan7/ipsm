(ns de.uni-stuttgart.iaas.ipsm.interaction-services.git.service
  (:require
   [cheshire.core :refer :all]
   [clj-http.client :as client]
   [de.uni-stuttgart.iaas.ipsm.interaction-services.service-protocol :as service-protocol]
   [de.uni-stuttgart.iaas.ipsm.ipe.model.resources :as resources]
   )
  (:import
   [javax.xml.namespace QName]))

;; (def commit-page (read-string (str "[" (slurp "crawl-data/pull-page-0.clj") "]")))

;; (count (read-string (str "[" (slurp "crawl-data/pull-page-0.clj") "]")))

;; (print (slurp "crawl-data/commit-page-1.clj"))
;; (:author commit-page)
;; (clojure.pprint/pprint commit-page)

;; (def author-logins (let [size 302
;;                          file-name "crawl-data/commit-page"]
;;                      (loop [index 0
;;                             return-vec []]
;;                        (print (str "Return vector is" return-vec))
;;                        (if (< index size)
;;                          (recur (inc index)
;;                                 (concat return-vec (get-user-names (read-git-file file-name index))))
;;                          (distinct return-vec)))))

;; (def author-logins (let [size 131
;;                          file-name "crawl-data/author"]
;;                      (loop [index 0]
;;                        (spit (str file-name "-" index ".clj") (into {} (read-git-file file-name index)))
;;                        (if (< index size)
;;                          (recur (inc index))))))


;; (empty? "")

;; (mapv
;;   (fn [map-item]
;;     (print (str "MAP ITEM" map-item))
;;     (into {} (filter (fn [map-field] (not-empty (second map-field))) map-item)))
;;   (read-git-file (str "crawl-data/author-1.clj")))

;; (mapv
;;  #(mapv
;;      (fn [map-item]
;;        (print (str "MAP ITEM" map-item))
;;        (filter (fn [map-field] (not-empty (second map-field))) map-item))
;;      (read-git-file (str "crawl-data/" %)))
;;  (.list (clojure.java.io/file "crawl-data")))



(defn read-git-file
  ([name number]
   (read-string (str "[" (slurp (str name "-" number ".clj")) "]")))
  ([name]
   (read-string (str "[" (slurp name) "]"))))

;; (def review-pages
;;   (mapv
;;   #(hash-map :url (:review_comments_url %) :id (:number %))
;;   commit-page))

;; (mapv
;;  #(crawl-url (:url %) (str "review-" (:id %)))
;;  review-pages)

;; (loop [index 0]
;;   (if (< index (count author-logins))
;;     (let [author (parse-string (:body (client/get (str "https://api.github.com/users/" (nth author-logins index)) {:basic-auth ["timur87" ]})) true)]
;;       (spit (str "crawl-data/author-" index ".clj") (apply str author))
;;       (recur (inc index)))))

(defn get-user-names
  [commit-page]
  (distinct (remove
             nil?
             (mapv
              #(:login (:author %))
              commit-page))))

(defn- crawle-commit-authors
  [login addition-fn]
  (try
    (let [author (parse-string (:body (client/get (str "https://api.github.com/users/" login ))) true)]
      (addition-fn author))
    (catch Exception e (str "caught exception: " (.getMessage e)))))

;; (let [links (:links res)
;;       body (parse-string (:body res) true)]
;;   (spit (str "crawl-data/" entity-name "-" index ".clj") body))

;(crawl-url "https://api.github.com/repos/jclouds/jclouds/commits?path=providers/aws-s3" "commits-providers-aws-s3")

;; (client/get "https://api.github.com/repos/jclouds/jclouds/commits" {:basic-auth ["timur87" "1qazxsw@#edc"] :headers {"path" "providers"}})


(defn crawl-url
  [url entity-name]
  (loop [res (client/get url {:basic-auth ["timur87" "1qazxsw@#edc"]})
         index 0]
    (let [links (:links res)
          body (parse-string (:body res) true)]
      (print res)
      (spit (str "crawl-data/" entity-name "-" index ".clj") (apply str body))
      (if (:next links)
        (recur (client/get (:href (:next links)) {:basic-auth ["timur87" "1qazxsw@#edc"]})
               (inc index))))))

;; (crawl-url "https://api.github.com/repos/jclouds/jclouds/pulls" "pull-page")
;; (def pull-page (client/get "https://api.github.com/repos/jclouds/jclouds/pulls" {:basic-auth ["timur87" "1qazxsw@#edc"]}))

;; (clojure.pprint/pprint pull-page)

(defn- get-commits
  ([owner repo]
   (parse-string (:body (client/get (str "https://api.github.com/repos/" owner "/" repo "/commits"))) true))
  ([owner repo path]
   (parse-string (:body (client/get (str "https://api.github.com/repos/" owner "/" repo "/commits")
                                    {:headers {:path path}})) true)))

(defn- get-pulls
  ([owner repo]
   (parse-string (:body (client/get (str "https://api.github.com/repos/" owner "/" repo "/pulls"))) true))
  ([owner repo branch]
   (parse-string (:body (client/get (str "https://api.github.com/repos/" owner "/" repo "/pulls")
                                    {:headers {:branch branch}})) true)))

(defn- get-assignees
  ([owner repo]
   (parse-string (:body (client/get (str "https://api.github.com/repos/" owner "/" repo "/assignees"))) true)))

(defn- get-issues
  ([org]
   (parse-string (:body (client/get (str "https://api.github.com/orgs/" org "/issues"))) true)))



(defn- get-reviews
  ([pulls]
   (mapv #((parse-string :body (client/get (:review_comments_url %)))) pulls)))

(defn- convert-resource-to-user
  [user]
  (resources/entity-map
   (or (:email user) (:login user))
   (:name user)
   "GIT resource"
   "http://www.iaas.uni-stuttgart.de/ipsm/resource-types/git/user"
   (:url user)
   "http://www.iaas.uni-stuttgart.de/ipsm/resource-types/git"))

(defn- create-resource-from-pull
  [pull]
  (convert-resource-to-user (:user pull)))

(defn- get-resource-commiters
  [commit-map]
  (distinct
   (mapv
    #(keyword (:email (:author (:commit %))))
    commit-map)))


(defn get-resource-commiter-maps
  [owner repo]
  (distinct
   (mapv
    #(:author %)
    (get-commits owner repo))))

;; (def commiters (get-resource-commiter-maps "jclouds" "jclouds"))

;; (count commiters)
;; (clojure.pprint/pprint commiters)

(defn- add-commit-details
  [commit-map committer-map]
  (into {} (mapv
            (fn [commiter]
              [commiter
               (mapv
                (fn [commit] {:date (:date (:author (:commit commit))) :message (:message (:commit commit))})
                (filter #(= commiter (keyword (:email (:author (:commit %))))) commit-map))])
            committer-map)))

(defn- get-resource-writers
  [owner repo path]
  (let [commit-map (get-commits owner repo path)
        committer-map (get-resource-commiters commit-map)]
    (add-commit-details commit-map committer-map)))

(defn- get-jclouds-provider-writer
  [provider]
  (get-resource-writers "jclouds" "jclouds" (str "/providers/" provider)))

(defn get-jclouds-ec2-capability-users
  []
  {:used-capability {:iri "http://www.iaas.uni-stuttgart.de/ipsm/informal-process/individuals#ContributorCapability"}
   :resources (get-jclouds-provider-writer "aws-ec2")})

;{:name "" :id :properties {:path :repositroy-name}}

(defn- create-path-based-interaction-capability-map
  [target-resource]
  [{:used-capability {:iri "http://www.iaas.uni-stuttgart.de/ipsm/informal-process/individuals#ContributorCapability"}
    :target-resource target-resource
    :src-resources (get-jclouds-provider-writer "aws-ec2")}])


(defn- fetch-resource-repo
  [resource]
  ())

(defn- get-commits-to-a-path
  [relevant-resource]
  (let [resource-type (:type relevant-resource)
        repo (:type relevant-resource)]))


(def capability-definitions [{:iri "http://www.iaas.uni-stuttgart.de/ipsm/informal-process/individuals#ContributorCapability"
                              :functional-definitions [{:name "Commit"
                                                        :description "Commit to repo"
                                                        :domain "http://www.iaas.uni-stuttgart.de/ipsm/capabilities/definitions/git/"
                                                        :relevant-resource-types ["http://www.iaas.uni-stuttgart.de/ipsm/resource-types/git/path"
                                                                                  "http://www.iaas.uni-stuttgart.de/ipsm/resource-types/git/repo"]
                                                        :target-resource-fn (fn [relevant-resource] relevant-resource)
                                                        :src-resource-fn (fn [relevant-resource] relevant-resource)}
                                                       {:name "PullRequest"
                                                        :description "Make a pull request"
                                                        :domain "http://www.iaas.uni-stuttgart.de/ipsm/domains/git/"
                                                        :relevant-resource-types ["http://www.iaas.uni-stuttgart.de/ipsm/resource-types/git/path"
                                                                                  "http://www.iaas.uni-stuttgart.de/ipsm/resource-types/git/repo"]
                                                        :target-resource-fn (fn [relevant-resource] relevant-resource)
                                                        :src-resource-fn (fn [relevant-resource] relevant-resource)}
                                                       {:name "IssueAssignees"
                                                        :description "Users assigned to issues"
                                                        :domain "http://www.iaas.uni-stuttgart.de/ipsm/domains/git/"
                                                        :relevant-resource-types ["http://www.iaas.uni-stuttgart.de/ipsm/resource-types/git/repo"]
                                                        :target-resource-fn (fn [relevant-resource] relevant-resource)
                                                        :src-resource-fn (fn [relevant-resource] relevant-resource)}]}
                             {:iri "http://www.iaas.uni-stuttgart.de/ipsm/informal-process/individuals#CoordinationCapability"
                              :functional-definitions [{:name "MakeReview"
                                                        :description "Make a review"
                                                        :domain "http://www.iaas.uni-stuttgart.de/ipsm/domains/git/"
                                                        :relevant-resource-types ["http://www.iaas.uni-stuttgart.de/ipsm/resource-types/git/pull-request"
                                                                                  "http://www.iaas.uni-stuttgart.de/ipsm/resource-types/git/user"]
                                                        :target-resource-fn (fn [relevant-resource] relevant-resource)
                                                        :src-resource-fn (fn [relevant-resource] relevant-resource)}]}])

(defn- fetch-matching-definitions
  [resource]
  (filter
   (fn [cap-def] (some
                  (fn [func-def]
                    (some #(= (:type resource) %) (:relevant-resource-types func-def)))
                  (:functional-definitions cap-def)))
   capability-definitions))



(defrecord service-impl []
    service-protocol/InteractionService
    (get-capability-definitions [this] capability-definitions)
    (get-all-interactions [this] ())
    (get-resource-interactions [this resources] ())
    (update-capability-definitions [this callback] ())
    (update-interactions [this callback] ()))

(def service-instance (service-impl.))
