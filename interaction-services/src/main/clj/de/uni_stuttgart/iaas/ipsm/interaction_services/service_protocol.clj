(ns de.uni-stuttgart.iaas.ipsm.interaction-services.service-protocol
  (:require
   [cheshire.core :refer :all]
   [clj-http.client :as client])
  (:import
   [javax.xml.namespace QName]))

(defprotocol InteractionService
  "Defines standard operations of an interaction service"
  (get-capability-definitions [this] "Returns domain specific capability definitions in the form of {:capabilityType1 capabilityDef :capabilityType2 capabilityDef}")
  (get-all-interactions [this] "Returns available capability definition for each capability in the domain, called at the start-up")
  (get-resource-interactions [this resources] "Returns available capability definition for each capability in the domain, called at the start-up")
  (update-capability-definitions [this callback] "Update capability definitions asynchronous")
  (update-interactions [this callback] "Asynchronous interaction updating"))
