(defproject de.uni-stuttgart.iaas.ipsm/interaction-services "0.0.1-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [clj-http "1.0.1"]
                 [com.taoensso/timbre "4.7.4"]
                 [de.uni-stuttgart.iaas.ipsm/commons "0.0.2-SNAPSHOT"]
                 [cheshire "5.4.0"]]
  :source-paths ["src/main/clj"]
  :checkout-deps-shares [:source-paths :resource-paths])
