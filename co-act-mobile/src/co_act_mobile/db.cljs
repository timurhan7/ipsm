(ns co-act-mobile.db
  (:require
    [co-act-mobile.style :refer [drawer]]))

;;;; initial state of app-db
(def app-db {:nav {:index    0
                   :key      :initial-list
                   :children [{:key :informal-process-definitions
                               :group :list
                               :title "Informal process definitions"}]}
             :login true
             :user {:id "001participant"
                    :username "Bob"
                    :subscribe {}}
             :new-post ""
             ;;;; a example for entity is
             ;{:group :intention-definitions
             ; :key :001intention
             ; :title "increase revenue"
             ; :entity-data {}}
             :static {:capability-types ["Functional capability"
                                         "Cross functional capability"
                                         "Relavent capability"]
                      :addable-list [:intention-definitions
                                     :strategy-definitions
                                     :informal-process-definitions
                                     :capability-definitions
                                     :context-definitions
                                     :namespaces
                                     :groups]
                      :completion-level ["10%" "20%" "30%" "40%" "50%" "60%" "70%" "80%" "90%" "100%"]
                      ;;;; For android simulator a local IP address is needed
                      :host "http://192.168.0.21:8080/winery/"
                      :priority-level ["1" "2" "3" "4" "5" "6" "7" "8" "9" "10"]
                      :priority-color ["grey" "aquamarine" "blue" "green" "yellowgreen"
                                       "khaki" "gold" "orange" "lightcoral" "red"]
                      :priority-color-code ["#808080", "#7fffd4", "#0000ff", "#00ff00", "#9acd32"
                                            "#f0e68c", "#ffd700", "#ffa500", "#f08080", "#ff0000" ]
                      :instance-state-map {:strategy-instances ["Preparing" "Running" "Stopped" "Finished"]
                                           :intention-instances ["Preparing" "Running" "Stopped" "Finished"]
                                           :capability-instances ["Preparing" "Running" "Stopped" "Finished"]
                                           :informal-process-instances ["Preparing" "Running" "Stopped" "Finished"]
                                           }
                      :privilege-level ["Reader" "Writer" "Executor" "Administratior"]}
             :strings {:no-target-intention "Please choose the target intention!"}
             :main-list   [{:group :intention-definitions
                            :text "Intentions"
                            :icon "my-location"
                            :color "cornflowerblue"}
                           ;{:text "intention-instances"
                           ; :icon "my-location"}
                           {:group :strategy-definitions
                            :text "Strategies"
                            :icon "gamepad"
                            :color "cornflowerblue"}
                           ;{:text "strategy-instances"
                           ; :icon "gamepad"}
                           {:group :capability-definitions
                            :text "Capabilities"
                            :icon "power"
                            :color "cornflowerblue"}
                           ;{:text "capability-instances"
                           ; :icon "power"}
                           {:group :context-definitions
                            :text "Contexts"
                            :icon "language"
                            :color "cornflowerblue"}
                           ;{:text "context-instances"
                           ; :icon "language"}
                           {:group :informal-process-definitions
                            :text "Informal Processes"
                            :icon "assignment"
                            :color "cornflowerblue"}
                           ;{:text "process-instances"
                           ; :icon "assignment-turned-in"}
                           {:group :participants
                            :text "Participants"
                            :icon "face"
                            :color "cadetblue"}
                           {:group :namespaces
                            :text "Namespaces"
                            :icon "link"
                            :color "cadetblue"}
                           {:group :groups
                            :text "Groups"
                            :icon "group"
                            :color "cadetblue"}
                           {:group :interactions
                            :text "Interactions"
                            :icon "touch-app"
                            :color "cadetblue"}
                           ]
             :target-definition-map {:capability-instances :capability-definitions
                                     :intention-instances :intention-definitions
                                     :strategy-instances :strategy-definitions
                                     :informal-process-instances :informal-process-definitions}
             :target-instance-group {:intention-definitions :intention-instances
                                     :strategy-definitions :strategy-instances
                                     :capability-definitions :capability-instances
                                     :informal-process-definitions :informal-process-instances}
             :title-map {:intention-definitions "Intention definition"
                         :intention-instances "Intention instance"
                         :strategy-definitions "Strategy definition"
                         :strategy-instances "Strategy instance"
                         :informal-process-definitions "Informal process definition"
                         :informal-process-instances "Informal process instance"
                         :capability-definitions "Capability definition"
                         :capability-instances "Capability instance"
                         :context-definitions "Context definition"
                         :context-instances "Context instance"
                         :interactions "Interaction history"
                         :participants "Participant"
                         :namespaces "Namespace"
                         :groups "Group"
                         }
             :drawer {:size 1
                      :opened false
                      :duration 200
                      ;animate value for main panel
                      :animate-value nil
                      :open-value (- (-> drawer :width))
                      :close-value 0
                      ;animate value for drawer
                      :open-value-drawer 0
                      :close-value-drawer (- (-> drawer :width))
                      :animate-value-drawer nil}
             :data
             {:interactions {:1interaction {:source "Bob"
                                            :target "001strategy"
                                            :group :strategy-definitions
                                            :id    "1interaction"
                                            :action "add new strategy"
                                            :snapshot-entity {:state           0
                                                              :instances {}
                                                              :name         "Finding the intersted business incubator"
                                                              :author          "James"
                                                              :implementations ["001process"]
                                                              :id              "001strategy"
                                                              :target-intention "001intention"
                                                              :contained-intentions []}
                                            ;:content "I add this because there is one incubator just contacted us."
                                            :time 1469606831240
                                            }}
              :intention-definitions {:001intention {:state            0
                                                     :interactive-entity-definition
                                                                       {:identifiable-entity-definition
                                                                        {:entity-identity
                                                                         {:name "Raising enough money to init the project"
                                                                          :target-namespace "001namespace"}}}
                                                     :author           "Bob"
                                                     :id               "001intention"
                                                     :priority         1
                                                     :due         [2016 8 30]
                                                     :achieving-strategies       ["001strategy" "002strategy" "003strategy"]
                                                     :active-strategy  ["002strategy"]
                                                     :instances {}
                                                     :organizational-capabilities    ["011capability" "007capability"]
                                                     :initial-contexts ["001context" "002context"]
                                                     :final-contexts   ["014context"]
                                                     }
                                      :002intention {:state            0
                                                     :interactive-entity-definition
                                                                       {:identifiable-entity-definition
                                                                        {:entity-identity
                                                                         {:name  "Develop the product to a complete version"
                                                                          :target-namespace "001namespace"}}}
                                                     :due         [2016 12 30]
                                                     :author           "David"
                                                     :priority         3
                                                     :instances {:001intention-instance {:instance-state-id  0
                                                                                         :identifiable-entity-definition
                                                                                                             {:entity-identity
                                                                                                              {:name "Product development intention instance"
                                                                                                               :target-namespace "001namespace"}}
                                                                                         :id "001intention-instance"
                                                                                         :start-date [2016 9 1]
                                                                                         :end-date [2016 9 5]
                                                                                         :posts   {}
                                                                                         :instance-uri "winery/intention-instances/001intention-instance"
                                                                                         :source-model "002intention"
                                                                                         :author "001participant"}}
                                                     :id               "002intention"
                                                     :organizational-capabilities   ["001capability" "002capability" "003capability" "013capability" "008capability"
                                                                                     "012capability" "006capability"]
                                                     :achieving-strategies       ["004strategy" "005strategy"]
                                                     :active-strategy  ["004strategy"]
                                                     :initial-contexts ["001context"]
                                                     :final-contexts   ["003context"]
                                                     }
                                      :003intention {:state            0
                                                     :interactive-entity-definition
                                                                       {:identifiable-entity-definition
                                                                        {:entity-identity
                                                                         {:name "Build a complete team"
                                                                          :target-namespace "001namespace"}}}
                                                     :due         [2016 12 30]
                                                     :author           "Bob"
                                                     :priority         9
                                                     :instances {}
                                                     :organizational-capabilities    ["006capability" "009capability"]
                                                     :id               "003intention"
                                                     :achieving-strategies       ["007strategy" "008strategy"]
                                                     :active-strategy  ["007strategy"]
                                                     :initial-contexts ["001context"]
                                                     :final-contexts   ["004context"]
                                                     }
                                      :004intention {:state            0
                                                     :interactive-entity-definition
                                                                       {:identifiable-entity-definition
                                                                        {:entity-identity
                                                                         {:name "Let more people know our product"
                                                                          :target-namespace "001namespace"}}}
                                                     :due         [2016 12 30]
                                                     :author           "Bob"
                                                     :priority         3
                                                     :instances {}
                                                     :id               "004intention"
                                                     :achieving-strategies       ["009strategy" "010strategy"]
                                                     :active-strategy  []
                                                     :organizational-capabilities    ["007capability" "010capability" "014capability"]
                                                     :initial-contexts ["001context"]
                                                     :final-contexts   ["005context"]
                                                     }
                                      :005intention {:state            0
                                                     :interactive-entity-definition
                                                                       {:identifiable-entity-definition
                                                                        {:entity-identity
                                                                         {:name "Get at least three customer companies"
                                                                          :target-namespace "001namespace"}}}
                                                     :due         [2016 12 30]
                                                     :author           "Bob"
                                                     :priority         4
                                                     :instances {}
                                                     :id               "005intention"
                                                     :achieving-strategies       ["006strategy"]
                                                     :active-strategy  []
                                                     :organizational-capabilities    ["010capability" "006capability"]
                                                     :initial-contexts ["001context"]
                                                     :final-contexts   ["006context"]
                                                     }
                                      :006intention {:state            0
                                                     :interactive-entity-definition
                                                                       {:identifiable-entity-definition
                                                                        {:entity-identity
                                                                         {:name "Complete first version Demo product"
                                                                          :target-namespace "001namespace"}}}
                                                     :due         [2016 7 30]
                                                     :author           "Bob"
                                                     :priority         7
                                                     :instances {}
                                                     :id               "006intention"
                                                     :achieving-strategies       []
                                                     :active-strategy  []
                                                     :organizational-capabilities    ["001capability" "002capability" "003capability" "013capability" "008capability"
                                                                                      "012capability" "006capability"]
                                                     :initial-contexts ["001context"]
                                                     :final-contexts   ["007context"]
                                                     }
                                      :007intention {:state            0
                                                     :interactive-entity-definition
                                                                       {:identifiable-entity-definition
                                                                        {:entity-identity
                                                                         {:name "Find people to set up core team"
                                                                          :target-namespace "001namespace"}}}
                                                     :due         [2016 7 30]
                                                     :author           "Bob"
                                                     :priority         9
                                                     :instances {}
                                                     :target-namespace "001namespace"
                                                     :id               "007intention"
                                                     :organizational-capabilities    ["001capability" "002capability" "003capability" "013capability" "008capability"
                                                                                      "012capability" "006capability"]
                                                     :achieving-strategies       []
                                                     :active-strategy  []
                                                     :initial-contexts ["001context"]
                                                     :final-contexts   ["008context"]
                                                     }
                                      :008intention {:state            0
                                                     :interactive-entity-definition
                                                                       {:identifiable-entity-definition
                                                                        {:entity-identity
                                                                         {:name "Developing API server and service server"
                                                                          :target-namespace "001namespace"}}}
                                                     :due         [2016 7 30]
                                                     :author           "Bob"
                                                     :priority         2
                                                     :instances {}
                                                     :id               "008intention"
                                                     :organizational-capabilities    ["001capability" "002capability" "003capability" "013capability" "008capability"
                                                                                      "012capability" "006capability"]
                                                     :achieving-strategies       []
                                                     :active-strategy  []
                                                     :initial-contexts ["001context"]
                                                     :final-contexts   ["009context"]
                                                     }
                                      :009intention {:state            0
                                                     :interactive-entity-definition
                                                                       {:identifiable-entity-definition
                                                                        {:entity-identity
                                                                         {:name  "Developing a web application"
                                                                          :target-namespace "001namespace"}}}
                                                     :due         [2016 7 30]
                                                     :author           "Bob"
                                                     :priority         2
                                                     :instances {}
                                                     :organizational-capabilities    ["001capability" "002capability" "003capability" "013capability" "008capability"
                                                                                      "012capability" "006capability"]
                                                     :id               "009intention"
                                                     :achieving-strategies       []
                                                     :active-strategy  []
                                                     :initial-contexts ["001context"]
                                                     :final-contexts   ["010context"]
                                                     }
                                      :010intention {:state            0
                                                     :interactive-entity-definition
                                                                       {:identifiable-entity-definition
                                                                        {:entity-identity
                                                                         {:name "Developing a mobile version including Android and iOS"
                                                                          :target-namespace "001namespace"}}}
                                                     :due         [2016 7 30]
                                                     :author           "Bob"
                                                     :priority         4
                                                     :instances {}
                                                     :organizational-capabilities    ["001capability" "002capability" "003capability" "013capability" "008capability"
                                                                                      "012capability" "006capability"]
                                                     :id               "010intention"
                                                     :achieving-strategies       []
                                                     :active-strategy  []
                                                     :initial-contexts ["001context"]
                                                     :final-contexts   ["011context"]
                                                     }
                                      }
              :strategy-definitions   {
                                       :001strategy {:state           0
                                                     :interactive-entity-definition
                                                                      {:identifiable-entity-definition
                                                                       {:entity-identity
                                                                        {:name  "Finding the intersted business incubator"
                                                                         :target-namespace "001namespace"}}}
                                                     :instances {:001strategy-instance {:instance-state-id  0
                                                                                        :identifiable-entity-definition
                                                                                                            {:entity-identity
                                                                                                             {:name "instance definition"
                                                                                                              :target-namespace "001namespace"}}
                                                                                        :id "001strategy-instance"
                                                                                        :start-date [2016 9 1]
                                                                                        :end-date [2016 9 5]
                                                                                        :posts   {}
                                                                                        :instance-uri "winery/strategy-instances/001strategy-instance"
                                                                                        :source-model "001strategy"
                                                                                        :author "001participant"}}
                                                     :author          "James"
                                                     :implementations ["001process"]
                                                     :id              "001strategy"
                                                     :target-intention "001intention"
                                                     :contained-intentions []
                                                     :priority         1
                                                     :due         [2016 11 30]
                                                     }
                                       :002strategy {:state           0
                                                     :interactive-entity-definition
                                                                      {:identifiable-entity-definition
                                                                       {:entity-identity
                                                                        {:name "Meeting the Investors in Shanghai"
                                                                         :target-namespace "001namespace"}}}
                                                     :instances {}
                                                     :author          "James"
                                                     :implementations ["002process"]
                                                     :id              "002strategy"
                                                     :target-intention "001intention"
                                                     :contained-intentions ["005intention"]
                                                     :priority         1
                                                     :due         [2016 11 30]
                                                     }
                                       :003strategy {:state           0
                                                     :interactive-entity-definition
                                                                      {:identifiable-entity-definition
                                                                       {:entity-identity
                                                                        {:name "Collecting money from core team member"
                                                                         :target-namespace "001namespace"}}}
                                                     :instances {}
                                                     :author          "James"
                                                     :implementations ["002process"]
                                                     :id              "003strategy"
                                                     :target-intention "001intention"
                                                     :contained-intentions []
                                                     :priority         1
                                                     :due         [2016 11 30]
                                                     }
                                       :004strategy {:state           0
                                                     :interactive-entity-definition
                                                                      {:identifiable-entity-definition
                                                                       {:entity-identity
                                                                        {:name  "Develop the applications by our own develop team"
                                                                         :target-namespace "001namespace"}}}
                                                     :instances {}
                                                     :author          "James"
                                                     :implementations ["002process"]
                                                     :id              "004strategy"
                                                     :target-intention "002intention"
                                                     :priority         1
                                                     :due         [2016 11 30]
                                                     :contained-intentions ["006intention" "008intention" "009intention" "010intention"]
                                                     }
                                       :005strategy {:state           0
                                                     :interactive-entity-definition
                                                                      {:identifiable-entity-definition
                                                                       {:entity-identity
                                                                        {:name "Find external software company to develop for us"
                                                                         :target-namespace "001namespace"}}}
                                                     :instances {}
                                                     :author          "James"
                                                     :implementations ["011process"]
                                                     :id              "005strategy"
                                                     :target-intention "002intention"
                                                     :contained-intentions []
                                                     :priority         1
                                                     :due         [2016 11 30]
                                                     }
                                       :006strategy {:state           0
                                                     :interactive-entity-definition
                                                                      {:identifiable-entity-definition
                                                                       {:entity-identity
                                                                        {:name "Contacting companies and discussing about the cooperations."
                                                                         :target-namespace "001namespace"}}}
                                                     :instances {}
                                                     :author          "James"
                                                     :implementations ["002process"]
                                                     :id              "006strategy"
                                                     :target-intention "005intention"
                                                     :contained-intentions []
                                                     :priority         1
                                                     :due         [2016 11 30]
                                                     }
                                       :007strategy {:state           0
                                                     :interactive-entity-definition
                                                                      {:identifiable-entity-definition
                                                                       {:entity-identity
                                                                        {:name "Recruiting more people from the social network"
                                                                         :target-namespace "001namespace"}}}
                                                     :instances {}
                                                     :author          "James"
                                                     :implementations ["002process"]
                                                     :id              "007strategy"
                                                     :target-intention "003intention"
                                                     :contained-intentions []
                                                     :priority         1
                                                     :due         [2016 11 30]
                                                     }
                                       :008strategy {:state           0
                                                     :interactive-entity-definition
                                                                      {:identifiable-entity-definition
                                                                       {:entity-identity
                                                                        {:name "Invite the friends who is trustful and experience"
                                                                         :target-namespace "001namespace"}}}
                                                     :instances {}
                                                     :author          "James"
                                                     :implementations ["002process"]
                                                     :id              "008strategy"
                                                     :target-intention "003intention"
                                                     :contained-intentions []
                                                     :priority         1
                                                     :due         [2016 11 30]
                                                     }
                                       :009strategy {:state           0
                                                     :interactive-entity-definition
                                                                      {:identifiable-entity-definition
                                                                       {:entity-identity
                                                                        {:name "Put commercial advertisement on other website"
                                                                         :target-namespace "001namespace"}}}
                                                     :instances {}
                                                     :author          "James"
                                                     :implementations ["002process"]
                                                     :id              "009strategy"
                                                     :target-intention "004intention"
                                                     :contained-intentions []
                                                     :priority         1
                                                     :due         [2016 11 30]
                                                     }
                                       :010strategy {:state           0
                                                     :interactive-entity-definition
                                                                      {:identifiable-entity-definition
                                                                       {:entity-identity
                                                                        {:name "Put commercial advertisement on other website"
                                                                         :target-namespace "001namespace"}}}
                                                     :instances {}
                                                     :author          "James"
                                                     :posts           {:010strategy1472939222 {:id "010strategy1472939222"
                                                                                               :user-id "001participant"
                                                                                               :content "It need to be done soon"
                                                                                               }
                                                                       :010strategy1472939246 {:id "010strategy1472939246"
                                                                                               :user-id "002participant"
                                                                                               :content "It really need to be done soon"
                                                                                               }}
                                                     :implementations ["002process"]
                                                     :id              "010strategy"
                                                     :target-intention "004intention"
                                                     :contained-intentions []
                                                     :priority         1
                                                     :due         [2016 11 30]
                                                     }
                                       }
              :context-definitions     {:001context {
                                                     :author  "Bob"
                                                     :interactive-entity-definition
                                                              {:identifiable-entity-definition
                                                               {:entity-identity
                                                                {:name "No fund, 6 team member, self funded server, 200 euro per month, no demon appllication"
                                                                 :target-namespace "001namespace"}}}
                                                     :id      "001context"
                                                     :contained-contexts ["002context"]
                                                     }
                                        :002context {
                                                     :author  "James"
                                                     :interactive-entity-definition
                                                              {:identifiable-entity-definition
                                                               {:entity-identity
                                                                {:name "Two partner potential companies, have not conntacted with others yet"
                                                                 :target-namespace "001namespace"}}}
                                                     :id      "002context"
                                                     :contained-contexts []
                                                     }
                                        :003context {
                                                     :author  "James"
                                                     :interactive-entity-definition
                                                              {:identifiable-entity-definition
                                                               {:entity-identity
                                                                {:name "Full version product in all platforms"
                                                                 :target-namespace "001namespace"}}}
                                                     :id      "003context"
                                                     :contained-contexts ["007context" "009context" "010context" "011context"]
                                                     }
                                        :004context {
                                                     :author  "James"
                                                     :interactive-entity-definition
                                                              {:identifiable-entity-definition
                                                               {:entity-identity
                                                                {:name "core team, marketing team, develop team, sales team"
                                                                 :target-namespace "001namespace"}}}
                                                     :id      "004context"
                                                     :contained-contexts ["008context"]
                                                     }
                                        :005context {
                                                     :author  "James"
                                                     :interactive-entity-definition
                                                              {:identifiable-entity-definition
                                                               {:entity-identity
                                                                {:name "annually 30,000,000 euro sales in europe market"
                                                                 :target-namespace "001namespace"}}}
                                                     :id      "005context"
                                                     :contained-contexts []
                                                     }
                                        :006context {
                                                     :author  "James"
                                                     :interactive-entity-definition
                                                              {:identifiable-entity-definition
                                                               {:entity-identity
                                                                {:name "Three partner companies"
                                                                 :target-namespace "001namespace"}}}
                                                     :id      "006context"
                                                     :contained-contexts []
                                                     }
                                        :007context {
                                                     :author  "James"
                                                     :interactive-entity-definition
                                                              {:identifiable-entity-definition
                                                               {:entity-identity
                                                                {:name "Demo product with core function"
                                                                 :target-namespace "001namespace"}}}
                                                     :id      "007context"
                                                     :contained-contexts []
                                                     }
                                        :008context {
                                                     :author  "James"
                                                     :interactive-entity-definition
                                                              {:identifiable-entity-definition
                                                               {:entity-identity
                                                                {:name "A core team with CTO COO CFO CEO"
                                                                 :target-namespace "001namespace"}}}
                                                     :id      "008context"
                                                     :contained-contexts []
                                                     }
                                        :009context {
                                                     :author  "James"
                                                     :interactive-entity-definition
                                                              {:identifiable-entity-definition
                                                               {:entity-identity
                                                                {:name "Backend Server which could hanlde high concurrent requests"
                                                                 :target-namespace "001namespace"}}}
                                                     :id      "009context"
                                                     :contained-contexts []
                                                     }
                                        :010context {
                                                     :author  "James"
                                                     :interactive-entity-definition
                                                              {:identifiable-entity-definition
                                                               {:entity-identity
                                                                {:name "Front end web application which could be use smoothly"
                                                                 :target-namespace "001namespace"}}}
                                                     :id      "010context"
                                                     :contained-contexts []
                                                     }
                                        :011context {
                                                     :author  "James"
                                                     :interactive-entity-definition
                                                              {:identifiable-entity-definition
                                                               {:entity-identity
                                                                {:name "Mobile application in both Android and iOS platform"
                                                                 :target-namespace "001namespace"}}}
                                                     :id      "011context"
                                                     :contained-contexts ["013context" "012context"]
                                                     }
                                        :012context {
                                                     :author  "James"
                                                     :interactive-entity-definition
                                                              {:identifiable-entity-definition
                                                               {:entity-identity
                                                                {:name "Android application with NFC function"
                                                                 :target-namespace "001namespace"}}}
                                                     :id      "012context"
                                                     :contained-contexts []
                                                     }
                                        :013context {
                                                     :author  "James"
                                                     :interactive-entity-definition
                                                              {:identifiable-entity-definition
                                                               {:entity-identity
                                                                {:name "iOS application with push notification"
                                                                 :target-namespace "001namespace"}}}
                                                     :id      "013context"
                                                     :contained-contexts []
                                                     }
                                        :014context {
                                                     :author  "James"
                                                     :interactive-entity-definition
                                                              {:identifiable-entity-definition
                                                               {:entity-identity
                                                                {:name "get 3,000,000 euro invest, 10 % company share, in first year, 60% development, 40% marketing"
                                                                 :target-namespace "001namespace"}}}
                                                     :id      "014context"
                                                     :contained-contexts []
                                                     }
                                        }
              :capability-definitions {:001capability {:state   0
                                                       :interactive-entity-definition
                                                                {:identifiable-entity-definition
                                                                 {:entity-identity
                                                                  {:name "Front-end Developer"
                                                                   :target-namespace "001namespace"}}}
                                                       :capability-type    0
                                                       :instances {:001capability-instance {:instance-state-id  0
                                                                                            :identifiable-entity-definition
                                                                                                                {:entity-identity
                                                                                                                 {:name "instance definition"
                                                                                                                  :target-namespace "001namespace"}}
                                                                                            :id "001capability-instance"
                                                                                            :start-date [2016 9 1]
                                                                                            :end-date [2016 9 5]
                                                                                            :posts   {}
                                                                                            :instance-uri "winery/capability-instance/001capability-instance"
                                                                                            :source-model "001capability"
                                                                                            :author "003participant"}}
                                                       :author  "James"
                                                       ;:title   "title"
                                                       :id      "001capability"
                                                       :contained-capabilities []
                                                       :providing-resources ["Co-Act-Mobile_Service" "Co-Act_Service"]
                                                       :desired-resources ["Co-Act-Mobile_Service"]
                                                       }
                                       :002capability {:state   0
                                                       :interactive-entity-definition
                                                                {:identifiable-entity-definition
                                                                 {:entity-identity
                                                                  {:name "Back-end Developer"
                                                                   :target-namespace "001namespace"}}}
                                                       :author  "James"
                                                       :capability-type    0
                                                       :instances {}
                                                       ;:title   "title"
                                                       :id      "002capability"
                                                       :contained-capabilities []
                                                       :providing-resources ["Co-Act-Mobile_Service" "Co-Act_Service"]
                                                       :desired-resources ["Co-Act-Mobile_Service"]
                                                       }
                                       :003capability {:state   0
                                                       :interactive-entity-definition
                                                                {:identifiable-entity-definition
                                                                 {:entity-identity
                                                                  {:name "Mobile Developer"
                                                                   :target-namespace "001namespace"}}}
                                                       :author  "James"
                                                       :capability-type    1
                                                       :instances {}
                                                       ;:title   "title"
                                                       :id      "003capability"
                                                       :contained-capabilities ["004capability" "005capability"]
                                                       :providing-resources ["Co-Act-Mobile_Service" "Co-Act_Service"]
                                                       :desired-resources ["Co-Act-Mobile_Service"]
                                                       }
                                       :004capability {:state   0
                                                       :interactive-entity-definition
                                                                {:identifiable-entity-definition
                                                                 {:entity-identity
                                                                  {:name "Android Developer"
                                                                   :target-namespace "001namespace"}}}
                                                       :author  "James"
                                                       :capability-type    0
                                                       :instances {}
                                                       ;:title   "title"
                                                       :id      "004capability"
                                                       :contained-capabilities []
                                                       :providing-resources ["Co-Act-Mobile_Service" "Co-Act_Service"]
                                                       :desired-resources ["Co-Act-Mobile_Service"]
                                                       }
                                       :005capability {:state   0
                                                       :interactive-entity-definition
                                                                {:identifiable-entity-definition
                                                                 {:entity-identity
                                                                  {:name "iOS Developer"
                                                                   :target-namespace "001namespace"}}}
                                                       :author  "James"
                                                       :capability-type    0
                                                       :instances {}
                                                       ;:title   "title"
                                                       :id      "005capability"
                                                       :contained-capabilities []
                                                       :providing-resources ["Co-Act-Mobile_Service" "Co-Act_Service"]
                                                       :desired-resources ["Co-Act-Mobile_Service"]
                                                       }
                                       :006capability {:state   0
                                                       :interactive-entity-definition
                                                                {:identifiable-entity-definition
                                                                 {:entity-identity
                                                                  {:name "System Architect"
                                                                   :target-namespace "001namespace"}}}
                                                       :author  "James"
                                                       :capability-type    0
                                                       :instances {}
                                                       ;:title   "title"
                                                       :id      "006capability"
                                                       :contained-capabilities []
                                                       :providing-resources ["Co-Act-Mobile_Service" "Co-Act_Service"]
                                                       :desired-resources ["Co-Act-Mobile_Service"]
                                                       }
                                       :007capability {:state   0
                                                       :interactive-entity-definition
                                                                {:identifiable-entity-definition
                                                                 {:entity-identity
                                                                  {:name "Marketing Management"
                                                                   :target-namespace "001namespace"}}}
                                                       :author  "James"
                                                       :capability-type    0
                                                       :instances {}
                                                       ;:title   "title"
                                                       :id      "007capability"
                                                       :contained-capabilities []
                                                       :providing-resources ["Co-Act-Mobile_Service" "Co-Act_Service"]
                                                       :desired-resources ["Co-Act-Mobile_Service"]
                                                       }
                                       :008capability {:state   0
                                                       :interactive-entity-definition
                                                                {:identifiable-entity-definition
                                                                 {:entity-identity
                                                                  {:name  "Data Specilist"
                                                                   :target-namespace "001namespace"}}}
                                                       :author  "James"
                                                       :capability-type    0
                                                       :instances {}
                                                       ;:title   "title"
                                                       :id      "008capability"
                                                       :contained-capabilities []
                                                       :providing-resources ["Co-Act-Mobile_Service" "Co-Act_Service"]
                                                       :desired-resources ["Co-Act-Mobile_Service"]
                                                       }
                                       :009capability {:state   0
                                                       :interactive-entity-definition
                                                                {:identifiable-entity-definition
                                                                 {:entity-identity
                                                                  {:name "Recruiter"
                                                                   :target-namespace "001namespace"}}}
                                                       :author  "James"
                                                       :capability-type    0
                                                       :instances {}
                                                       ;:title   "title"
                                                       :id      "009capability"
                                                       :contained-capabilities []
                                                       :providing-resources ["Co-Act-Mobile_Service" "Co-Act_Service"]
                                                       :desired-resources ["Co-Act-Mobile_Service"]
                                                       }
                                       :010capability {:state   0
                                                       :interactive-entity-definition
                                                                {:identifiable-entity-definition
                                                                 {:entity-identity
                                                                  {:name "Business Meeting"
                                                                   :target-namespace "001namespace"}}}
                                                       :author  "James"
                                                       :capability-type    1
                                                       :instances {}
                                                       ;:title   "title"
                                                       :id      "010capability"
                                                       :contained-capabilities ["011capability"]
                                                       :providing-resources ["Co-Act-Mobile_Service" "Co-Act_Service"]
                                                       :desired-resources ["Co-Act-Mobile_Service"]
                                                       }
                                       :011capability {:state   0
                                                       :interactive-entity-definition
                                                                {:identifiable-entity-definition
                                                                 {:entity-identity
                                                                  {:name "Partner Companies"
                                                                   :target-namespace "001namespace"}}}
                                                       :author  "James"
                                                       :capability-type    0
                                                       :instances {}
                                                       ;:title   "title"
                                                       :id      "011capability"
                                                       :contained-capabilities []
                                                       :providing-resources ["Co-Act-Mobile_Service" "Co-Act_Service"]
                                                       :desired-resources ["Co-Act-Mobile_Service"]
                                                       }
                                       :012capability {:state   0
                                                       :interactive-entity-definition
                                                                {:identifiable-entity-definition
                                                                 {:entity-identity
                                                                  {:name "Code Repository"
                                                                   :target-namespace "001namespace"}}}
                                                       :author  "James"
                                                       :capability-type    0
                                                       :instances {}
                                                       ;:title   "title"
                                                       :id      "012capability"
                                                       :contained-capabilities []
                                                       :providing-resources ["Co-Act-Mobile_Service" "Co-Act_Service"]
                                                       :desired-resources ["Co-Act-Mobile_Service"]
                                                       }
                                       :013capability {:state   0
                                                       :interactive-entity-definition
                                                                {:identifiable-entity-definition
                                                                 {:entity-identity
                                                                  {:name "Server"
                                                                   :target-namespace "001namespace"}}}
                                                       :author  "James"
                                                       :capability-type    0
                                                       :instances {}
                                                       ;:title   "title"
                                                       :id      "013capability"
                                                       :contained-capabilities []
                                                       :providing-resources ["Co-Act-Mobile_Service" "Co-Act_Service"]
                                                       :desired-resources ["Co-Act-Mobile_Service"]
                                                       }
                                       :014capability {:state   0
                                                       :interactive-entity-definition
                                                                {:identifiable-entity-definition
                                                                 {:entity-identity
                                                                  {:name "Onlione Shop"
                                                                   :target-namespace "001namespace"}}}
                                                       :author  "James"
                                                       :capability-type    0
                                                       :instances {}
                                                       ;:title   "title"
                                                       :id      "014capability"
                                                       :contained-capabilities []
                                                       :providing-resources ["Co-Act-Mobile_Service" "Co-Act_Service"]
                                                       :desired-resources ["Co-Act-Mobile_Service"]
                                                       }
                                       }
              :node-types {}
              :service-templates {}
              :relationship-types {}
              :node-type-instances {}
              :service-template-instances {}
              :relationship-type-instances {}
              :informal-process-definitions     {:001process {:state   0
                                                              :interactive-entity-definition
                                                                       {:identifiable-entity-definition
                                                                        {:entity-identity
                                                                         {:name "Develop software TechBlock with core functionality"
                                                                          :target-namespace "001namespace"}}}
                                                              :target-intention "003intention"
                                                              :author  "James"
                                                              :id      "001process"
                                                              :model   "002strategy"
                                                              :resource-model {:id "Co-Act-Mobile_Service"}
                                                              :resources ["001resource" "002resource" "010resource" "009resource"]
                                                              :instances {:001instance {:instance-state-id    1
                                                                                       :identifiable-entity-definition
                                                                                                             {:entity-identity
                                                                                                              {:name "Develop demo application with core functionality"
                                                                                                               :target-namespace "001namespace"}}
                                                                                       :author  "James"
                                                                                       :id      "001instance"
                                                                                       :posts   {}
                                                                                       :start-date [2016 7 1]
                                                                                       :end-date [2016 7 23]
                                                                                       :instance-uri "winery/informal-process-instances/001instance"
                                                                                       :source-model "001process"
                                                                                       }}
                                                              }
                                                 :002process {:state   0
                                                              :interactive-entity-definition
                                                                       {:identifiable-entity-definition
                                                                        {:entity-identity
                                                                         {:name "Have meeting with Daimler about the investment"
                                                                          :target-namespace "001namespace"}}}
                                                              :target-intention "005intention"
                                                              :author  "James"
                                                              :resource-model {:id "Co-Act-Mobile_Service"}
                                                              :id      "002process"
                                                              :model   "002strategy"
                                                              :resources ["004resource" "006resource" "016resource"]
                                                              :instances {:002instance {:instance-state-id    0
                                                                                        :identifiable-entity-definition
                                                                                                              {:entity-identity
                                                                                                               {:name "Have meeting with Daimler about the investment"
                                                                                                                :target-namespace "001namespace"}}
                                                                                        :author  "James"
                                                                                        :id      "002instance"
                                                                                        :posts   {}
                                                                                        :start-date [2016 7 29]
                                                                                        :end-date [2016 7 30]
                                                                                        :instance-uri "winery/informal-process-instances/002instance"
                                                                                        :source-model "002process"
                                                                                        :parent-instance "001instance"
                                                                                        }}
                                                              }
                                                 }
              :participants {:001participant {:id "001participant"
                                              :name "Bob"
                                              :email "bob@co-act-mobile.de"
                                              :target-namespace "001namespace"
                                              :phone "1017600000001"
                                              :web "http://www.github.com/bob"}
                             :002participant {:id "002participant"
                                              :name "David"
                                              :target-namespace "001namespace"
                                              :email "bob@co-act-mobile.de"
                                              :phone "1015100000002"
                                              :web "http://www.github.com/david"}
                             :003participant {:id "003participant"
                                              :name "James"
                                              :target-namespace "001namespace"
                                              :email "james@co-act-mobile.de"
                                              :phone "1017600000003"
                                              :web "http://www.github.com/james"}
                             :004participant {:id "004participant"
                                              :name "Li"
                                              :target-namespace "001namespace"
                                              :email "Lee@co-act-mobile.de"
                                              :phone "1017600000004"
                                              :web "http://www.github.com/Lee"}
                             :005participant {:id "005participant"
                                              :name "Alice"
                                              :target-namespace "001namespace"
                                              :email "alice@co-act-mobile.de"
                                              :phone "1017600000005"
                                              :web "http://www.linkedin.com/alice"}
                             :006participant {:id "006participant"
                                              :name "Anna"
                                              :target-namespace "001namespace"
                                              :email "anna@co-act-mobile.de"
                                              :phone "1017600000006"
                                              :web "http://www.github.com/anna"}
                             }
              :namespaces {:001namespace {:id "001namespace"
                                          :name "product-development"
                                          :namespace "co-act-mobile/intentions/product-development"
                                          }
                           :002namespace {:id "002namespace"
                                          :name "mobile-developer"
                                          :namespace "co-act-mobile/capabilities/mobile-developer"
                                          }
                           :003namespace {:id "003namespace"
                                          :name "all-developer"
                                          :namespace "co-act-mobile/capabilities/all-developer"
                                          }
                           :004namespace {:id "004namespace"
                                          :name "self-development"
                                          :namespace "co-act-mobile/capabilities/self-development"
                                          }
                           :005namespace {:id "005namespace"
                                          :name "user-james"
                                          :namespace "co-act-mobile/capabilities/user-james"
                                          }
                           :006namespace {:id "006namespace"
                                          :name "Ubuntu-14.04"
                                          :namespace "http://toscafy.github.io/generated/csar-bricks"
                                          }
                           :007namespace {:id "007namespace"
                                          :name "develop-intention-instance"
                                          :namespace "co-act-mobile/capabilities/develop-intention-instance"
                                          }
                           :008namespace {:id "008namespace"
                                          :name "Init-context"
                                          :namespace "co-act-mobile/contexts/init-context"
                                          }}
              :groups {
                       :001group {:id "001group"
                                  :target-namespace "001namespace"
                                  :name "Backend developers"
                                  :members ["001participant" "003participant" "006participant"]
                                  :posts           {:001group1472939222 {:id "001group1472939222"
                                                                         :user-id "001participant"
                                                                         :content "Hey, guys, welcome aborad!"
                                                                         }}}
                       :002group {:id "002group"
                                  :target-namespace "001namespace"
                                  :name "Management team"
                                  :members ["002participant" "003participant"]
                                  :posts           {:002group1472939222 {:id "002group1472939222"
                                                                         :user-id "002participant"
                                                                         :content "It need to be done soon."
                                                                         }
                                                    :002group1472939246 {:id "002group1472939246"
                                                                         :user-id "003participant"
                                                                         :content "We need configuring the problem exist first."
                                                                         }}}
                       :003group {:id "003group"
                                  :target-namespace "001namespace"
                                  :name "All developers"
                                  :members ["001participant" "003participant" "004participant" "006participant"]
                                  :posts           {:003group1472939221 {:id "003roup1472939221"
                                                                         :user-id "003participant"
                                                                         :content "Techinical Meeting at Wilhelmsplatz 3 at 15:00 Wed 21. September"
                                                                         }
                                                    :003group1472939222 {:id "003roup1472939222"
                                                                         :user-id "001participant"
                                                                         :content "Sorry! My flight has postpone for two hours, I will arrived at 16:00"
                                                                         }
                                                    :003group1472939223 {:id "003roup1472939223"
                                                                         :user-id "001participant"
                                                                         :content "-- Wilhelm-Geiger-Platz, 70469 Stuttgart"
                                                                         }
                                                    :003group1472939224 {:id "003roup1472939224"
                                                                         :user-id "001participant"
                                                                         :content "$ 602.85  14:24 20 Sep. 2016"
                                                                         }
                                                    }}
                       }
              }
             })
