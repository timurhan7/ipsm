(ns co-act-mobile.components.textInput
  (:require [reagent.core :as r :refer [atom]]
            [cljs.spec :as s]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.ui :as ui]))

(def style {:input {}
            :view {:margin (-> common :component :margin)}
            :height {:big 110
                     :middle 80
                     :small 50}
            })

(defn textInput-component
  "A generic text input"
  [props]
  {:pre [(s/valid? some? (:entity props))
         (s/valid? keyword? (:size props))
         (s/valid? some? (:target-property props))]}
  (fn [props]
    (let [size (:size props)
          attribute-name (if(false? (vector? (:target-property props)))
                           (conj [] (:target-property props))
                           (:target-property props))
          text (get-in (:entity props) attribute-name)
          ]
      ;(println "count:" (count text))
      [ui/view {:style (-> style :view)}
       [ui/input {:defaultValue text
                  :multiline true
                  :underlineColorAndroid "blue"
                  :onChangeText #(dispatch [:nav/cache [attribute-name (clojure.string/trim %)]])
                  :style (merge
                           (-> style :input)
                           {:font-size (-> common :text size)
                            :color (if (some? (:color props))
                                     (:color props)
                                     "black")
                            :height (cond
                                      (> (count text) 80) (-> style :height :big)
                                      (> (count text) 43 ) (-> style :height :middle)
                                      :else (-> style :height :small))})}]])))