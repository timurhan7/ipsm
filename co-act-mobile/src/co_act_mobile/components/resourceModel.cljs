(ns co-act-mobile.components.resourceModel
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.subs]
            [cljs.spec :as s]
            [co-act-mobile.ui :as ui]
            [co-act-mobile.android.lib :as lib]
            [co-act-mobile.api :as api]
            [cemerick.url :refer [url-encode]]
            [co-act-mobile.components.textInput :refer [textInput-component]]))

(def style {:view   {:padding (-> common :component :padding)
                     :flex-direction "column"
                     :align-items "stretch"
                     :justify-content "space-between"
                     ;:border-width 1
                     :flex 1}
            :touchable {:view   {:padding 0
                                 :flex 1
                                 :flex-direction "row"
                                 :align-items "center"
                                 :justify-content "space-between"}
                        :text-container {:flex-direction "column"
                                         }
                        :text   {:flex 1
                                 :font-size (-> common :text :small)}
                        :text-namespace   {:flex 1
                                           :font-size 12}
                        :delete {:outer-style {:width (-> common :icon :width)}
                                 :inner-style {
                                               :color "grey"
                                               }}
                        }})

(defn resource-model-component
  "Resouce model link component"
  [props]
  {:pre []}
  (fn [props]
    (let [enable? (nil? (:disable props))
          attribute-name (if enable?
                           (if (false? (vector? (:name props)))
                             (conj [] (:name props))
                             (:name props))
                           )
          current (subscribe [:nav/current-entity])
          entity-id (:id props)]
      (if (some? entity-id)
        (let [entity (subscribe [:data/find [:service-templates entity-id]])]
          ;in case of any internet failure
          (if (some? @entity)
            ;(println "entity-id is" entity-id @entity)
            [ui/view {:style {:flex 1}}
             [ui/view {:style (-> style :view)}
              [ui/touchable-opacity {:activeOpacity (-> common :button :opacity)
                                     :on-press      #(dispatch [:nav/push {:key           (keyword (str (:id @current) "-resource-model"))
                                                                           :group         :resources
                                                                           :title         "Resource model"
                                                                           :requesting    false
                                                                           :resource-id    true
                                                                           :resource-data  @entity
                                                                           }])}
               [ui/view {:style (-> style :touchable :view)}
                [ui/view {:style (-> style :touchable :text-container)}
                 [ui/text {:style (-> style :touchable :text)} (or (:name @entity) (:id @entity))]
                 ;[ui/text {:style           (-> style :touchable :text-namespace)
                 ;          :number-of-lines 1} (:namespace @entity)]
                 ]
                (if enable?
                  [ui/button (merge (-> style :touchable :delete)
                                    {:on-press (if (vector? (get-in @current attribute-name))
                                                 (fn[]
                                                   (dispatch [:nav/cache [attribute-name (get-in (update-in @current attribute-name
                                                                                                            (fn[value]
                                                                                                              (vec (filter (fn[el] (not= entity-id el)) value)))
                                                                                                            )
                                                                                                 attribute-name)
                                                                          ]]))
                                                 #(dispatch [:nav/cache [attribute-name nil]])
                                                 )})
                   [ui/m-icons {:key  (str "cancel" (:id @entity))
                                :name (-> common :icon-name :delete)
                                :size (-> common :icon :size-fix)}]])
                ]]]]))))))