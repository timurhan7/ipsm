(ns co-act-mobile.components.name
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.subs]
            [cljs.spec :as s]
            [co-act-mobile.ui :as ui]))

(defn component
  "A template for quick create component"
  [props]
  (let [state (if (nil? (:state props)) false (:state props))]
    (fn [props]
      )))