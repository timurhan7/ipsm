(ns co-act-mobile.components.entityLink
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [cljs.spec :as s]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.handlers]
            [co-act-mobile.subs]
            [co-act-mobile.ui :as ui]))

(def style {:view   {:padding (-> common :component :padding)
                     :flex-direction "column"
                     :align-items "stretch"
                     :justify-content "space-between"
                     ;:border-width 1
                     :flex 8}
            :touchable {:view   {:padding 0
                                 :flex 7
                                 :flex-direction "row"
                                 :align-items "center"
                                 :justify-content "space-between"}
                        :text   {:flex 1
                                 :font-size (-> common :text :small)}
                        :delete {:outer-style {:width (-> common :icon :width)}
                                 :inner-style {:color "grey"}}}})

(defn entityLink-component
  "A single text entity link"
  [props]
  {:pre [(s/valid? keyword? (:group props))
         (s/valid? some? (:entity props))
         (s/valid? some? (:name props))
         (s/valid? some? (:display props))]}
  (fn [props]
    (let [attribute-name (if(false? (vector? (:name props)))
                           (conj [] (:name props))
                           (:name props))
          display-name (if(false? (vector? (:display props)))
                         (conj [] (:display props))
                         (:display props))
          entity-id (get-in (:entity props) attribute-name)
          ]
      (if (some? entity-id)
        (let [entity (subscribe [:data/find [(:group props) entity-id]])]
          ;(println "entity-id is" entity-id @entity)
          [ui/view {:style {:flex 1}}
           [ui/view {:style (-> style :view)}
            [ui/touchable-opacity {:activeOpacity (-> common :button :opacity)
                                   :on-press #(dispatch [:nav/push-entity [(:group props) @entity]])}
             [ui/view {:style (-> style :touchable :view)}
              [ui/text {:style (-> style :touchable :text)} (get-in @entity display-name)]
              (if (nil? (:require props))
                [ui/button (merge (-> style :touchable :delete)
                                  {:on-press (fn[](do
                                                    ;(println "new list is" (remove #(= entity-id %) entityIds))
                                                    (dispatch [:nav/cache [attribute-name nil]])))})
                 [ui/m-icons {:key (str "cancel" (:id @entity))
                              :name (-> common :icon-name :delete)
                              :size (-> common :icon :size-fix)}]])]]]])))))