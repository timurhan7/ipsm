(ns co-act-mobile.components.drawer
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.style :refer [common color drawer]]
            [co-act-mobile.subs]
            [co-act-mobile.ui :as ui]))

(def style {:text {:fontFamily  ".HelveticaNeueInterface-MediumP4"
                   :font-size (-> common :text :small)
                   :margin 5
                   :flex 4
                   :textAlign   "center"}
            :icon {:margin 5
                   :justify-content "center"
                   :align-items "center"
                   :flex 1}
            :outer {:height 50
                    :borderBottomWidth 0.4
                    :borderColor (-> color :border)
                    }
            :group {:flex 5
                    :background-color "white"
                    :elevation 0}})

(defn drawer-component
  "The drawer display on the right of screen"
  []
  (let [list (subscribe [:main-list])
        animate-value (:close-value-drawer (subscribe [:drawer]))
        animate-final (ui/animated-value. animate-value)
        width (.-width (.get ui/dimensions "window"))
        ]
    (dispatch [:drawer/init-drawer animate-final])
    ;(for [item list] (println "key direct argument is" (keyword item)))
    (fn []
      ;which would force updated according to the list
      ;more info : https://github.com/reagent-project/reagent/issues/18
      @list
      [ui/animated-view {:style {:flex 1
                                 :position "absolute"
                                 :width (-> drawer :width)
                                 :right animate-final
                                 :elevation 15
                                 :borderColor (-> color :border)
                                 :top 0
                                 :bottom 0
                                 :alignItems     "stretch"
                                 }}

       [ui/scroll {:contentContainerStyle {:align-items "stretch"
                                           :justifyContent "flex-start"}
                   :style  {:flexDirection  "column"
                            :padding-top 10
                            :background-color "white"
                            ;:borderLeftWidth 2
                            :flex 1
                            }}
        (doall (for [item @list]
                 ^{:key item} [ui/button {:outer-style (-> style :outer)
                                          :group-style (-> style :group)
                                          :on-press (fn []
                                                      (dispatch [:drawer/pull])
                                                      (dispatch [:nav/direct (:group item)]))}
                               ^{:key (str (name (:group item)) "-icon")}
                               [ui/view {:style (-> style :icon)}
                                ;; [ui/m-icons {:color (:color item)
                                ;;              :key (name (:group item))
                                ;;              :name (:icon item)
                                ;;              :size (-> common :icon :size)}]
                                ]
                               ^{:key (str (name (:group item)) "-text")}
                               [ui/text {:style (merge (-> style :text) {:color (:color item)})} (:text item)]]))]])))
