(ns co-act-mobile.components.refresh
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [cljs.spec :as s]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.handlers]
            [co-act-mobile.lib :as lib]
            [co-act-mobile.subs]
            [co-act-mobile.ui :as ui]))

(def style {:container {:flex-direction "column"
                        :align-items "center"}
            :view   {:padding 0
                     :flex 3
                     :flex-direction "row"
                     :align-items "center"
                     :justify-content "space-between"}
            :button {:outer-style {:width (-> common :icon :width)
                                   :margin-bottom (-> common :component :margin-compen)
                                   }
                     :inner-style {:color (:icon color)
                                   :text-align "center"
                                   :font-size (-> common :text :middle)}
                     :group-style {:border-width 1
                                   :border-radius 5
                                   :justifyContent "center"
                                   :margin 10}}
            :text {:fontFamily  ".HelveticaNeueInterface-MediumP4"
                   :font-size (-> common :text :middle)
                   :margin 5
                   :fontWeight  "bold"
                   :textAlign   "center"}
            :icon {:font-size (-> common :text :middle)
                   :margin 5
                   :fontWeight  "bold"
                   :textAlign   "center"}
            })

(defn refresh-component
  "Bar component : refresh button"
  [props]
  (let []
    (fn [props]
      [ui/view {:style (-> style :container)}
       [ui/button (merge (-> style :button) {:on-press (:on-press props)})
        ;^{:key "save-button-text"}[ui/text {:style (merge (-> style :text) (-> style :color :save))} "save" ]
        [ui/m-icons {:color (-> color :yes)
                     :key "refresh"
                     :name "refresh"
                     :size (-> common :icon :size)}]]
       [ui/text "Refresh"]])))