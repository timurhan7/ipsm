(ns co-act-mobile.components.relationPick
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.subs]
            [co-act-mobile.lib :as lib]
            [cljs.spec :as s]
            [co-act-mobile.ui :as ui]))

(def style {:view   {:padding (-> common :component :padding)
                     :flex-direction "column"
                     :align-items "stretch"
                     :justify-content "space-between"
                     ;:border-width 1
                     :flex 8}
            :touchable {:view   {:padding 0
                                 :flex 7
                                 :flex-direction "row"
                                 :align-items "center"
                                 :justify-content "space-between"}
                        :text   {:flex 1
                                 :font-size (-> common :text :small)}
                        :delete {:outer-style {:width (-> common :icon :width)}
                                 :inner-style {:color (:icon color) }}
                        }})

(defn relation-pick-component
  "Picker component of the source and target node for resouce relationship"
  [props]
  {:pre [(s/valid? boolean? (:source props))]}
  (fn [props]
    (let [model (subscribe [:nav/last-resource-definitions])
          id (subscribe [:nav/resource-id])
          entity-vec (get-in @model [:relationships @id :content])
          entity-ids (vec (map name (keys (get @model :nodes))))
          source? (:source props)
          picked  (if source?
                    (lib/find-source-id (get-in @model [:relationships @id :content]))
                    (lib/find-target-id (get-in @model [:relationships @id :content])))
          picked-name [:relationships @id :content]
          tag (if source? :SourceElement :TargetElement)
          ]
      (if (and (some? @model) (some? @id))
        [ui/view {:style {:flex 1}}
         (doall (for [entity-id entity-ids]
                  (let [picked? (= entity-id picked)
                        entity-name (get-in @model [:nodes (keyword entity-id) :attributes :name])]
                    ;(println "entity name" entity-name)
                    ^{:key (str (name tag) entity-id)}
                    [ui/view {:style (-> style :view)}
                     [ui/touchable-opacity {:activeOpacity (-> common :button :opacity)
                                            :on-press (fn[] (let []
                                                              (do
                                                                (if picked?
                                                                  (dispatch [:nav/cache [picked-name (vec (remove #(= tag (:tag %)) entity-vec))]])
                                                                  (if (some #(= tag (:tag %)) entity-vec)
                                                                    (dispatch [:nav/cache [picked-name (vec (map #(if (= tag (:tag %))
                                                                                                                   (assoc-in % [:attributes :ref] entity-id)
                                                                                                                   %) entity-vec))]])
                                                                    (dispatch [:nav/cache [picked-name (conj entity-vec {:tag tag, :attributes {:ref entity-id}, :content []})]]))
                                                                  ))))}
                      [ui/view {:style (-> style :touchable :view)}
                       [ui/button (merge (-> style :touchable :delete)
                                         {:on-press #()})
                        (if picked?
                          [ui/m-icons {:key (str "checkbox" entity-id)  :name "check-box" :size (-> common :icon :size)}]
                          [ui/m-icons {:key (str "checkbox" entity-id)  :name "check-box-outline-blank" :size (-> common :icon :size)}])]
                       [ui/text {:style (-> style :touchable :text)} entity-name]
                       ]]])))]
        ))))