(ns co-act-mobile.components.generatedLinks
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [cljs.spec :as s]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.handlers]
            [co-act-mobile.subs]
            [co-act-mobile.ui :as ui]))

(def style {:view   {:padding (-> common :component :padding)
                     :flex-direction "column"
                     :align-items "stretch"
                     :justify-content "space-between"
                     ;:border-width 1
                     :flex 8}
            :touchable {:view   {:padding 0
                                 :flex 1
                                 :flex-direction "row"
                                 :align-items "center"
                                 :justify-content "space-between"}
                        :text   {:flex 1
                                 :font-size (-> common :text :small)}
                        }})

(defn generatedLinks-component
  "A list of buttons which are not clickable, without interaction functions"
  [props]
  {:pre [(s/valid? not-empty (:entities props))
         (s/valid? keyword? (:group props))]}
  (fn [props]
    ;(println "entities are" (:entities props))
    (let [resource? (some? (:resource props))]
      [ui/view {:style {:flex 1}}
       (doall (for [entity (:entities props)]
                (let []
                  ^{:key (:id entity)}
                  [ui/view {:style (-> style :view)}
                   [ui/touchable-opacity {:activeOpacity (-> common :button :opacity)
                                          :on-press #(if (nil? (:disable props))
                                                      (if resource?
                                                        (dispatch [:nav/push ])
                                                        (dispatch [:nav/push-entity [(:group props) entity]])
                                                        )
                                                      )}
                    [ui/view {:style (-> style :touchable :view)}
                     [ui/text {:style (-> style :touchable :text)}
                      (if resource?
                        (:id entity)
                        (get-in entity [:interactive-entity-definition
                                        :identifiable-entity-definition
                                        :entity-identity
                                        :name]))]]]])))])))