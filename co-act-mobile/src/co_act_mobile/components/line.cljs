(ns co-act-mobile.components.line
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.subs]
            [cljs.spec :as s]
            [co-act-mobile.ui :as ui]))

(defn line-component
  "A simple line"
  [props]
  (let [margin (if (nil? (:margin props)) 5 (:margin props))]
    (fn [props]
      [ui/view {:style {:border-width 0.2
                        :border-color "grey"
                        :margin-top margin
                        :margin-bottom margin}}])))