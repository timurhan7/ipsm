(ns co-act-mobile.components.convert
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.subs]
            [cljs.spec :as s]
            [co-act-mobile.lib :as lib]
            [co-act-mobile.ui :as ui]))

(def style {:view   {:padding-top 5
                     :flex 3
                     :flex-direction "row"
                     :align-items "center"
                     :justify-content "space-between"}
            :button {:outer-style {:width (-> common :icon :width)
                                   :flex 1
                                   }
                     :inner-style {:color (:icon color)
                                   :text-align "center"
                                   :font-size (-> common :text :middle)}
                     :group-style {:border-width 0
                                   :border-radius 5
                                   :justifyContent "center"
                                   :margin 10}}
            :text {:fontFamily  ".HelveticaNeueInterface-MediumP4"
                   :font-size (-> common :text :small)
                   :margin 5
                   :fontWeight  "bold"
                   :textAlign   "center"}
            :icon {:font-size (-> common :text :small)
                   :margin 5
                   :fontWeight  "bold"
                   :textAlign   "center"}
            :string {:definition "Definition"
                     :instance "Instance"}
            })

(defn convert-component
  "Convert for strategy to convert to informal proces definitino or informal process instance"
  [props]
  (let [strings (subscribe [:strings])
        target-intention (:target-intention props)]
    (fn [prop]
      [ui/view {:style (-> style :view)}
       [ui/button (merge (-> style :button) {:on-press #(if (some? target-intention)
                                                         (dispatch [:data/convert-definition [target-intention]])
                                                         (lib/alert (:no-target-intention @strings))
                                                         )})
        ^{:key "save-button-text"}[ui/text {:style (merge (-> style :text) (-> style :color :save))} (-> style :string :definition) ]
        [ui/m-icons {:color (-> color :icon)
                     :key "assignment"
                     :name "assignment"
                     :size (-> common :icon :size)}]]
       [ui/button (merge (-> style :button) {:on-press #(if (some? target-intention)
                                                         (dispatch [:data/convert-instance [target-intention]])
                                                         (lib/alert (:no-target-intention @strings))
                                                         )})
        ^{:key "revert-button-text"}[ui/text {:style (merge (-> style :text) (-> style :color :revert))} (-> style :string :instance) ]
        [ui/m-icons {:color (-> color :icon)
                     :key "assignment-turned-in"
                     :name "assignment-turned-in"
                     :size (-> common :icon :size)}]]
       ])))