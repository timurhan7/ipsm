(ns co-act-mobile.components.posts
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.android.lib :as lib]
            [co-act-mobile.subs]
            [cljs.spec :as s]
            [co-act-mobile.ui :as ui]))

(def style {:total-view {:padding (-> common :component :padding)
                         :margin (-> common :component :margin)
                         :flex 1}
            :title {:font-size (-> common :text :small)
                    :color (:icon color)
                    :font-weight "bold"
                    :padding (-> common :component :padding)}
            :view {:elevation 3
                   :flex-direction  "column"
                   :align-items     "stretch"
                   :justify-content "space-between"
                   :margin-bottom 8
                   :background-color (-> color :background)
                   :borderBottomWidth 1
                   :borderBottomColor "black"
                   :flex            3}
            :text   {:flex 1
                     :text-align "center"
                     :font-size (-> common :text :small)}
            :user {:flex-direction "row"
                   :justify-content "space-between"
                   :align-items "center"
                   :padding-left 30
                   :padding-right 30}
            :user-name {:margin  5
                        :color (-> color :icon)}
            :user-edit {:flex-direction "row"}
            :content {:flex 1
                      :height 40
                      :padding-left 8
                      :padding-right 0
                      :justify-content "center"
                      :align-items "center"}
            :button {:outer-style {:width 85
                                   :flex 1}
                     :inner-style {:color (:icon color)
                                   :text-align "center"
                                   :font-size (-> common :text :middle)}
                     :group-style {:border-width 1
                                   :border-color "#EEEEEE"
                                   :border-radius 5
                                   :elevation 0
                                   :justifyContent "center"
                                   :padding (-> common :component :padding)
                                   :margin 2}}
            :input {:text-align "left"
                    :height 50
                    :padding-left 5
                    :padding-right 5
                    :font-size (-> common :text :small)}
            :add{ :flex 1
                 :elevation 3
                 :background-color (-> color :background)
                 :borderColor (-> color :border)
                 :height 100
                 :justify-content "center"
                 :flex-direction "column"
                 :align-items "center"}
            :add-icons {:flex-direction "row"
                        :align-items "flex-end"
                        :justify-content "flex-end"
                        }
            })

(defn post-component
  "A note component"
  [props]
  (let [editing (atom false)]
    (fn [props]
      (let [posts (:posts props)
            post (:post props)
            user (:user props)
            new-post (subscribe [:ui/new-post])
            value (:content post)
            id (:id post)
            username @(subscribe [:data/find-user-name (:user-id post)])]
        [ui/view {:style (-> style :view)}
         ;post title
         [ui/view {:style (-> style :user)}
          [ui/text {:style (-> style :user-name)} username]
          ;render if it is created by same user
          (if(= (:user-id post) (:id user))

            (if (true? @editing)
              ;render buttons when editing
              [ui/view {:style (-> style :user-edit)}
               [ui/button (merge (-> style :button) {:on-press (fn[]
                                                                 (reset! editing false)
                                                                 )})
                ^{:key (str id "save")}
                [ui/text {:style (-> style :text)} "Save" ]
                [ui/m-icons {:color (-> color :icon)
                             :key (str id "saveIcon")
                             :name "save"
                             :size (-> common :icon :size)}]]]


              ;render buttons when no editing
              [ui/view {:style (-> style :user-edit)}
               [ui/button (merge (-> style :button) {:on-press #(reset! editing true)})
                ^{:key (str id "edit")}
                [ui/text {:style (-> style :text)} "Edit" ]
                [ui/m-icons {:color (-> color :icon)
                             :key (str id "editIcon")
                             :name "mode-edit"
                             :size (-> common :icon :size)}]]
               [ui/button (merge (-> style :button) {:on-press (fn[]
                                                                 (dispatch [:nav/cache
                                                                            [:posts (dissoc posts (keyword
                                                                                                    id))]]))})
                ^{:key (str id "delete")}
                [ui/text {:style (-> style :text)} "Delete" ]
                [ui/m-icons {:color (-> color :icon)
                             :key (str id "deleteIcon")
                             :name "delete"
                             :size (-> common :icon :size)}]]])

            ;else render reply button

            [ui/view {:style (-> style :user-edit)}
             [ui/button (merge (-> style :button) {:on-press (fn[]
                                                               (println @new-post)
                                                               (reset! editing false)
                                                               (dispatch [:data/new-post (str "Reply " username ":")])
                                                               (println @new-post)
                                                               )})
              ^{:key (str id "reply")}
              [ui/text {:style (-> style :text)} "Reply" ]
              [ui/m-icons {:color (-> color :icon)
                           :key (str id "replayIcon")
                           :name "reply"
                           :size (-> common :icon :size)}]]]
            )]

         ;;;; post content
         [ui/view {:style (merge (-> style :content)
                                 (if (or
                                       (clojure.string/includes? (:content post) "--")
                                       (clojure.string/includes? (:content post) "$")
                                       )
                                   {:flex-direction "row"}
                                   {}
                                   ))}
          (if (clojure.string/includes? (:content post) "--")
            [ui/m-icons {:color (-> color :icon)
                         :key (str id "location-on")
                         :name "location-on"
                         :size (-> common :icon :size)}]
            )
          (if (clojure.string/includes? (:content post) "$")
            [ui/m-icons {:color (-> color :icon)
                         :key (str id "attach-money")
                         :name "attach-money"
                         :size (-> common :icon :size)}]
            )
          (if (true? @editing)
            [ui/input {:defaultValue value
                       :multiline true
                       :numberOfLine 2
                       :style (-> style :input)
                       :onChangeText (fn[text]
                                       ;(reset! value text)
                                       (dispatch [:nav/cache [:posts (assoc-in posts [
                                                                                      (keyword id) :content] text
                                                                               )]]))}]
            [ui/text (clojure.string/replace (:content post) "$" "--")])]]))))

(defn posts-component
  "Note list component"
  [props]
  {:pre []}
  (fn [props]
    (let [entity @(subscribe [:nav/current-entity])
          posts-vector (if (some? (:posts entity))
                         (vec (vals (:posts entity)))
                         [])
          value @(subscribe [:ui/new-post])
          posts (:posts entity)
          user @(subscribe [:user])]
      [ui/view {:style (-> style :total-view)}
       [ui/text {:style (-> style :title)} "Notes"]
       ;;;; render the post list
       (doall (for [post posts-vector]
                ^{:key (:id post)}
                [post-component {:post post
                                 :posts posts
                                 :user user}]))
       ;;;;render the post text input
       [ui/view {:style (-> style :add)}
        [ui/input {
                   :defaultValue ""
                   :value value
                   :multiline true
                   :placeholder "Add new note here."
                   :numberOfLine 3
                   :style (-> style :input)
                   :onChangeText (fn[text]
                                   (dispatch [:data/new-post text]))}]
        [ui/view {:style (-> style :add-icons)}
         [ui/button (merge (-> style :button)  {:on-press (fn[]
                                                            (lib/current-location #(dispatch [:data/new-post %])))})
          ^{:key (str (:id user) "location")}
          ;[ui/text {:style (-> style :text)} "Post" ]
          [ui/m-icons {:color (-> color :icon)
                       :key (str (:id user)"locationIcon")
                       :name "location-on"
                       :size (-> common :icon :size)}]]
         [ui/button (merge (-> style :button)  {:on-press #(dispatch [:api/bitcoin-price])})
          ^{:key (str (:id user) "attach-money")}
          ;[ui/text {:style (-> style :text)} "Post" ]
          [ui/m-icons {:color (-> color :icon)
                       :key (str (:id user)"attach-money")
                       :name "attach-money"
                       :size (-> common :icon :size)}]]
         ;[ui/button (merge (-> style :button) {:on-press #()})
         ; ^{:key (str (:id user) "camera")}
         ; ;[ui/text {:style (-> style :text)} "Post" ]
         ; [ui/m-icons {:color (-> color :icon)
         ;              :key (str (:id user)"cameraIcon")
         ;              :name "photo-camera"
         ;              :size (-> common :icon :size)}]]
         [ui/button (merge (-> style :button) {:on-press (fn[]
                                                           (let [post-id  (keyword (str (:id entity) (.now js/Date)))
                                                                 post-map {:id post-id
                                                                           :user-id (:id user)
                                                                           :content  value}]
                                                             (dispatch [:nav/cache
                                                                        [:posts (assoc posts post-id post-map)]])
                                                             (dispatch [:data/new-post ""])))})
          ^{:key (str (:id user) "send")}
          ;[ui/text {:style (-> style :text)} "Post" ]
          [ui/m-icons {:color (-> color :icon)
                       :key (str (:id user)"sendIcon")
                       :name "send"
                       :size (-> common :icon :size)}]]
         ]
        ]])))