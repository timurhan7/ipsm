(ns co-act-mobile.components.smallTitle
  (:require [reagent.core :as r :refer [atom]]
            [cljs.spec :as s]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.ui :as ui]))

(def style {:view {:padding (-> common :component :padding)}
            :text {:font-size (-> common :text :small)
                   :color (:icon color)
                   :font-weight "bold"}})

(defn small-title-component
  "A small title component"
  [text]
  {:pre [(s/valid? string? text)]}
  (let []
    (fn [text]
      [ui/view {:style (-> style :view)}
       [ui/text {:style (-> style :text)} text]])))