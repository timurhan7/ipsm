(ns co-act-mobile.components.resourceNamespace
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [cljs.spec :as s]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.handlers]
            [co-act-mobile.subs]
            [co-act-mobile.ui :as ui]))

;if specified the on-press property it is a link, else it is just a text display
;font-size is an another optional property, default is small
(def style {:view   {:padding (-> common :component :padding)
                     :flex 3
                     :flex-direction "row"
                     :align-items "center"
                     :justify-content "space-between"}
            :title  {:font-size (-> common :text :small)
                     :font-weight "bold"
                     :color (:icon color)
                     :flex 1}
            :text   {:flex 2
                     :text-align "center"
                     :font-size (-> common :text :small)}
            })

(defn resource-namespace-component
  "Display namespace according to the type of resource"
  [props]
  {:pre [(s/valid? boolean? (:node? props))
         ]}
  (let [node? (:node? props)
        entity (if node?
                 (subscribe [:nav/resource-node])
                 (subscribe [:nav/resource-relationship]))]
    (fn [props]
      (when (some? @entity)
        (let [entity-type (-> @entity :attributes :type
                              (clojure.string/replace "winery:" "")
                              (keyword))
              entity-definition (if node?
                                  (subscribe [:data/resource-definition [:node-types entity-type]])
                                  (subscribe [:data/resource-definition [:relationship-types entity-type]]))
              ]
          (println "entity type " entity-type "entity definition" @entity-definition)
          [ui/view {:style (-> style :view)}
           [ui/text {:style (-> style :title)} "Namespace"]
           (if-let [size (:font-size props)]
             [ui/text {:style (merge (-> style :text)
                                     {:font-size (-> common :text size)})}
              (:namespace @entity-definition)]
             [ui/text {:style (-> style :text)}  (:namespace @entity-definition)])]
          )))))