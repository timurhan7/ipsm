(ns co-act-mobile.components.extract
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [cljs.spec :as s]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.handlers]
            [co-act-mobile.subs]
            [co-act-mobile.ui :as ui]))

(def style {:container {:flex-direction "column"
                        :align-items "center"}
            :button {:outer-style {:width (-> common :icon :width)
                                   :margin-bottom (-> common :component :margin-compen)
                                   }
                     :inner-style {:color (:icon color)
                                   :text-align "center"
                                   :font-size (-> common :text :middle)}
                     :group-style {:border-width 1
                                   :border-radius 5
                                   :justifyContent "center"
                                   :margin 10}}})

(defn extract-component
  "Bar component : Extract button"
  []
  (let []
    (fn []
      [ui/view {:style (-> style :container)}
       [ui/button (merge (-> style :button) {:on-press #(dispatch [:data/extract])})
        ;^{:key "save-button-text"}[ui/text {:style (merge (-> style :text) (-> style :color :save))} "save" ]
        [ui/m-icons {:color (-> color :init)
                     :key "redo"
                     :name "redo"
                     :size (-> common :icon :size)}]]
       [ui/text "Extract"]])))