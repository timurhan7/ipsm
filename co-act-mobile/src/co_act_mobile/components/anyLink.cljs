(ns co-act-mobile.components.anyLink
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [cljs.spec :as s]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.handlers]
            [co-act-mobile.subs]
            [co-act-mobile.ui :as ui]))

(def style {:view   {:padding (-> common :component :padding)
                     :flex-direction "column"
                     :align-items "stretch"
                     :justify-content "space-between"
                     ;:border-width 1
                     :flex 8}
            :touchable {:view   {:padding 0
                                 :flex 7
                                 :flex-direction "row"
                                 :align-items "center"
                                 :justify-content "space-between"}
                        :text   {:flex 1
                                 :font-size (-> common :text :small)}
                        :delete {:outer-style {:width (-> common :icon :width)}
                                 :inner-style {:color (:icon color) }}
                        }})

(defn any-link-component
  "A text link could direct to one of any kind of entity"
  [props]
  {:pre [
         (s/valid? some? (:entity props))
         (s/valid? some? (:name props))]}
  (fn [props]
    (let [attribute-name (if(false? (vector? (:name props)))
                           (conj [] (:name props))
                           (:name props))
          target (get-in (:entity props) attribute-name)]
      ;(println "target is" target)
      (if (not-empty target)
        (let [target-group (target 0)
              entity (subscribe [:data/find-keyword target])
              display (if (some? (:name @entity))
                        :name
                        :definition)]
          ;(println "entity in any link is" @entity )
          [ui/view {:style {:flex 1}}
           [ui/view {:style (-> style :view)}
            [ui/touchable-opacity {:activeOpacity (-> common :button :opacity)
                                   :on-press #(dispatch [:nav/push-entity [target-group @entity]])}
             [ui/view {:style (-> style :touchable :view)}
              [ui/text {:style (-> style :touchable :text)} (get @entity display)]
              [ui/button (merge (-> style :touchable :delete)
                                {:on-press (fn[](do
                                                  (dispatch [:nav/cache [attribute-name []]])))})
               [ui/m-icons {:key (str "cancel" (:id @entity))  :name "cancel" :size (-> common :icon :size)}]]]]]])))))