(ns co-act-mobile.components.typePick
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.subs]
            [co-act-mobile.lib :as lib]
            [cljs.spec :as s]
            [co-act-mobile.ui :as ui]))

(def style {:view   {:padding (-> common :component :padding)
                     :flex-direction "column"
                     :align-items "stretch"
                     :justify-content "space-between"
                     ;:border-width 1
                     :flex 8}
            :touchable {:view   {:padding 0
                                 :flex 7
                                 :flex-direction "row"
                                 :align-items "center"
                                 :justify-content "space-between"}
                        :text   {:flex 1
                                 :font-size (-> common :text :small)}
                        :delete {:outer-style {:width (-> common :icon :width)}
                                 :inner-style {:color (:icon color) }}
                        }})

(defn type-pick-component
  "Type picker for resource nodes and relationships"
  [props]
  {:pre [(s/valid? boolean? (:node? props))]}
  (fn [props]
    (let [node? (:node? props)
          model (subscribe [:nav/last-resource-definitions])
          id (subscribe [:nav/resource-id])
          entities-map (if node?
                         (subscribe [:data/group :node-types])
                         (subscribe [:data/group :relationship-types]))
          entity-ids (map name (vec (keys @entities-map)))
          ;picked id
          picked  (if-let [winery-type (if node?
                                         (get-in @model [:nodes @id :attributes :type])
                                         (get-in @model [:relationships @id :attributes :type]))]
                    (clojure.string/replace winery-type "winery:" ""))
          picked-name (if node?
                        [:nodes @id :attributes :type]
                        [:relationships @id :attributes :type])
          ]
      (if (and (some? @model) (some? @id))
        [ui/view {:style {:flex 1}}
         (doall (for [entity-id entity-ids]
                  (let [picked?  (= entity-id picked)]
                    ^{:key (str "resource-" entity-id)}
                    [ui/view {:style (-> style :view)}
                     [ui/touchable-opacity {:activeOpacity (-> common :button :opacity)
                                            :on-press (fn[] (let []
                                                              (if (false? picked?)
                                                                (dispatch [:nav/cache [picked-name (str "winery:" entity-id)]])
                                                                )))}
                      [ui/view {:style (-> style :touchable :view)}
                       [ui/button (merge (-> style :touchable :delete)
                                         {:on-press (fn[] (let []
                                                            (if (false? picked?)
                                                              (dispatch [:nav/cache [picked-name (str "winery:" entity-id)]])
                                                              )))})
                        (if picked?
                          [ui/m-icons {:key (str "checkbox" entity-id)  :name "check-box" :size (-> common :icon :size)}]
                          [ui/m-icons {:key (str "checkbox" entity-id)  :name "check-box-outline-blank" :size (-> common :icon :size)}])]
                       [ui/text {:style (-> style :touchable :text)} entity-id]]]])))]))))