(ns co-act-mobile.style)

(def common {:app        {}
             :text       {:small  15
                          :middle 20
                          :big    25
                          :title  40}
             :icon       {:width 30
                          :size 20
                          :size-fix 15}
             :component  {
                          :padding 8
                          :padding-top 5
                          :padding-bottom 5
                          :padding-left 10
                          :padding-right 10
                          :margin-compen -8
                          :margin-bottom 40
                          :margin 4}
             :container  {}
             :button     {:opacity 0.2
                          :width 150}
             :icon-name {:add "add"
                    :delete "clear"}
             })

(def drawer {:width 250})

(def color {:icon "cornflowerblue"
            :border "grey"
            :background "#EEEEEE"
            :subscribe "black"
            :yes "darkblue"
            :no "coral"
            :init "goldenrod"
            :disable "grey"
            })