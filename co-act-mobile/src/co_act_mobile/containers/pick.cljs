(ns co-act-mobile.containers.pick
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.subs]
            [cljs.spec :as s]
            [co-act-mobile.ui :as ui]
            [co-act-mobile.components.index :as components]))

(def style {:container {:flex 1
                        :padding-left 10}
            :view   {:padding (-> common :component :padding)
                     :flex-direction "column"
                     :align-items "stretch"
                     :justify-content "space-between"
                     ;:border-width 1
                     :flex 8}
            :touchable {:view   {:padding 0
                                 :flex 7
                                 :flex-direction "row"
                                 :align-items "center"
                                 :justify-content "space-between"}
                        :text   {:flex 1
                                 :font-size (-> common :text :small)}
                        :button {:inner-style {:color (:icon color) }}}
            :buttons-view   {:padding 0
                             :flex 3
                             :flex-direction "row"
                             :align-items "center"
                             :justify-content "space-between"}
            :button {:outer-style {:width (-> common :icon :width)
                                   :flex 1}
                     :inner-style {:color (:icon color)
                                   :text-align "center"
                                   :font-size (-> common :text :middle)}
                     :group-style {:border-width 0
                                   :border-radius 5
                                   :justifyContent "center"
                                   :margin 10}}
            :text {:fontFamily  ".HelveticaNeueInterface-MediumP4"
                   :font-size (-> common :text :middle)
                   :margin 5
                   :fontWeight  "bold"
                   :textAlign   "center"}
            :group-title {:font-size (-> common :text :small)
                   :color (:icon color)}
            :icon {:font-size (-> common :text :middle)
                   :margin 5
                   :fontWeight  "bold"
                   :textAlign   "center"}})

(def filter-map {:link []
                 :target-strategy []})

(defn pick-container
  "A list for picking items"
  [props]
  (let [picked (atom false)
        all-data (subscribe [:data])
        pick-entity (subscribe [:nav/current-pick])
        require? (:require @pick-entity)
        group-data (vec (vals (get-in @all-data [(:group @pick-entity)])))
        filter-data (if (some? (:filter @pick-entity))
                      (filterv #(= (get % (:filter @pick-entity)) (get filter-map (:filter @pick-entity)))
                                   group-data)
                      group-data
                      )
        group-data (vec (vals (get-in @all-data [:groups])))
        ]
    ;(println "pick-entity is" @pick-entity)
    (fn [props]
      [ui/scroll {:style (-> style :container)
                  :contentContainerStyle {:align-items "stretch"}}
       ;here is the list
       [ui/view
        (doall (for [entity filter-data]
                 (let [picked (:picked-ids @pick-entity)
                       entity-id (:id entity)
                       picked? (if (:multi @pick-entity)
                                 (some #(= entity-id %) picked)
                                 (= entity-id picked))]
                   ;(println "picked id is" picked " and list is" picked? "entity id is " entity-id)
                   ^{:key entity-id}
                   [ui/view {:style (-> style :view)}
                    [ui/touchable-opacity {:activeOpacity (-> common :button :opacity)
                                           :on-press      (if (some? (:disable-redirect @pick-entity))
                                                            (fn[]
                                                              (let []
                                                              (do
                                                                (if picked?
                                                                  ;remove
                                                                  (if (nil? require?)
                                                                    ;only if this is not required.
                                                                    (if (true? (:multi @pick-entity))
                                                                      (dispatch [:nav/cache-pick {:picked-ids (remove #(= entity-id %) picked)}])
                                                                      (dispatch [:nav/cache-pick {:picked-ids nil}])
                                                                      ))

                                                                  ;add
                                                                  (if (true? (:multi @pick-entity))
                                                                    (dispatch [:nav/cache-pick {:picked-ids (conj picked entity-id)}])
                                                                    ;if it is single select
                                                                    (dispatch [:nav/cache-pick {:picked-ids entity-id}]))))))
                                                            #(dispatch [:nav/push-entity [(:group @pick-entity) entity]])
                                                            )
                                           }
                     [ui/view {:style (-> style :touchable :view)}
                      [ui/button (merge (-> style :touchable :button)
                                        {:on-press (fn[] (let []
                                                           (do
                                                             (if picked?
                                                               ;remove
                                                               (if (nil? require?)
                                                                 ;only if this is not required.
                                                                 (if (true? (:multi @pick-entity))
                                                                   (dispatch [:nav/cache-pick {:picked-ids (remove #(= entity-id %) picked)}])
                                                                   (dispatch [:nav/cache-pick {:picked-ids nil}])
                                                                   ))

                                                               ;add
                                                               (if (true? (:multi @pick-entity))
                                                                 (dispatch [:nav/cache-pick {:picked-ids (conj picked entity-id)}])
                                                                 ;if it is single select
                                                                 (dispatch [:nav/cache-pick {:picked-ids entity-id}]))))))})
                       (if picked?
                         [ui/m-icons {:key (str "checkbox" (:id entity))
                                      :name "check-box"
                                      :size (-> common :icon :size)}]
                         [ui/m-icons {:key (str "checkbox" (:id entity))
                                      :name "check-box-outline-blank"
                                      :size (-> common :icon :size)}])]
                      [ui/text {:style (-> style :touchable :text)} (get-in entity (:display-property @pick-entity)) ]
                      ]]
                    ])))]

       ;(if (and (= (:group @pick-entity) :participants) (not= (:picked-name @pick-entity) [:members]))
       ;
       ;  [ui/view {:style {:flex 1
       ;                    :align-items "stretch"
       ;                    }}
       ;   [ui/view {:style {:border-width 1
       ;                     :border-color "grey"
       ;                     :margin-top 5
       ;                     :margin-bottom 5}}]
       ;
       ;   [ui/text {:style (-> style :group-title)} "Groups"]
       ;   [ui/view
       ;    (doall (for [entity group-data]
       ;             (let [picked (:picked-ids @pick-entity)
       ;                   entity-id (:id entity)
       ;                   picked? (if (:multi @pick-entity)
       ;                             (some #(= entity-id %) picked)
       ;                             (= entity-id picked))]
       ;               ;(println "picked id is" picked " and list is" picked? "entity id is " entity-id)
       ;               ^{:key entity-id}
       ;               [ui/view {:style (-> style :view)}
       ;                [ui/touchable-opacity {:activeOpacity (-> common :button :opacity)
       ;                                       :on-press #(dispatch [:nav/push-entity [(:group @pick-entity) entity]])}
       ;                 [ui/view {:style (-> style :touchable :view)}
       ;                  [ui/button (merge (-> style :touchable :button)
       ;                                    {:on-press (fn[] (let []
       ;                                                       (do
       ;                                                         (if picked?
       ;                                                           ;remove
       ;                                                           (if (true? (:multi @pick-entity))
       ;                                                             (dispatch [:nav/cache-pick {:picked-ids (remove #(= entity-id %) picked)}])
       ;                                                             (dispatch [:nav/cache-pick {:picked-ids nil}])
       ;                                                             )
       ;
       ;                                                           ;add
       ;                                                           (if (true? (:multi @pick-entity))
       ;                                                             (dispatch [:nav/cache-pick {:picked-ids (conj picked entity-id)}])
       ;                                                             ;if it is single select
       ;                                                             (dispatch [:nav/cache-pick {:picked-ids entity-id}]))))))})
       ;                   (if picked?
       ;                     [ui/m-icons {:key (str "checkbox" (:id entity))
       ;                                  :name "check-box"
       ;                                  :size (-> common :icon :size)}]
       ;                     [ui/m-icons {:key (str "checkbox" (:id entity))
       ;                                  :name "check-box-outline-blank"
       ;                                  :size (-> common :icon :size)}])]
       ;                  [ui/text {:style (-> style :touchable :text)} (get entity (:display-property @pick-entity)) ]
       ;                  ]]
       ;                ])))]
       ;  ]
       ;  )


       ;here is the save button
       [ui/view {:style (-> style :buttons-view)}
        [ui/button (merge (-> style :button) {:on-press #(dispatch [:nav/pick-save
                                                                      [(:picked-name @pick-entity) (:picked-ids @pick-entity)]
                                                                    ])})
         ^{:key "confirm-button-text"}[ui/text {:style (merge (-> style :text) (-> style :color :save))} "confirm" ]
         [ui/m-icons {:color (-> color :yes)
                      :key "save"
                      :name "save"
                      :size (-> common :icon :size)}]]
        [ui/button (merge (-> style :button) {:on-press #(dispatch [:nav/pop])})
         ^{:key "cancel-button-text"}[ui/text {:style (merge (-> style :text) (-> style :color :revert))} "cancel" ]
         [ui/m-icons {:color (-> color :no)
                      :key "cancel"
                      :name "cancel"
                      :size (-> common :icon :size)}]]
        ]
       ])))