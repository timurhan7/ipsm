(ns co-act-mobile.containers.index
  (:require
            [co-act-mobile.containers.list :refer [list-container]]
            [co-act-mobile.containers.pick :refer [pick-container]]
            [co-act-mobile.containers.resources :refer [resources-container]]
            [co-act-mobile.containers.node :refer [node-container]]
            [co-act-mobile.containers.relationship :refer [relationship-container]]
            [co-act-mobile.containers.addNode :refer [add-node-container]]
            [co-act-mobile.containers.addRelationship :refer [add-relationship-container]]
            [co-act-mobile.containers.recommendation :refer [recommendation-container]]
            [co-act-mobile.ui :as ui]))

(def listView list-container)
(def pick pick-container)
(def resources resources-container)
(def node node-container)
(def relationship relationship-container)
(def add-node add-node-container)
(def add-relationship add-relationship-container)
(def recommendation recommendation-container)
