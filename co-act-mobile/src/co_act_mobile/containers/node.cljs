(ns co-act-mobile.containers.node
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.android.ui :as ui]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.lib :as lib]
            [co-act-mobile.android.components.index :as components]
            ))

(def style {:view {:padding (-> common :component :padding)}
            :text {:font-size (-> common :text :small)
                   :color (:icon color)
                   :font-weight "bold"}})

(defn node-container
  "Resource node configuration screen"
  [props]
  (let []
    (fn [props]
      (let [model (subscribe [:nav/last-resource-definitions])
            id (subscribe [:nav/resource-id])]
        [ui/scroll {:style (-> style :view)}
         [components/line]
         [components/type-pick {:node? true}]

         [components/line]
         [components/resource-namespace {:node? true}]

         [components/line]
         [components/small-title "Name"]
         [components/text-input {:target-property [:nodes @id :attributes :name]
                                 :size :small
                                 :entity @model}]

         [components/line]
         [components/small-title "Winery:X"]
         [components/text-input {:target-property [:nodes @id :attributes :winery:x]
                                 :size :small
                                 :entity @model}]

         [components/line]
         [components/small-title "Winery:Y"]
         [components/text-input {:target-property [:nodes @id :attributes :winery:y]
                                 :size :small
                                 :entity @model}]
         ]))))