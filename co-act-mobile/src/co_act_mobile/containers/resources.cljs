(ns co-act-mobile.containers.resources
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.android.ui :as ui]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.lib :as lib]
            [co-act-mobile.android.components.index :as components]
            ))

(def style {:view {:padding (-> common :component :padding)}
            :text {:font-size (-> common :text :small)
                   :color (:icon color)
                   :font-weight "bold"}})

(defn resources-container
  "Resource model configuration screen"
  [props]
  (let [init-model (subscribe [:nav/resource-data])
        requesting (subscribe [:nav/requesting])]
    ;fetch resource on first load
    (if (and
          (some? @init-model)
          (nil? (:definitions @init-model)))
      (dispatch [:api/fetch-resource-service @init-model]))
    (fn [props & children]
      (when (some? @init-model)
        ;(println "init model " @init-model)
        (let [current-model (subscribe [:nav/resource-definitions])
              correlation (or (-> @current-model :attributes :correlation-coefficient)
                              1)]
          [ui/view {:flex 1
                    :flex-direction "column"
                    :justify-content "center"}
           [ui/scroll {:margin-bottom (-> common :component :margin-bottom)
                       :showsVerticalScrollIndicator false}
            (if (some? @current-model)
              [ui/view
               [components/small-title "Name"]
               [components/text-input {:target-property [:attributes :name]
                                       :size            :small
                                       :color           (lib/judge-color correlation)
                                       :entity          @current-model}]
               [components/line]
               [components/single-link {:title        "Target Namespace"
                                        :display-text (-> @current-model :attributes :target-namespace)
                                        :font-size    :small
                                        ;:on-press #(dispatch)
                                        }]])
            [components/line]
            [components/single-link {:title        "Correlation coefficient"
                                     :display-text (str correlation)
                                     :font-size    :small
                                     }]
            [components/line]
            [components/add-button {:text "Resource model element"
                                    :on-press #(dispatch [:resource/add-node])}]
            [components/resource-nodes]
            [components/line]
            [components/add-button {:text "Relationships model element"
                                    :on-press #(dispatch [:resource/add-relationship])}]
            [components/resource-relationships]]
           [components/bar
            [components/save {:on-press #(dispatch [:data/save-resource (:id @init-model)])}]
            [components/revert {:on-press #(dispatch [:data/revert-resource (:id @init-model)])}]
            [components/refresh {:on-press #(dispatch [:api/fetch-resource-service @init-model])}]]])))))