(ns co-act-mobile.containers.list
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.android.ui :as ui]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.lib :as lib]
            [co-act-mobile.components.index :as components]))

(def style {:view {:flex-direction "column"}
            :entity {:border-width 1
                     :border-radius 5
                     :border-color (-> color :border)
                     :padding 5
                     :elevation 2
                     :margin 5}})

(defn list-container
  "List page"
  []
  (let [current (subscribe [:nav/current])
        addable-list (subscribe [:static/addable-list])
        ;current (last children)
        all-data (subscribe [:data])]
    (fn []
      [ui/view {:style {:flex 1}}
       [ui/scroll {:flexDirection  "row"
                   :style (-> style :view)
                   :showsVerticalScrollIndicator false
                   :contentContainerStyle {:align-items "stretch"}
                   }
        ; get the entities list
        (doall (for [entity  (vec (vals (get-in @all-data [(:key @current)])))]
                 ^{:key (:id entity)} [ui/touchable-highlight {:on-press #(dispatch
                                                                           [:nav/push-entity [(:key @current) entity]]
                                                                           )
                                                               :style (-> style :entity)}
                                       ; get the entity group to render the page
                                       ; the key of the list page is the group of entity page
                                       [ui/view [(get components/entities (:key @current)) entity]]]))]
       (if (some #(= (:key @current) %) @addable-list) [components/float-button])])))

