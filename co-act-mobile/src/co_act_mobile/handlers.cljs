(ns co-act-mobile.handlers
  (:require
    [re-frame.core :refer [reg-event-db  after subscribe dispatch]]
    [cljs.spec :as s]
    [clojure.string :as string]
    [co-act-mobile.db :refer [app-db]]
    [co-act-mobile.api :as api]
    [co-act-mobile.android.lib :as lib]
    [co-act-mobile.android.ui :as ui]))

;;;; -- Helpers ------------------------------------------------------------

(defn dec-to-zero
  "Same as dec if not zero"
  [arg]
  (if (< 0 arg)
    (dec arg)
    arg))

(defn refine-title
  "Refine the title according to the keyword"
  [keyword]
  (-> keyword
      (name)
      (string/replace "-" " ")
      (string/capitalize)))

(defn remove-nils
  "remove pairs of key-value that has nil value from a (possibly nested) map. also transform map to nil if all of its value are nil"
  [nm]
  (clojure.walk/postwalk
    (fn [el]
      (if (map? el)
        (let [m (into {} (remove (comp nil? second) el))]
          (when (seq m)
            m))
        el))
    nm))

;;;; -- Middleware ------------------------------------------------------------
;;;;
;;;; See https://github.com/Day8/re-frame/wiki/Using-Handler-Middleware
;;;;
(defn check-and-throw
  "throw an exception if db doesn't match the schema."
  [a-schema db]
  true)

(def validate-schema-mw
  (if goog.DEBUG
    (after (partial check-and-throw :schema-key))
    []))

;;;; -- Handlers --------------------------------------------------------------

(reg-event-db
  :initialize-db
  validate-schema-mw
  (fn [_ _]
    app-db))

;;;; -- Nav Handlers -----------------------------------------------------------
(reg-event-db
  :nav/push
  validate-schema-mw
  (fn [db [_ value]]
    ;(println "push data is" value)
    (-> db
        (update-in [:nav :index] inc)
        (update-in [:nav :children] #(conj % value)))))

;;;; push into nav, create new page
(reg-event-db
  :nav/push-entity
  validate-schema-mw
  (fn [db [_ [group entity]]]
    (let [;two way binding keys
          nowMil (.now js/Date)
          push-entity {:key (keyword (str (:id entity) nowMil))
                       :group group
                       :title (get-in db [:title-map group])
                       ;merge the keys into entity temporarorily
                       :entity-data entity}
          ]
      (-> db
          (update-in [:nav :index] inc)
          (update-in [:nav :children] #(conj % push-entity))))))

;;;; save current state as snapshot
(reg-event-db
  :nav/push-snapshot
  validate-schema-mw
  (fn [db [_ [group entity]]]
    (let [;two way binding keys
          nowMil (.now js/Date)
          push-entity {:key (keyword (str (:id entity) nowMil))
                       :group group
                       :snapshot true
                       :title (get-in db [:title-map group])
                       ;merge the keys into entity temporarorily
                       :entity-data entity}
          ]
      (-> db
          (update-in [:nav :index] inc)
          (update-in [:nav :children] #(conj % push-entity))))))

;;;; pop out from nav, go back function
(reg-event-db
  :nav/pop
  validate-schema-mw
  (fn [db [_ _]]
    ;(.log js/console "previous nav is" (clj->js (get-in db [:nav])))
    (-> db
        (update-in [:nav :index] dec-to-zero)
        (update-in [:nav :children] pop))))

;;;; find the data and push it into nav, value is id pair
(reg-event-db
  :nav/find-push
  validate-schema-mw
  (fn [db [_ value]]
    (let [data (get-in db [:data (:group value)])
          found (some #(when (= (:id value) (:id %))
                        %) data)]
      ;(.log js/console "previous nav is" (clj->js (get-in db [:nav])))
      (-> db
          (update-in [:nav :index] inc)
          (update-in [:nav :children] #(conj % {:key (:id value)
                                                :group (:group value)
                                                :title (:title found)
                                                :entity-data found}))))))


;;;; save the state to cache when user edit the entity
(reg-event-db
  :nav/cache
  validate-schema-mw
  (fn [db [_ [path value]]]
    (let [index (get-in db [:nav :index])]
      (cond
        ;in handling resource model
        (= :resources (get-in db [:nav :children index :group]))
        (-> db
            (assoc-in (into [:nav :children index :resource-data :definitions] path) value)
            (update-in [:nav :children index 1 :resource-data :definitions] remove-nils))
        (some? (get-in db [:nav :children index :resource-id]))
        ;cache
        (-> db
            (assoc-in (into [:nav :children (- index 1) :resource-data :definitions] path) value)
            (update-in [:nav :children (- index 1) :resource-data :definitions] remove-nils))
        ;in handling normal entity
        (some? (get-in db [:nav :children index :entity-data]))
        (cond-> db
                (vector? path)(assoc-in (into [:nav :children index :entity-data] path) value)
                (keyword? path)(assoc-in (conj [:nav :children index :entity-data] path) value)
                true (update-in [:nav :children index :entity-data] remove-nils)
                )
        ;otherwise do nothing
        :else db))))

;;;; save the state to pick cache when user edit the pick-data
(reg-event-db
  :nav/cache-pick
  validate-schema-mw
  (fn [db [_ value]]
    (let [index (get-in db [:nav :index])]
      (-> db
          (update-in [:nav :children index :pick-data] merge value)))))

;;;; save the state to entity cache when user edit the pick list
(reg-event-db
  :nav/pick-save
  validate-schema-mw
  (fn [db [_ [path value]]]
    (let [index (- (get-in db [:nav :index]) 1)]
      ;(println "index is" index value  "save value is " (remove-nils value))
      (-> db
          (assoc-in (into [:nav :children index :entity-data] path) value)
          (update-in [:nav :children index :entity-data] remove-nils)
          (update-in [:nav :index] dec-to-zero)
          (update-in [:nav :children] pop)))))


;;;; save the state to cache when user edit the entity
(reg-event-db
  :nav/cache-add
  validate-schema-mw
  (fn [db [_ value]]
    (let [index (get-in db [:nav :index])]
      ;(println "index is" index value  (get-in db [:nav :children index :entity-data]))
      (-> db
          (update-in [:nav :children index :entity-data] merge value)))))

;;;; redirect to other list, clear the nav stack
(reg-event-db
  :nav/direct
  validate-schema-mw
  (fn [db [_ value]]
    ;(.log js/console "previous nav is" (clj->js (get-in db [:nav])))
    ;(println "nav is " (get-in db [:nav]))
    (-> db
        (assoc-in [:nav :index] 0)
        (assoc-in [:nav :children] (vector {:group :list
                                            :key value
                                            :title (refine-title value)})))))

;;;; go back home page and clear stack
(reg-event-db
  :nav/home
  validate-schema-mw
  (fn [db [_ _]]
    (-> db
        (assoc-in [:nav :index] 0)
        (assoc-in [:nav :children] (vector (get-in db [:nav :children 0]))))))

;;;; -- Drawer Handlers -----------------------------------------------------------

;;;; pull the drawer
(reg-event-db
  :drawer/pull
  validate-schema-mw
  (fn [db [_ to-open]]
    (let [opened (get-in db [:drawer :opened])
          duration (get-in db [:drawer :duration])
          animate-value (get-in db [:drawer :animate-value])
          target-value (if (true? opened)
                         (get-in db [:drawer :close-value])
                         (get-in db [:drawer :open-value]))
          animate-value-drawer (get-in db [:drawer :animate-value-drawer])
          target-value-drawer (if (true? opened)
                                (get-in db [:drawer :close-value-drawer])
                                (get-in db [:drawer :open-value-drawer]))]

      (when (or
              ;toggle if no argument passed
              (nil? to-open)
              ;if the drawer is already open
              (not= to-open opened))
        (->
          ;;;; set the animation properties and start it off
          (.timing ui/animated animate-value #js {:toValue target-value :duration duration})
          (.start))
        (->
          ;;;; set the animation properties and start it off
          (.timing ui/animated animate-value-drawer #js {:toValue target-value-drawer :duration duration})
          (.start))
        )
      (-> db
          (assoc-in [:drawer :opened] (if (nil? to-open)
                                        (not opened)
                                        to-open
                                        )))
      )))

;;;; init the drawer in core
(reg-event-db
  :drawer/init
  validate-schema-mw
  (fn [db [_ animate-value]]
    (-> db
        (assoc-in [:drawer :animate-value] animate-value)
        )
    ))

;;;; init the drawer in drawer.cljs
(reg-event-db
  :drawer/init-drawer
  validate-schema-mw
  (fn [db [_ animate-value]]
    (-> db
        (assoc-in [:drawer :animate-value-drawer] animate-value)
        )
    ))

;;;; -- User Handlers -----------------------------------------------------------

;;;; subscribe a entity
(reg-event-db
  :user/subscribe
  validate-schema-mw
  (fn [db [_ [group value]]]

    (let [id (:id value)]
      ;(println "in subscribe " group id)
      (-> db
          (assoc-in [:user :subscribe (keyword id)] {:id id
                                                     :group group})))))

;;;; unsubscribe a entity
(reg-event-db
  :user/unsubscribe
  validate-schema-mw
  (fn [db [_ id]]
    (-> db
        (update-in [:user :subscribe] dissoc (keyword id))
        )))

;;;; -- Data Handlers -----------------------------------------------------------

;;;; save the state to cache when user edit the entity
(reg-event-db
  :data/save
  validate-schema-mw
  (fn [db [_ _]]
    (let [index (get-in db [:nav :index])
          group (get-in db [:nav :children index :group])
          data (get-in db [:nav :children index :entity-data])
          display-name (or (get-in data [:interactive-entity-definition
                                         :identifiable-entity-definition
                                         :entity-identity
                                         :name])
                           (get-in data [:identifiable-entity-definition
                                         :entity-identity
                                         :name])
                           )
          data-id (keyword (:id data))
          origin-data (get-in db [:data group data-id])
          interactions (get-in db [:data :interactions])
          new-id (-> (.now js/Date)
                     (str "interactions"))
          new-interactions (merge interactions
                                  {(keyword new-id) {:source (get-in db [:user :username])
                                                     :target (:id data)
                                                     :group  group
                                                     :id     new-id
                                                     :action (if (some? (or (get-in data [:interactive-entity-definition
                                                                                      :identifiable-entity-definition
                                                                                      :entity-identity
                                                                                      :name])
                                                                            (get-in data [:identifiable-entity-definition
                                                                                          :entity-identity
                                                                                          :name])))
                                                               (str "Edit " (name group))
                                                               (str "Edit " (name group))
                                                               )
                                                     :snapshot-entity data
                                                     ;:content "I add this because there is one incubator just contacted us."
                                                     :time   (.now js/Date)
                                                     }})
          strategy? (true? (= group :strategy-definitions))
          origin-target-id (if strategy?
                             (get-in db [:data :strategy-definitions (keyword (:id data)) :target-intention]))
          subscribe? (subscribe [:user/subscribe?])
          ]
      (if @subscribe? (.localNotification ui/notification (clj->js {:title  (str display-name " Updated.")
                                                                   :ticker "My Notification Ticker" ; (optional)
                                                                   :autoCancel false ; (optional) default: true
                                                                   :number 10
                                                                   :color "green"
                                                                   :bigText (str  (get-in db [:user :username])
                                                                                  " updated "
                                                                                  (refine-title group)
                                                                                  " : "
                                                                                  display-name)
                                                                   ;:subText ""
                                                                   :message "Co-Act-Mobile"})))
      ;(println "new save is" namespace-removed? namespace-set? namespace-changed?[group (keyword (:id data))])
      (cond-> db
              (and strategy? (some? origin-target-id)) (update-in [:data :intention-definitions (keyword  origin-target-id) :achieving-strategies]
                                                                  #(filterv (fn[id-string] (not= (:id data) id-string)) %))
              (and strategy? (some? (:target-intention data))) (update-in [:data :intention-definitions (keyword (:target-intention data)) :achieving-strategies]
                                                                          into (:id data))
              true (assoc-in [:data :interactions] new-interactions)
              true (assoc-in [:data group data-id]  data)
              ))))

;;;; save the state to cache when user edit the instance
(reg-event-db
  :data/save-instance
  validate-schema-mw
  (fn [db [_ _]]
    (let [index (get-in db [:nav :index])
          group (get-in db [:nav :children index :group])
          data (get-in db [:nav :children index :entity-data])
          display-name (get-in data [:identifiable-entity-definition
                                         :entity-identity
                                         :name])
          data-id (keyword (:id data))
          source-id (keyword (:source-model data))
          source-group (subscribe [:static/target-definition])
          interactions (get-in db [:data :interactions])
          new-id (-> (.now js/Date)
                     (str "interactions"))
          new-interactions (merge interactions
                                  {(keyword new-id) {:source (get-in db [:user :username])
                                                     :target (:id data)
                                                     :group  group
                                                     :id     new-id
                                                     :action (if (some? (get-in data [:identifiable-entity-definition
                                                                                      :entity-identity
                                                                                      :name]))
                                                               (str "Edit " (name group))
                                                               (str "Edit " (name group))
                                                               )
                                                     :snapshot-entity data
                                                     ;:content "I add this because there is one incubator just contacted us."
                                                     :time   (.now js/Date)
                                                     }})
          subscribe? (subscribe [:user/subscribe?])
          ]
      (if @subscribe? (.localNotification ui/notification (clj->js {:title  (str display-name " Updated.")
                                                                    :ticker "My Notification Ticker" ; (optional)
                                                                    :autoCancel false ; (optional) default: true
                                                                    :number 10
                                                                    :color "green"
                                                                    :bigText (str  (get-in db [:user :username])
                                                                                   " updated "
                                                                                   (refine-title group)
                                                                                   " : "
                                                                                   display-name)
                                                                    ;:subText ""
                                                                    :message "Co-Act-Mobile"})))
      (println "new save is" [:data @source-group source-id :instances data-id] data)
      (-> db
              (assoc-in [:data :interactions] new-interactions)
              (assoc-in [:data @source-group source-id :instances data-id] data)
              ))))

;;;; save the resource into database and remotely
(reg-event-db
  :data/save-resource
  validate-schema-mw
  (fn [db [_ id]]
    {:pre [(s/valid? string? id)]}
    (let [index (get-in db [:nav :index])
          current-data (get-in db [:nav :children index :resource-data :definitions])]
      (-> db
          (assoc-in [:data :service-templates (keyword id) :definitions] current-data)))))

;;;; revert the local modification
(reg-event-db
  :data/revert
  validate-schema-mw
  (fn [db [_ _]]
    (let [index (get-in db [:nav :index])
          group (get-in db [:nav :children index :group])
          data (get-in db [:nav :children index :entity-data])
          data-id (keyword (:id data))
          origin-data (get-in db [:data group data-id])]
      (-> db
          (assoc-in [:nav :children index :entity-data] origin-data)))))

;;;; revert the local modification for instance
(reg-event-db
  :data/revert-instance
  validate-schema-mw
  (fn [db [_ _]]
    (let [index (get-in db [:nav :index])
          group (get-in db [:nav :children index :group])
          data (get-in db [:nav :children index :entity-data])
          data-id (keyword (:id data))
          source-id (keyword (:source-model data))
          source-group (subscribe [:static/target-definition])
          origin-data (get-in db [:data @source-group source-id :instances data-id])]
      (println "origin-data" origin-data "srouce-group" [:data @source-group source-id :instances data-id])
      (-> db
          (assoc-in [:nav :children index :entity-data] origin-data)))))

;;;; revert the change on the current resource
(reg-event-db
  :data/revert-resource
  validate-schema-mw
  (fn [db [_ id]]
    {:pre [(s/valid? string? id)]}
    (let [index (get-in db [:nav :index])
          origin-data (get-in db [:data :service-templates (keyword id) :definitions])]
      (-> db
          (assoc-in [:nav :children index :resource-data :definitions] origin-data)))))

;;;; convert a strategy definition to a informal process definition
(reg-event-db
  :data/convert-definition
  validate-schema-mw
  (fn [db [_ [intention]]]
    (let [index (get-in db [:nav :index])
          strategy (get-in db [:nav :children index :entity-data])
          get-resource (fn [resources capability-id]
                         (let [capability (get-in db [:data :capability-definitions (keyword capability-id)])]
                           (into resources (:desired-resources capability))))
          resources (vec (distinct (reduce get-resource [] (:capability-definitions strategy))))
          nowMil (.now js/Date)
          new-definition-id (str nowMil "process")
          new-interaction-id (str nowMil "interaction")
          middle-definition {:key (keyword new-definition-id)
                           :group :informal-process-definitions
                           :title "Informal Process Definition"
                           :entity-data {:state 0
                                         :target-intention (:id intention)
                                         :interactive-entity-definition (:interactive-entity-definition strategy)
                                         :id new-definition-id
                                         :model (:id strategy)
                                         :resources resources
                                         :author (get-in db [:user :username])}}
          push-definition (update-in middle-definition [:entity-data
                                                        :interactive-entity-definition
                                                        :identifiable-entity-definition
                                                        :entity-identity
                                                        :name] #(str % " process"))
          new-interaction  {:source (get-in db [:user :username])
                            :target new-definition-id
                            :group :informal-process-definitions
                            :id    new-interaction-id
                            :action (str "convert strategy " " to informal process definition")
                            :time nowMil
                            :snapshot-entity (:entity-data push-definition)
                            }
          ]
      ;(println (update-in db [:data :informal-process-definitions] #(merge % {(keyword new-id) push-definition})))
      (-> db
          (update-in [:data :interactions] #(merge % {(keyword new-interaction-id) new-interaction}))
          (update-in [:data :informal-process-definitions] #(merge % {(keyword new-definition-id) (:entity-data push-definition)}))
          (update-in [:nav :index] inc)
          (update-in [:nav :children] #(conj % push-definition))))))

;;;; convert a strategy defintion to an informal process instance and it corresponding definition
(reg-event-db
  :data/convert-instance
  validate-schema-mw
  (fn [db [_ [intention]]]
    (let [index (get-in db [:nav :index])
          strategy (get-in db [:nav :children index :entity-data])
          get-resource (fn [resources capability-id]
                         (let [capability (get-in db [:data :capability-definitions (keyword capability-id)])]
                           (into resources (:desired-resources capability))))
          resources (vec (distinct (reduce get-resource [] (:capability-definitions strategy))))
          nowMil (.now js/Date)
          new-definition-id (str nowMil "process")
          new-instance-id (str nowMil "instance")
          new-interaction-id (str nowMil "interaction")
          now (js/Date. nowMil)
          date-vec (vector (+ (.getYear now) 1900) (+ (.getMonth now) 1) (.getDate now))
          middle-definition {:state 0
                             :interactive-entity-definition (:interactive-entity-definition strategy)
                              :id new-definition-id
                              :target-intention (:id intention)
                              :model (:id strategy)
                              :resources resources
                              :author (get-in db [:user :username])}
          process-definition  (update-in middle-definition [:interactive-entity-definition
                                                            :identifiable-entity-definition
                                                            :entity-identity
                                                            :name] #(str "convert from " %))
          middle-instance {:id new-instance-id
                           :identifiable-entity-definition (:interactive-entity-definition strategy)
                            :start-date date-vec
                            :end-date date-vec
                            :instance-state-id  0
                            :source-model new-definition-id
                            :instance-uri (str "winery/informal-process-instances/" new-instance-id)
                            :model new-definition-id
                            :author (get-in db [:user :username])
                            :posts {}
                            }
          process-instance  (update-in middle-instance [:identifiable-entity-definition
                                                        :entity-identity
                                                        :name] #(str "convert from " %))
          push-definition {:key (keyword new-instance-id)
                           :group :informal-process-instances
                           :title "Informal Process Instance"
                           :entity-data process-instance}
          new-interaction  {:source (get-in db [:user :username])
                            :target new-instance-id
                            :group :informal-process-instances
                            :id    new-interaction-id
                            :action (str "convert strategy " " to informal process instance")
                            :time nowMil
                            :snapshot-entity process-instance
                            }]
      ;(println (update-in db [:data :informal-process-definitions] #(merge % {(keyword new-id) push-definition})))
      (-> db
          (update-in [:data :interactions] #(merge % {(keyword new-interaction-id) new-interaction}))
          (update-in [:data :informal-process-definitions] #(merge % {(keyword new-definition-id) process-definition}))
          (update-in [:data :informal-process-instances] #(merge % {(keyword new-instance-id) process-instance}))
          (update-in [:nav :index] inc)
          (update-in [:nav :children] #(conj % push-definition))))))

;;;; instaitate a definition
(reg-event-db
  :data/instantiate
  validate-schema-mw
  (fn [db [_ []]]
    (let [index (get-in db [:nav :index])
          current-entity (get-in db [:nav :children index :entity-data])
          current-group (get-in db [:nav :children index :group])
          target-group (get-in db [:target-instance-group current-group]) ;keyword
          nowMil (.now js/Date)
          new-instance-id (str nowMil (name current-group) "instance")
          new-interaction-id (str nowMil "interaction")
          now (js/Date. nowMil)
          date-vec (vector (+ (.getYear now) 1900) (+ (.getMonth now) 1) (.getDate now))
          middle-instance {:id new-instance-id
                           :identifiable-entity-definition (get-in current-entity [:interactive-entity-definition
                                                                                   :identifiable-entity-definition])
                           :start-date date-vec
                           :end-date date-vec
                           :instance-state-id  0
                           :instance-uri (str "winry/" (name target-group) "/" new-instance-id)
                           :source-model (:id current-entity)
                           :author (get-in db [:user :username])
                           :posts {}
                           }
          entity-instance (update-in middle-instance [:identifiable-entity-definition
                                                      :entity-identity
                                                      :name] #(str % " Instantiation"))
          push-instance {:key (keyword new-instance-id)
                         :group target-group
                         :title (refine-title target-group)
                         :entity-data entity-instance}
          new-interaction  {:source (get-in db [:user :username])
                            :target new-instance-id
                            :group target-group
                            :id    new-interaction-id
                            :action (str "initate"  (name current-group) " to instance")
                            :time nowMil
                            :snapshot-entity entity-instance
                            }]
      (println "push entity is" entity-instance)
      (-> db
          ;update cached definition
          (update-in [:nav :children index :entity-data :instances] assoc (keyword new-instance-id) entity-instance)
          ;update database
          (update-in [:data current-group (keyword (:id current-entity)) :instances] assoc (keyword new-instance-id) entity-instance)
          (update-in [:data :interactions] #(merge % {(keyword new-interaction-id) new-interaction}))
          (update-in [:nav :index] inc)
          (update-in [:nav :children] #(conj % push-instance))))))

;;;; extract a instance to a definition
(reg-event-db
  :data/extract
  validate-schema-mw
  (fn [db [_ []]]
    (let [index (get-in db [:nav :index])
          current-instance (get-in db [:nav :children index :entity-data])
          current-group (get-in db [:nav :children index :group])
          target-group (->> (get-in db [:target-instance-group])
                            (keep #(when (= (val %) current-group)
                                    (key %)))
                            (first))
          entity-indentity (get-in current-instance [:identifiable-entity-definition])
          nowMil (.now js/Date)
          new-definition-id (str nowMil (name target-group))
          new-interaction-id (str nowMil "interaction")
          model (get-in db [:data target-group (keyword (:source-model current-instance))])
          new-definition (-> model
                             (assoc-in [:instances] {})
                             (dissoc :identifiable-entity-definition)
                             (assoc-in [:interactive-entity-definition
                                        :identifiable-entity-definition] entity-indentity)
                             (assoc-in [:id] new-definition-id)
                             (update-in [:interactive-entity-definition
                                         :identifiable-entity-definition
                                         :entity-identity
                                         :name] #(str % " Extraction"))
                             )
          now (js/Date. nowMil)
          date-vec (vector (+ (.getYear now) 1900) (+ (.getMonth now) 1) (.getDate now))
          push-definition {:key (keyword new-definition-id)
                           :group target-group
                           :title (refine-title target-group)
                           :entity-data new-definition}
          new-interaction  {:source (get-in db [:user :username])
                            :target new-definition-id
                            :group target-group
                            :id    new-interaction-id
                            :action (str "extract"  (name current-group) " from instance")
                            :time nowMil
                            :snapshot-entity new-definition
                            }]
      ;(println "target group" target-group new-definition)
      (-> db
          (update-in [:data :interactions] #(merge % {(keyword new-interaction-id) new-interaction}))
          (update-in [:data target-group] #(merge % {(keyword new-definition-id) new-definition}))
          (update-in [:nav :index] inc)
          (update-in [:nav :children] #(conj % push-definition))
          ))))

;;;; generate a recommendation based on an informal process definition
(reg-event-db
  :data/recommendation
  validate-schema-mw
  (fn [db [_ []]]
    (let [index (get-in db [:nav :index])
          current-entity (get-in db [:nav :children index :entity-data])
          current-group (get-in db [:nav :children index :group])
          nowMil (.now js/Date)
          new-definition-id (str nowMil (name current-group))
          new-interaction-id (str nowMil "interaction")
          new-definition (-> current-entity
                             (assoc-in [:instances] {})
                             (assoc-in [:id] new-definition-id)
                             (update-in [:interactive-entity-definition
                                         :identifiable-entity-definition
                                         :entity-identity
                                         :name] #(str % " Recommendation"))
                             )
          now (js/Date. nowMil)
          push-definition {:key (keyword new-definition-id)
                           :group current-group
                           :title (refine-title current-group)
                           :entity-data new-definition}
          new-interaction  {:source (get-in db [:user :username])
                            :target new-definition-id
                            :group current-group
                            :id    new-interaction-id
                            :action (str "generate "  (name current-group) " recommendation")
                            :time nowMil
                            :snapshot-entity new-definition
                            }]
      (-> db
          (update-in [:data :interactions] #(merge % {(keyword new-interaction-id) new-interaction}))
          (update-in [:data current-group] #(merge % {(keyword new-definition-id) new-definition}))
          (update-in [:nav :index] inc)
          (update-in [:nav :children] #(conj % push-definition))
          ))))

;;;; Create a new entity
(reg-event-db
  :data/new
  validate-schema-mw
  (fn [db [_ group]]
    (let [nowMil (.now js/Date)
          now (js/Date. nowMil)
          date-vec (vector (+ (.getYear now) 1900) (+ (.getMonth now) 1) (.getDate now))
          user (get-in db [:user :username])
          namespace (-> (get-in db [:data :namespaces])
                        (keys)
                        (first)
                        (name)
                        )
          new-definition-id (str nowMil (name group))
          new-interaction-id (str nowMil "interaction")
          new-definition (cond
                           (= :intention-definitions group)
                           {:id new-definition-id
                            :interactive-entity-definition
                                {:identifiable-entity-definition
                                 {:entity-identity
                                  {:name "New intention"
                                   :target-namespace namespace
                                   }}}
                            :author user
                            :priority  1
                            :due        date-vec
                            :achieving-strategies       []
                            :active-strategy  []
                            :instances {}
                            :organizational-capabilities    []
                            :initial-contexts []
                            :final-contexts   []
                            }
                           (= :strategy-definitions group)
                           {:id              new-definition-id
                            :interactive-entity-definition
                                             {:identifiable-entity-definition
                                              {:entity-identity
                                               {:name "New strategy"
                                                :target-namespace namespace
                                                }}}
                            :author user
                            :priority  1
                            :due         date-vec
                            :instances {}
                            :implementations []
                            :contained-intentions []
                            }
                           (= :capability-definitions group)
                           {:id new-definition-id
                            :interactive-entity-definition
                                {:identifiable-entity-definition
                                 {:entity-identity
                                  {:name "New capability"
                                   :target-namespace namespace
                                   }}}
                            :author  user
                            :capability-type    0
                            :instances {}
                            :contained-capabilities []
                            :providing-resources []
                            :desired-resources []}
                           (= :context-definitions group)
                           {:id new-definition-id
                            :interactive-entity-definition
                                {:identifiable-entity-definition
                                 {:entity-identity
                                  {:name "New context"
                                   :target-namespace namespace
                                   }}}
                            :author user
                            :contained-contexts []}
                           (= :informal-process-definitions group)
                           {:id new-definition-id
                            :interactive-entity-definition
                                {:identifiable-entity-definition
                                 {:entity-identity
                                  {:name "New informal process"
                                   :target-namespace namespace
                                   }}}
                            :author  user
                            :instances {}
                            :recommendations []
                            :resources []}
                           (= :namespaces group)
                           {:id         new-definition-id
                            :name "New namespace"
                            :namespace ""
                            }
                           (= :groups group)
                           {:id new-definition-id
                            :name "New group"
                            :target-namespace namespace
                            :members []
                            :posts {}}
                           )
          new-interaction  {:source (get-in db [:user :username])
                            :target new-definition-id
                            :group group
                            :id    new-interaction-id
                            :action (str "create new " (string/replace (string/replace (name group)"-" " ") "definitions" "definition"))
                            :time nowMil
                            :snapshot-entity new-definition
                            }]
      (println "create new definition " new-definition)
      (let []
        (-> db
            (update-in [:data :interactions] #(merge % {(keyword new-interaction-id) new-interaction}))
            (update-in [:data group] #(merge % {(keyword new-definition-id) new-definition}))
            )))))

;;;; Set the value of new-post
(reg-event-db
  :data/new-post
  validate-schema-mw
  (fn [db [_ value]]
    {:pre [(s/valid? string? value)]}
    (-> db
        (assoc-in [:new-post] value))
    ))

;;;; -- Api Handlers -----------------------------------------------------------

;;;; Update resources according to the requesting JSON file
(reg-event-db
  :api/update-resources
  validate-schema-mw
  (fn [db [_ [type value]]]
    {:pre [(s/valid? keyword? type)
           (s/valid? some? value)]}
    ;(println "get result list " value)
    (-> db
        (assoc-in [:data type] value))))

;;;; Fetch JSON resource list from server
(reg-event-db
  :api/fetch-resources
  validate-schema-mw
  (fn [db [_ _]]
    (let []
      (api/get-list "servicetemplates" #(dispatch [:api/update-resources [:service-templates %]]))
      (api/get-list "nodetypes" #(dispatch [:api/update-resources [:node-types %]]))
      (api/get-list "relationshiptypes" #(dispatch [:api/update-resources [:relationship-types %]]))
      db
      )))

;;;; Fetch a resource node
(reg-event-db
  :api/fetch-resource-node
  validate-schema-mw
  (fn [db [_ value]]
    (let []
      (api/get-node (str "nodetypes" (lib/format-url value))
                    (fn[result]
                      (println "nodetype result" result)
                      ;(dispatch [:api/update-node-type map]))
                      ))
      db
      )))

;;;; Fetch a resource relationship
(reg-event-db
  :api/fetch-resource-relationship
  validate-schema-mw
  (fn [db [_ value]]
    (let []
      (api/get-relationship (str "relationshiptypes" (lib/format-url value))
                            (fn[result]
                              (println "nodetype result" result)
                              ;(dispatch [:api/update-relationship-type map]))
                              ))
      db
      )))

;;;; Fetch a resource service template
(reg-event-db
  :api/fetch-resource-service
  validate-schema-mw
  (fn [db [_  value]]
    {:pre [(s/valid? some? value)]}
    (let [index (get-in db [:nav :index])
          requesting (subscribe [:nav/requesting])]
      ;(println "requesting in handler" @requesting)
      (if (false? @requesting)
        (api/get-service (str "servicetemplates" (lib/format-url value))
                         (fn [map]
                           (println "result is" map)
                           (dispatch [:api/update-service-template [index map]]))))
      ;(println "now set requesting value")
      (-> db
          (assoc-in [:nav :children index :requesting] true)
          ))))

;;;; Update service template in local and remote
(reg-event-db
  :api/update-service-template
  validate-schema-mw
  (fn [db [_  [request-index map]]]
    {:pre [
           (s/valid? some? map)]}
    (let [index (get-in db [:nav :index])]
      (cond-> db
              (>= index request-index)(assoc-in [:nav :children request-index :requesting] false)
              true (assoc-in [:nav :children index :resource-data :definitions] map)
              true (assoc-in [:data :service-templates (keyword (-> map :attributes :id)) :definitions] map)))))

;;;; Fetch a resource node
(reg-event-db
  :api/bitcoin-price
  validate-schema-mw
  (fn [db [_ _]]
    (let []
      (api/get-bitcoin
                    (fn[result]
                      (println "get-bitcoin result" result)
                      (dispatch [:data/new-post result])
                      ))
      db
      )))

;;;; -- Resource Handlers -----------------------------------------------------------

;;;; Add a new resource node in local database
(reg-event-db
  :resource/add-node
  validate-schema-mw
  (fn [db [_ value]]
    (let [index (get-in db [:nav :index])
          nowMil (.now js/Date)
          resource-id (str nowMil "node")
          type (->> (get-in db [:data :node-types])
                    (keys)
                    (first)
                    (name)
                    (str "winery:")
                    )
          new-data {:tag :NodeTemplate
                    :attributes {:name "New node"
                                 :id resource-id
                                 :type type
                                 :winery:x "0"
                                 :winery:y "0"}
                    :content []}
          ]
      (-> db
          ;add node in cache
          (assoc-in [:nav :children index :resource-data :definitions :nodes (keyword resource-id)] new-data)
          ;now push into nav
          (update-in [:nav :index] inc)
          (update-in [:nav :children] #(conj % {:key           (keyword (str nowMil "-add-resource-node"))
                                                :group         :add-node
                                                :title         "New resource node"
                                                :resource-id    (keyword resource-id)
                                                }))
          ))))

;;;; Add a new resource relationship in local database
(reg-event-db
  :resource/add-relationship
  validate-schema-mw
  (fn [db [_ value]]
    (let [index (get-in db [:nav :index])
          nowMil (.now js/Date)
          resource-id (str nowMil "relationship")
          type (->> (get-in db [:data :relationship-types])
                    (keys)
                    (first)
                    (name)
                    (str "winery:")
                    )
          new-data {:tag :RelationshipTemplate
                    :attributes {:name "New relationship"
                                 :id resource-id
                                 :type type
                                 :winery:x "0"
                                 :winery:y "0"}
                    :content []}
          ]
      (-> db
          ;add node in cache
          (assoc-in [:nav :children index :resource-data :definitions :relationships (keyword resource-id)] new-data)
          ;now push into nav
          (update-in [:nav :index] inc)
          (update-in [:nav :children] #(conj % {:key           (keyword (str nowMil "-add-resource-relationship"))
                                                :group         :add-relationship
                                                :title         "New resource relationship"
                                                :resource-id    (keyword resource-id)
                                                }))
          ))))

;;;; Delete a resource node in local databse
(reg-event-db
  :resource/delete-node
  validate-schema-mw
  (fn [db [_ id]]
    {:pre [(s/valid? keyword? id)]}
    (let [index (get-in db [:nav :index])]
      (-> db
          ;add node in cache
          (update-in [:nav :children index :resource-data :definitions :nodes] dissoc id)
          ))))

;;;; Delelte a resource relationship in local database
(reg-event-db
  :resource/delete-relationship
  validate-schema-mw
  (fn [db [_ id]]
    {:pre [(s/valid? keyword? id)]}
    (let [index (get-in db [:nav :index])]
      (-> db
          ;add node in cache
          (update-in [:nav :children index :resource-data :definitions :relationship] dissoc id)
          ))))
