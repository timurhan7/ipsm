(ns co-act-mobile.entity.ios.intention
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.subs]
            [co-act-mobile.ui :as ui]
            [co-act-mobile.components.ios.index :as components]))

(defn intention-entity []
  (fn []
    ; caution : the nav will changed before the subscribe change!
    (let [intention-map (subscribe [:nav/current-entity])
          related-intentions (subscribe [:related-intentions @intention-map])
          priority-level (subscribe [:static/priority-level])
          ]
      [ui/view {:flex 1
                :flex-direction "column"
                :justify-content "center"
                }

       [components/text-input {:target-property :name
                               :entity @intention-map
                               :size :big}]

       [components/line]

       [components/small-title "definition"]
       [components/text-input {:target-property :definition
                               :size :small
                               :entity @intention-map}]

       [components/add-button {:text "contexts"
                               :on-add-click test }]


       [components/small-title "initial context"]
       [components/entity-links {:group :context-definitions
                                 :name :initial-contexts
                                 :entity  @intention-map}]

       [components/small-title "final context"]
       [components/entity-links {:group :context-definitions
                                 :name :final-contexts
                                 :entity  @intention-map}]

       [components/add-button {:text "strategies"
                               :on-add-click test }]

       [components/pick-list {:group :strategy-definitions
                              :entity-ids (:strategy-definitions @intention-map)
                              :picked-ids (:active-strategy @intention-map)
                              :name :strategy-definitions
                              :picked-name :active-strategy
                              :multi false}]

       (if (not-empty @related-intentions)
         [ui/view
          [components/line]
          [components/small-title "Related intentions"]
          [components/generated-links {:entities @related-intentions
                                       :group :intention-definitions}]
          ])

       [components/line]

       [components/selector {:options @priority-level
                             :text "Priority"
                             :options-color ["red" "orange" "green"]
                             :selected-value (:priority @intention-map)
                             :on-select #(dispatch [:nav/cache {:priority %}])}]
       [components/date-picker {:on-change #(dispatch [:nav/cache {:due-date %}])
                                :text "Due date"
                                :value (:due-date @intention-map)}]

       [components/save]
       ])))