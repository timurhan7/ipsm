(ns co-act-mobile.entity.ios.processDefinition
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.subs]
            [co-act-mobile.components.ios.index :as components]
            [co-act-mobile.ui :as ui]))

(defn process-definition-entity []
  (let [process-map (subscribe [:nav/current-entity])
        contained-resources (subscribe [:data/find-group [(:resources @process-map) :resources]])
        strategy-model (subscribe [:data/find [:strategy-definitions (:model @process-map)]])
        ]
    ;(println "log out process definition ")
    (fn []
      [ui/view {:flex 1
                :flex-direction "column"
                :justify-content "center"
                }
       [components/text-input {:target-property :name
                               :entity @process-map
                               :size :big}]

       [components/small-title "definition"]
       [components/text-input {:target-property :definition
                               :size :small
                               :entity @process-map}]

       (if (not-empty @contained-resources)
         [ui/view
          [components/line]
          [components/small-title "Contained resources"]
          [components/generated-links {:entities @contained-resources
                                       :group :resouces}]
          ])

       [components/line]

       [components/single-link {:title "Model:"
                                :display-text (:name @strategy-model)
                                :on-press #(dispatch [:nav/push-entity [:strategy-definitions @strategy-model]])}]

       [components/line]

       [components/save]
       ])))