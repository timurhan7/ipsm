(ns co-act-mobile.entity.ios.strategy
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.subs]
            [co-act-mobile.components.ios.index :as components]
            [co-act-mobile.ui :as ui]))

(defn strategy-entity []
  (let [strategy-map (subscribe [:nav/current-entity])
        target-intention (subscribe [:target-intention (:id @strategy-map)])]
    ;(println "log out strategy" @strategy-map)
    (fn []
      [ui/view {:flex 1
                :flex-direction "column"
                ;:align-self "stretch"
                ;:align-items "stretch"
                :justify-content "center"
                }
       [components/text-input {:target-property :name
                               :entity @strategy-map
                               :size :big}]

       [components/small-title "definition"]
       [components/text-input {:target-property :definition
                               :size :small
                               :entity @strategy-map}]

       (if (some? @target-intention)
         [ui/view

          [components/line]

          [components/single-link {:title "Target Intention:"
                                   :display-text (:name @target-intention)
                                   :on-press #(dispatch [:nav/push-entity [:intention-definitions @target-intention]])}]

          [components/line]])


       [components/add-button {:text "contained intentions"
                               :on-add-click #() }]

       [components/entity-links {:group :intention-definitions
                                 :name :contained-intentions
                                 :entity @strategy-map  }]

       [components/add-button {:text "Organizational Capabilities"
                               :on-add-click #() }]

       [components/entity-links {:group :capability-definitions
                                 :name :capability-definitions
                                 :entity @strategy-map}]

       [components/add-button {:text "Strategy Implementations"
                               :on-add-click #() }]

       [components/entity-links {:group :process-definitions
                                 :name :implementations
                                 :entity @strategy-map}]

       [components/line]

       [components/save]
       ])))