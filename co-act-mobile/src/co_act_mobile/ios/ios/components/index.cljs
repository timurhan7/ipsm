(ns co-act-mobile.components.ios.index
  (:require
            [co-act-mobile.components.drawer :refer [drawer-component]]
            [co-act-mobile.components.entityLink :refer [entityLink-component]]
            [co-act-mobile.components.addButton :refer [addButton-component]]
            [co-act-mobile.components.line :refer [line-component]]
            [co-act-mobile.components.smallTitle :refer [small-title-component]]
            [co-act-mobile.components.listEntity :refer [entities-map]]
            [co-act-mobile.components.pickList :refer [pickList-component]]
            [co-act-mobile.components.entityLinks :refer [entityLinks-component]]
            [co-act-mobile.components.textInput :refer [textInput-component]]
            [co-act-mobile.components.save :refer [save-component]]
            [co-act-mobile.components.convert :refer [convert-component]]
            [co-act-mobile.components.generatedLinks :refer [generatedLinks-component]]
            [co-act-mobile.components.singleLink :refer [singleLink-component]]
            [co-act-mobile.components.ios.datePicker :refer [date-picker-component]]
            [co-act-mobile.components.ios.selector :refer [selector-component]]))

(def drawer drawer-component)
(def entities entities-map)
(def add-button addButton-component)
(def entity-link entityLink-component)
(def entity-links entityLinks-component)
(def single-link singleLink-component)
(def selector selector-component)
(def save save-component)
(def convert convert-component)
(def line line-component)
(def text-input textInput-component)
(def pick-list pickList-component)
(def generated-links generatedLinks-component)
(def date-picker date-picker-component)
(def small-title small-title-component)