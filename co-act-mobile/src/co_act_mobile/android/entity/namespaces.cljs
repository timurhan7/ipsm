(ns co-act-mobile.android.entity.namespaces
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.subs]
            [co-act-mobile.android.components.index :as components]
            [co-act-mobile.android.ui :as ui]))

(defn namespaces-entity
  "namespace entity"
  []
  (fn []
    (let [namespace-map (subscribe [:nav/current-entity])]
      ;(println "log out namespace " @namespace-map)
      [ui/view {:flex 1
                :flex-direction "column"
                :justify-content "center"}
       [ui/scroll {:margin-bottom (-> common :component :margin-bottom)
                   :showsVerticalScrollIndicator false}
        [components/small-title "Name"]
        [components/text-input {:target-property :name
                                :entity @namespace-map
                                :size :middle}]
        [components/line]
        [components/small-title "Namespace"]
        [components/text-input {:target-property :namespace
                                :entity @namespace-map
                                :size :small}]]

       [components/bar
        [components/subscribe]
        [components/save]
        [components/revert]]]
      )))
