(ns co-act-mobile.android.entity.index
  (:require [co-act-mobile.android.entity.intention :refer [intention-entity]]
            [co-act-mobile.android.entity.strategy :refer [strategy-entity]]
            [co-act-mobile.android.entity.capability :refer [capability-entity]]
            [co-act-mobile.android.entity.context :refer [context-entity]]
            [co-act-mobile.android.entity.processDefinition :refer [process-definition-entity]]
            [co-act-mobile.android.entity.interactions :refer [interactions-entity]]
            [co-act-mobile.android.entity.participants :refer [participants-entity]]
            [co-act-mobile.android.entity.namespaces :refer [namespaces-entity]]
            [co-act-mobile.android.entity.group :refer [group-entity]]
            [co-act-mobile.android.entity.instanceDescriptor :refer [instance-descriptor-entity]]
            ))

(def entitiesMap {:intention-definitions intention-entity
                  :intention-instances instance-descriptor-entity
                  :strategy-definitions strategy-entity
                  :strategy-instances instance-descriptor-entity
                  :informal-process-definitions process-definition-entity
                  :informal-process-instances instance-descriptor-entity
                  :capability-definitions capability-entity
                  :instance-descriptor instance-descriptor-entity
                  :capability-instances instance-descriptor-entity
                  :context-definitions context-entity
                  :interactions interactions-entity
                  :participants participants-entity
                  :namespaces namespaces-entity
                  :groups group-entity
                  })
