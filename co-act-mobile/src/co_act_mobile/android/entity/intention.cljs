(ns co-act-mobile.android.entity.intention
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.subs]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.android.ui :as ui]
            [co-act-mobile.android.components.index :as components]))

(defn intention-entity
  "Intention definition entity"
  []
  (fn []
    ; caution : the nav will changed before the subscribe change!
    (let [intention-map (subscribe [:nav/current-entity])
          related-intentions (subscribe [:related-intentions @intention-map])
          priority-level (subscribe [:static/priority-level])
          priority-color (subscribe [:static/priority-color])
          related-resources  (subscribe [:related-resources @intention-map])
          ]
      ;(println "related resources " @related-resources)
      [ui/view {:flex 1
                :flex-direction "column"
                :justify-content "center"
                }
       [ui/scroll {:margin-bottom (-> common :component :margin-bottom)
                   :showsVerticalScrollIndicator false}
        [components/small-title "Name"]
        [components/text-input {:target-property [:interactive-entity-definition
                                                  :identifiable-entity-definition
                                                  :entity-identity
                                                  :name]
                                :size :small
                                :entity @intention-map}]

        [components/line]
        [components/add-button {:text "Target namespace"
                                :entity @intention-map
                                :add-name [:interactive-entity-definition
                                           :identifiable-entity-definition
                                           :entity-identity
                                           :target-namespace]
                                :add-group :namespaces
                                :multi false
                                :require true
                                :display-property :namespace}]
        [components/entity-link {:group :namespaces
                                 :name [:interactive-entity-definition
                                        :identifiable-entity-definition
                                        :entity-identity
                                        :target-namespace]
                                 :display :namespace
                                 :require true
                                 :entity @intention-map}]
        [components/line]
        [components/privilege]
        [components/line]
        [components/add-button {:text "Initial contexts"
                                :entity @intention-map
                                :add-name :initial-contexts
                                :add-group :context-definitions}]
        [components/entity-links {:group :context-definitions
                                  :name :initial-contexts
                                  :entity  @intention-map}]

        [components/line]
        [components/add-button {:text "Final contexts"
                                :entity @intention-map
                                :add-name :final-contexts
                                :add-group :context-definitions}]
        [components/entity-links {:group :context-definitions
                                  :name :final-contexts
                                  :entity  @intention-map}]

        [components/line]
        [components/small-title "Achieving strategies"]

        [components/pick-list    {:multi false
                                  :entity-ids (:achieving-strategies @intention-map)
                                  :picked-ids (:active-strategy @intention-map)
                                  :group :strategy-definitions
                                  :picked-name :active-strategy}]
        (if (not-empty @related-intentions)
          [ui/view
           [components/line]
           [components/small-title "Related intentions"]
           [components/generated-links {:entities @related-intentions
                                        :group :intention-definitions
                                        :display :name}]])
        [components/line]
        [components/add-button {:entity @intention-map
                                :add-name :organizational-capabilities
                                :add-group :capability-definitions
                                :text "Organizational capabilities"}]
        [components/entity-links {:group :capability-definitions
                                  :name :organizational-capabilities
                                  :entity @intention-map}]
        (if (not-empty @related-resources)
          [ui/view
           [components/line]
           [components/small-title "Desired Resources"]
           (doall
             (for [resource @related-resources]
               ^{:key (str "desired-resource-" (:id resource))}
               [components/resource-model {:disable true
                                           :id (:id resource)}]))])
        [components/line]
        [components/small-title "Instances"]
        [components/instance-links {:group :intention-instances}]

        [components/line]
        [components/selector {:options @priority-level
                              :text "Priority"
                              :options-color @priority-color
                              :selected-value (:priority @intention-map)
                              :on-select #(dispatch [:nav/cache [:priority %]])}]
        [components/line]
        [components/date-picker {:on-change #(dispatch [:nav/cache [:due %]])
                                 :text "Due"
                                 :value (:due @intention-map)}]
        ]
       [components/bar
        [components/subscribe]
        [components/save]
        [components/revert]
        [components/init]
        ]
       ])))