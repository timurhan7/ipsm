(ns co-act-mobile.android.entity.context
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.subs]
            [co-act-mobile.android.components.index :as components]
            [co-act-mobile.android.ui :as ui]))

(defn context-entity
  "Context definition entity"
  []
  (let [context-map (subscribe [:nav/current-entity])]
    ;(println "log out context " @context-map)
    (fn []
      [ui/view {:flex 1
                :flex-direction "column"
                :justify-content "center"
                }
       [ui/scroll {:margin-bottom (-> common :component :margin-bottom)
                   :showsVerticalScrollIndicator false}

        [components/small-title "Name"]
        [components/text-input {:target-property [:interactive-entity-definition
                                                  :identifiable-entity-definition
                                                  :entity-identity
                                                  :name]
                                :size :small
                                :entity @context-map}]

        [components/add-button {:text "Target namespace"
                                :entity @context-map
                                :add-name [:interactive-entity-definition
                                           :identifiable-entity-definition
                                           :entity-identity
                                           :target-namespace]
                                :add-group :namespaces
                                :multi false
                                :require true
                                :display-property :namespace}]

        [components/entity-link {:group :namespaces
                                 :name [:interactive-entity-definition
                                        :identifiable-entity-definition
                                        :entity-identity
                                        :target-namespace]
                                 :display :namespace
                                 :require true
                                 :entity @context-map}]
        [components/line]
        [components/privilege]
        [components/line]
        [components/add-button {:text "Contained contexts"
                                :entity @context-map
                                :add-name :contained-contexts
                                :add-group :context-definitions}]
        [components/entity-links {:group :context-definitions
                                  :name :contained-contexts
                                  :entity @context-map  }]
        ]

       [components/bar
        [components/subscribe]
        [components/save]
        [components/revert]
        ]]
      )))