(ns co-act-mobile.android.entity.instanceDescriptor
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.subs]
            [cljs.spec :as s]
            [co-act-mobile.android.lib :as lib]
            [co-act-mobile.android.components.index :as components]
            [co-act-mobile.android.ui :as ui]))

(defn instance-descriptor-entity
  "Instance descriptor"
  [props & children]
  (let [instance-map (subscribe [:nav/current-entity])
        group (subscribe [:nav/current-group])
        target-group (subscribe [:static/target-definition])
        process-model (subscribe [:data/find [@target-group (:source-model @instance-map)]])
        instance-state (subscribe [:static/instance-state [@group]])
        parent-instance (if (some? (:parent-instance @instance-map))
                          (subscribe [:data/find [@target-group (keyword (:parent-instance @instance-map))]]))]
    (fn [props & children]
      [ui/view {:flex 1
                :flex-direction "column"
                :justify-content "center"}
       [ui/scroll {:margin-bottom (-> common :component :margin-bottom)
                   :showsVerticalScrollIndicator false}

        [components/small-title "Name"]
        [components/text-input {:target-property [:identifiable-entity-definition
                                                  :entity-identity
                                                  :name]
                                :size :small
                                :entity @instance-map}]

        [components/add-button {:text "Target namespace"
                                :entity @instance-map
                                :add-name :target-namespace
                                :add-group :namespaces
                                :require true
                                :multi false
                                :display-property :namespace}]

        [components/entity-link {:group :namespaces
                                 :name [:identifiable-entity-definition
                                        :entity-identity
                                        :target-namespace]
                                 :require true
                                 :display :namespace
                                 :entity @instance-map}]
        [components/line]
        [components/privilege]

        [components/line]

        [components/single-link {:title "Source model:"
                                 :display-text (get-in @process-model [:interactive-entity-definition
                                                                       :identifiable-entity-definition
                                                                       :entity-identity
                                                                       :name])
                                 :on-press #(dispatch [:nav/push-entity [:capability-definitions @process-model]])}]

        [components/line]

        (if (and (some? parent-instance) (not-empty @parent-instance))
          [components/single-link {:title        "Parent :"
                                   :display-text (get-in @parent-instance [:identifiable-entity-definition
                                                                           :entity-identity
                                                                           :name])
                                   :on-press     #(dispatch [:nav/push-entity [:capability-definitions @parent-instance]])}])

        [components/single-link {:title        "URI :"
                                 :font-size    :small
                                 :display-text (:instance-uri @instance-map)
                                 :on-press     #(.web lib/communicator (:instance-uri @instance-map))}]

        [components/line]
        [components/selector {:options @instance-state
                              :text "State"
                              :selected-value (:instance-state-id  @instance-map)
                              :on-select #(dispatch [:nav/cache [:instance-state-id  %]])}]
        [components/line]
        [components/date-picker {:on-change #(dispatch [:nav/cache [:start-date %]])
                                 :text "Start date"
                                 :value (:start-date @instance-map)}]
        [components/line]
        [components/date-picker {:on-change #(dispatch [:nav/cache [:end-date %]])
                                 :text "Due date"
                                 :value (:end-date @instance-map)}]

        [components/line]
        [components/posts]]
       [components/bar
        [components/subscribe]
        [components/save {:on-press #(dispatch [:data/save-instance])}]
        [components/revert {:on-press #(dispatch [:data/revert-instance])}]
        [components/extract]]]

      )))