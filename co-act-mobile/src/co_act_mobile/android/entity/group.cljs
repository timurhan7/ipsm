(ns co-act-mobile.android.entity.group
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.subs]
            [cljs.spec :as s]
            [co-act-mobile.android.ui :as ui]
            [co-act-mobile.android.lib :as lib]
            [co-act-mobile.android.components.index :as components ]))

(defn group-entity
  "Group entity"
  [props & children]
  (let [group-map (subscribe [:nav/current-entity])]
    ;(println "log out interaction " lib/communicator)
    (fn []
      [ui/view {:flex 1
                :flex-direction "column"
                :justify-content "center"
                }
       [ui/scroll {:margin-bottom (-> common :component :margin-bottom)
                   :showsVerticalScrollIndicator false}

        [components/small-title "Name"]
        [components/text-input {:target-property :name
                                :size :small
                                :entity @group-map}]

        [components/add-button {:text "Target namespace"
                                :entity @group-map
                                :add-name :target-namespace
                                :add-group :namespaces
                                :multi false
                                :require true
                                :display-property :namespace}]

        [components/entity-link {:group :namespaces
                                 :require true
                                 :name :target-namespace
                                 :display :namespace
                                 :entity @group-map}]
        [components/line]
        [components/add-button {:text "Members"
                                :entity @group-map
                                :add-name :members
                                :add-group :participants
                                :multi true}]
        [components/entity-links {:group :participants
                                  :name :members
                                  :display :name
                                  :entity @group-map}]
        [components/line]
        [components/privilege]
        [components/line]
        [components/posts]]
       [components/bar
        [components/subscribe]
        [components/save]
        [components/revert]]]
      )))