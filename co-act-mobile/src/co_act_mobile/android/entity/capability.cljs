(ns co-act-mobile.android.entity.capability
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.subs]
            [co-act-mobile.android.components.index :as components]
            [co-act-mobile.android.ui :as ui]))

(defn capability-entity
  "Capability definition entity"
  []
  (let [capability-map (subscribe [:nav/current-entity])
        capability-types (subscribe [:static/capability-types])]
    (fn []
      [ui/view {:flex 1
                :flex-direction "column"
                :justify-content "center"}
       [ui/scroll {:margin-bottom (-> common :component :margin-bottom)
                   :showsVerticalScrollIndicator false}
        [components/small-title "Name"]
        [components/text-input {:target-property [:interactive-entity-definition
                                                  :identifiable-entity-definition
                                                  :entity-identity
                                                  :name]
                                :size :small
                                :entity @capability-map}]
        [components/add-button {:text "Target namespace"
                                :entity @capability-map
                                :add-name [:interactive-entity-definition
                                           :identifiable-entity-definition
                                           :entity-identity
                                           :target-namespace]
                                :add-group :namespaces
                                :require true
                                :multi false
                                :display-property :namespace}]
        [components/entity-link {:group :namespaces
                                 :name [:interactive-entity-definition
                                        :identifiable-entity-definition
                                        :entity-identity
                                        :target-namespace]
                                 :require true
                                 :display :namespace
                                 :entity @capability-map}]
        [components/line]
        [components/privilege]

        [components/line]
        [components/selector {:options @capability-types
                              :text "Type"
                              :selected-value (:capability-type @capability-map)
                              :on-select #(dispatch [:nav/cache [:capability-type %]])}]

        [components/line]
        [components/add-button {:text "Contained capablities"
                                :entity @capability-map
                                :add-name :contained-capabilities
                                :add-group :capability-definitions}]

        (if (not-empty (:contained-capabilities @capability-map))
          [components/entity-links {:group :capability-definitions
                                    :name :contained-capabilities
                                    :entity @capability-map  }])
        [components/line]
        [components/add-button {:text "Providing Resources"
                                :multi true
                                :disable-redirect true
                                :display-property :id
                                :entity @capability-map
                                :add-name :providing-resources
                                :add-group :service-templates}]

        (doall
          (for [resource-id (:providing-resources @capability-map)]
            ^{:key (str "providing-resource-" resource-id)}
            [components/resource-model {:name :providing-resources
                                        :id resource-id}]))

        [components/line]
        [components/add-button {:text "Desired Resource"
                                :multi true
                                :disable-redirect true
                                :display-property :id
                                :entity @capability-map
                                :add-name :desired-resources
                                :add-group :service-templates}]
        (doall
          (for [resource-id (:desired-resources @capability-map)]
            ^{:key (str "desired-resource-" resource-id)}
            [components/resource-model {:name :desired-resources
                                        :id resource-id}]))
        [components/line]
        [components/small-title "Instances"]
        [components/instance-links {:group :capability-instances}]]
       [components/bar
        [components/subscribe]
        [components/save]
        [components/revert]
        [components/init]
        ]

       ])))
