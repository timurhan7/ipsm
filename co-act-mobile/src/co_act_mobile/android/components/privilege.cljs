(ns co-act-mobile.android.components.privilege
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [co-act-mobile.handlers]
            [cljs.spec :as s]
            [co-act-mobile.style :refer [common color]]
            [co-act-mobile.subs]
            [co-act-mobile.ui :as ui]))

(def style {:view   {:padding (-> common :component :padding)
                     :flex-direction "row"
                     :align-items "center"
                     :justify-content "space-between"
                     ;:border-width 1
                     :flex 10}
            :text   {:font-size (-> common :text :small)
                     :color (:icon color)
                     :font-weight "bold"
                     :flex 8}
            :button {:outer-style {:width (-> common :icon :width)}
                     :inner-style {:color (:icon color)}}})

(defn privilege-component
  "privilege configuration panel"
  [props]
  {:pre []}
  (let [entity-map (subscribe [:nav/current-entity])
        privilege (subscribe [:user/privilege])
        privilege-level (subscribe [:static/privilege-level])
        editing (atom false)]
    (fn [props]
      (when @privilege
        [ui/view {:style (-> style :container)}
         [ui/view {:style (get-in style [:view])}
          [ui/text {:style (get-in style [:text])} "Privilege management"]
          [ui/button (merge  (get-in style [:button])
                             {:on-press #(swap! editing not)})
           (if (true? @editing)
             [ui/m-icons {:key "privilege settings collapse"  :name "arrow-drop-up" :size (-> common :icon :size)}]
             [ui/m-icons {:key "privilege settings expand"  :name "arrow-drop-down" :size (-> common :icon :size)}]
             )]]
         (if (and (true? @editing) (not-empty @privilege-level))
           (doall (for [item @privilege-level]
                    ^{:key item}
                    [ui/view {:style (get-in style [:view])}
                     [ui/text {:style (get-in style [:text])} item]
                     [ui/button (merge  (get-in style [:button])
                                        {:on-press #(dispatch [:nav/push {:key (keyword (str item "-privilege-"))
                                                                          :group :pick
                                                                          :title (str "Select participant with " item " permission")
                                                                          :pick-data {:group :participants
                                                                                      :picked-ids (get @entity-map
                                                                                                       (keyword item))
                                                                                      :picked-name [(keyword item)]
                                                                                      :multi true
                                                                                      :display-property [:name]}}])})
                      [ui/m-icons {:key (str item "-icon")  :name "settings" :size (-> common :icon :size)}]]])))]))))
