(ns co-act-mobile.android.lib
  (:require [reagent.core :as r]
            [cemerick.url :refer [url url-encode]]))
; react-native
(defonce ReactNative (js/require "react-native"))
(defn alert [title message optionsVector]
  (.alert (.-Alert ReactNative) title message optionsVector))
(def geocoder (.-default (js/require "react-native-geocoder")))
(def communicator (.-default (js/require "react-native-communications")))
(def cheerio "(js/require \"cheerio\")")

(defn current-location
  "Get the current location and geocoding it"
  [callback]
  (.getCurrentPosition (.-geolocation js/navigator)
                       (clj->js (fn [coords]
                                  ;(->
                                  ;  (.geocodePosition geocoder  (clj->js {:lat (aget coords "coords" "longitude")
                                  ;                               :lng (aget coords "coords" "latitude")}))
                                  ;  (.then #(->
                                  ;           (js->clj %)
                                  ;           (nth 0)
                                  ;           (get "formattedAddress")
                                  ;           (callback))
                                  ;         ))
                                  (callback (str "Longitude: " (aget coords "coords" "longitude") " Latitude: " (aget coords "coords" "latitude")))
                                  ))
                       (clj->js #(println "error is " %))
                       (clj->js {:enableHighAccuracy true :timeout 20000 :maximumAge 1000})
                       ))

(defn encode-twice [value]
  "Helper function to encode twice url"
  (url-encode (url-encode value)))

(defn format-url
  "Transfer url winery recognised format"
  ([resource] (str
                "/"
                (encode-twice (:namespace resource))
                "/"
                (encode-twice (:id resource))))
  ([namespace id] (str
                    "/"
                    (encode-twice namespace)
                    "/"
                    (encode-twice id))))

(defn api-vec2map
  "Refine the vector to map"
  [v]
  (reduce (fn [map node]
            (let [new (clojure.set/rename-keys node {"id" :id,
                                                     "name" :name,
                                                     "namespace" :namespace})]
              (assoc-in map [(keyword (:id new))] new))
            ) {} v))

(defn find-source-id
  "Find source node id in parsed xml"
  [v]
  (some #(if (= :SourceElement (:tag %))
          (-> % :attributes :ref)) v))

(defn find-target-id
  "Find target node id in parsed xml"
  [v]
  (some #(if (= :TargetElement (:tag %))
          (-> % :attributes :ref)) v))

(defn judge-color
  "Return the corresponding color for correlation coefficient"
  [v]
  (cond
    (> v 0.5) "green"
    (> v 0) "yellow"
    (> v -0.5) "orange"
    (> v -1) "red"))
;(def parser (js/require "xml-parser"))
;react-native-router-flux
;(defonce ReactNativeRouterFlux (js/require "react-native-router-flux"))
;(defonce Drawer (r/adapt-react-class (js/require "react-native-drawer")))
;
;(defonce Navigation (r/adapt-react-class (aget ReactNativeRouterFlux "Navigation")))
;React Class
;(defonce Scene (r/adapt-react-class (aget ReactNativeRouterFlux "Scene")))
;(defonce Router (r/adapt-react-class (aget ReactNativeRouterFlux "Router")))
;(defonce DefaultRenderer (r/adapt-react-class (aget ReactNativeRouterFlux "DefaultRenderer")))
;
;(defonce Actions (aget ReactNativeRouterFlux "Actions"))
