(ns co-act-mobile.subs
  (:require-macros [reagent.ratom :refer [reaction]])
  (:require [re-frame.core :refer [reg-sub subscribe]]
            [cljs.spec :as s]
            [co-act-mobile.lib :as lib]))

(reg-sub
  :get-greeting
  (fn [db _]
    (reaction
      (get @db :greeting))))

;;;; -- Nav Subs -----------------------------------------------------------

;;;; Index in nav
(reg-sub
  :nav/index
  (fn [db _]
    (get-in @db [:nav :index])))

;;;; First key, home page
(reg-sub
  :nav/first-key
  (fn [db _]
    (get-in @db [:nav :children 0 :key])))

;;;; Whole nav state
(reg-sub
  :nav/state
  (fn [db [sid, cid]]
    (get @db :nav)))

;;;; Current nav object
(reg-sub
  :nav/current
  (fn [db _]
    (let [index (subscribe [:nav/index])]
      (get-in @db [:nav
                   :children
                   @index]))))

;;;; Current entity data
(reg-sub
  :nav/current-entity
  (fn [db _]
    (let [index (subscribe [:nav/index])]
      (get-in @db [:nav
                   :children
                   @index
                   :entity-data]))))

;;;; Latest entity data
(reg-sub
  :nav/latest-entity
  (fn [db _]
    (let [index (subscribe [:nav/index])]
      (get-in @db [:nav
                   :children
                   (- @index 1)
                   :entity-data]))))

;;;; Current resource data
(reg-sub
  :nav/resource-data
  (fn [db _]
    (let [index (subscribe [:nav/index])]
      (get-in @db [:nav
                   :children
                   @index
                   :resource-data]))))

;;;; Resource data in last page
(reg-sub
  :nav/last-resource-definitions
  (fn [db _]
    (let [index (subscribe [:nav/index])]
      (get-in @db [:nav
                   :children
                   (- @index 1)
                   :resource-data
                   :definitions]))))

;;;; A resource node data
(reg-sub
  :nav/resource-node
  (fn [db _]
    (let [index (subscribe [:nav/index])
          id (subscribe[:nav/resource-id])]
      (get-in @db [:nav
                   :children
                   (- @index 1)
                   :resource-data
                   :definitions
                   :nodes
                   @id]))))

;;;; A resource relationship data
(reg-sub
  :nav/resource-relationship
  (fn [db _]
    (let [index (subscribe [:nav/index])
          id (subscribe[:nav/resource-id])]
      (get-in @db [:nav
                   :children
                   (- @index 1)
                   :resource-data
                   :definitions
                   :relationships
                   @id]))))

;;;; Current resource id
(reg-sub
  :nav/resource-id
  (fn [db _]
    (let [index (subscribe [:nav/index])]
      (get-in @db [:nav
                   :children
                   @index
                   :resource-id]))))

;;;; Definitions of current resource model
(reg-sub
  :nav/resource-definitions
  (fn [db _]
    (let [index (subscribe [:nav/index])]
      (get-in @db [:nav
                   :children
                   @index
                   :resource-data
                   :definitions]))))

;;;; If app is now requsting server
(reg-sub
  :nav/requesting
  (fn [db _]
    (let [index (subscribe [:nav/index])]
      (get-in @db [:nav
                   :children
                   @index
                   :requesting]))))

;;;; Current pick list data
(reg-sub
  :nav/current-pick
  (fn [db _]
    (let [index (subscribe [:nav/index])]
      (get-in @db [:nav
                   :children
                   @index
                   :pick-data]))))

;;;; Current keyword of group
(reg-sub
  :nav/current-group
  (fn [db _]
    (let [index (subscribe [:nav/index])]
      (get-in @db [:nav
                   :children
                   @index
                   :group]))))

;;;; If current in a snapshot page
(reg-sub
  :nav/snapshot?
  (fn [db _]
    (let [index (subscribe [:nav/index])]
      (some? (get-in @db [:nav
                          :children
                          @index
                          :snapshot])))))


;;;; -- UI Subs -----------------------------------------------------------

;;;; All the list meta information
(reg-sub
  :main-list
  (fn [db _]
    (get-in db [:main-list])))

;;;; All the drawer information
(reg-sub
  :drawer
  (fn [db _]
    (get-in db [:drawer])))

;;;; If drawer is open?
(reg-sub
  :drawer/opened
  (fn [db _]
    (get-in db [:drawer :opened])))

;;;; Get target-intention for certain strategy id
(reg-sub
  :target-intention
  (fn [db [_ strategy-id]]
    {:pre [(s/valid? string? strategy-id)]}
    (some
     (fn[intention]
       (when (some #(= strategy-id %) (:active-strategy intention))
         intention))
     (vec (vals (get-in db [:data :intention-definitions]))))))

;;;; Get multiple target intentions
(reg-sub
  :target-intention-multi
  (fn [db [_ strategy-id]]
    {:pre [(s/valid? string? strategy-id)]}
    (let [intentions (vec (vals (get-in db [:data :intention-definitions])))
          get-intention (fn [targets intention]
                          (let []
                                        ;here we don't use active-strategy
                            (if (some #(= strategy-id %) (:strategy-definitions intention))
                              (conj targets intention)
                              targets)))
          resources (vec (distinct (reduce get-intention [] intentions)))
          ]
      resources)))

;;;; Get related resources for current entity
(reg-sub
  :related-resources
  (fn [db [_ intention]]
    (let [get-resource (fn [resources capability-id]
                         (let [capability (get-in db [:data :capability-definitions (keyword capability-id)])]
                           (into resources (:desired-resources capability))))
          resources (vec (distinct (reduce get-resource [] (:organizational-capabilities intention))))]
      (mapv #(get-in db [:data :service-templates (keyword %)]) resources)
      )))

;;;; Get prerequisite intentions for certain intention
(reg-sub
  :related-intentions
  (fn [db [_ intention]]
    ; can't use pre : the nav will changed before the subscribe change!
    ;{:pre [(s/valid? some? intention)]}
    (let [activ-strategy  (if (not-empty (:active-strategy intention))
                            (get-in db [:data :strategy-definitions (keyword (nth (:active-strategy intention) 0))]))
          ]
      (filterv
       (fn[intention]
         (some #(= (:id intention) %) (:contained-intentions activ-strategy)))
       (vec (vals (get-in db [:data :intention-definitions])))))))

;;;; The new post data
(reg-sub
  :ui/new-post
  (fn [db _]
    (get-in db [:new-post])))

;;;; -- Data Subs -----------------------------------------------------------

;;;; All the entity data
(reg-sub
  :data
  (fn [db _]
    (get-in db [:data])))

;;;; Find a bunch of items in one group
(reg-sub
  :data/find-group
  (fn [db [_ [search-array target-group]]]
    {:pre [(s/valid? (s/* string?) search-array)
           (s/valid? keyword? target-group)]}
    ; can't use pre : the nav will changed before the subscribe change!
    ;{:pre [(s/valid? some? intention)]}
    (filterv
     (fn[entity]
       (some #(= (:id entity) %) search-array))
     (vec (vals (get-in db [:data target-group]))))))


;;;; Find certain item in one group
(reg-sub
  :data/find
  (fn [db [_ [group id]]]
    {:pre [(s/valid? string? id)
           (s/valid? keyword? group)]}
    (get-in db [:data group (keyword id)])))

;;;; Find entity by id
(reg-sub
  :data/find-keyword
  (fn [db [_ [group id]]]
    {:pre [(s/valid? keyword? id)
           (s/valid? keyword? group)]}
    (get-in db [:data group id])))

;;;; Current user name
(reg-sub
  :data/find-user-name
  (fn [db [_ id]]
    {:pre [(s/valid? string? id)]}
    (get-in db [:data :participants (keyword id) :name])))

;;;; A certain resource data by group and id
(reg-sub
  :data/resource-definition
  (fn [db [_ [group id]]]
    {:pre [(s/valid? keyword? group)
           (s/valid? keyword? id)]}
    (get-in db [:data
                 group
                 id])))

;;;; Return group of one kind of entity data
(reg-sub
  :data/group
  (fn [db [_ group]]
    {:pre [(s/valid? keyword? group)
           ]}
    (let []
      (get-in db [:data
                   group]))))

;;;; -- User Subs -----------------------------------------------------------

;;;; All User data
(reg-sub
  :user
  (fn [db _]
    (get-in db [:user])))

;;;; If user has login?
(reg-sub
  :user/login
  (fn [db _]
    (get-in db [:login])))

;;;; Subscribed entities
(reg-sub
  :user/subscribe
  (fn [db _]
    (get-in db [:user :subscribe])))

;;;; If user subscribed current entity
(reg-sub
  :user/subscribe?
  (fn [db [_ _]]
    (let [index (subscribe [:nav/index])
             entity(get-in db [:nav
                               :children
                               @index
                               :entity-data])
             list (get-in db [:user :subscribe])]
      (if (some? entity)
        (contains? list (keyword (:id entity)))
        false
        )
      )))

;;;; Privilege information for current enitty
(reg-sub
  :user/privilege
  (fn [db _]
    true))

;;;; -- Static Subs -----------------------------------------------------------

;;;; All the strings
(reg-sub
  :strings
  (fn [db _]
    (get-in db [:strings])))

;;;; Find target group of definition for instance
(reg-sub
  :static/target-definition
  (fn [db [_ _]]
    (let [group (subscribe [:nav/current-group])]
      (get-in db [:target-definition-map @group]))))

;;;; Priority levels vector
(reg-sub
  :static/priority-level
  (fn [db _]
    (get-in db [:static :priority-level])))

;;;; Completion levels vector
(reg-sub
  :static/completion-level
  (fn [db _]
    (get-in db [:static :completion-level])))

;;;; Priority level colors vector
(reg-sub
  :static/priority-color
  (fn [db _]
    (get-in db [:static :priority-color])))

;;;; Capability types vector
(reg-sub
  :static/capability-types
  (fn [db _]
    (get-in db [:static :capability-types])))

;;;; Instace state vector for certain kind of entity
(reg-sub
  :static/instance-state
  (fn [db [_ [group]]]
    {:pre [(s/valid? keyword? group)]}
    ;(println "group is" group)
    (get-in db [:static :instance-state-map group])))

;;;; Privilege levels vector
(reg-sub
  :static/privilege-level
  (fn [db _]
    (get-in db [:static :privilege-level])))

;;;; Entities vector which could be add by user
(reg-sub
  :static/addable-list
  (fn [db _]
    (get-in db [:static :addable-list])))

;;;; Host server addrss
(reg-sub
  :static/host
  (fn [db _]
    (get-in db [:static :host])))
