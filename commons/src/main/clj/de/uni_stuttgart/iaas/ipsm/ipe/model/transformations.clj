(ns de.uni-stuttgart.iaas.ipsm.ipe.model.transformations
  (:require [taoensso.timbre :as timbre]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.domain-language :as domain-language]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.tosca :as t]
            [uni-stuttgart.jaxb-conversions.core :as jaxb]
            [de.uni-stuttgart.iaas.ipsm.protocols.core :as protos])
  (:import [de.uni_stuttgart.iaas.ipsm.v0 ObjectFactory TResourceDrivenProcessDefinition ObjectFactory]
           [org.oasis_open.docs.tosca.ns._2011._12 TRelationshipTemplate TNodeTemplate Definitions]
           [javax.xml.bind JAXBContext Marshaller]))

(timbre/refer-timbre)


(def defaults (atom {:default-target-namespace "http://www.uni-stuttgart.de/ipsm/"}))

(defn- add-jaxb-context
  [m]
  (-> m
      (assoc :jaxb-context
             (JAXBContext/newInstance (or (:jaxb-namespace m)
                                          "de.uni_stuttgart.iaas.ipsm.v0")))))


(defn get-definitions-obj-with-types
  [type-definiton & type-definitions]
  {:post [(instance? Definitions %)]}
  (debug "Types are" [type-definiton type-definitions])
  (-> {:service-template-or-node-type-or-node-type-implementation (apply vector type-definiton type-definitions)
       :target-namespace (:default-target-namespace @defaults)}
      (jaxb/map->jaxb-type-obj (Definitions.) {:type-keyword :redo/entity-type
                                               :type-mappings domain-language/jaxb-mappings})))


(defn- add-marshaller
  [m]
  {:pre [(:jaxb-context m)]}
  (assoc m :marshaller (doto (.createMarshaller (:jaxb-context m))
                         (.setProperty Marshaller/JAXB_FORMATTED_OUTPUT true))))

(defn- add-unmarshaller
  [m]
  {:pre [(:jaxb-context m)]}
  (assoc m :unmarshaller (.createUnmarshaller (:jaxb-context m))))

(defn- add-marshalled-str
  [m]
  {:pre [(:marshaller m) (:jaxb-obj m)]}
  (with-open [os (java.io.ByteArrayOutputStream.)]
    (debug "Marshalling" (:jaxb-obj m) "into" os)
    (.marshal (:marshaller m) (:jaxb-obj m) os)
    (assoc m :marshalled-str (slurp (.toByteArray os)))))


(defn- add-unmarshalled-obj
  [m]
  {:pre [(:unmarshaller m)]}
  (assoc m :jaxb-obj
         (.unmarshal (:unmarshaller m)
                     (clojure.java.io/input-stream (.getBytes (:xml-str m))))))

(defn get-jaxb-of-str
  [m]
  {:pre [(:xml-str m)]}
  (-> m
      add-jaxb-context
      add-unmarshaller
      add-unmarshalled-obj
      :jaxb-obj))


;; (defn get-definitions-obj-with-types
;;   [type-definiton & type-definitions]
;;   (-> {:service-template-or-node-type-or-node-type-implementation (apply vector type-definiton type-definitions)}
;;       (jaxb/map->jaxb-type-obj (Definitions.) {:type-keyword :entity-map
;;                                                :type-mappings domain-language/jaxb-mappings})))


(defn get-definitions-xml-with-types
  [type-definiton & type-definitions]
  (-> {}
      add-jaxb-context
      (assoc :jaxb-obj (apply get-definitions-obj-with-types type-definiton type-definitions))
      add-marshaller
      add-marshalled-str
      :marshalled-str))


(defn get-types-of-defs-map
  [defs-m]
  (-> defs-m
      :redo/service-template-or-node-type-or-node-type-implementation))

(defn get-imports-of-defs-map
  [defs-m]
  (-> defs-m
      :imports))

(defn get-type-definition-maps
  [definitions-obj]
  (-> definitions-obj
      (jaxb/jaxb-type-obj->map-recursively {:map-ns "redo"
                                            :type-keyword :redo/entity-type
                                            :type-mappings domain-language/jaxb-mappings})
      :redo/service-template-or-node-type-or-node-type-implementation))

(defn add-types-of-defs-map
  [m]
  {:pre [(:defs m)]
   :post [(:types %)]}
  (assoc m :types (or (get-types-of-defs-map (:defs m))
                      [])))


(defn add-imports-of-definitions
  [m]
  {:pre [(:defs m)]
   :post [(:imports %)]}
  (assoc m :imports (or (get-imports-of-defs-map (:defs m))
                      [])))

(defn get-definitions-map
  [definitions-obj]
  (-> definitions-obj
      (jaxb/jaxb-type-obj->map-recursively {:map-ns "redo"
                                            :type-keyword :redo/entity-type
                                            :type-mappings domain-language/jaxb-mappings})))

(defn add-definitions-map
  [m]
  {:pre [(:defs-obj m)]}
  (assoc m :defs (get-definitions-map (:defs-obj m))))

(defn get-types-from-definitions-xml
  [xml-str]
  (-> {:xml-str xml-str}
      add-jaxb-context
      get-jaxb-of-str
      get-type-definition-maps))


(defn get-str-of-jaxb-obj
  [jaxb-obj]
  (-> {}
      add-jaxb-context
      (assoc :jaxb-obj jaxb-obj)
      add-marshaller
      add-marshalled-str
      :marshalled-str))


(defn convert-map-into-jaxb-type
  [m]
  {:pre [(:entity-data m) (:jaxb-type-obj m)]}
  (jaxb/map->jaxb-type-obj (:entity-data m)
                           (:jaxb-type-obj m)
                           {:type-keyword :redo/entity-type
                            :type-mappings (or (:type-mappings m)
                                               domain-language/jaxb-mappings)}))


(defn defs-map->str
  [m]
  (-> m
      (assoc :jaxb-type-obj (Definitions.))
      convert-map-into-jaxb-type
      add-marshaller
      add-marshalled-str
      :marshalled-str))


(defn get-informal-process-definition-in-xml
  [m]
  (->> (assoc m
              :jaxb-type-obj (TResourceDrivenProcessDefinition.)
              :object-factory (ObjectFactory.))
       convert-map-into-jaxb-type
       (.createInformalProcessDefinition (ObjectFactory.))
       get-str-of-jaxb-obj))


(defn get-map-of-xml-str
  [xml-str]
  (-> {:xml-str xml-str}
      get-jaxb-of-str
      (jaxb/jaxb-type-obj->map-recursively {:map-ns "redo"
                                            :type-keyword :redo/entity-type
                                            :type-mappings domain-language/jaxb-mappings})))

(defn get-map-of-jaxb-type-obj
  [obj]
  (-> obj
      (jaxb/jaxb-type-obj->map-recursively {:map-ns "redo"
                                            :type-keyword :redo/entity-type
                                            :type-mappings domain-language/jaxb-mappings})))


(defn get-map-of-xml-element-str
  [xml-str]
  (-> {:xml-str xml-str}
      get-jaxb-of-str
      (jaxb/jaxb-type-obj->map-recursively {:map-ns "redo"
                                            :type-keyword :redo/entity-type
                                            :type-mappings domain-language/jaxb-mappings})
      :value))

(defn get-node-template-of-map
  [m]
  (->> (assoc m
              :jaxb-type-obj (TNodeTemplate.)
              :object-factory (ObjectFactory.))
       convert-map-into-jaxb-type))

(defn get-relationship-template-of-map
  [m]
  (->> (assoc m
              :jaxb-type-obj (TRelationshipTemplate.)
              :object-factory (ObjectFactory.))
       convert-map-into-jaxb-type))


(defn get-process-definition-in-xml
  [m]
  (->> (assoc m :jaxb-type-obj (TResourceDrivenProcessDefinition.))
       convert-map-into-jaxb-type
       (.createProcessDefinition (ObjectFactory.))
       get-str-of-jaxb-obj))

(defn- add-process-id-and-target-namespace
  [m]
  (-> m
      domain-language/add-base-type-ns-and-name-into-map
      (clojure.set/rename-keys {:name :id})))

(defn get-relationship-and-node-templates-for-ipe-process-definition
  [xml-str]
  (->> {:entity-data (get-map-of-xml-str xml-str)}
       add-process-id-and-target-namespace
       t/get-matching-service-template
       (assoc {} :entity-data)
       t/get-relationsip-and-node-templates-of-service-template))
