(ns de.uni-stuttgart.iaas.ipsm.ipe.model.domain-language
  (:require [de.uni-stuttgart.iaas.ipsm.utils.constants :as constants]
            [de.uni-stuttgart.iaas.ipsm.utils.conversions :as conv]
            [de.uni-stuttgart.iaas.ipsm.protocols.core :as protocols]
            [uni-stuttgart.jaxb-conversions.core :as j]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.tosca :as to]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.schema :as ds]
            [clojure.spec.alpha :as sc]
            [taoensso.timbre :as timbre]
            [de.uni-stuttgart.iaas.ipsm.protocols.core :as protos])
  (:import [javax.xml.namespace QName]))



(timbre/refer-timbre)


(defn- create-standard-entity
  [entity-type entity-map]
  {:redo/entity-type entity-type :entity-map entity-map})

(defn- add-class-type
  [m]
  {:pre [(:entity-map m) (:redo/entity-type m) (:schema m)]}
  (-> m
      (assoc-in [:entity-map :redo/entity-type]
                (:redo/entity-type m))))

(defn validate-entity-map
  [m]
  {:pre [(:entity-map m) (:schema m)]}
  (if (sc/valid? (:schema m) (:entity-map m))
    (:entity-map m)
    (do (error "Invalid type" (:schema m) (:entity-map m))
        (error (sc/explain-str (:schema m) (:entity-map m)))
        nil)))

(defn add-data-type-and-validate
  [m]
  {:pre [(:redo/entity-type m) (:schema m) (:entity-map m)]}
  (-> m
      add-class-type
      validate-entity-map))

(defn resource-definition
  "A resource definition adressing TOSCA NodeTypes"
  [m]
  (add-data-type-and-validate {:entity-map m :redo/entity-type :redo/node-type :schema :redo/node-type}))

(defn relationship-definition
  "A resource definition adressing TOSCA NodeTypes"
  [m]
  (add-data-type-and-validate {:entity-map m :redo/entity-type :redo/relationship-type :schema :redo/relationship-type}))

(defn artifact-type
  "A resource definition adressing TOSCA NodeTypes"
  [m]
  (add-data-type-and-validate {:entity-map m :redo/entity-type :redo/artifact-type :schema :redo/tosca-artifact-type}))


(defn artifact-reference
  [m]
  (add-data-type-and-validate {:entity-map m :redo/entity-type :redo/artifact-reference :schema :redo/artifact-reference}))

(defn artifact-template
  [m]
  (add-data-type-and-validate {:entity-map m :redo/entity-type :redo/artifact-template :schema :redo/artifact-template}))


(defn deployment-artifact
  [m]
  (add-data-type-and-validate {:entity-map m :redo/entity-type :redo/deployment-artifact :schema :redo/single-deployment-artifact}))


(defn implementation-artifact
  [m]
  (add-data-type-and-validate {:entity-map m :redo/entity-type :redo/implementation-artifact :schema :redo/single-implementation-artifact}))


(defn node-type-implementation
  "A resource definition adressing TOSCA NodeTypes"
  [m]
  (add-data-type-and-validate {:entity-map m :redo/entity-type :redo/node-type-implementation :schema :redo/node-type-implementation}))

(defn relationship-type-implementation
  "A resource definition adressing TOSCA NodeTypes"
  [m]
  (add-data-type-and-validate {:entity-map m :redo/entity-type :redo/relationship-type-implementation :schema :redo/relationship-type-implementation}))

(defn node-template
  [m]
  (add-data-type-and-validate {:entity-map m :redo/entity-type :redo/node-template :schema :redo/node-template}))

(defn topology-template
  [m]
  (add-data-type-and-validate {:entity-map m :redo/entity-type :redo/topology-template :schema :redo/topology-template}))

(defn service-template
  [m]
  (add-data-type-and-validate {:entity-map m :redo/entity-type :redo/service-template :schema :redo/service-template}))


(defn definition
  [m]
  (add-data-type-and-validate {:entity-map m :redo/entity-type :redo/definition :schema :redo/definition-schema}))


(defn context-definition
  [m]
  (add-data-type-and-validate {:entity-map m :redo/entity-type :redo/context :schema :redo/context-definition}))

(defn intention-definition
  [m]
  (add-data-type-and-validate {:entity-map m :redo/entity-type :redo/intention :schema :redo/intention-definition}))

(defn definitions
  [m]
  (add-data-type-and-validate {:entity-map m :redo/entity-type :redo/definitions :schema :redo/definitions}))

(defn content
  [m]
  (add-data-type-and-validate {:entity-map m :redo/entity-type :redo/content :schema :redo/content}))


(defn instance-descriptor-type
  [m]
  (add-data-type-and-validate {:entity-map m :redo/entity-type :redo/instance-descriptor :schema :redo/instance-descriptor}))

(defn instance-descriptor-element
  [m]
  (add-data-type-and-validate {:entity-map m :redo/entity-type :redo/instance-descriptor-element :schema :redo/instance-descriptor}))

(defn resource-driven-process-definition
  [m]
  (add-data-type-and-validate {:entity-map m :redo/entity-type :redo/resource-driven-process-definition :schema :redo/resource-driven-process-definition}))


(defn resource-model
  [m]
  (add-data-type-and-validate {:entity-map m :redo/entity-type :redo/resource-model :schema :redo/resource-model}))

(defn interface
  [m]
  (add-data-type-and-validate {:entity-map m :redo/entity-type :redo/interface :schema :redo/interface}))

(defn operation
  [m]
  (add-data-type-and-validate {:entity-map m :redo/entity-type :redo/operation :schema :redo/single-operation}))

(defn execution-environment-integrator
  [m]
  (add-data-type-and-validate {:entity-map m :redo/entity-type :execution-environment-integrator :schema :redo/execution-environment-integrator}))

(defn operation-message
  [m]
  (add-data-type-and-validate {:entity-map m :redo/entity-type :operation-message :schema :redo/operation-message}))

(defn def-import
  [m]
  (add-data-type-and-validate {:entity-map m :redo/entity-type :redo/import :schema :redo/import}))

(defn domain-manager-data
  [m]
  (add-data-type-and-validate {:entity-map m :redo/entity-type :domain-manager-data :schema :redo/domain-manager-message}))




(defn add-base-type-ns-and-name-into-map
  [m]
  {:pre [(:entity-data m)]}
  (-> m
      (assoc :redo/target-namespace (:redo/target-namespace (:entity-data m)))
      (assoc :redo/name (:redo/name (:entity-data m)))))

(defn get-resources
  [m]
  {:pre [(= (:redo/entity-type (:entity-data m)) :redo/resource-driven-process-definition)]}
  (or (filterv #(= (:redo/entity-type %) :redo/node-template)
               (-> m
                   (update-in [:entity-data] #(get-in % [:redo/resource-model :redo/model-content]))
                   (assoc :redo/id (:ref (:entity-data m)))
                   (assoc :redo/target-namespace (:redo/target-namespace (:entity-data m)))
                   to/get-matching-service-template
                   to/get-relationsip-and-node-templates-of-service-template
                   :entity-data))
      []))

(defn get-relationships
  [m]
  {:pre [(= (:redo/entity-type (:entity-data m)) :redo/resource-driven-process-definition)]}
  (or (filterv #(= (:redo/entity-type %) :redo/relationship-template)
               (-> m
                   (update-in [:entity-data] #(get-in % [:redo/resource-model :redo/model-content]))
                   (assoc :redo/id (:ref (:entity-data m)))
                   (assoc :redo/target-namespace (:redo/target-namespace (:entity-data m)))
                   to/get-matching-service-template
                   to/get-relationsip-and-node-templates-of-service-template
                   (#(do (debug "Getting relationships %" %) %))
                   :entity-data))
      []))


(defn- add-matching-criteria
  [criteria m]
  (assoc m :match-criteria criteria))



(defn- add-updated-service-template
  [m]
  (debug "Adding updated service template")
  (-> m
      (assoc :model-id (:redo/entity-identity (ds/add-entity-identity-of-entity-data m)))
      (assoc :field-key :redo/service-template-or-node-type-or-node-type-implementation)
      ds/update-field-data-of-service-template-with-parent-process-id))


(defn replace-resources-or-relationships-of-informal-process
  "Replace resources and relationships based on matching criteria id
  and target-namespace of given new values "
  [m]
  {:pre [(= (:redo/entity-type (:entity-data m)) :redo/resource-driven-process-definition)]}
  (->> (assoc m :field-key :redo/model-content)
       ds/add-field-data
       :field-data

       (assoc m :entity-data)
       to/get-matching-service-template
       :entity-data
       (assoc m :service-template)
       (add-matching-criteria [:redo/id :redo/type])
       to/update-node-templates-of-service-template

       add-updated-service-template
       :entity-data

       (assoc m :entity-data)))


(defn add-informal-process-definitions
  [m]
  {:pre [(= (:redo/entity-type (:entity-data m)) :redo/resource-driven-process-definition)]}
  (debug "Adding definition of resource-driven process")
  (->> (assoc m :field-key :redo/model-content)
       ds/add-field-data
       :field-data
       (assoc m :definitions)))


(defn- filter-execution-environments
  [m]
  {:pre [(:execution-environment-integrators m)
         (:target-interface m)]}
  (debug "Filtering execution environments")
  (->> m
       :execution-environment-integrators
       (filterv #(some->> %
                          :interfaces
                          (mapv (fn [interface] (= (:redo/name (:target-interface m)) (:redo/name interface))))
                          (reduce (fn [v1 v2] (or v1 v2)))))))



(defn add-type-of-execution-environment-for-the-resource
  [m]
  {:pre [(:target-resource m)]}
  (some->> (assoc m :target-template (:target-resource m))
           add-informal-process-definitions
           to/add-type-of-a-template
           to/add-interfaces-of-node-type
           :interfaces
           (filterv #(.contains (:redo/name %) (constants/property :life-cycle-interface-postfix)))
           first
           (assoc m :target-interface)
           filter-execution-environments
           first
           (assoc m :matching-ee-integrator)))


(defn- add-implementation-type
  [m]
  {:pre [(:target-template m)]}
  (cond
    (= (:redo/entity-type (:target-template m)) :redo/node-template)
    (assoc m :implementation-type :node-type-implementation)

    (= (:redo/entity-type (:target-template m)) :redo/relationship-template)
    (assoc m :implementation-type :relationship-type-implementation)

    :default
    (assoc m :implementation-type :node-type-implementation)))


(defn add-execution-environment-using-tosca-persistence
  [m]
  {:pre [(:target-template m)
         (:tosca-persistence m)]
   :post [(:matching-ee-integrator %)]}
  (assoc m :matching-ee-integrator
         (some->> m
                  add-informal-process-definitions
                  to/add-type-of-a-template
                  add-implementation-type
                  (#(protocols/add-type-implementation (:tosca-persistence m) %))
                  to/add-implementation-artifacts-of-implementation
                  to/add-matching-artifact-template-ref
                  to/add-matching-artifact-type
                  (#(protocols/add-artifact-template (:tosca-persistence m) %))
                  :artifact-template
                  :redo/artifact-references
                  :redo/artifact-reference
                  first
                  :redo/reference)))


(defn add-execution-environment-using-data
  [m]
  {:pre [(:target-template m)]
   :post [(:matching-ee-integrator %)]}
  (assoc m :matching-ee-integrator
         (some->> m
                  add-informal-process-definitions
                  to/add-type-of-a-template
                  to/add-type-implementation-of-template
                  to/add-implementation-artifacts-of-implementation
                  to/add-matching-artifact-template-ref
                  to/add-matching-artifact-type
                  to/add-matching-artifact-template-from-ref-and-type
                  :artifact-template
                  :artifact-references
                  :artifact-reference
                  first
                  :reference)))

(defn add-execution-environment
  [m]
  {:pre [(:target-resource m)]}
  (debug "Checking the availablility of the tosca persistence" (:tosca-persistence m))
  (if (:tosca-persistence m)
    (add-execution-environment-using-tosca-persistence m)
    (add-execution-environment-using-data m)))


(defn resource-definition?
  [m]
  (= :redo/node-type (:redo/entity-type m)))

(defn relationship-type?
  [m]
  (= :redo/relationship-type (:redo/entity-type m)))


(defn add-deployable-uri-of-target-resource-using-tosca-persistence
  [m]
  {:pre [(:target-entity m)
         (:tosca-persistence m)
         (:deployable-artifact-type-name m)]}
  (debug "Adding deployable uri using tosca persistence")
  (assoc m :deployable-uri
         (some->> (assoc m
                         :target-template (:target-entity m)
                         :type-name (:deployable-artifact-type-name m))
                  add-informal-process-definitions
                  to/add-type-of-a-template
                  add-implementation-type
                  (#(protocols/add-type-implementation (:tosca-persistence m) %))
                  (#(do (debug "Adding type imp" [(:type-name %) (:implementation %)]) %))
                  to/add-deployment-artifact-based-on-type-local-name
                  (#(do (debug "Adding artifact type ref 1" (:deployment-artifact %)) %))
                  to/add-artifact-ref-from-deployment-artifact
                  (#(do (debug "Adding artifact type ref 2") %))
                  to/add-artifact-type-from-deployment-artifact
                  (#(do (debug "Adding artifact type") %))
                  (#(protocols/add-artifact-template (:tosca-persistence m) %))
                  (#(do (debug "Adding artifact template") %))
                  to/add-artifact-references
                  (#(do (debug "Adding artifact references") %))
                  :artifact-references
                  first
                  :redo/reference)))




(defn add-deployable-uri-of-target-resource-using-resource-model
  [m]
  {:pre [(:target-entity m)]}
  (debug "Adding deployable uri")
  (assoc m :deployable-uri
         (some->> (assoc m
                         :node-template (:target-entity m)
                         :type-name (constants/property :deployable-artifact-type-name))
                  add-informal-process-definitions
                  to/add-node-type-implementation-of-node-template
                  to/add-deployment-artifact-based-on-type-local-name
                  to/add-artifact-ref-from-deployment-artifact
                  to/add-artifact-type-from-deployment-artifact
                  to/add-matching-artifact-template-from-ref-and-type
                  to/add-artifact-references
                  :artifact-references
                  first
                  :reference)))


(defn add-deployable-uri-of-target-resource
  [m]
  (debug "Checking the availablility of the tosca persistence" (:tosca-persistence m))
  (if (:tosca-persistence m)
    (add-deployable-uri-of-target-resource-using-tosca-persistence m)
    (add-deployable-uri-of-target-resource-using-resource-model
     m)))

(defn- any-list->vector-if-neeeded
  [m]
  (update-in m [:any]
             #(if (instance? clojure.lang.Associative %)
                %
                (vector %))))


(defn- add-instance-descriptor-to-target-resource
  [m]
  {:pre [(:target-resource m) (:instance-descriptor m)]}
  (update-in m [:target-resource :redo/any] #(if %1 (conj %1 %2) [%2]) (:instance-descriptor m)))



(defn- add-new-resource-instance-state
  [m]
  {:pre [(:target-resource m)
         (:new-state m)
         (:redo/parent-instance m)]}
  (assoc m :target-resource
         (-> (assoc m :entity-data (:target-resource m))
             (assoc :redo/entity-type :redo/node-template)
             (assoc :field-key :redo/any)
             (assoc :new-data (assoc (-> m
                                         (assoc :entity-data (:target-resource m))
                                         (assoc :field-key :redo/any)
                                         (assoc :redo/entity-type :redo/node-template)
                                         ds/add-field-data-with-entity-identiy
                                         :field-data)
                                     :redo/instance-state (:new-state m)))
             ds/update-field-data-with-parent-process-id
             :entity-data)))


(defn update-resource-instance-state
  [m]
  {:pre [(:target-resource m)
         (:new-state m)
         (:redo/parent-instance m)]}
  (assoc m :target-resource
         (-> (assoc m :entity-data (:target-resource m))
             (assoc :field-key :redo/any)
             (assoc :new-data (assoc (-> m
                                         (assoc :entity-data (:target-resource m))
                                         (assoc :field-key :redo/any)
                                         ds/add-field-data-with-parent-process-id
                                         :field-data)
                                     :redo/instance-state (:new-state m)))
             ds/update-field-data-with-parent-process-id
             :entity-data)))





(defn add-created-instance-descriptor-for-resource-model-element
  [m]
  {:pre [(:resource-instance-id m)
         (:node-template-identity m)]
   :post [(:instance-descriptor %)]}
  (assoc m :instance-descriptor
         (instance-descriptor-type
          {:redo/identifiable-entity-definition {:redo/entity-identity (:resource-instance-id m)}
           :redo/instance-state :initializing
           :redo/start-time (or (:current-time m) (str (new java.util.Date)))
           :redo/source-model (:node-template-identity m)
           :redo/instance-uri (str (:uri m) "/instance-descriptors/" (conv/get-unique-id-from-entity-identity (:resource-instance-id m))) ;; must be updated
           :redo/importance (:importance m)
           :redo/parent-instance (:process-id m)
           :redo/propogate-success false
           :redo/propogate-failure true})))

(defn- add-new-resource-instance-id
  [m]
  {:pre [(:process-id m)
         (:target-resource m)]
   :post [(:resource-instance-id %)]}
  (debug (:target-resource m))
  (let [addition-time (str (new java.util.Date))]
    (-> m
        (assoc :current-time addition-time)
        (assoc  :resource-instance-id {:redo/target-namespace (.getNamespaceURI (QName/valueOf (:redo/type (:target-resource m))))
                                       :redo/name (str (:redo/name (:target-resource m))
                                                       "-"
                                                       addition-time
                                                       "-"
                                                       (conv/get-unique-id-from-entity-identity (:process-id m))
                                                       "/"
                                                       (conv/create-unique-id))}))))

(defn- add-node-template-identity
  [m]
  {:pre [(:target-resource m)]}
  (assoc m :node-template-identity {:redo/target-namespace (.getNamespaceURI (QName/valueOf (:redo/type (:target-resource m))))
                                    :redo/name (:redo/name (:target-resource m))}))

(defn add-new-instance-descriptor-for-resource-model-element
  [m]
  (->> m
       add-new-resource-instance-id
       add-node-template-identity
       add-created-instance-descriptor-for-resource-model-element))

(defn get-resource-instance-descriptor
  [m]
  {:pre [(:entity-data m)
         (:redo/parent-instance m)]}
  (-> m
      (assoc :field-key :redo/any)
      (assoc :redo/entity-type :redo/node-template)
      ds/add-field-data-with-parent-process-id
      :field-data))

(defn get-resource-state
  [m]
  (:redo/instance-state (get-resource-instance-descriptor m)))


(defn update-resource-instance-descriptor
  [m]
  {:pre [(:instance-descriptor m)
         (:entity-data m)
         (:redo/parent-instance m)]}
  (-> m
      (assoc :redo/entity-type :redo/node-template)
      (assoc :field-key :redo/any)
      (assoc :new-data (:instance-descriptor m))
      ds/update-field-data-with-parent-process-id))

(defn- resource->resource-instance
  [m]
  {:pre [(or (= :node-template (:redo/entity-type m)))]}
  (if (= :node-template (:redo/entity-type m))
    (into m {:any [(instance-descriptor-element {:redo/id (:redo/id m)
                                                 :redo/instance-state :initializable})]})
    m))


(defn resources->resource-instances
  [m]
  {:pre [(:entity-data m)]}
  (->> m
       get-resources
       (mapv resource->resource-instance)
       (assoc m :new-vals)
       replace-resources-or-relationships-of-informal-process))

(defn relationship->relationship-instances
  [m]
  {:pre [(get-in m [:entity-data :entity-map :resources])
         (vector? (get-in m [:entity-data :entity-map :resources]))]}
  (update-in m [:entity-data :entity-map :relationships] #(mapv resource->resource-instance %)))


(defn update-informal-process-resources-state
  [m]
  (->> m
       get-resources
       (mapv #(-> m
                  (assoc :target-resource %)
                  update-resource-instance-state
                  :target-resource))
       (assoc m :new-vals)
       replace-resources-or-relationships-of-informal-process))


(defn update-informal-process-instance-state
  [m]
  {:pre [(:new-state m) (:entity-data m)]}
  (-> m
      (assoc :field-key :redo/instance-descriptors)
      (assoc :new-data (assoc (-> m
                                  (assoc :field-key :redo/instance-descriptors)
                                  ds/add-field-data-with-entity-identiy
                                  :field-data)
                              :redo/instance-state (:new-state m)))
      ds/update-field-data-with-entity-identiy))


(defn add-main-intention
  [m]
  {:pre [(:entity-data m)]}
  (assoc m :target-intentions (:redo/target-intentions (:entity-data m))))

(defn- create-artifact-uri-reference
  [m]
  {:pre [(:uri m)
         (:type m)
         (:access-protocol-prefix m)
         (:deployable-path m)
         (:artifact-reference-uri-path m)]}
  (if (.startsWith (:uri m) (:access-protocol-prefix m))
    (str (if (.endsWith (str (:uri m)) "/")
           (str (:uri m))
           (str (str (:uri m)) "/"))
         (:deployable-path m)
         (str
          (conv/winery-encode (.getNamespaceURI (QName/valueOf (:artifact-reference-uri-path m))))
          "/"
          (conv/winery-encode (.getLocalPart (QName/valueOf (:artifact-reference-uri-path m))))))
    (str (:access-protocol-prefix m)
         (if (.endsWith (str (:uri m)) "/")
           (str (:uri m))
           (str (str (:uri m)) "/"))
         (:deployable-path m)
         (str
          (conv/winery-encode (.getNamespaceURI (QName/valueOf (:artifact-reference-uri-path m))))
          "/"
          (conv/winery-encode (.getLocalPart (QName/valueOf (:artifact-reference-uri-path m))))))))

(defn- create-artifact-references
  [m]
  [(artifact-reference
    {:redo/reference (create-artifact-uri-reference m)})])

(defn create-artifact-template
  [m]
  {:pre [(:artifact-template-name m)
         (:artifact-type m)
         (:artifact-template-id m)]}
  (artifact-template
   {:redo/id (:artifact-template-id m)
    :redo/name (:artifact-template-name m)
    :redo/type (:type m)
    :redo/artifact-references {:redo/artifact-reference (create-artifact-references m)}}))

(defn add-artifact-references
  [m]
  {:pre [(:artifact-template m)]}
  (->> m
       :artifact-template
       :artifact-references
       :artifact-reference
       (assoc m :artifact-references)))


(defn- add-artifact-templates
  [m]
  (->> m
       :node-types
       (mapv #(-> m
                  (assoc :node-type %)

                  create-artifact-template))
       (assoc m :artifact-templates)))


(defn add-artifact-template
  [m]
  {:post [(:artifact-template %)]}
  (assoc m :artifact-template (create-artifact-template m)))

(defn add-instance-descriptor-into-definition-model
  [m]
  {:pre [(:instance-descriptor m)
         (:entity-data m)]}
  (update-in m [:entity-data :redo/initializable-entity-definition :redo/instance-descriptors]
             #(if %1
                (conj %1 %2)
                (conj [] %2)) (:instance-descriptor m)))



(def jaxb-mappings
  {:redo/instance-descriptor-type "de.uni_stuttgart.iaas.ipsm.v0.TInstanceDescriptor"
   :redo/instance-descriptor-element "de.uni_stuttgart.iaas.ipsm.v0.InstanceDescriptor"
   :redo/content "de.uni_stuttgart.iaas.ipsm.v0.TContent"
   :redo/definitions "org.oasis_open.docs.tosca.ns._2011._12.TDefinitions"
   :redo/capability-type "org.oasis_open.docs.tosca.ns._2011._12.TCapabilityType"
   :redo/capability "org.oasis_open.docs.tosca.ns._2011._12.TCapability"
   :redo/definitions-element "org.oasis_open.docs.tosca.ns._2011._12.Definitions"
   :redo/definition "de.uni_stuttgart.iaas.ipsm.v0.TDefinition"
   :redo/context "de.uni_stuttgart.iaas.ipsm.v0.TContext"
   :redo/intention "de.uni_stuttgart.iaas.ipsm.v0.TIntention"
   :redo/informal-process-definition "de.uni_stuttgart.iaas.ipsm.v0.TInformalProcessDefinition"
   :redo/informal-process-instance "de.uni_stuttgart.iaas.ipsm.v0.TInformalProcessInstance"
   :redo/resource-model "de.uni_stuttgart.iaas.ipsm.v0.TResourceModel"
   :redo/resource-model-instance "de.uni_stuttgart.iaas.ipsm.v0.TResourceModelInstance"
   :redo/contexts "de.uni_stuttgart.iaas.ipsm.v0.TContexts"
   :redo/process-definition "de.uni_stuttgart.iaas.ipsm.v0.TProcessDefinition"
   :redo/service-template "org.oasis_open.docs.tosca.ns._2011._12.TServiceTemplate"
   :redo/topology-template "org.oasis_open.docs.tosca.ns._2011._12.TTopologyTemplate"
   :redo/node-template "org.oasis_open.docs.tosca.ns._2011._12.TNodeTemplate"
   :redo/relationship-template "org.oasis_open.docs.tosca.ns._2011._12.TRelationshipTemplate"
   :redo/node-type-implementation "org.oasis_open.docs.tosca.ns._2011._12.TNodeTypeImplementation"
   :redo/relationship-type-implementation "org.oasis_open.docs.tosca.ns._2011._12.TRelationshipTypeImplementation"
   :redo/implementation-artifact "org.oasis_open.docs.tosca.ns._2011._12.TImplementationArtifact"
   :redo/artifact-template "org.oasis_open.docs.tosca.ns._2011._12.TArtifactTemplate"
   :redo/artifact-reference "org.oasis_open.docs.tosca.ns._2011._12.TArtifactReference"
   :redo/artifact-references "org.oasis_open.docs.tosca.ns._2011._12.TArtifactTemplate$ArtifactReferences"
   :redo/deployment-artifact "org.oasis_open.docs.tosca.ns._2011._12.TDeploymentArtifact"
   :redo/interface "org.oasis_open.docs.tosca.ns._2011._12.TInterface"
   :redo/artifact-type "org.oasis_open.docs.tosca.ns._2011._12.TArtifactType"
   :redo/operation "org.oasis_open.docs.tosca.ns._2011._12.TOperation"
   :redo/input-parameters "org.oasis_open.docs.tosca.ns._2011._12.TOperation$InputParameters"
   :redo/output-parameters "org.oasis_open.docs.tosca.ns._2011._12.TOperation$OutputParameters"
   :redo/operation-messsage "de.uni_stuttgart.iaas.ipsm.v0.TOperationMessage"
   :redo/parameter "org.oasis_open.docs.tosca.ns._2011._12.TParameter"
   :redo/import "org.oasis_open.docs.tosca.ns._2011._12.TImport"
   :redo/relationship-type "org.oasis_open.docs.tosca.ns._2011._12.TRelationshipType"
   :redo/node-type "org.oasis_open.docs.tosca.ns._2011._12.TNodeType"
   :redo/instance-descriptor "de.uni_stuttgart.iaas.ipsm.v0.TInstanceDescriptor"

   :instance-descriptor-type "de.uni_stuttgart.iaas.ipsm.v0.TInstanceDescriptor"
   :instance-descriptor-element "de.uni_stuttgart.iaas.ipsm.v0.InstanceDescriptor"
   :content "de.uni_stuttgart.iaas.ipsm.v0.TContent"
   :definitions "org.oasis_open.docs.tosca.ns._2011._12.TDefinitions"
   :capability-type "org.oasis_open.docs.tosca.ns._2011._12.TCapabilityType"
   :capability "org.oasis_open.docs.tosca.ns._2011._12.TCapability"
   :definitions-element "org.oasis_open.docs.tosca.ns._2011._12.Definitions"
   :definition "de.uni_stuttgart.iaas.ipsm.v0.TDefinition"
   :context "de.uni_stuttgart.iaas.ipsm.v0.TContext"
   :intention "de.uni_stuttgart.iaas.ipsm.v0.TIntention"
   :informal-process-definition "de.uni_stuttgart.iaas.ipsm.v0.TInformalProcessDefinition"
   :informal-process-instance "de.uni_stuttgart.iaas.ipsm.v0.TInformalProcessInstance"
   :resource-model "de.uni_stuttgart.iaas.ipsm.v0.TResourceModel"
   :resource-model-instance "de.uni_stuttgart.iaas.ipsm.v0.TResourceModelInstance"
   :contexts "de.uni_stuttgart.iaas.ipsm.v0.TContexts"
   :process-definition "de.uni_stuttgart.iaas.ipsm.v0.TProcessDefinition"
   :service-template "org.oasis_open.docs.tosca.ns._2011._12.TServiceTemplate"
   :topology-template "org.oasis_open.docs.tosca.ns._2011._12.TTopologyTemplate"
   :node-template "org.oasis_open.docs.tosca.ns._2011._12.TNodeTemplate"
   :relationship-template "org.oasis_open.docs.tosca.ns._2011._12.TRelationshipTemplate"
   :node-type-implementation "org.oasis_open.docs.tosca.ns._2011._12.TNodeTypeImplementation"
   :relationship-type-implementation "org.oasis_open.docs.tosca.ns._2011._12.TRelationshipTypeImplementation"
   :implementation-artifact "org.oasis_open.docs.tosca.ns._2011._12.TImplementationArtifact"
   :artifact-template "org.oasis_open.docs.tosca.ns._2011._12.TArtifactTemplate"
   :artifact-reference "org.oasis_open.docs.tosca.ns._2011._12.TArtifactReference"
   :artifact-references "org.oasis_open.docs.tosca.ns._2011._12.TArtifactTemplate$ArtifactReferences"
   :deployment-artifact "org.oasis_open.docs.tosca.ns._2011._12.TDeploymentArtifact"
   :interface "org.oasis_open.docs.tosca.ns._2011._12.TInterface"
   :artifact-type "org.oasis_open.docs.tosca.ns._2011._12.TArtifactType"
   :operation "org.oasis_open.docs.tosca.ns._2011._12.TOperation"
   :input-parameters "org.oasis_open.docs.tosca.ns._2011._12.TOperation$InputParameters"
   :output-parameters "org.oasis_open.docs.tosca.ns._2011._12.TOperation$OutputParameters"
   :operation-messsage "de.uni_stuttgart.iaas.ipsm.v0.TOperationMessage"
   :parameter "org.oasis_open.docs.tosca.ns._2011._12.TParameter"
   :import "org.oasis_open.docs.tosca.ns._2011._12.TImport"
   :relationship-type "org.oasis_open.docs.tosca.ns._2011._12.TRelationshipType"
   :node-type "org.oasis_open.docs.tosca.ns._2011._12.TNodeType"})
