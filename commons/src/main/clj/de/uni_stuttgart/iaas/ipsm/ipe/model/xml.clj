(ns de.uni-stuttgart.iaas.ipsm.ipe.model.xml
  (:require [clojure.data.xml :as xml]
            [de.uni-stuttgart.iaas.ipsm.utils.conversions :as conversions]
            [de.uni-stuttgart.iaas.ipsm.utils.types :as t]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.domain-language :as domain-language]
            [taoensso.timbre :as timbre]
            [de.uni-stuttgart.iaas.ipsm.protocols.core :as protos]))


(timbre/refer-timbre)

(defn- convert-entity-type-to-keyword-if-not
  [m]
  {:pre [(:entity-type (:entity-data m))]}
  ;; (debug "Converting entiyt type" m )
  (update-in m [:entity-data :entity-type] #(if (keyword? %) % (keyword %))))

(defn- look-up-type
  [m]
  {:pre [(:entity-map (:entity-data m))]}
  ;; (debug "Look up type" m)
  (if (and (:xml-mappings m)
           (:type ((:entity-type (:entity-data m)) (:xml-mappings m))))
    (:type ((:entity-type (:entity-data m)) (:xml-mappings m)))
    (if (or (map? (:entity-map (:entity-data m)))
            (seq? (:entity-map (:entity-data m)))
            (vector? (:entity-map (:entity-data m))))
      :element
      :attribute)))

(defn- add-xml-type
  [m]
  {:pre [(:entity-map (:entity-data m))]}
  (->> m
       convert-entity-type-to-keyword-if-not
       look-up-type
       (assoc m :xml-type)))

(defn- create-xml-tag
  [m]
  {:pre [(:entity-type (:entity-data m))]}
  (if (and (:xml-mappings m) (:xml-tag ((:entity-type (:entity-data m)) (:xml-mappings m))))
    (:xml-tag ((:entity-type (:entity-data m)) (:xml-mappings m)))
    (name (:entity-type (:entity-data m)))))

(defn- add-xml-tag
  [m]
  (->> m
       convert-entity-type-to-keyword-if-not
       create-xml-tag
       (assoc m :xml-tag)))


(declare transform-entity-recursively)


(defn- create-xml-element
  [m]
  {:pre [(:entity-data m)]}
  (cond
    ;; First returned vector is attribute and the second is content
    ;; If contained type is a map, i.e., entity call parent func
    (map? (:entity-map (:entity-data m)))
    (xml/element (:xml-tag m) {}
                 (-> m
                     (assoc :entity-data (:entity-map (:entity-data m)))
                     transform-entity-recursively))
    ;; If contents are array
    (or (vector? (:entity-map (:entity-data m))) (seq? (:entity-map (:entity-data m))))
    (do
      ;; (debug "Entity contents are vector or seq" (:entity-map (:entity-data m)))
      (xml/element (:xml-tag (add-xml-tag m)) {}
                   (mapv
                    #(transform-entity-recursively (assoc m :entity-data %))
                    (:entity-map (:entity-data m)))))
    ;; Else it means it's a value. It can be either an element or an attribute
    :else
    (do
      ;; (debug "Entity is value,e.g., string xml simple type or attribute will be created" (:entity-data m))
      (xml/element (:xml-tag (add-xml-tag m)) {} (:entity-map (:entity-data m))))))


(defn- create-xml-attr
  [m]
  {:pre [(:xml-tag m)
         (:entity-map (:entity-data m))]}
  (vector [(:xml-tag m) (:entity-map (:entity-data m))]))


(defn- update-attr-element-vec
  [m]
  {:pre [(:attr-element-vec m)]}
  ;; (debug "Update attribute and element vector" m)
  (if (= :attribute (:xml-type m))
    (update-in m [:attr-element-vec 0] #(into % (create-xml-attr m)))
    (update-in m [:attr-element-vec 1] #(conj % (create-xml-element m)))))


(defn- add-elements-and-attributes-recursively
  [m current-property xml-attr-content]
  {:pre []}
  ;; (debug "Adding elements recursively:" m)
  ;; (debug "Current element is" current-property)
  (-> m
      (assoc-in [:entity-data :entity-map] (second current-property))
      (assoc-in [:entity-data :entity-type] (first current-property))
      (assoc :attr-element-vec xml-attr-content)
      add-xml-tag
      add-xml-type
      update-attr-element-vec
      :attr-element-vec))


(defn- create-xml-construct
  [m]
  {:pre [(:entity-map (:entity-data m))]}
  ;; (debug "Creating parent structure for the map" m)
  ;; (debug "Valid property count is" (filterv
  ;;                                   #(and (second %) (not= "" (second %))
  ;;                                         (or (not (vector? (second %))) (seq (second %)))
  ;;                                         (or (not (seq? (second %))) (seq (second %))))
  ;;                                   (:entity-map (:entity-data m))))
  (apply xml/element
         (:xml-tag m)
         (let [properties (filterv
                           #(and (second %) (not= "" (second %))
                                 (or (not (vector? (second %))) (seq (second %)))
                                 (or (not (seq? (second %))) (seq (second %))))
                           (:entity-map (:entity-data m)))
               property-count (count properties)]
           (loop [index 0
                  xml-attr-content [{} []]]
             (if (< index property-count)
               (recur (inc index)
                      (add-elements-and-attributes-recursively m
                                                               (get properties index)
                                                               xml-attr-content))
               (do
                 ;; (debug "Resulting xml attr content" xml-attr-content)
                 xml-attr-content))))))


(defn- add-entity-key-from-type
  [m]
  {:pre [(:entity-data m) (:entity-type (:entity-data m))]}
  (assoc m :entity-key (:entity-type (:entity-data m))))


(defn- add-initial-data-for-transformation
  [m]
  (-> m
      add-xml-type
      add-xml-tag))


(defn- transform-entity-recursively
  [m]
  ;; (debug "Following entity will be transformed recursively:" m)
  (-> m
      add-initial-data-for-transformation
      create-xml-construct))


(defn transform-into-xml
  [m]
  {:pre [(:entity-data m)
         (:entity-map (:entity-data m))
         (:entity-type (:entity-data m))]}
  (-> m
      transform-entity-recursively
      xml/indent-str))

(defn- find-first-matching-tag
  [m]
  {:pre [(:xml-mappings m) (:xml-tag m)]}

  (first
   (filterv
    #(= (:xml-tag m) (:xml-tag (second %)))
    (:xml-mappings m))))

(defn- find-matching-keyword
  [m]
  (-> m
      find-first-matching-tag
      first))

(defn- find-matching-type
  [m]
  ;; (debug "Matching type is:" (-> m
  ;;                                find-first-matching-tag
  ;;                                second
  ;;                                :type))
  (-> m
      find-first-matching-tag
      second
      :type))

(defn- add-entity-type
  [m]
  {:pre [(:entity-data m)]}
  (assoc-in m [:entity-data :entity-type]
            (if (:xml-mappings m)
              (or (-> m
                      (assoc :xml-tag (name (:tag (:current-tag m))))
                      find-matching-keyword)
                  (:tag (:current-tag m)))
              (:tag (:current-tag m)))))

(defn- add-domain-specific-entity-type
  [m]
  (assoc m :domain-specific-entity-type :vector))

(declare reverse-transform-entity-recursively)

(defn- create-value-property-tag
  [m]
  {:pre [(:current-tag m) (:type m)]}
  (if (seq (:content (:current-tag m)))
    (if (= 1 (count (:content (:current-tag m))))
      (if (map? (first (:content (:current-tag m))))
        (if (= :vector (:type m))
          [(reverse-transform-entity-recursively
            (assoc m :current-tag (first (:content (:current-tag m)))))]
          (reverse-transform-entity-recursively
           (assoc m :current-tag (first (:content (:current-tag m))))))
        (if (= :vector (:type m))
          [(first (:content (:current-tag m)))]
          (first (:content (:current-tag m)))))
      (mapv
       #(reverse-transform-entity-recursively (assoc m :current-tag %))
       (:content (:current-tag m))))
    (if (= :vector (:type m))
      []
      {})))

(defn- add-type-of-current-tag
  [m]
  (assoc m :type (-> m
                     (assoc :xml-tag (name (:tag (:current-tag m))))
                     find-matching-type)))

(defn- create-content-property-values
  [m]
  (if (seq (:content (:current-tag m)))
    (mapv
     #(vector (or (-> m
                      (assoc :xml-tag (name (:tag %)))
                      find-matching-keyword)
                  (:tag %))
              (-> m
                  (assoc :current-tag %)
                  add-type-of-current-tag
                  create-value-property-tag))
     (:content (:current-tag m)))
    []))

(defn- add-properties-from-content
  [m]
  {:pre [(:current-tag m)]}
  ;; (debug "Adding content" m)
  (assoc m :content-properties (create-content-property-values m)))

(defn- add-properties-from-attributes
  [m]
  {:pre [(:current-tag m)]}
  ;; (debug "Adding attributes" m)
  (assoc m :attr-properties (if (seq (:attrs (:current-tag m)))
                            (mapv
                             #(vector (or (-> m
                                              (assoc :xml-tag (name (first %)))
                                              find-matching-keyword)
                                          (first %))
                                      (second %))
                             (:attrs (:current-tag m)))
                            [])))

(defn- merge-attributes-and-content
  [m]
  {:pre [(:content-properties m) (:attr-properties m)]}
  ;; (debug "Merging maps" (:content-properties m) (:attr-properties m))
  (into {} (into (:content-properties m) (:attr-properties m))))


(defn- add-entity-map
  [m]
  (assoc-in m [:entity-data :entity-map]
            (-> m
                merge-attributes-and-content)))



(defn- reverse-transform-entity-recursively
  [m]
;;(debug "Reverse transforming the following map" m)
  (-> m
      add-entity-type
      add-domain-specific-entity-type
      add-properties-from-attributes
      add-properties-from-content
      add-entity-map
      :entity-data))

(defn- add-initial-current-tag
  [m]
  (assoc m :current-tag (if (:source-str m)
                          (xml/parse-str (:source-str m))
                          (xml/parse (:source-stream m)))))

(defn- add-empty-entity-data
  [m]
  (assoc m :entity-data {}))

(defn get-entities
  [m]
  {:pre [(or (:source-str m) (:source-stream m))]}
  (-> m
      add-empty-entity-data
      add-initial-current-tag
      reverse-transform-entity-recursively))
