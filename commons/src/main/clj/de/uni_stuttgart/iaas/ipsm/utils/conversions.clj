(ns de.uni-stuttgart.iaas.ipsm.utils.conversions
  (:require [clojure.data.json :as json]
            [ring.util.codec :as encoder]
            [digest :as digest]
            [clojure.spec.alpha :as sc]
            [taoensso.timbre :as timbre]))

(timbre/refer-timbre)


(defn read-json
  "Converts json to EDN. Parameter is in json format."
  [json-string]
  (json/read-str json-string :key-fn keyword))

(defn url-encode
  "URL-encodes one time the given url string"
  [url]
  (encoder/url-encode url "UTF-8"))

(defn url-decode
  "URL-decodes one time the given url string"
  [url]
  (encoder/url-decode url "UTF-8"))

(defn winery-encode
  "Winery compatible encoding. Currently two times encoding."
  [url]
  (url-encode (url-encode url)))

(defn winery-decode
  "Winery compatible encoding. Currently two times encoding."
  [url]
  (url-decode (url-decode url)))


(defn deserialize-from-str
  [str-to-desirialize]
  (read-string str-to-desirialize))

(defn serialize-to-str
  [obj-to-serialize]
  (str obj-to-serialize))

(defn copy-possible-fields
  "Copy all the keys of the source map into target map if the target map is not null"
  [src-map target-map]
  (let [src-keys (keys src-map)
        src-size (count src-keys)]
;    (debug "keys" src-keys)
;    (debug "size" src-size)
    (loop [index 0
           current-map target-map]
      (if (< index src-size)
        (let [current-key (nth src-keys index)
              src-value (get src-map current-key)
              target-value (get target-map current-key)]
 ;         (debug "index" index)
  ;        (debug "current-key" current-key)
   ;       (debug "src-value" src-value)
    ;      (debug "target-value" target-value)
     ;     (debug "current-map" target-map)
          (if (and (nil? target-value) (not (nil? src-value)))
            (recur (inc index)
                   (assoc current-map current-key (current-key src-map)))
            (recur (inc index)
                   current-map)))
        current-map))))

(defn create-unique-id
  "Converts the given string to a unique id"
  ([]
   (debug "random unique id will be created" (.substring (digest/sha-256 (str (System/nanoTime))) 0 24))
   (.substring (digest/sha-256 (str (System/nanoTime))) 0 24))) ;; only 24 to support mongo ids

(defn create-unique-id-from-str
  "Converts the given string to a unique id"
  ([str-arg]
   (.substring (digest/sha-256 str-arg) 0 24))
  ([name type]
   (.substring (digest/sha-256 (str name type)) 0 24)))

(defn get-unique-id-from-entity-identity
  "Converts the given string to a unique id"
  ([m]
   (if (and (:redo/name m) (:redo/target-namespace m))
     (.substring (digest/sha-256 (str (:redo/name m)
                                      (:redo/target-namespace m))) 0 24)
     nil)))

(defn remove-element-with-id-from-list
  "Removes a map with a certain id from a list"
  [identifiable-list id]
  (remove
   #(= id (:id %))
   identifiable-list))


(defn static? [field]
  (java.lang.reflect.Modifier/isStatic
   (.getModifiers field)))

(defn get-record-field-names [record]
  (->> record
       .getDeclaredFields
       (remove static?)
       (map #(.getName %))
       (remove #{"__meta" "__extmap"})))

(defmacro empty-record [record]
  (let [klass (Class/forName (name record))
        field-count (count (get-record-field-names klass))]
    `(new ~klass ~@(repeat field-count nil))))

(defn get-current-time-in-str
  []
  (.format (java.text.SimpleDateFormat. "yyyy-MM-dd'T'HH:mm:ss") (java.util.Date.)))


(defn entity-type-str>entity-type-keyword
  [m]
  (->> m
       (mapv #(vector (first %)
                      (if (= :entity-type (first %))
                        (if-not (keyword? (second %))
                          (keyword (second %))
                          (second %))
                        (cond
                          (map? (second %))
                          (entity-type-str>entity-type-keyword (second %))
                          (vector? (second %))
                          (mapv (fn [e] (entity-type-str>entity-type-keyword e)) (second %))
                          :else
                          (second %)))))
       (into {})))
