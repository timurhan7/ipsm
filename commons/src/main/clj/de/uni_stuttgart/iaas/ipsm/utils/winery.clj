(ns de.uni-stuttgart.iaas.ipsm.utils.winery
  (:require [taoensso.timbre :as timbre]
            [clojure.java.io :as io]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.transformations :as tx]
            [de.uni-stuttgart.iaas.ipsm.utils.constants :as c]
            [de.uni-stuttgart.iaas.ipsm.utils.conversions :as co]
            [clojure.data.json :as j]
            [clj-http.client :as http])
  (:import [java.net URI]
           [org.eclipse.winery.common Util]))


(timbre/refer-timbre)


(defn- upload-definitions-into-winery!
  [m]
  {:pre [(:winery-uri m) (:defs-str m)]}
  (debug "Posting maps")
  (http/post (:winery-uri m)
             {:content-type :xml
              :body (:defs-str m)}))


(defn upload-definitions-map-into-winery!
  [m]
  {:pre [(:defs m) (:defs-str m)]}
  (debug "Posting maps" m)
  (http/post (:winery-uri m)
             {:content-type :xml
              :body (tx/defs-map->str (:defs m))}))


;;TODO must be completed
(defn- upload-csar-into-winery!
  [m]
  {:pre [(:winery-uri m)
         (:csar m)]}
  (http/post (:winery-uri m)
             {:multipart [{:name "file" :content (:csar m)}
                          {:name "overwrite" :content true}]
              :body (:defs-str m)}))

(defn- imports?
  [m]
  {:pre [(:entity-data m)]
   :post [(:imports? %)]}
  (assoc m :imports? (:imports (:entity-data m))))


(defn- add-csar-name
  [m]
  {:post [(:csar-name %)]}
  (assoc m :csar-name (str (co/create-unique-id) ".csar")))


(defn- add-csar-folder-path
  [m]
  {:pre [(:csar-name m)]
   :post [(:csar-name-path %)]}
  (assoc m :csar-name-path (str "temp/" (:csar-name m))))


(defn- add-target-namespace-if-available
  [m]
  {:pre [(:entity-data m) (:entity-obj m)]}
  (if (:target-namespace (:entity-data m))
    (.setTargetNamespace (:entity-obj m) (:target-namespace (:entity-data m))))
  m)


(defn- add-definitions-str-for-types
  [m]
  (->> m
       :entity-data
       :types
       (apply tx/get-definitions-obj-with-types)
       (assoc m :entity-obj)
       add-target-namespace-if-available
       :entity-obj
       tx/get-str-of-jaxb-obj
       (assoc m :defs-str)))


(defn- prepare-for-addition
  [m]
  (-> m
      add-csar-name
      add-csar-folder-path
      add-definitions-str-for-types))


;; removes the types relevant to a domain manager data we delete them just before uploading the new ones to avoid the gap in between


(defn- post-types-into-winery!
  [m]
  (->> m
       :entity-data
       tx/get-definitions-xml-with-types
       (assoc m :defs-str)
       upload-definitions-into-winery!))


(defn- add-list-response
  [m]
  {:pre [(:winery-entity-path m)]}
  (debug "Winery entity path" (:winery-entity-path m))
  (try (assoc m :response
              (http/get (:winery-entity-path m)
                        {:accept :json}))
       (catch Exception e
         (error "List of entiries could not be retrieved!" (:winery-entity-path m) e)
         (assoc m :response "[]"))))

(defn- add-entity-list
  [m]
  {:pre [(:response m)]
   :post [(:entity-list %)]}
  (assoc m :entity-list (if (:body (:response m))
                          (j/read-json (:body (:response m)))
                          [])))


(defn- add-url-based-on-ns-id
  [m]
  {:pre [(:winery-entity-path m)
         (:id (:entity-data m))
         (:namespace (:entity-data m))]}
  (debug "Winery encode will be used"
         (:namespace (:entity-data m))
         (:id (:entity-data m)))
  (if-not (and (or (:namespace (:entity-data m))
                   (:target-namespace (:entity-data m)))
               (or (:id (:entity-data m))
                   (:name (:entity-data m))))
    (error "Namespace OR target-namespace = nil AND id or name = nil. This condition must evaluate to true " (:entity-data m)))
  (assoc m
         :url (str (:winery-entity-path m)
                     (let [qn (javax.xml.namespace.QName.
                               (:namespace (:entity-data m))
                               (:id (:entity-data m)))]
                       ;; Must be corrected too hacky
                       (str (Util/DoubleURLencode (.getNamespaceURI qn))
                            "/"
                            (Util/DoubleURLencode (.getLocalPart qn)))) ;; This encoding must be compatible with ids
                     "/")
         :id (:id (:entity-data m))
         :namespace (:namespace (:entity-data m))))


(defn- add-form-params-based-on-ns-id
  [m]
  (if-not (and (:namespace (:entity-data m))
               (:name (:entity-data m)))
    (error "Namespace OR target-namespace = nil AND id or name = nil. This condition must evaluate to true " (:entity-data m)))
  (assoc m :form-params (let [qn (javax.xml.namespace.QName.
                                  (:namespace (:entity-data m))
                                  (:name (:entity-data m)))]
                          {:name (.getLocalPart qn)
                           :namespace (.getNamespaceURI qn)})))


(defn- add-urls-for-entity-list
  [m]
  {:pre [(:entity-list m)]
   :post [(:urls %)]}
  (->> m
       :entity-list
       (mapv #(-> (assoc m :entity-data %)
                  add-url-based-on-ns-id
                  :url))
       (assoc m :urls)))


(defn- add-url->types
  [m]
  {:pre [(:url m)]
   :post [(:defs %)]}
  (debug "Fetching" (:url m))
  (assoc m :defs (let [resp (http/get (:url m) {:accept :xml})]
                   (debug "Result of fetch" (tx/get-types-from-definitions-xml (:body resp)))
                   (if (= (:status resp) 200)
                     (tx/get-types-from-definitions-xml (:body resp))
                     nil))))


(defn- add-urls->types
  [m]
  (assoc m :types (some->> m
                           :urls
                           (mapcat #(-> m
                                        (assoc :url %)
                                        add-url->types
                                        :defs))
                           (filterv identity))))


(defn- add-possible-entities-using-response
  [m]
  {:pre [(:response m)]
   :post [(:entity-data %)]}
  (cond-> m
    (= 200 (:status (:response m))) add-entity-list
    (= 200 (:status (:response m))) add-urls-for-entity-list
    (= 200 (:status (:response m))) add-urls->types
    (not= 200 (:status (:response m))) (assoc :entity-data [])))


(defn- add-winery-entity-type-path
  [m]
  {:pre [(:winery-uri m)
         (:redo/entity-type (:entity-data m))
         (:winery-path-mappings m)]}
  (debug "Adding entity path for" (:redo/entity-type (:entity-data m)) (get (:winery-path-mappings m)
                                                                            (:redo/entity-type (:entity-data m))))
  (assoc m :winery-entity-path (str (:winery-uri m)
                                    (get (:winery-path-mappings m)
                                         (:redo/entity-type (:entity-data m))))))



(defn get-entity-type-from-winery
  "Adds :types for a specific entity type such as :node-type. Entity
  type should be stored in {:entity-data {:entity-type :node-type}}"
  [m]
  (-> m
      add-winery-entity-type-path
      add-list-response
      add-entity-list
      add-possible-entities-using-response))


(defn- delete-entity!
  [m]
  {:pre [(:url m)]}
  (debug "Deleting on remote!" (:url m))
  (http/delete (:url m)))


(defn delete-entity-from-winery!
  [m]
  (-> m
      add-winery-entity-type-path
      add-url-based-on-ns-id
      delete-entity!))



(defn get-entity-from-winery
  [m]
  (-> m
      add-winery-entity-type-path
      add-url-based-on-ns-id
      add-url->types
      :defs
      first))

(defn- filter-namespaces-if-needed
  [m]
  {:pre [(:entity-list m)]
   :post [(:entity-list %)]}
  (debug "Entity list is" (:entity-list m) (:target-namespace m))
  (if (:target-namespace m)
    (->> m
         :entity-list
         (filterv #(= (:target-namespace m) (:namespace %)))
         (assoc m :entity-list))
    m))


(defn delete-entity-type-from-winery!
  [m]
  (->> m
       add-winery-entity-type-path
       add-list-response
       add-entity-list
       filter-namespaces-if-needed
       add-urls-for-entity-list
       :urls
       (mapv #(delete-entity! (assoc m :url %))))
  nil)


(defn delete-single-entity-from-winery!
  [m]
  (debug "Deleting single entity" (:entity-data m))
  (->> m
       add-winery-entity-type-path
       add-url-based-on-ns-id
       delete-entity!)
  nil)




(defn get-entity-winery-url
  [m]
  (->> m
       add-winery-entity-type-path
       add-url-based-on-ns-id))

(defn create-entity-in-winery!
  [m]
  (debug "Adding entity type" m)
  (->> m
       add-winery-entity-type-path
       add-form-params-based-on-ns-id
       (#(do
           (debug "Posting to the path" (:winery-entity-path %) "with contents" {:form-params (:form-params %)})
           (http/post
            (:winery-entity-path %)
            {:form-params (:form-params %)})))))

(defn delete-entity-types-from-winery!
  [m]
  (if (and (:entity-types m) (vector? (:entity-types m)))
    (->> m
         :entity-types
         (mapv #(delete-entity-type-from-winery! (assoc-in m [:entity-data :redo/entity-type] %))))
    (delete-entity-type-from-winery! m))
  nil)


(defn get-entity-types-from-winery
  [m]
  (if (and (:entity-types m) (vector? (:entity-types m)))
    (some->> m
             :entity-types
             (mapv #(-> m (assoc :entity-data {:entity-type %}) get-entity-type-from-winery :types))
             (reduce into)
             (remove empty?)
             (filterv identity)
             (assoc m :types))
    (get-entity-from-winery m)))

(defn- delete-domain-manager-relevant-types!
  [m]
  (debug "Deleting domain management relevant types" (:redo/target-namespace (:redo/entity-identity (:entity-data m))))
  (-> m
      (assoc :target-namespace (:redo/target-namespace (:redo/entity-identity (:entity-data m))))
      (assoc :entity-types [:artifact-type
                            :node-type
                            :node-type-implementation
                            :relationship-type-implementation
                            :relationship-type
                            :artifact-template])
      delete-entity-types-from-winery!))

(defn add-domain-manager-data-into-winery!
  [m]
  {:pre [(:entity-data m)]}
  (debug (:entity-data m))
  (let [m (prepare-for-addition m)]
    (delete-domain-manager-relevant-types! m)
    (cond
      (or (not (:imports (:entity-data m)))
          (not (seq (:imports (:entity-data m))))) (upload-definitions-into-winery! m)
      (seq (:imports (:entity-data m))) (upload-csar-into-winery! m))))

(defn- add-winery-url->csar-stream
  [m]
  {:pre [(:csar-url m)]
   :post [(:csar-stream %)]}
  (assoc m :csar-stream (io/input-stream (:winery-uri m))))

(defn- add-winery-url->csar-url
  [m]
  {:pre [(:url m)]
   :post [(:csar-url %)]}
  (assoc m :csar-url (if (.endsWith (:url m) "/")
                       (str (:url m) "?csar")
                       (str (:url m) "/?csar"))))


(defn add-tosca-id-into-entity-data
  [m]
  {:pre [(get-in m [:entity-data :name])]}
  (assoc-in m [:entity-data :id] (Util/makeNCName (get-in m [:entity-data :name]))))


(defn add-csar-stream
  [m]
  (-> m
      add-winery-entity-type-path
      add-url-based-on-ns-id
      add-winery-url->csar-url
      add-winery-url->csar-stream))


(defn add-resolved-xml-of-service-template
  [m]
  {:pre [(:service-template-uri m)]
   :post [(:defs %)]}
  (debug (:service-template-uri m))
  (assoc m :defs (let [resp (http/get (:service-template-uri m) {:accept :xml})]
                   (if (= (:status resp) 200)
                     (tx/get-map-of-xml-str (:body resp))
                     nil))))

(defn- add-if-matching-implementation
  [m]
  {:pre [(:candidate m)
         (:type m)]}
  (let [type-keyword (case (:redo/entity-type (:entity-data m))
                       :node-type-implementation :redo/node-type
                       :relationship-type-implementation :redo/relationship-type)]
    (debug "Candidate" (get (:candidate m) type-keyword) (:type m))
    (if (= (get (:candidate m) type-keyword) (:type m))
      (assoc m :match (:candidate m))
      m)))

(defn- add-if-matching-artifact-template
  [m]
  {:pre [(:candidate m)
         (:artifact-ref m)
         (:artifact-type m)]}
  (debug "Artifact template" [(:candidate m)
                              (:artifact-ref m)
                              (:artifact-type m)])
  (if (and (= (:redo/type (:candidate m)) (:artifact-type m))
           (.contains (:artifact-ref m) (:redo/id (:candidate m))))
    (assoc m :match (:candidate m))
    m))


(defn- fetch-iteratively-and-return-if-matched
  [m]
  {:pre [(:matcher-fn m)]}
  (let [size (count (:entity-list m))
        return-list m]
    (loop [index 0
           return-list m]
      (debug "In loop" size index (:match return-list))
      (if (and (< index size)
               (not (:match return-list)))
        (recur (inc index)
               (-> m
                   (assoc :entity-data (get (:entity-list m) index))
                   (assoc-in [:entity-data :redo/entity-type] (:redo/entity-type (:entity-data m)))
                   get-entity-from-winery
                   (#(assoc m :candidate %))
                   (#((:matcher-fn m) %))))
        return-list))))


(defn add-matching-artifact-template
  [m]
  (->> m
       add-winery-entity-type-path
       add-list-response
       add-entity-list
       (#(assoc % :matcher-fn add-if-matching-artifact-template))
       fetch-iteratively-and-return-if-matched
       :match
       (#(do (debug "Mathing artifact template is" %) %))
       (assoc m :artifact-template)))




(defn add-matching-type-implementation
  [m]
  (->> m
       add-winery-entity-type-path
       add-list-response
       add-entity-list
       (#(assoc % :matcher-fn add-if-matching-implementation))
       fetch-iteratively-and-return-if-matched
       :match
       (#(do (debug "Mathing type implementation is" %) %))
       (assoc m :implementation)))
