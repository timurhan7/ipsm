(ns de.uni-stuttgart.iaas.ipsm.utils.communications
  (:require [taoensso.timbre :as timbre]
            [langohr.core      :as rmq]
            [langohr.channel   :as lch]
            [langohr.queue     :as lq]
            [langohr.exchange  :as le]
            [langohr.consumers :as lc]
            [langohr.basic     :as lb]
            [clojure.java.io :as io]
            [clj-http.client :as client]
            [de.uni-stuttgart.iaas.ipsm.protocols.core :as protocols]
            [ring.adapter.jetty :refer [run-jetty]]
            [liberator.core :refer [resource defresource]]
            [ring.middleware.format-response :refer [wrap-clojure-response]]
            [ring.middleware.keyword-params :refer [wrap-keyword-params]]
            [ring.middleware.format-params :refer [wrap-clojure-params
                                                   wrap-json-params]]
            [ring.middleware.params :refer [wrap-params]]
            [ring.middleware.multipart-params :refer [wrap-multipart-params]]
            [compojure.core :refer [routes ANY]]
            [de.uni-stuttgart.iaas.ipsm.utils.constants :as constants]
            [de.uni-stuttgart.iaas.ipsm.utils.conversions :as conversions])
  (:import [java.net URI]))

(timbre/refer-timbre)


(defn add-connection-data
  [connection-map]
  (debug "Adding connection data!")
  (assoc connection-map :channel
         (lch/open (if (:connection-settings connection-map)
                     (rmq/connect (:connection-settings connection-map))
                     (rmq/connect)))))

(defn publish-message!
  [publish-data]
  "Needs a :channel, and :queue-name, :request"
  (debug "Message" (:request publish-data) "will be published in channel" (:channel publish-data) " in  queue name" (:queue-name publish-data))
  (lb/publish (:channel publish-data)
              (:queue-name publish-data)
              ""
              (str (:request publish-data))
              {:content-type "text/plain" :type (:message-type publish-data)}))



(defn subscribe-to-a-queue!
  [subscription-map]
  (debug "Subscribing to a queue with the following properties:"
         (:channel subscription-map)
         (:queue-name subscription-map)
         (:handler subscription-map))
  (lc/subscribe (:channel subscription-map)
                (:queue-name subscription-map)
                (:handler subscription-map))
  subscription-map)


(defn add-edn-message-body
  "Receives a message payload as byte array and converts it into EDN
  using UTF-8 adds it to :message-paylaod field of a map"
  [m payload]
  (debug (String. payload "UTF-8"))
  (debug (type payload))
  (assoc m :message-payload (read-string (String. payload "UTF-8"))))

(defn declare-queue-or-exchange!
  "Creates a queue or an exchange using :queue-name, :channel
  keywords, if :binding is available it's also bound. Exchange is
  created in case :exchange keyword is there"
  [queue-map]
  {:pre [(:queue-name queue-map)(:channel queue-map)]}
  (debug "Declaring queue with the following config" queue-map)
  (if (:exchange queue-map)
    (le/declare (:channel queue-map)
                (:queue-name queue-map)
                (:exchange queue-map))
    (lq/declare (:channel queue-map)
                (:queue-name queue-map)
                {:exclusive false :auto-delete true}))
  queue-map)

(defn bind-queue-to-exchange!
  [queue-map]
  "Binds a queue to an exchange. The input map must
  have :channel :queue-name :binding"
  (debug "Binding queue to exchange  with the following config"
         "Channel" (:channel queue-map)
         "Queue name" (:queue-name queue-map)
         "Binding" (:binding queue-map))
  (lq/bind (:channel queue-map)
           (:queue-name queue-map)
           (:binding queue-map))
  queue-map)


(def ^:private queue-map
  {:resource-list-queue "default-resource-list-queue"
   :resource-queue "default-resource-queue"
   :resource-requestor-queue "default-resource-requestor-queue"
   :ee-announcement-queue "default-ee-announcement-queue"})

(defn get-queue-name
  [queue-key]
  (or (constants/property queue-key) (queue-key queue-map)))


(defn get-resource-list-request
  []
  {:request-type :list-resources
   :id (conversions/create-unique-id)})

(defn get-resource-request
  []
  {:request-type :resource
   :id (conversions/create-unique-id)})



(defn create-exchanges!
  "creates the exchanges given in the list of exchanges"
  [m]
  (debug "Exchanges are created!")
  (mapv
   #(declare-queue-or-exchange! (assoc m
                                       :exchange "fanout"
                                       :queue-name (:queue-name %)))
   (:exchanges m)))

(defn create-service-specific-queue-name
  [queue-name service-suffix]
  (str queue-name service-suffix))

(defn create-queues-and-bind-them-to-exchanges!
  "creates the exchanges given in the list of exchanges"
  [m]
  {:pre [(:service-suffix m) (:exchanges m)]}
  (mapv
   #(doto (assoc m
                 :queue-name (create-service-specific-queue-name (:queue-name %) (:service-suffix m))
                 :binding (:queue-name %))
      declare-queue-or-exchange!
      bind-queue-to-exchange!)
   (:exchanges m)))

(defn subscribe-to-queues!
  "creates the exchanges given in the list of exchanges"
  [m]
  (mapv
   #(if (:handler %)
        (doto (assoc m
                 :queue-name (create-service-specific-queue-name (:queue-name %) (:service-suffix m))
                 :handler (:handler %))
          subscribe-to-a-queue!))
   (:exchanges m))
  m)

(defn create-response
  [request-map]
  ;(debug "Request map" request-map)
  (debug "Creating response" )
  (debug "Creating response for the following body" (type (:response-data request-map)))
  (debug "Creating response for the following body" (:response-data request-map))
  (assoc request-map :request
         {:response (:response-data request-map)
          :response-format (:response-format request-map)
          :source-name (.getName (:metadata request-map))
          :domain-uri (str (.getTargetNamespace (:metadata request-map)))
          :correlation-id (:correlation-id (:request-payload request-map))}))

(defn create-request
  [request-type request-body]
  {:type request-type :body request-body})



(defn- create-route
  [route-config opts]
  {:pre [(:resource-path route-config)
         (:allowed-methods route-config)
         (if (some #{:get} (:allowed-methods route-config)) (:handle-ok route-config) true)
         (if (some #{:post} (:allowed-methods route-config)) (:post! route-config) true)
         (if (some #{:put!} (:allowed-methods route-config)) (:put! route-config) true)]}
  (debug "Context path is" (:context-path opts))
  (ANY (if (:context-path opts)
         (str (:context-path opts) (:resource-path route-config))
         (:resource-path route-config)) request
       (resource route-config)))


(defn add-routes
  [m]
  {:pre [(:route-configs (:resource-config m)) (seq (:route-configs (:resource-config m)))]}
  (debug "Adding routes for the following config" m)
  (assoc m :routes (-> (apply routes (mapv #(create-route % m) (get-in m [:resource-config :route-configs])))
                       wrap-keyword-params
                       wrap-params
                       wrap-multipart-params
                       wrap-clojure-params
                       wrap-clojure-response
                       wrap-json-params)))

(defn run-server!
  [m]
  {:pre [(:host m) (:port m) (:routes m)]}
  (debug "Starting REST request handler" [(:host m) (:port m) (:routes m)])
  (some-> (:routes m)
          wrap-keyword-params
          wrap-params
          wrap-multipart-params
          wrap-clojure-params
          wrap-clojure-response
          wrap-json-params
          (run-jetty m)
          (#(do (debug "Result value" %) %))))

(defn run-server-detached!
  [m]
  (run-server! (assoc m :join? false)))

(defn download-deployable-via-http!
  [m]
  {:pre [(:deployable-uri m) (:file-path m)]}
  (debug "Downloading the deployable on the path" (:deployable-uri m)
         {:form-params {:target-intention (:target-intention m)}
          :accept [constants/deployable-media-type constants/dipea-media-type] :as :byte-array})
  (debug "Saving the deployable on the path" (:file-path m))
  (io/copy
   (:body (client/post
           (:deployable-uri m)
           {:form-params {:target-intention (:target-intention m)}
            :accept [constants/deployable-media-type constants/dipea-media-type] :as :byte-array}))
   (io/file (:file-path m))))


(defn download-deployable!
  [m]
  {:pre [(:deployable-uri m)
         (:deployable-file m)]}
  (debug "Downloading deployable!")
  (let [deployment-provider (:deployable-uri m)
        file-path (:deployable-file m)]
    (if deployment-provider
      (try
        (let [deployment-provider-uri (URI/create deployment-provider)
              uri-scheme (.getScheme deployment-provider-uri)]
          (case uri-scheme
            "http" (download-deployable-via-http! (assoc m
                                                         :deployment-uri deployment-provider
                                                         :file-path file-path))
            (error "Unsupported transportation scheme in the deployment provider URI" uri-scheme)))
        (catch Exception e (error "Deployment provider cannot be downloaded!!" e)))
      (error "Deployment provider is not specified!!"))))

(defn download-deployables!
  [deployables download-directory]
  (debug "Following deployables will be downloaded" deployables)
  (debug "Download directory is" download-directory)
  (mapv
   #(download-deployable! % download-directory)
   deployables)
  nil)

(defn- return-file-stream
  [path id]
  (io/input-stream (io/file (str path "/" id))))

(defn default-id-based-file-handler
  [m]
  {:pre [(:file-path m) (:media-type m) (:file-suffix m)]}
  (debug "Default id based file handler will return" m)
  (fn [request]
    (let [media-type (get-in request [:representation :media-type])]
      (condp = media-type

        (:media-type m)
        (let [return-stream (return-file-stream (:file-path m) (str (conversions/winery-decode
                                                                                            (:id (:params (:request request))))
                                                                                           (:file-suffix m)))]
          (debug "Return stream is" return-stream)
          (if return-stream
            (do
              (debug "A deployable will be returned" return-stream)
              return-stream)
            "<html><h1>Deployable could not be generated</h1></html>"))

        "text/html"
        "<html><h1>You requested HTML</h1></html>"))))

(defn- download-war!
  [m]
  {:pre [(:war-download-uri m)
         (:war-path m)]}
  (debug "Downloading war from" (:war-download-uri m) "to the path (:war-path)")
  (io/copy (io/input-stream (:war-download-uri m)) (io/file (:war-path m))))

(defn- download-war-if-not-exists!
  [m]
  {:pre [(not= nil(:war-exists? m))]}
  (if-not (:war-exists? m)
    (download-war! m)))

(defn- add-war-exists?
  [m]
  {:pre [(:war-path m)]}
  (debug "File exists?" (.exists (io/file (:war-path m))))
  (assoc m :war-exists? (.exists (io/file (:war-path m)))))

(defn prepare-war-for-deployment!
  "Checks existence of a war file and downloads it if its not present
  in the given path"
  [m]
  (-> m
      add-war-exists?
      download-war-if-not-exists!))
