(ns de.uni-stuttgart.iaas.ipsm.utils.xml
  (:require [de.uni-stuttgart.iaas.ipsm.utils.conversions :as conversions]
            [clojure.xml :as xml-core]
            [clojure.java.io :as io]
            [taoensso.timbre :as timbre]
            [clojure.string :as str]
            [clojure.zip :as zip]
            [into-edn :refer [into-edn]]
            [clojure.data.zip.xml :as zf])
            (:import [java.io StringWriter ByteArrayInputStream]
                     [javax.xml.bind Marshaller JAXBContext]
                     [javax.xml.namespace QName]))

(timbre/refer-timbre)


(def document-builder
  (.newDocumentBuilder (doto (javax.xml.parsers.DocumentBuilderFactory/newInstance)
                         (.setNamespaceAware true)
                         (.setValidating true))))

(defn convert-string-to-doc
  [s]
  (.parse document-builder (ByteArrayInputStream. (.getBytes s java.nio.charset.StandardCharsets/UTF_8))))

(defn unmarshal-document
  ":context is required which is a JAXBObject
  :document an xml Document
  :target-class representing target jaxb class"
  [m]
  (.getValue (.unmarshal (.createUnmarshaller (:context m)) (:document m) (:target-class m))))

(defn- apply-transformation
  "Applies given transformation xslt to the document doc, both should be compiled "
  [doc xslt-fn ]
  (xslt-fn doc))

(defn- object-to-xml-marshaler
  "Jaxb object to xml transformer for the specified type"
  [object-type & object-types]
  (doto (.createMarshaller
         (JAXBContext/newInstance (apply into-array object-type object-types)))
        (.setProperty Marshaller/JAXB_FORMATTED_OUTPUT true)))

(defn convert-jaxb-to-xml-string
  "Create and return a JAXB object representation of an IPE model. Accepts a JAXB object and the type of that object, i.e., .class"
  [model-obj model-obj-type]
  (let [out (StringWriter.)]
    (.marshal (object-to-xml-marshaler model-obj-type) model-obj out)
    (.toString out)))

(defn convert-jaxb-to-xml-edn
  "Create and return a JAXB object representation of an IPE model. Accepts a JAXB object and the type of that object, i.e., .class"
  [model-obj model-obj-type]
  (xml-core/parse (convert-jaxb-to-xml-string model-obj model-obj-type)))

(defn parse-xml
  "Parses given string by converting it to a ByteArrayinputstream"
  [str]
  (clojure.xml/parse
   (java.io.ByteArrayInputStream. (.getBytes str))))

(defn query
  "Make an xml query in the given document. The first argument is the query string and the second is the query document in string format."
  [query-str document]
  ;; (saxon/query (saxon/compile-xpath query-str) (saxon/compile-xml document))
  )

(defn apply-xsl-transform
  "Applies an XSL transformation to a XML document. Both arguments are strings."
  [xsl document]
  ;; (.toString (apply (saxon/compile-xslt xsl) (saxon/compile-xml document)))
  )

(defn qnames-are-equal?
  "checks if two given QNames are identical by comparing their namespace URI and localparts"
  [qname1 qname2]
  (let [first-uri (.getNamespaceURI qname1)
        second-uri (.getNamespaceURI qname2)
        first-localname (.getLocalPart qname1)
        second-localname (.getLocalPart qname2)]
    (and (= first-uri second-uri) (= first-localname second-localname))))

(defn create-qname-obj
	    "Creates a QName Object based on input values, i.e., namespace localname or prefix."
     ([namespace localname prefix]
       (QName. namespace localname prefix))
     ([namespace localname]
       (QName. namespace localname))
     ([qname-as-str]
      (QName/valueOf qname-as-str)))

(defn get-qname-using-prefix-look-up
  "Uses the prefix look-up function provided. The parameters are local-name, prefix, prefix look-up function."
  [localname prefix prefix-look-up-fn]
  (create-qname-obj (apply prefix-look-up-fn [prefix]) localname prefix))

;; (defn get-qname-using-def-prefix-look-up
;;   "Uses the default prefix look-up function provided in constants namespace. The parameters are local-name, prefix, prefix look-up function."
;;   [localname prefix]
;;   (debug "Default look up function will be used to resolve the following prefix" prefix)
;;   (get-qname-using-prefix-look-up localname prefix constants/resolve-uri-of-prefix))

(defn get-qname-from-string
  "Parses QName from a string and returns a Java QName. String should be in the following format {namespace}localname"
  [string]
  (QName/valueOf string))

(defn resolve-qname-query
  "Resolve the qname of the given qname type"
  [qname-str element]
  (str "resolve-QName('" qname-str "'," element ")"))


(defn select-first-containing-element
  "A query to select the first element that contains the given attribute value"
  [attr-val]
  (str "(//*[@*='" attr-val "'])[1]"))

(defn resolve-qname
  "Resolves a given qname String using xpath resolve-Qname(qname,element) function"
  [qname document]
  (let [saxon-qname (query (resolve-qname-query qname (select-first-containing-element qname)) document)]
    (QName.
     (.getNamespaceURI saxon-qname)
     (.getLocalName saxon-qname)
     (.getPrefix saxon-qname)) ))


(defn fetch-qname-from-element-map
  "Convert prefix:localname into {uri}localname based on the uri information derived from the element map. "
  [attr]
  ;;(debug "Namespace attr" attr)
  (fn [loc]
    (if (zf/attr loc attr)
      (let [ncname (zf/attr loc attr)
            prefix (first (str/split ncname #":"))
            localname (last (str/split ncname #":"))
            uri (zf/attr loc (keyword (str "xmlns" ":" prefix)))]
        (.toString (create-qname-obj uri localname))))))

(defn- convert-boolean-string
  "converts a boolean string to boolean type, nil is false"
  [boolean-str]
  ;(debug "if " boolean-str " = " "true" (= boolean-str "true"))
  (if (= boolean-str "true")
    true
    false))


(defn fetch-boolean-str
  "Returns a function which fetches the attr from the given loc and converts it to a boolean value, false & nil => false else true"
  [attr]
  (fn [loc]
    ;;(debug "Attribute is " attr "result of the query is" (zf/attr loc attr))
    (convert-boolean-string (zf/attr loc attr))))


(defn create-edn-map-from-xml
  "Creates edn map for the given xml string based on the xml spec given"
  [xml-string xml-spec]
  (into-edn xml-spec (some-> xml-string
                             parse-xml
                             zip/xml-zip)))


;;has taken from http://ravi.pckl.me/short/functional-xml-editing-using-zippers-in-clojure/

(defn tree-edit
  "Take a zipper, a function that matches a pattern in the tree,
   and a function that edits the current location in the tree.  Examine the tree
   nodes in depth-first order, determine whether the matcher matches, and if so
   apply the editor."
  [zipper matcher editor]
  (loop [loc zipper]
    (if (zip/end? loc)
      (zip/root loc)
      (if-let [matcher-result (matcher loc)]
        (let [new-loc (zip/edit loc editor)]
          (if (not (= (zip/node new-loc) (zip/node loc)))
            (recur (zip/next new-loc))))
        (recur (zip/next loc))))))


;; clojure xml map oprations

(defn get-attr
  "return given attribute, given attribute is given as string"
  [xml-map attribute-name]
  (get (:attrs xml-map) attribute-name))

(defn set-attr
  "return given attribute, given attribute is given as string"
  [xml-map attribute-name value]
  (assoc (:attrs xml-map) (keyword attribute-name) value))



;
;; Currently assumed that only one capability is defined in an resource, which is a simplification.
;(defn capability-matcher
; [loc]
;   (let [current-tag (:tag (zip/node loc))]
;     (if (= :Resource current-tag) ;; if resource has been found
;       (let [child-node (zip/down loc)]
;         (if (= (:tag (zip/ child-node) "Capability"))))
;       false)))

;; edit function
;(defn editor [node]
;  (let [capability-node (zip/node node)
;        resource-node-type (get capability-list (keyword (conversions/url-encode (resolve-qname (:type (:attrs capability-node))))))
;        resource-node (zip/up (zip/up node))
;        resource-node-attrs (:attrs resource-node)
;        updated-attr-list (assoc (assoc resource-node-attrs :type resource-node-type) :realizationDomain (:oTosca constants/available-ns))]
;    (assoc-in resource-node [:content] new-content)))



;
;(def edited (tree-edit books-zipper match-book? editor))  


