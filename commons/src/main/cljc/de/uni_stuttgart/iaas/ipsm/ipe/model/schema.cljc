(ns de.uni-stuttgart.iaas.ipsm.ipe.model.schema
  (:require [clojure.spec.alpha :as sc])
  #?(:clj (:import [java.net URI])))



;; Single entities
(sc/def :redo/entity-type  keyword?)
(sc/def :redo/name string?)
(sc/def :redo/target-namespace (sc/or :s string? :k keyword?))
(sc/def :redo/definition-language string?)
(sc/def :redo/value string?)
(sc/def :redo/element string?)
(sc/def :redo/type string?)
(sc/def :redo/required string?)
(sc/def :redo/start-time string?)
(sc/def :redo/end-time string?)
(sc/def :redo/instance-state (sc/or :s string? :k keyword?))
(sc/def :redo/instance-uri string?)

(sc/def :redo/type-ref string?)
(sc/def :redo/id string?)
(sc/def :redo/abstract boolean?)
(sc/def :redo/final boolean?)
(sc/def :redo/priority (sc/and int? #(<= 10 %) #(>= 0 %)))
(sc/def :redo/due string?)
(sc/def :redo/participant-ref string?)
(sc/def :redo/participant-type string?)

(sc/def :redo/operation-type string?)
(sc/def :redo/preconditioner-relationship (sc/coll-of :redo/relationship-template))
(sc/def :redo/preconditioner-relationships (sc/keys :req-un [:redo/preconditioner-relationship]))

(sc/def :redo/dependency (sc/coll-of :redo/node-template))
(sc/def :redo/dependencies (sc/keys :req-un [:redo/dependency]))

(sc/def :redo/abstract #{"yes" "no"})
(sc/def :redo/final #{"yes" "no"})
(sc/def :redo/required #{"yes" "no"})
(sc/def :redo/derived-from string?)
(sc/def :redo/properties-definition (sc/keys :opt [:redo/element :redo/type]))
(sc/def :redo/tag (sc/coll-of (sc/keys :opt [:redo/name :redo/value])))
(sc/def :redo/tags (sc/keys :req [:redo/tag]))
(sc/def :redo/pattern string?)
(sc/def :redo/reference string?)

(sc/def :redo/include (sc/keys :req [:redo/pattern]))
(sc/def :redo/exclude (sc/keys :req [:redo/pattern]))

(sc/def :redo/extensible-elements (sc/keys :opt [:redo/any
                                                 :redo/documentation
                                                 :redo/import
                                                 :redo/other-attributes]))

(sc/def :redo/parameter (sc/merge :redo/extensible-elements (sc/keys :req [:redo/name
                                                                           :redo/type
                                                                           :redo/required])))

(sc/def :redo/input-parameter (sc/coll-of :redo/parameter))
(sc/def :redo/output-parameter (sc/coll-of :redo/parameter))

(sc/def :redo/input-parameters (sc/keys :req [:redo/input-parameter
                                              :redo/entity-type]))

(sc/def :redo/output-parameters (sc/keys :req [:redo/output-parameter
                                               :redo/entity-type]))

(sc/def :redo/single-operation (sc/merge :redo/extensible-elements
                                         (sc/keys :opt [:redo/input-parameters
                                                        :redo/output-parameters]
                                                  :req [:redo/name])))

(sc/def :redo/operation (sc/coll-of (sc/merge :redo/extensible-elements
                                              (sc/keys :opt [:redo/input-parameters
                                                             :redo/output-parameters]
                                                       :req [:redo/name]))))

(sc/def :redo/interface (sc/keys :opt [:redo/operation
                                       :redo/name]
                                 :req [:redo/entity-type]))

(sc/def :redo/interfaces (sc/keys :req [:redo/interface]))
(sc/def :redo/source-interfaces (sc/keys :req [:redo/interface]))
(sc/def :redo/target-interfaces (sc/keys :req [:redo/interface]))

(sc/def :redo/tosca-entity-type (sc/merge :redo/extensible-elements (sc/keys :req [:redo/name
                                                                                   :redo/target-namespace]
                                                                             :opt [:redo/tags
                                                                                   :redo/abstract
                                                                                   :redo/final
                                                                                   :redo/properties-definition
                                                                                   :redo/derived-from])))

(sc/def :redo/node-type (sc/merge :redo/tosca-entity-type
                                  (sc/keys :opt [:redo/req-unuirement-definitions
                                                 :redo/capability-definitions
                                                 :redo/instance-states
                                                 :redo/interfaces]
                                           :req [:redo/entity-type])))

(sc/def :redo/valid-source (sc/keys :req [:redo/type-ref]))
(sc/def :redo/valid-target (sc/keys :req [:redo/type-ref]))

(sc/def :redo/relationship-type (sc/merge :redo/tosca-entity-type
                                          (sc/keys :opt [:redo/valid-target
                                                         :redo/valid-source
                                                         :redo/instance-states
                                                         :redo/capability-definitions
                                                         :redo/source-interfaces
                                                         :redo/target-interfaces]
                                                   :req [:redo/entity-type])))

(sc/def :redo/tosca-artifact-type (sc/merge :redo/tosca-entity-type (sc/keys :req [:redo/entity-type])))

(sc/def :redo/artifact-reference (sc/keys :req [:redo/reference :redo/entity-type]
                                          :opt-un [:redo/include :redo/exclude]))

(sc/def :redo/tosca-entity-template (sc/merge
                                     :redo/extensible-elements
                                     (sc/keys :req [:redo/id
                                                    :redo/type
                                                    :redo/entity-type]
                                              :opt [:redo/properties
                                                    :redo/property-constraints])))



(sc/def :redo/min-instances number?)
(sc/def :redo/max-instances string?)

(sc/def :redo/source-element
  (sc/keys :ref [:redo/ref]))
(sc/def :redo/target-element
  (sc/keys :ref [:redo/ref]))

(sc/def :redo/node-template
  (sc/merge :redo/tosca-entity-template
            (sc/keys :opt [:redo/requirements
                           :redo/capabilities
                           :redo/policies
                           :redo/deployment-artifacts
                           :redo/name
                           :redo/min-instances
                           :redo/max-instances]
                     :req [:redo/entity-type])))

(sc/def :redo/relationship-template
  (sc/merge :redo/tosca-entity-template
            (sc/keys :opt [:redo/relationship-constraints
                           :redo/capabilities
                           :redo/policies
                           :redo/deployment-artifacts
                           :redo/name
                           :redo/min-instances
                           :redo/max-instances]
                     :req [:redo/entity-type
                           :redo/source-element
                           :redo/target-element])))

(sc/def :redo/single-deployment-artifact
  (sc/keys :req [:redo/name
                 :redo/artifact-type
                 :redo/artifact-ref
                 :redo/entity-type]))

(sc/def :redo/deployment-artifact
  (sc/coll-of :redo/single-deployment-artifact))

(sc/def :redo/deployment-artifacts
  (sc/keys :req [:redo/deployment-artifact]))


(sc/def :redo/single-implementation-artifact
  (sc/keys :req [:redo/interface-name
                 :redo/operation-name
                 :redo/artifact-type
                 :redo/artifact-ref
                 :redo/entity-type]))

(sc/def :redo/implementation-artifact
  (sc/coll-of :redo/single-implementation-artifact))

(sc/def :redo/implementation-artifacts
  (sc/keys :req [:redo/implementation-artificat]))


(sc/def :q/node-type string?)
(sc/def :q/relationship-type string?)

(sc/def :redo/node-type-implementation
  (sc/keys :req [:redo/name
                 :redo/target-namespace
                 :q/node-type
                 :redo/entity-type]
           :opt [:redo/abstract
                 :redo/final
                 :redo/tags
                 :redo/derived-from
                 :redo/required-container-features
                 :redo/implementation-artifacts
                 :redo/deployment-artifacts]))

(sc/def :redo/relationship-type-implementation
  (sc/keys :req [:redo/name
                 :redo/target-namespace
                 :q/relationship-type
                 :redo/entity-type]
           :opt [:redo/abstract
                 :redo/final
                 :redo/tags
                 :redo/derived-from
                 :redo/required-container-features
                 :redo/implementation-artifacts]))

(sc/def :redo/operation-coverage
  (sc/keys :opt [:redo/name
                 :str/interface
                 :redo/operation-target]))

(sc/def :redo/artifact-template :redo/tosca-entity-template)

(sc/def :redo/execution-environment-integrator
  (sc/keys :req [:redo/entity-identity
                 :redo/resource-specific-type-groups
                 :redo/domain-specific-type-groups
                 :redo/uri
                 :redo/entity-type]
           :redo/opt-un [:redo/artifact-type
                         :redo/artifact-templates]))




(sc/def :redo/target-resource :redo/node-template)
(sc/def :redo/relationship-target-resource :redo/node-template)
(sc/def :redo/relationship-source-resource :redo/node-template)
(sc/def :redo/target-relationship  :redo/relationship-template)

(sc/def :redo/operation-message
  (sc/keys :req [:redo/process-id
                 :redo/operation-type
                 :redo/resource-driven-process-definition
                 :redo/entity-type]
           :redo/opt-un [:redo/dependencies
                         :redo/intention-definitions
                         :redo/preconditioner-relationships
                         :redo/target-resource
                         :redo/target-relationship
                         :redo/relationship-source-resource
                         :redo/relationship-source-resource-instance-descriptor
                         :redo/relationship-target-resource-instance-descriptor
                         :redo/instance-descriptor
                         :redo/instance-uri
                         :redo/tosca-model-repo-uri
                         :redo/error-definition]))

(sc/def :redo/imports
  (sc/coll-of (sc/keys :req [:redo/namespace
                             :redo/location
                             :redo/imported-type])))

(sc/def :dm/types
  (sc/coll-of :redo/node-type
              :redo/relationship-type
              :redo/node-type-implementation
              :redo/artifact-type
              :redo/artifact-template))

(sc/def :redo/domain-manager-message
  (sc/keys :req [:dm/types
                 :redo/entity-identity]
           :opt [:redo/imports]))


(sc/def :redo/entity-identity
  (sc/keys :req [:redo/name
                 :redo/target-namespace]))

(sc/def :redo/process-id :redo/entity-identity)

(sc/def :redo/source-model :redo/entity-identity)
(sc/def :redo/parent-instance :redo/entity-identity)


(sc/def :redo/entity-definition
  (sc/keys :req [:redo/definition-content
                 :redo/definition-language]
           :opt [:redo/properties]))

(sc/def :redo/entity-definitions
  (sc/coll-of :redo/entity-definition))

(sc/def :redo/identifiable-entity-definition
  (sc/keys :opt [:redo/entity-identity
                 :redo/source-identity
                 :redo/entity-definitions]))

(sc/def :redo/importance integer?)
(sc/def :redo/propogate-success boolean?)
(sc/def :redo/propogate-failure boolean?)

(sc/def :redo/instance-descriptor
  (sc/keys :re789q [:redo/instance-state
                 :redo/source-model
                 :redp/identifiable-entity-definition]
           :opt [:redo/start-time
                 :redo/end-time
                 :redo/instance-uri
                 :redo/importance
                 :redo/propogate-success
                 :redo/propogate-failure
                 :redo/parent-instance]))

(sc/def :redo/instance-descriptors
  (sc/coll-of :redo/instance-descriptor))

(sc/def :redo/initializable-entity-definition
  (sc/keys :req [:redo/identifiable-entity-definition]
           :opt [:redo/instance-descriptors]))

(sc/def :redo/participant
  (sc/keys :req [:redo/participant-ref]
           :opt [:redo/participant-type]))

(sc/def :redo/participant-list (sc/coll-of :redo/participant))
(sc/def :redo/interactive-entity-definition
  (sc/keys :req [:redo/identifiable-entity-definition]
           :opt [:redo/participant-list]))
(sc/def :redo/interactive-initializable-entity-definition
  (sc/keys :req [:redo/initializable-entity-definition]
           :opt [:redo/participant-list]))

(sc/def :redo/initial-context :redo/entity-identity)
(sc/def :redo/final-context :redo/entity-identity)

(sc/def :redo/intention-definition
  (sc/keys :req [:redo/interactive-initializable-entity-definition
                 :redo/entity-type]
           :opt [:redo/initial-context
                 :redo/final-context
                 :redo/required-capabilities
                 :redo/due]))

(sc/def :redo/organizational-capability (sc/coll-of string?))
(sc/def :redo/organizational-capabilities (sc/keys :req [:redo/organizational-capability]))

(sc/def :redo/required-intention (sc/coll-of string?))
(sc/def :redo/required-intentions (sc/coll-of :redo/entity-identity))

(sc/def :redo/operational-process (sc/coll-of string?))
(sc/def :redo/operational-processes (sc/keys :req [:redo/organizational-capability]))

(sc/def :redo/strategy-definition (sc/keys :req [:redo/interactive-initializable-entity-definition]
                                           :opt [:redo/required-intentions
                                                 :redo/organizational-capabilities
                                                 :redo/operational-processes]))

(sc/def :redo/strategy-definitions (sc/coll-of :redo/strategy-definition))


(sc/def :redo/context-definition
  (sc/keys :opt [:redo/contained-contexts]
           :req [:redo/interactive-entity-definition
                 :redo/entity-type]))

(sc/def :redo/context-definitions (sc/coll-of :redo/context-definition))

;; (sc/def :redo/capability-type number?)
(sc/def :redo/providing-resources (sc/coll-of string?))
(sc/def :redo/desired-resources (sc/coll-of string?))
(sc/def :redo/capability-definition
  (sc/keys :opt [:redo/capability-type
                 :redo/providing-resources
                 :redo/composing-capabilities]
           :req [:redo/interactive-entity-definition
                 :redo/entity-type]))

(sc/def :redo/capability-definitions (sc/map-of keyword? :redo/capability-definition))

(sc/def :redo/target-intentions
  (sc/coll-of :redo/entity-identity))

;; TODO one of these must exist in a resource model
(sc/def :redo/resource-model
  (sc/keys :opt [:redo/model-uri
                 :redo/model-namespace
                 :redo/model-id
                 :redo/model-content]))


(sc/def :redo/resource-driven-process-definition
  (sc/keys :opt [:redo/target-intentions
                 :redo/resource-model]
           :req [:redo/initializable-entity-definition
                 :redo/entity-type]))

(sc/def :redo/resource-driven-process-definitions
  (sc/map-of keyword? :redo/resource-driven-process-definition))


(sc/def :redo/organizational-processes (sc/coll-of :redo/resource-driven-process-definition))
(sc/def :redo/organizational-intentions (sc/coll-of :redo/intention-definition))
(sc/def :redo/organizational-contexts (sc/coll-of :redo/context-definition))
(sc/def :redo/organizational-capabilities (sc/coll-of :redo/capability-definition))


(sc/def :redo/organizational-definitions (sc/keys :req [:redo/identifiable-entity-definition]
                                                  :opt [:redo/organizational-capabilities
                                                        :redo/organizational-intentions
                                                        :redo/entity-type
                                                        :redo/organizational-contexts
                                                        :redo/organizational-processes]))

(sc/def :redo/entity-query (sc/keys :req [:redo/entity-type]
                                    :opt [:redo/entity-identity]))


(defn get-entity-identity-of-top-modeling-element
  [m]
  (cond
    (get-in m [:redo/initializable-entity-definition]) (get-in m [:redo/initializable-entity-definition :redo/identifiable-entity-definition :redo/entity-identity])
    (get-in m [:redo/identifiable-entity-definition]) (get-in m [:redo/identifiable-entity-definition :redo/entity-identity])
    (get-in m [:redo/interactive-initializable-entity-definition]) (get-in m [:redo/interactive-initializable-entity-definition :redo/initializable-entity-definition :redo/identifiable-entity-definition :redo/entity-identity])
    (get-in m [:redo/interactive-entity-definition]) (get-in m [:redo/interactive-entity-definition :redo/identifiable-entity-definition :redo/entity-identity])))


(defn add-new-instance-desc-to-instance-descriptors-of-initializable-entity-def
  [m]
  {:pre [(:entity-data m) (:new-instance-desc m)]}
  (cond
    (get-in m [:redo/initializable-entity-definition]) (update-in m [:redo/initializable-entity-definition :redo/instance-descriptors] conj (:new-instance-desc m))
    (get-in m [:redo/interactive-initializable-entity-definition]) (update-in m [:redo/interactive-initializable-entity-definition :redo/initializable-entity-definition :redo/instance-descriptors] conj (:new-instance-desc m))))


(def resources [{:name        "Resource-driven Process Definitions"
                 :redo/entity-type :redo/resource-driven-process-definition
                 :path        "/resource-driven-process-definitions"
                 :field-paths {:redo/entity-identity [:redo/initializable-entity-definition :redo/identifiable-entity-definition]
                               :redo/entity-definitions [:redo/initializable-entity-definition :redo/identifiable-entity-definition]
                               :redo/target-namespace [:redo/initializable-entity-definition :redo/identifiable-entity-definition :redo/entity-identity]
                               :redo/instance-descriptors [:redo/initializable-entity-definition]
                               :redo/instance-descriptor [:redo/initializable-entity-definition :redo/instance-descriptors]
                               :redo/name [:redo/initializable-entity-definition :redo/identifiable-entity-definition :redo/entity-identity]
                               :redo/model-content [:redo/resource-model]
                               :redo/model-namespace [:redo/resource-model]
                               :redo/model-id [:redo/resource-model]
                               :redo/model-uri [:redo/resource-model]
                               :redo/service-template-or-node-type-or-node-type-implementation [:redo/resource-model :redo/model-content]
                               :redo/target-intentions []}}
                {:name        "Goal Definitions"
                 :redo/entity-type :redo/intention-definition
                 :path        "/intentions-definitions"
                 :field-paths {:redo/entity-identity [:redo/initializable-entity-definition :redo/identifiable-entity-definition]
                               :redo/entity-definitions [:redo/initializable-entity-definition :redo/identifiable-entity-definition]
                               :redo/target-namespace [:redo/initializable-entity-definition :redo/identifiable-entity-definition :redo/entity-identity]
                               :redo/name [:redo/initializable-entity-definition :redo/identifiable-entity-definition :redo/entity-identity]
                               :redo/initial-context []
                               :redo/instance-descriptors [:redo/initializable-entity-definition]
                               :redo/final-context []
                               :redo/due []
                               :redo/required-capabilities []}}
                {:name        "Capability Definitions"
                 :redo/entity-type :redo/capability-definition
                 :path        "/capability-definitions"
                 :field-paths {:redo/entity-identity [:redo/initializable-entity-definition :redo/identifiable-entity-definition]
                               :redo/entity-definitions [:redo/initializable-entity-definition :redo/identifiable-entity-definition]
                               :redo/target-namespace [:redo/initializable-entity-definition :redo/identifiable-entity-definition :redo/entity-identity]
                               :redo/name [:redo/initializable-entity-definition :redo/identifiable-entity-definition :redo/entity-identity]
                               :redo/composing-capabilities []
                               :redo/providing-resources []}}
                {:name        "Instance Definitions"
                 :redo/entity-type :redo/instance-descriptor
                 :path        "/instance-descriptors"
                 :field-paths {:redo/entity-identity [:redo/identifiable-entity-definition]
                               :redo/entity-definitions [:redo/identifiable-entity-definition]
                               :redo/target-namespace [:redo/identifiable-entity-definition :redo/entity-identity]
                               :redo/parent-instance []
                               :redo/name [:redo/identifiable-entity-definition :redo/entity-identity]}}
                {:name        "Context Definitions"
                 :redo/entity-type :redo/context-definition
                 :path        "/contexts-definitions"
                 :field-paths {:redo/entity-identity [:redo/identifiable-entity-definition]
                               :redo/entity-definitions [:redo/identifiable-entity-definition]
                               :redo/target-namespace [:redo/identifiable-entity-definition :redo/entity-identity]
                               :redo/name [:redo/identifiable-entity-definition :redo/entity-identity]}}
                {:name        "Domain Manager Message"
                 :redo/entity-type :redo/domain-manager-message
                 :path        "/domain-manager-messages"
                 :field-paths {:redo/entity-identity []
                               :redo/target-namespace [:redo/entity-identity]
                               :redo/name [:redo/entity-identity]}}
                {:name        "Domain Manager Data"
                 :redo/entity-type :dm-data
                 :path        "/domain-managers"
                 :field-paths {:redo/entity-identity []
                               :redo/target-namespace [:redo/entity-identity]
                               :redo/name [:redo/entity-identity]}}
                {:name        "Execution Environment Message"
                 :redo/entity-type :execution-environment-integrator
                 :path        "/execution-environment-integrators"
                 :field-paths {:redo/entity-identity []
                               :redo/target-namespace [:redo/entity-identity]
                               :redo/name [:redo/entity-identity]}}
                {:name        "Initialization Resource Models"
                 :redo/entity-type :redo/initializing-informal-process-instance
                 :path        "/execution-environment-integrators"
                 :field-paths {:redo/entity-identity []
                               :redo/target-namespace [:redo/entity-identity]
                               :redo/name [:redo/entity-identity]}}
                {:name        "Failed Resource Models"
                 :redo/entity-type :redo/failed-process-instant
                 :path        "/execution-environment-integrators"
                 :field-paths {:redo/entity-identity []
                               :redo/target-namespace [:redo/entity-identity]
                               :redo/name [:redo/entity-identity]}}
                {:name        "Resource definitions"
                 :redo/entity-type :redo/node-template
                 :path        "/resource-definitions"
                 :field-paths {:redo/target-namespace []
                               :redo/any []}}
                {:name        "Relationship definitions"
                 :redo/entity-type :redo/relationship-template
                 :path        "/relationship-definitions"
                 :field-paths {:redo/target-namespace []
                               :redo/any []}}])



(defn- add-resource
  [m]
  {:pre [(:redo/entity-type m)
         (:resources m)]}
  (->> m
       :resources
       (filterv #(= (:redo/entity-type %) (:redo/entity-type m)))
       first
       (assoc m :resource)))

(defn- add-field-paths
  [m]
  (->> m
       :resource
       :field-paths
       (assoc m :field-paths)))

(defn- add-field-path
  [m]
  {:pre [(:field-key m)]}
  (assoc m :field-path (get (->> m
                                 add-resource
                                 :resource
                                 :field-paths)
                            (:field-key m))))

(defn- append-field-path-with-field-key
  [m]
  {:pre [(:field-key m)
         (:field-path m)]}
  (->> m
       :field-key
       (conj (:field-path m))
       (assoc m :field-path)))

(defn- append-index-data-if-applicable
  [m]
  {:pre [(:field-key m)
         (:field-path m)]}
  (if (:field-index m)
    (->> m
         :field-index
         (conj (:field-path m))
         (assoc m :field-path))
    m))

(defn- prepend-field-path-with-entity-data
  [m]
  {:pre [(:field-path m)]}
  (update-in m [:field-path] #(into [:entity-data] %)))

(defn add-field-data
  [m]
  (->> m
       (#(assoc % :redo/entity-type (keyword (or (:redo/entity-type (:entity-data %))
                                                 (:redo/entity-type %)))))
       add-field-path
       append-field-path-with-field-key
       prepend-field-path-with-entity-data
       :field-path
       (get-in m)
       (assoc m :field-data)))


(defn add-update-fn
  [m]
  {:post [(:update-fn %)]}
  (assoc m :update-fn (if (:field-index m)
                        conj
                        assoc)))

(defn add-eval-value-of-update-fn
  [m]
  {:pre [(:new-data m)]}
  (assoc m :update-fn (if (:field-index m)
                        conj
                        assoc)))


(defn update-field-data
  [m]
  {:pre [(:new-data m)]}
  (assoc-in m
            (->> m
                 (#(assoc % :redo/entity-type (keyword (or (:redo/entity-type (:entity-data %))
                                                           (:redo/entity-type %)))))
                 add-field-path
                 append-field-path-with-field-key
                 append-index-data-if-applicable
                 add-update-fn
                 prepend-field-path-with-entity-data
                 :field-path)
            (:new-data m)))


(defn add-name-of-entity
  [m]
  {:post [(:redo/name %)]}
  (->> (assoc m :field-key :redo/name)
       add-field-data
       :field-data
       (assoc m :redo/name)))

(defn add-target-namespace-of-entity
  [m]
  {:post [(:redo/target-namespace %)]}
  (->> (assoc m :field-key :redo/target-namespace)
       add-field-data
       :field-data
       (assoc m :redo/target-namespace)))


(defn add-resolved-entity-type-of-path
  "Resolve entity type based on the given resources and path. "
  [m]
  {:pre [(:resources m)
         (:path m)]
   :post [(:redo/entity-type %)]}
  (some->> m
           :resources
           (filterv #(= #?(:clj (.replaceAll (:path m) "/" "")
                           :cljs (.join (.split (:path m) "/") ""))
                        #?(:clj (.replaceAll (:path %) "/" "")
                           :cljs (.join (.split (:path %) "/") ""))))
           first
           :redo/entity-type
           (assoc m :redo/entity-type)))

(defn add-resolved-path-of-entity-type
  "Resolve entity type based on the given resources and path. "
  [m]
  {:pre [(:resources m)
         (:redo/entity-type m)]
   :post [(:path %)]}
  (some->> m
           :resources
           (filterv #(= (keyword (:redo/entity-type m))
                        (keyword (:redo/entity-type %))))
           first
           :path
           (assoc m :path)))


(defn add-resolved-title-of-entity-type
  "Resolve entity type based on the given resources and path. "
  [m]
  {:pre [(:resources m)
         (:redo/entity-type m)]
   :post [(:title %)]}
  (some->> m
           :resources
           (filterv #(= (keyword (:redo/entity-type m))
                        (keyword (:redo/entity-type %))))
           first
           :name
           (assoc m :title)))


(defn add-entity-identity-of-entity-data
  [m]
  {:pre [(:resources m)
         (:entity-data m)
         (:redo/entity-type (:entity-data m))]
   :post [(:redo/entity-identity %)]}
  (some->> m
           :resources
           (filterv #(= (keyword (:redo/entity-type (:entity-data m)))
                        (keyword (:redo/entity-type %))))
           first
           :field-paths
           :redo/entity-identity
           (get-in (:entity-data m))
           :redo/entity-identity
           (assoc m :redo/entity-identity)))


(defn- add-entity-identity-based-transformer
  [m]
  {:pre [(:redo/entity-identity m)]
   :post [(:xf %)]}
  (assoc m :xf (filter #(let [entity-identity (-> (assoc m :entity-data %)
                                                  add-entity-identity-of-entity-data
                                                  :redo/entity-identity)]
                          (and (= (:redo/name entity-identity)
                                  (:redo/name (:redo/entity-identity m)))
                               (= (:redo/target-namespace entity-identity)
                                  (:redo/target-namespace (:redo/entity-identity m))))))))

(defn- add-parent-process-id-based-transformer
  [m]
  {:pre [(:redo/parent-instance m)]
   :post [(:xf %)]}
  (assoc m :xf (filter #(let [parent-process-id (-> (assoc m :entity-data %)
                                                   (assoc :field-key :redo/parent-instance)
                                                   add-field-data
                                                   :field-data)]
                          (and (= (:redo/name parent-process-id)
                                 (:redo/name (:redo/parent-instance m)))
                              (= (:redo/target-namespace parent-process-id)
                                 (:redo/target-namespace (:redo/parent-instance m))))))))

(defn add-entity-identity-based-index-transformer
  [m]
  {:pre [(:redo/entity-identity m)]}
  (assoc m :xf (comp
                (map-indexed vector)
                (filter #(let [entity-identity (-> (assoc m :entity-data (second %))
                                                  add-entity-identity-of-entity-data
                                                  :redo/entity-identity)]
                          (and (= (:redo/name entity-identity)
                                  (:redo/name (:redo/entity-identity m)))
                               (= (:redo/target-namespace entity-identity)
                                  (:redo/target-namespace (:redo/entity-identity m))))))
                (map first))))

(defn add-parent-process-id-based-index-transformer
  [m]
  {:pre [(:redo/parent-instance m)]}
  (assoc m :xf (comp
                (map-indexed vector)
                (filter #(let [parent-process-id (-> (assoc m :entity-data (second %))
                                                     (assoc :field-key :redo/parent-instance)
                                                     add-field-data
                                                     :field-data)]
                           (and (= (:redo/name parent-process-id)
                                   (:redo/name (:redo/parent-instance m)))
                                (= (:redo/target-namespace parent-process-id)
                                   (:redo/target-namespace (:redo/parent-instance m))))))
                (map first))))


(defn add-service-template-id-based-index-transformer
  [m]
  {:pre [(:model-id m)]}

  (assoc m :xf (comp
                (map-indexed vector)
                (filter #(do
                           (and (= (:redo/name (second %))
                                 (:redo/name (:model-id m)))
                             (= (:redo/target-namespace (second %))
                                (:redo/target-namespace (:model-id m))))))
                (map first))))


(defn add-field-data-with-criteria
  "Finds a field specified in field-key identifies an item using entity identity"
  [m]
  {:pre [(:xf m)]}
  (->> m
       add-field-data
       (#(into [] (:xf m) (:field-data %)))
       (#(if (vector? %) (first %) %))
       (assoc m :field-data)))

(defn add-field-data-with-entity-identiy
  "Finds a field specified in field-key identifies an item using entity identity"
  [m]
  (->> m
       add-entity-identity-based-transformer
       add-field-data-with-criteria))

(defn add-field-data-with-parent-process-id
  "Finds a field specified in field-key identifies an item using entity identity"
  [m]
  (->> m
       add-parent-process-id-based-transformer
       add-field-data-with-criteria))

(defn- add-entity-type-from-entity-data
  [m]
  {:pre [(:entity-data m)]}
  (assoc m :redo/entity-type (:redo/entity-type (:entity-data m))))


(defn update-field-data-with-criteria
  "Finds a field specified in field-key identifies an item using entity identity"
  [m]
  {:pre [(:resources m)
         (:entity-data m)
         (:xf m)]}

  (let [m-with-field-data (add-field-data m)]
    (->> m-with-field-data
         (#(into [] (:xf %) (:field-data %)))
         (#(if (vector? %) (first %) %))

         (#(if (nil? %) (count (:field-data m-with-field-data)) %)) ;; index is null if null

         (assoc m :field-index)
         add-entity-type-from-entity-data
         update-field-data)))

(defn update-field-data-with-entity-identiy
  "Finds a field specified in field-key identifies an item using entity identity"
  [m]
  (->> m
       add-entity-identity-based-index-transformer
       update-field-data-with-criteria))


(defn update-field-data-with-parent-process-id
  "Finds a field specified in field-key identifies an item using entity identity"
  [m]
  (->> m
       add-parent-process-id-based-index-transformer
       update-field-data-with-criteria))

(defn update-field-data-of-service-template-with-parent-process-id
  "Finds a field specified in field-key identifies an item using entity identity"
  [m]
  (->> m
       add-service-template-id-based-index-transformer
       update-field-data-with-criteria))
