(def tk-version "1.5.2")

(defproject uni-stuttgart.ipsm/aleph-webserver "0.0.1-SNAPSHOT"
  :description "A trapperkeeper service representing an aleph based web server"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [environ "1.0.2"]
                 [prismatic/schema "1.0.4"]
                 [com.taoensso/timbre "4.2.0"]
                 [puppetlabs/trapperkeeper ~tk-version]
                 [aleph "0.4.1-beta3"]]
  :profiles {:dev {:source-paths ["dev"]
                   :dependencies [[puppetlabs/trapperkeeper ~tk-version :classifier "test" :scope "test"]
                                  [org.clojure/tools.namespace "0.2.11"]]}}
  :repl-options {:init-ns user}
  :source-paths ["src/main/clj"]
  :uberjar-name "persistence.jar"
  :resource-paths ["src/main/resources"]
  :aliases {"tk" ["trampoline" "run" "--config" "dev-resources/config.conf"]}
  :main puppetlabs.trapperkeeper.main)
