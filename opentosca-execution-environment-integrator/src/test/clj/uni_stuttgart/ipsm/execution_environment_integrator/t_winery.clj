(ns uni-stuttgart.ipsm.execution-environment-integrator.t-winery
  "Test library for private functions"
  (:require [de.uni-stuttgart.iaas.ipsm.execution-environment-integrator.winery :as w]
            [clojure.java.io :as io]
            [expectations :as e]))



(def s (atom {:opentosca-csar-control-uri "http://localhost:1337/containerapi/CSARControl/"
              :opentosca-csars-path "http://localhost:1337/containerapi/CSARs/"
              :application-file-name "Moodle.csar"
              :application-id "{http://www.example.com/tosca/ServiceTemplates/Moodle}Moodle"
              :deployment-state-path "DeploymentState"
              :opentosca-csar-upload-uri "http://localhost:1337/containerapi/CSARs/"
              :csar-input-stream (io/input-stream "http://www.iaas.uni-stuttgart.de/OpenTOSCA/CSARMoodle/Moodle.csar")
              :timeout 1000000
              :interval 1000}))




(e/expect #(= false (:request-sucessful? %)) (@#'w/request-sucessful? {:response {:status 100}}))

(e/expect #(= true (:request-sucessful? %)) (@#'w/request-sucessful? {:response {:status 200}}))


;; LIVE TESTS

(defn prepare-deployment-seq
  []
  {:expectations-options :before-run}
  (let [remove-deployable! @#'w/remove-deployable!]
    (-> @s
        remove-deployable!)))

(e/expect :response (e/in (@#'w/deploy-csar! @s)))

(e/expect :runnable-deployed? (e/in (@#'w/runnable-deployed? @s)))

(e/expect (@#'w/handle-deployment-result! @s))

(e/run-all-tests)
