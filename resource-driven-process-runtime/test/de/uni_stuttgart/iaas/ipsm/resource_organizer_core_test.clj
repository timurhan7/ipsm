(ns de.uni-stuttgart.iaas.ipsm.resource-organizer-core-test
  (:require [clojure.test :refer :all]
            [de.uni-stuttgart.iaas.ipsm.resource-organizer-core :refer :all]))

(deftest hello-test
  (testing "says hello to caller"
    (is (= "Hello, foo!" (hello "foo")))))
