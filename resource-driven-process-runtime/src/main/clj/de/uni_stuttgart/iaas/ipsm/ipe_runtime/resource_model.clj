(ns de.uni-stuttgart.iaas.ipsm.ipe-runtime.resource-model
  (:require [de.uni-stuttgart.iaas.ipsm.ipe.model.domain-language :as domain-language]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.resource-model :as resource-model]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.schema :as ipsm-schema]
            [de.uni-stuttgart.iaas.ipsm.utils.constants :as constants]
            [dire.core :as dire]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.transformations :as transformations]
            [de.uni-stuttgart.iaas.ipsm.utils.communications :as communications]
            [clj-http.client :as client]
            [de.uni-stuttgart.iaas.ipsm.utils.conversions :as conversions]
            [de.uni-stuttgart.iaas.ipsm.protocols.core :as protocols]
            [clojure.java.io :as io]
            [overtone.at-at :as at]
            [taoensso.timbre :as timbre
             :refer (log  trace  debug  info  warn  error  fatal  report
                          logf tracef debugf infof warnf errorf fatalf reportf
                          spy logged-future with-log-level with-logging-config
                          sometimes)]))

(def ^:private in-memory-resource-models (atom {}))



(defonce ^:private in-memory-resource-models-locks (atom {}))


(defn get-process-lock
  [m]
  {:pre [(:redo/entity-identity m)]}
  (debug "Getting process lock for" (:redo/entity-identity m) (keyword (conversions/get-unique-id-from-entity-identity (:redo/entity-identity m))))
  (if (get @in-memory-resource-models-locks (keyword (conversions/get-unique-id-from-entity-identity (:redo/entity-identity m))))
    (get @in-memory-resource-models-locks (keyword (conversions/get-unique-id-from-entity-identity (:redo/entity-identity m))))
    (do
      (swap! in-memory-resource-models-locks assoc (keyword (conversions/get-unique-id-from-entity-identity (:redo/entity-identity m))) (Object.))
      (get @in-memory-resource-models-locks (keyword (conversions/get-unique-id-from-entity-identity (:redo/entity-identity m)))))))

(def resources-handler-path "/resources/")

;; caching
(defn- add-in-memory-entity-to-cache!
  [m]
  {:pre [(:redo/entity-identity (:graph-data m))]}
  (swap! in-memory-resource-models assoc-in [(:redo/entity-type (:graph-data m)) (keyword (conversions/get-unique-id-from-entity-identity (:redo/entity-identity (:graph-data m))))] (:graph-data m)))

(defn- delete-entity-from-cache!
  [m]
  {:pre [(:redo/entity-identity (:graph-data m)) (:redo/entity-type (:graph-data m))]}
  (swap! in-memory-resource-models update-in [(:redo/entity-type (:graph-data m))]
         #(dissoc % (keyword (:id (:graph-data m))))))

(defn- add-in-memory-entity-to-persistence!
  [m]
  {:pre [(:persistence m) (:graph-data m)]}
  (protocols/delete-entity-by-id! (:persistence m) (:graph-data m))
  (protocols/add-entity! (:persistence m) (:graph-data m)))

(defn- delete-entity-from-persistence!
  [m]
  {:pre [(:persistence m) (:graph-data m)]}
  (protocols/delete-entity-by-id! (:persistence m) (:graph-data m)))

(defn- remove-resource-model-from-cache-and-persistence!
  [m]
  (delete-entity-from-cache! m)
  (delete-entity-from-persistence! m)
  )

(defn- get-graph-data-from-memory
  [m]
  {:pre [(:graph-data m) (:redo/entity-identity (:graph-data m)) (:redo/entity-type (:graph-data m))]}
  (let [target-key (keyword (conversions/get-unique-id-from-entity-identity (:redo/entity-identity (:graph-data m))))] ;; process state contains the key or document type first checks the cache then persistence store
    (if (and (get @in-memory-resource-models (:redo/entity-type (:graph-data m)))
             (target-key (get @in-memory-resource-models (:redo/entity-type (:graph-data m)))))
      (target-key (get @in-memory-resource-models (:redo/entity-type (:graph-data m))))
      nil)))



(defn- get-graph-data-from-ds
  [m]
  {:pre [(:graph-data m) (:persistence m)]}
  (protocols/get-entity (:persistence m) (:graph-data m)))

(defn- get-graph-data-from-cache-or-persistence!
  [m]
  (let [cache-val (get-graph-data-from-memory m)]
    (if cache-val
      cache-val
      (let [ds-val (get-graph-data-from-ds m)]
        (if ds-val
          (let [ret-val (assoc m :graph-data ds-val)]
            (add-in-memory-entity-to-cache! m)
            ret-val)
          nil)))))


(defn- add-target-resource-from-resource-id
  [m]
  (assoc m :target-resource (resource-model/get-target-from-resource-id m)))


(defn- add-target-resource
  [m]
  {:pre [(:index m) (:graph-data m) (:acquirement-vector (:graph-data m))]}
  (->> (assoc m :target-resource-id (:redo/id (get (:acquirement-vector (:graph-data m)) (:index m))))
       add-target-resource-from-resource-id))

(defn- add-nodes-relationships-map
  [m]
  (debug "Adding nodes and relationships..."  )
  (assoc m :resources-relationships-map (resource-model/resource-graph->resources-relationships-map m)))

(defn- increment-index
  [m]
  {:pre [(:index m)]}
  (update-in m [:index] inc))

(defn- remove-resource-identified-by-index
  [acquirement-vector index]
  (vec (concat (subvec acquirement-vector 0 index) (subvec acquirement-vector (inc index) (count acquirement-vector)))))


(defn- update-acquirement-vector
  [m]
  {:pre [(:index m) (:entity-data m) (:acquirement-vector (:graph-data m))]}
  (debug "Updating acquirement vector")
  ;; (debug "Index" (:index m))
  ;; (debug "After update" (:acquirement-vector (:entity-data (update-in m [:entity-data :acquirement-vector]
  ;;                                                                     #(remove-resource-identified-by-index % (:index m))))))
  (update-in m [:graph-data :acquirement-vector]
             #(remove-resource-identified-by-index % (:index m))))


(defn- add-new-initializing-resource
  [m]
  {:pre [(:target-resource m)]}
  (->> (assoc m
              :new-state :initializing
              :entity-data (:target-resource m))
       domain-language/add-new-instance-descriptor-for-resource-model-element
       domain-language/update-resource-instance-descriptor
       :entity-data
       (assoc m :new-resource)))

(defn- add-new-removed-resource
  [m]
  {:pre [(:target-resource m)]}
  (->> (assoc m :new-state :removed)
       domain-language/update-resource-instance-state
       :target-resource
       (assoc m :new-resource)))

(defn- update-resource-graph-with-initializing-resource
  [m]
  ;; (debug "Resource graph will be updated" m)
  (-> m
      resource-model/update-target-resource-in-graph))

(defn- persist-changes-in-cache-and-persistence!
  [m]
  (debug "Persisting changes in cache and persistence storage")
  (add-in-memory-entity-to-cache! m)
  (add-in-memory-entity-to-persistence! m))

(defn- add-resource-neighbours
  [m]
  (-> m
      resource-model/add-dependent-resources
      resource-model/add-inbound-relationships
      resource-model/add-outbound-relationships
      resource-model/add-preconditioner-relationships
      resource-model/add-postconditioner-relationships))

(defn- resource-initialized?
  [m]
  (or
   (= ":engaged" (domain-language/get-resource-state m))
   (= :engaged (domain-language/get-resource-state m))))

(defn- resource-removed?
  [m]
  (or
   (= ":removed" (domain-language/get-resource-state m))
   (= :removed (domain-language/get-resource-state m))))

(defn- resource-acquirement-complete?
  [m]
  (or
   (resource-initialized? m)
   (resource-removed? m)))

(defn- return-true-if-all-are-true-or-empty
  [v]
  (case (count v)
    0 true
    1 (first v)
    (reduce
     #(and %1 %2)
     v)))


(defn- all-dependencies-are-initialized?
  [m]
  {:pre [(:dependencies m)]}
  (->> (:dependencies m)
       (mapv #(-> (assoc m :entity-data %) resource-initialized?))
       return-true-if-all-are-true-or-empty))

(defn- to-be-removed?
  [m]
  (or
   (= (domain-language/get-resource-state (assoc m :entity-data (:target-resource m))) ":to-be-removed")
   (= (domain-language/get-resource-state (assoc m :entity-data (:target-resource m))) :to-be-removed)))

(defn- engaged?
  [m]
  (or
   (= (domain-language/get-resource-state (assoc m :entity-data (:target-resource m))) ":engaged")
   (= (domain-language/get-resource-state (assoc m :entity-data (:target-resource m))) :engaged)))

(defn- get-callback-address
  [m]
  {:pre [(:process-id m) (:uri m)]}
  (if (.endsWith (str (:uri m)) "/")
    (str (str (:uri m)) resources-handler-path  (str (conversions/url-encode (:redo/target-namespace (:process-id m)))
                                                     "/"
                                                     (conversions/url-encode (:redo/name (:process-id m)))))
    (str (str (:uri m)) resources-handler-path  (str (conversions/url-encode (:redo/target-namespace (:process-id m)))
                                                         "/"
                                                         (conversions/url-encode (:redo/name (:process-id m)))))))

(defn- get-resource-handler-path
  []
  (str resources-handler-path ":target-namespace/:name"))


(defn- create-initialization-parts
  [m]
  {:pre [(:target-resource m)
         (:dependencies m)
         (:dependents m)
         (:instance-descriptor m)
         (:postconditioner-relationship-definitions m)
         (:preconditioner-relationship-definitions m)
         (:operation-type m)]}
  (debug "Creating initializing message parts")
  (filterv
   #(:content %)
   [{:name (name :operation-request)
     :content (str (domain-language/operation-message
                    {:redo/process-id (:process-id m)
                     :redo/resource-driven-process-definition (:entity-data m)
                     :redo/instance-descriptor (:instance-descriptor m)
                     :redo/intentions-definitions (:target-intentions m)
                     :redo/target-resource (:target-resource m)
                     :redo/dependencies {:dependency (:dependencies m)}
                     :redo/operation-type (:operation-type m)
                     :redo/preconditioner-relationships {:preconditioner-relationship (:preconditioner-relationship-definitions m)}}))}
    {:name (name :callback-address) :content (get-callback-address m)}]))

(defn- create-release-parts
  [m]
  {:pre [(:target-resource m)]}

  (filterv
   #(:content %)
   [{:name (name :operation-request)
     :content (str (domain-language/operation-message
                    {:redo/process-id (:process-id m)
                     :redo/resource-driven-process-definition (:entity-data m)
                     :redo/instance-descriptor (:instance-descriptor m)
                     :redo/target-resource (:target-resource m)
                     :redo/dependencies {:dependency (:dependencies m)}
                     :redo/operation-type (constants/property :release-resource-operation-name)}))}
    {:name (name :callback-address) :content (get-callback-address m)}]))


(defn- create-establishment-multi-part
  [m]
  {:pre [(:target-template m)
         (:relationship-target-resource m)
         (:relationship-source-resource m)
         (:instance-descriptor m)
         (:process-id m)
         (:operation-type m)]}
  (debug "Creating establishment messeage parts")
  (filterv
   #(:content %)
   [{:name (name :operation-request)
     :content (str (domain-language/operation-message
                    {:redo/process-id (:process-id m)
                     :redo/resource-driven-process-definition (:entity-data m)
                     :redo/instance-descriptor (:instance-descriptor m)
                     :redo/intentions-definitions (:target-intentions m)
                     :redo/target-relationship (:target-template m)
                     :redo/relationship-target-resource (:relationship-target-resource m)
                     :redo/relationship-source-resource (:relationship-source-resource m)
                     :redo/relationship-target-resource-instance-descriptor (:relationship-target-resource-instance m)
                     :redo/relationship-source-resource-instance-descriptor (:relationship-source-resource-instance m)
                     :redo/operation-type (:operation-type m)}))}
    {:name (name :callback-address) :content (get-callback-address m)}]))

(defn- add-request-body
  [m]
  {:pre [(:multipart m)]}
  (debug "Adding request body")
  (assoc m :request-body
         {:multipart (:multipart m)}))

(defn- add-initialization-multipart
  [m]
  (assoc m :multipart (create-initialization-parts m)))

(defn- add-establishment-multipart
  [m]
  (assoc m :multipart (create-establishment-multi-part m)))

(defn- add-release-multipart
  [m]
  (debug "Adding release parts")
  (assoc m :multipart (create-release-parts m)))

(defn- post-request!
  [m]
  {:pre [(:request-body m)]}
  (debug "Posting the request for the resource operation"
         (:operation-type m)
         (:recipient-address m))
  (if (:recipient-address m)
    (client/post (:recipient-address m) (:request-body m))
    (warn "Operation on the resource" (:target-resource m) "is not supported" (:operation-type m))))

(defn- add-recipient-address
  [m]
  {:pre [(:matching-ee-integrator m)]}
  (debug "Adding recipient address")
  (assoc m :recipient-address (:matching-ee-integrator m)))

(defn- submit-resource-initialization-request!
  [m]
  {:pre [(:target-resource m)]}
  (doto (-> m
            add-recipient-address
            add-initialization-multipart
            add-request-body)
    post-request!))


(defn- add-matching-ee-integrator
  [m]
  {:pre [(:persistence m) (:target-template m)]}
  (debug "Adding matching iee for" (:redo/id (:target-template m)))
  (->> m
       domain-language/add-execution-environment
       :matching-ee-integrator
       (assoc m :matching-ee-integrator)))



(defn- add-process-model
  [m]
  {:pre [(:entity-data m)]}
  (assoc-in m [:graph-data :entity-data] (:entity-data m)))

(defn- update-resource-model-of-informal-process-instance
  [m]
  {:pre [(:entity-data (:graph-data m)) (:resources-relationships-map m)]}
  (debug "New resources will be added to the process def")
  (->> {:entity-data (:entity-data (:graph-data m))
        :new-vals (or (into (:relationships (:resources-relationships-map m))
                            (:resources (:resources-relationships-map m)))
                      [])}
       (into m)
       domain-language/replace-resources-or-relationships-of-informal-process))



(defn- add-instance-descriptor-of-target-resource
  [m]
  {:pre [(:target-resource m)]}
  (debug "Target resource")
  (->> (assoc m :entity-data (:target-resource m))
       domain-language/get-resource-instance-descriptor
       (assoc m :instance-descriptor)))


(defn- prepare-for-initialization
  [m]
  (-> m
      (assoc :operation-type (constants/property :acquire-resource-operation-name))
      (assoc :target-template (:target-resource m))
      add-matching-ee-integrator
      add-instance-descriptor-of-target-resource
      add-resource-neighbours
      update-acquirement-vector
      ;; add-new-initializing-resource ;; they are initialzed at the beginning
      ;; update-resource-graph-with-initializing-resource
      add-nodes-relationships-map
      update-resource-model-of-informal-process-instance
      add-process-model))

(defn- prepare-for-skip
  [m]
  (-> m
      update-acquirement-vector))

(defn- prepare-for-removal
  [m]
  (debug "Preparing for removal")
  (-> m
      (assoc :operation-type (constants/property :release-resource-operation-name))
      (assoc :target-template (:target-resource m))
      add-matching-ee-integrator
      update-acquirement-vector
      add-new-removed-resource
      resource-model/update-target-resource-in-graph))

(defn- initialize-resource!
  [m]
  (debug "Resource will be requested with index" (:index m))
  (doto m
    submit-resource-initialization-request!
    persist-changes-in-cache-and-persistence!))



(defn- add-lifecylce-interface-as-target-interface
  [m]
  {:post [(:target-interface %)]}
  (debug "Adding life-cycle interface as target interface")
  (assoc m :target-interface (constants/property :life-cycle-interface-uri)))


(defn- release-resource!
  [m]
  (debug "Releasing resource!")
  (doto (-> m
            (assoc :operation-type (constants/property :release-resource-operation-name))
            add-lifecylce-interface-as-target-interface
            (assoc :target-template (:target-resource m))
            add-matching-ee-integrator
            add-recipient-address
            add-release-multipart
            add-request-body)
    post-request!))

(declare iterate-through-acquriement-vector!)

(defn- start-initialization-or-proceed!
  [m]
  (debug "Starting initialization or proceeding:")
  (cond
    (to-be-removed? m)
    (doto (prepare-for-removal m)
      release-resource!
      iterate-through-acquriement-vector!)

    (engaged? m)
    (doto (prepare-for-skip m)
      iterate-through-acquriement-vector!)

    :else ;; new algortihm not depend on this
    (doto (prepare-for-initialization m)
      initialize-resource! ;; do not increment the index as one resource will be removed
      iterate-through-acquriement-vector!)))


(defn- resources-acquirement-complete?
  [m]
  (not (seq (remove
             #(-> (assoc m :entity-data %) resource-acquirement-complete?)
             (-> m
                 :resources-relationships-map
                 :resources)))))



(defn complete-resource-acquirement!
  [m]
  {:pre [(:resource-model-init-succ-fn! m)]}
  (debug "Completing resource acquirement!")
  ((:resource-model-init-succ-fn! m) m))



(defn- iterate-through-acquriement-vector!
  [m]
  {:pre [(:index m) (:graph-data m) (:acquirement-vector (:graph-data m))]}
  (debug "Iterating through" (:index m))
  (if (< (:index m) (count (:acquirement-vector (:graph-data m))))
    (doto (-> m
              add-target-resource
              resource-model/add-dependency-resources)
      start-initialization-or-proceed!)
    (let [iteration-res (some->> m
                                 add-nodes-relationships-map
                                 resources-acquirement-complete?
                                 (assoc m :acquirement-complete?)
                                 add-nodes-relationships-map
                                 update-resource-model-of-informal-process-instance
                                 add-process-model)]
      (if iteration-res
          (complete-resource-acquirement! iteration-res)))))

(defn- initialize-next-resource!
  "Return next initializble resource, change its state and save it, or if not found return nil. Acquiers a lock"
  [m]
  {:pre [(:process-id m) (:persistence m)]}
  (debug "Initializing next resource" )
  (locking (get-process-lock (assoc m :redo/entity-identity (:process-id m))) ;; a lock for this specific process instance
    (let [fetched-map (get-graph-data-from-cache-or-persistence! m)]
      (-> m
          (assoc :graph-data fetched-map)
          (assoc :index 0)
          iterate-through-acquriement-vector!))))

(defn add-precondioner-relationships
  [m]
  (assoc
   m
   :preconditioner-relationship-definitions
   (vec
    (reduce
     #(= (:redo/entity-type) :preconditioner-relationship-definition)
     (:relationships m)))))

(defn- get-precondioned-resources
  [v]
  (reduce
   #(= (:redo/entity-type) :preconditioner-relationship-definition)
   v))


(defn- deployable-exists?
  [resource-map folder-path]
  ;; (debug "Existence of the following deployable will be controlled:" resource-map)
  ;; (debug "Root path is" folder-path)
  (let [deployable-path (:deployable-path (:entity-map (:deployment-descriptor (:entity-map resource-map))))]
    (if (and deployable-path (.exists (io/file (str folder-path "/" deployable-path))))
      true
      false)))


(defn- deployable-stream
  [resource-map folder-path]
  (let [deployable-path (:deployable-path (:entity-map (:deployment-descriptor (:entity-map resource-map))))]
    (if (and deployable-path (.exists (io/file (str folder-path "/" deployable-path))))
      (io/input-stream (io/file (str folder-path "/" deployable-path)))
      nil)))

(defn- bring-all-deployables-together
  [m]
  (assoc m :deployables (vec
                         (filter
                          #(deployable-exists? % (:root-folder-path m))
                          (concat [(:target-resource m)]
                                     (:dependencies m)
                                     (:dependents m)
                                     (:inbound-relationships m)
                                     (:outbound-relationships m))))))


(defn- create-entiries-for-deployables
  [m]
  (:pre [(:deployables m)])
  ;; (debug "Creating file entries for the following files" (:deployables m))
  (assoc m :file-entries (mapv
                          #(hash-map :name (:id (:entity-map %)) :content-type constants/deployable-media-type  :content (deployable-stream % (:root-folder-path m)))
                          (:deployables m))))



(defn- create-file-entries
  [m]
  (-> m
      bring-all-deployables-together
      create-entiries-for-deployables
      :file-entries))

;;

 ;; (defn- prepare-and-initialize-resource!
 ;;  [m]
 ;;  (doto (-> m
 ;;            prepare-resource-for-initialization)
 ;;    initialize-resource!))

;; (defn- initialize-resource-iteratively!
;;   [m]
;;   {:pre [(:acquirement-vector m) (vector? (:acquirement-vector m))] }
;;   (let [resource-count (count (:acquirement-vector m))]
;;     (loop [index 0]
;;       (if (< index resource-count)
;;         (prepare-and-initialize-resource! (-> m
;;                                               (assoc :index index)
;;                                               add-target-resource))))))
                                        ;

;; (defn- initialize-resources!
;;   [m]
;;   {:pre [(:resource-model m)]}
;;   (doto (-> m
;;             resource-model/resource-model->acquirement-vector)
;;     (initialize-resource-iteratively!)))


(defn- persist-resource-graph!
  [m]
  {:pre [(:persistence m) (:resource-model m)]}
  (protocols/delete-entity-by-id! (:persistence m) (:resource-model m))
  (protocols/add-entity! (:persistence m) (:resource-model m)))


(defn- update-entity-type
  [m]
  (assoc-in m [:graph-data :redo/entity-type] :redo/initializing-informal-process-instance))

(defn- add-process-state
  [m]
  (-> m
      (assoc :field-key :redo/instance-descriptors)
      (assoc :new-data (assoc (-> m
                                    (assoc :field-key :redo/instance-descriptors)
                                    ipsm-schema/add-field-data-with-entity-identiy
                                    :field-data)
                                :redo/instance-state "initializing"))
      ipsm-schema/update-field-data-with-entity-identiy))

(defn- add-process-id
  [m]
  {:pre [(:process-id m)]
   :post [(:redo/entity-identity (:graph-data %))]}
  (assoc-in m [:graph-data :redo/entity-identity] (:process-id m)))

(defn- add-parent-process-id
  [m]
  {:pre [(:process-id m)]
   :post [(:redo/parent-instance %)]}
  (debug "Recieved parent process id" (:process-id m))
  (assoc m :redo/parent-instance (:process-id m)))

(defn- update-target-resources-in-graph
  [m]
  {:pre [(:target-resources m)]}
  (let [size (count (:target-resources m))]
    (debug "Updating target resources")
    (loop [index 0
           return-map m]
      (if (< index size)
        (recur
         (inc index)
         (-> return-map
             (assoc :target-resource (get (:target-resources m) index))
             (assoc :new-resource (->> (assoc m
                                               :target-resource (get (:target-resources m) index)
                                               :entity-data (get (:target-resources m) index))
                                        domain-language/add-new-instance-descriptor-for-resource-model-element
                                        domain-language/update-resource-instance-descriptor
                                        :entity-data))
             resource-model/update-target-resource-in-graph))
        (do
          (debug "Return map after updating target resources")
          return-map)))))


(defn- merge-resources-relationships
  [m]
  {:pre [(:resources-relationships-map m)]}
  (assoc m :resources-relationships-vectors
         (into (:resources (:resources-relationships-map m))
               (:relationships (:resources-relationships-map m)))))

;; TODO relatiosnhips are ignored this must be changed
(defn- add-new-instance-descriptors-for-each-model-element
  [m]
  (->> m
       add-nodes-relationships-map
       :resources-relationships-map
       :resources
       (assoc m :target-resources)
       update-target-resources-in-graph
       add-nodes-relationships-map
       update-resource-model-of-informal-process-instance))

(defn- add-updated-process-to-graph-data
  [m]
  {:pre [(:entity-data m)]}
  (assoc-in m [:graph-data :entity-data] (:entity-data m)))

(defn initialize-resource-model!
  [m]
  (debug "Resources will be initialized for the map")
  (doto (-> m
            add-parent-process-id
            resource-model/add-graph-for-informal-process
            resource-model/mark-additional-to-be-removed-resources
            add-process-model
            add-new-instance-descriptors-for-each-model-element
            add-process-model ;; update the contents of graph data
            resource-model/add-acquirement-vector
            add-lifecylce-interface-as-target-interface
            update-entity-type
            (assoc :redo/entity-identity (:process-id m))
            add-process-id
            add-process-state)
    (persist-changes-in-cache-and-persistence!)
    (initialize-next-resource!))
  nil)



(defn- add-operation-type-from-message
  [m]
  (debug "Adding operation type")
  (if (some-> m :operation-message :redo/operation-type)
    (assoc m :operation-type (some-> m :operation-message :redo/operation-type))
    (error "Operation type was not specified")))

(defn- add-process-id-from-params
  [m]
  (if (or (:redo/id (:params m)) (:redo/process-id (:operation-message m)))
    (assoc m :process-id (or (:redo/id (:params m)) (:redo/process-id (:operation-message m))))
    (error "Missing process id!!!")))

(defn- add-request-params
  [m]
  {:pre [(:message m)]}
  ;; (debug "Adding request params" (:params (:request (:message m))))
  (-> m
      (assoc :params (:params (:request (:message m))))))

(defn- add-content-type
  [m]
  (->> m
       :message
       :request
       :content-type
       (assoc m :content-type)))

(defn- add-resource-instance-state
  [m]
  (debug "Instance state retrieved is " (:redo/instance-state (:operation-message m)))
  (if (:redo/instance-state (:operation-message m))
    (assoc m :instance-state (:redo/instance-state (:operation-message m)))
    (error "Resoruce state is not specified")))

(defn- add-resource-instance-location
  [m]
  (if (:redo/instance-uri (:operation-message m))
    (assoc m :instance-uri (:redo/instance-uri (:operation-message m)))
    (do
      (error "Resource instance location is not specified")
      m)))


(defn- add-instance-descriptor
  [m]
  (if (:redo/instance-descriptor (:operation-message m))
    (assoc m :instance-descriptor (:redo/instance-descriptor (:operation-message m)))
    (do
      (error "Resource instance descriptor is not specified")
      m)))

(defn- add-new-resource-from-parameters
  [m]
  {:pre [(:params m) (:content-type m)]}
  (if (:redo/target-resource (:operation-message m))
    (case (:content-type m)
      "application/xml" (assoc m :new-resource (error "Not yet implemented"))
      "application/edn" (assoc m :new-resource (:redo/target-resource (:operation-message m))))
    (error "resource-instance must be specified")))


(defn- add-resource-graph-from-storage
  [m]
  (assoc m :graph-data (get-graph-data-from-cache-or-persistence! m)))

(defn- add-process-data-from-persistence
  [m]
  {:pre [(:graph-data m)
         (:persistence m)]}
  (assoc m :entity-data (protocols/get-entity (:persistence m) (:entity-data (:graph-data m)))))


(defn- add-resource-graph
  [m]
  (-> m
      ;; first grab the old state and update it to the newest one

      add-resource-graph-from-storage
      add-process-data-from-persistence
      resource-model/add-graph-for-informal-process
      resource-model/mark-additional-to-be-removed-resources
      add-process-model))




(defn- add-resource-id-from-new-resource
  [m]
  {:pre [(:new-resource m)]}
  (-> m
      (assoc :target-resource-id (:redo/id (:new-resource m)))
      (dissoc :params)))

(defn- filter-engaged-and-initializing-resources
  [m]
  (->> m
       :resources-relationships-map
       :resources
       (filterv
        #(or (= (domain-language/get-resource-state (assoc m :entity-data %)) :engaged)
             (= (domain-language/get-resource-state (assoc m :entity-data %)) ":engaged")
             (= (domain-language/get-resource-state (assoc m :entity-data %)) ":initializing")
             (= (domain-language/get-resource-state (assoc m :entity-data %)) :initializing)))))

(defn- filter-to-be-removed-resources
  [m]
  (->> m
       domain-language/get-resources
       (filterv
        #(or (= (domain-language/get-resource-state (assoc m :entity-data %)) :to-be-removed)
             (= (domain-language/get-resource-state (assoc m :entity-data %)) ":to-be-removed")))))




(defn- release-all-resources!
  [m]
  {:pre [(:resources-relationships-map m) (:resources (:resources-relationships-map m))]}
  (->> m

       filter-engaged-and-initializing-resources
       (mapv #(release-resource! (assoc m :target-resource %)))))

(defn release-resources-of-an-informal-process-instance!
  [m]
  (->> m
       domain-language/get-resources
       filter-engaged-and-initializing-resources
       (mapv #(release-resource! (assoc m :target-resource %)))))


(defn release-to-be-removed-resources!
  [m]
  (->> m

       filter-to-be-removed-resources
       (mapv #(release-resource! (assoc m :target-resource %)))))

(defn- initialization-failed!
  [m]
  {:pre [(:resource-model-init-fail-fn! m)]}
  (debug "Executing failure procedure!!!")
  (locking (get-process-lock m)
    (remove-resource-model-from-cache-and-persistence! m)
    (release-all-resources! m)
    (add-in-memory-entity-to-persistence! (assoc-in m [:graph-data :redo/entity-type] :redo/failed-process-instant))
    ((:resource-model-init-fail-fn! m) m )));; store it for logging purposes


(dire/with-handler!
  #'start-initialization-or-proceed!
  "Problem with the initialization. Fail it!"
  java.lang.Exception
  (fn [e m]
    (error "Resource with the following spec could not be initialized" (:target-resource m))
    (error "Exception during resource acquirement:" e)
    (.printStackTrace e)
    (-> m
        add-nodes-relationships-map
        (assoc :instance-state-description "There was an exception during the acquirement of" (:name (:target-resource m)) "please contanct your technical admin.")
        initialization-failed!)))




(defn- prepare-relationship-initialization
  [m]
  {:pre [(:relationships (:resources-relationships-map m))
         (:target-resource m)]}
  (->> (assoc m
              :source-node (:target-resource m)
              :relationships (:relationships (:resources-relationships-map m))
              :operation-type (constants/property :acquire-relationship-operation-name))
       resource-model/filter-relationships))



(defn- add-relationship-target-resource
  [m]
  {:pre [(:resources-relationships-map m)
         (:resources (:resources-relationships-map m))
         (:target-template m)]
   :post [(:relationship-target-resource %)]}
  (->> m
       :resources-relationships-map
       :resources
       (filterv #(#{(:redo/id %)}
                  (:redo/ref (:redo/target-element (:target-template m)))))
       first
       (assoc m :relationship-target-resource)))



(defn- add-relationship-source-resource-instance-desc
  [m]
  {:pre [(:relationship-source-resource m)]
   :post [(:relationship-source-resource-instance %)]}
  (->> m
       :relationship-source-resource
       (assoc m :entity-data)
       domain-language/get-resource-instance-descriptor
       (assoc m :relationship-source-resource-instance)))


(defn- add-relationship-target-resource-instance-desc
  [m]
  {:pre [(:relationship-target-resource m)]
   :post [(:relationship-target-resource-instance %)]}
  (->> m
       :relationship-target-resource
       (assoc m :entity-data)
       domain-language/get-resource-instance-descriptor
       (assoc m :relationship-target-resource-instance)))


(defn- add-relationship-source-resource
  [m]
  {:pre [(:target-resource m)]
   :post [(:relationship-source-resource %)]}
  (assoc m :relationship-source-resource (:target-resource m)))


(defn- add-operation-provider-available?
  [m]
  (assoc m :operation-provider-available? (boolean (:matching-ee-integrator m))))

(defn- establish-relationship-if-possible!
  [m]
  (if (:operation-provider-available? m) ;; a simple guard
    (doto (-> m
              add-establishment-multipart
              add-recipient-address
              add-request-body)
      post-request!)
    (error "Relationship cannot be established, as no such ee exists!" (:operation-provider-available? m))))


(defn- initialize-possible-relationships!
  [m]
  {:pre [(:entity-data (:graph-data m))]}
  (doseq [relationship (:relationships m)]
    (->> (assoc m
                :target-template relationship
                :entity-data (:entity-data (:graph-data m)))
         add-lifecylce-interface-as-target-interface
         add-matching-ee-integrator
         add-relationship-target-resource
         add-relationship-source-resource
         add-relationship-target-resource-instance-desc
         add-relationship-source-resource-instance-desc
         add-operation-provider-available?
         establish-relationship-if-possible!)))





(defn- initialize-post-conditioner-relationships!
  [m]
  (debug "relationships will be initialized!!!" (count (:resources-relationships-map m)) )
  (doto (-> m
            prepare-relationship-initialization)
    initialize-possible-relationships!))


(defn proceed-resource-initalization!
  [m]
  {:pre [(:new-resource m) (:resources (:resources-relationships-map m))]}
  (debug "Proceeding with initialization!!" (resource-initialized? (assoc m :entity-data (:new-resource m))))
  (if (resource-initialized? (assoc m :entity-data (:new-resource m)))
    (do
      (initialize-post-conditioner-relationships! m)
      (debug "Resource acquirement completed?" )
      (if (resources-acquirement-complete? m)
        (-> m
            (assoc :process-initialized? true)
            complete-resource-acquirement!)
        (initialize-next-resource! m)))
    (initialization-failed! m)))



(defn- add-empty-entity-data
  [m]
  {:pre [(:process-id m)]}
  (assoc m :graph-data {:id (:process-id m)}))

(defn- add-updated-instance-descriptor
  [m]
  {:pre [(:instance-descriptor m)]}
  (assoc m :instance-descriptor (assoc (:instance-descriptor m)
                                       :redo/instance-uri (:instance-uri m)
                                       :redo/instance-state (:instance-state m))))


(defn- update-new-resource
  [m]
  {:pre [(:instance-descriptor m)]}
  (->> m
       :new-resource
       (assoc m :entity-data)
       domain-language/update-resource-instance-descriptor
       :entity-data
       (assoc m :new-resource)))

(defn- update-target-resource
  [m]
  {:pre [(:instance-descriptor m)
         (:target-resource m)]}
  (->> m
       :target-resource
       (assoc m :entity-data)
       domain-language/update-resource-instance-descriptor
       :entity-data
       (assoc m :new-resource)))

(defn- add-instance-descriptor-of-new-resource
  [m]
  {:pre [(:new-resource m)]}
  (->> (assoc m :entity-data (:new-resource m))
       domain-language/get-resource-instance-descriptor
       (assoc m :instance-descriptor)))

(defn- prepare-new-resource
  [m]
  (debug "Preparing the resource")
  (->> m
       add-new-resource-from-parameters
       add-resource-instance-state
       add-resource-instance-location
       add-instance-descriptor-of-new-resource
       add-updated-instance-descriptor
       update-new-resource
       :new-resource
       (assoc m :new-resource)))

(defn- prepare-new-resource-instance-descriptor
  [m]
  (debug "Preparing the resource")
  (->> m
       add-resource-instance-state
       add-resource-instance-location
       add-instance-descriptor
       add-updated-instance-descriptor
       :instance-descriptor
       (assoc m :instance-descriptor)))

(defn- add-entity-identity-of-process
  [m]
  {:pre [(:process-id m)]}
  (assoc m :redo/entity-identity (:process-id m)))


(defn- add-process-model-id
  [m]
  {:post [(:redo/entity-identity %)]}
  (assoc m :process-model-id
         (-> m
             (assoc :entity-data (get-in m [:operation-message :redo/resource-driven-process-definition]))
             ipsm-schema/add-entity-identity-of-entity-data
             :redo/entity-identity)))

(defn- prepare-contents-for-proceeding-acquirement
  [m]
  (-> m
      add-process-id-from-params
      add-parent-process-id
      add-content-type
      add-new-resource-from-parameters
      prepare-new-resource-instance-descriptor
      add-entity-identity-of-process
      add-process-model-id
      ;; add-process-state
      add-resource-id-from-new-resource
      add-empty-entity-data
      update-entity-type
      add-process-id))

(defn- make-decision-for-execution!
  [m]
  {:pre [(:process-model-id m)]}
  (if (and (:graph-data m)
           (= :redo/initializing-informal-process-instance (:redo/entity-type (:graph-data m))))
    (locking (get-process-lock (assoc m :redo/entity-identity (:process-model-id m))) ;; lock all instances
      (debug "State will be changed hopefully only once")
      (doto
          (doto
              (-> m
                  add-resource-graph
                  add-target-resource-from-resource-id
                  update-target-resource
                  resource-model/update-target-resource-in-graph
                  add-nodes-relationships-map
                  update-resource-model-of-informal-process-instance
                  add-process-model)
            persist-changes-in-cache-and-persistence!)
        proceed-resource-initalization!))
    (error "No process is being acquired with the specfied id" (:process-id m))))


(defmulti handle-callback! :operation-type)

(defmethod handle-callback! (constants/property :acquire-resource-operation-name)
  [m]
  {:pre [(:persistence m)
         (:tosca-persistence m)]}
  (-> m
      prepare-contents-for-proceeding-acquirement
      make-decision-for-execution!))


(defn- add-operation-response
  [m]
  (debug "Operation response")
  (if (:operation-response (:params m))
    (assoc m :operation-message (:operation-response (:params m)))
    (error "No operation response included in the message")))

(defn- handle-resource-operation-callback!-fn
  [m]
  {:pre [(:persistence m)
         (:tosca-persistence m)]}
  (fn [request]
    (debug "Change status request has been recieved!!!")
    (doto
        (-> m
            (assoc :message request)
            add-request-params
            add-operation-response
            add-operation-type-from-message)
      handle-callback!)
    "Your request will be processed"))


(defn get-route-configs
  [m]
  [{:resource-path (get-resource-handler-path)
    :available-media-types ["application/xml" "application/edn"]
    :allowed-methods [:post]
    :post! (handle-resource-operation-callback!-fn m)}])
