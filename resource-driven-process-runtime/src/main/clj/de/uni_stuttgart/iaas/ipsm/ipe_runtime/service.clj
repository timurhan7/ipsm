(ns de.uni-stuttgart.iaas.ipsm.ipe-runtime.service
  (:require [de.uni-stuttgart.iaas.ipsm.protocols.core :as protocols]
            [de.uni-stuttgart.iaas.ipsm.ipe-runtime.core :as core]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.schema :as redo-schema]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.domain-language :as domain-language]
            [clojure.spec.alpha :as s]
            [puppetlabs.trapperkeeper.core :as trapperkeeper]
            [puppetlabs.trapperkeeper.services :as services]
            [taoensso.timbre :as timbre
             :refer (log  trace  debug  info  warn  error  fatal  report
                          logf tracef debugf infof warnf errorf fatalf reportf
                          spy logged-future with-log-level with-logging-config
                          sometimes)])
  (:import
   [java.net URI URL]
   [java.util.zip ZipEntry ZipOutputStream]
   [javax.jws WebService WebMethod]))


(s/def ::initialization-data-schema (s/keys :req-un [::service-suffix
                                                     ::service-name
                                                     ::ws-port
                                                     ::ws-host
                                                     ::uri
                                                     ::executor-wsdl-client-url]
                                            :opt-un [::war-path
                                                     ::war-download-uri
                                                     ::war-handler-path]))


(trapperkeeper/defservice RedoRuntimeService
  protocols/RedoRuntime
  [[:ConfigService get-config]
   [:WebserverService add-war-handler add-ring-handler]
   PersistenceProtocol
   ToscaPersistenceProtocol]
  (init [this context]
        (info "Initializing" (:service-name (:redo-runtime (get-config))))
        (info "Initializing" (:redo-runtime (get-config)))
        (-> (get-config)
            :redo-runtime
            (assoc :persistence (services/get-service this :PersistenceProtocol))
            (assoc :tosca-persistence (services/get-service this :ToscaPersistenceProtocol))
            (update-in [:uri] #(do
                                 (debug %)
                                 (URI/create %)))
            (assoc :resources redo-schema/resources)
            core/add-rest-resource-routes
            :routes
            (add-ring-handler (:context-path (:redo-runtime (get-config))) {:redirect-if-no-trailing-slash true}))
        context)
  (start [this context]
         (info "Validating initialization data" (:service-name (:redo-runtime (get-config))))
         (info "Starting" (:service-name (:redo-runtime (get-config))))
         (doto (as-> (into (:redo-runtime (get-config)) context) loc
                 (assoc loc :persistence (services/get-service this :PersistenceProtocol))
                 (assoc loc :tosca-persistence (services/get-service this :ToscaPersistenceProtocol))
                 (update-in loc [:uri] #(do
                                          (debug %)
                                          (URI/create %)))))
         context)
  (stop [this context]
        (info "Shutting down" (:redo-runtime (get-config)))
        (core/stop-services (:redo-runtime (get-config)))
        context)
  (set-service-initials [_ m] (swap! (:redo-runtime (get-config)) into m))

  (init-resource-driven-process-using-entity-identity! [_ entity-identity]
                                                       (debug "Initialization will be started" entity-identity)
                                                       (as-> (:redo-runtime (get-config)) loc
                                                         (assoc loc :redo/entity-identity  entity-identity)
                                                         (assoc loc :persistence (services/get-service _ :PersistenceProtocol))
                                                         (assoc loc :tosca-persistence (services/get-service _ :ToscaPersistenceProtocol))
                                                         (assoc loc :process-model-should-be-added? true)
                                                         (core/initilize-informal-process! loc)))
  (init-resource-driven-process-model! [_ process-model]
                                       (domain-language/resource-driven-process-definition process-model)
                                       (as-> (:redo-runtime (get-config)) loc
                                         (assoc-in loc [:entity-data] process-model)
                                         (assoc loc :persistence (services/get-service _ :PersistenceProtocol))
                                         (assoc loc :tosca-persistence (services/get-service _ :ToscaPersistenceProtocol))
                                         (core/initilize-informal-process! loc)))
  (init-resource-driven-process-model! [_ process-model instance-id]
                                       (as-> (:redo-runtime (get-config)) loc
                                         (assoc-in loc [:entity-data] process-model)
                                         (assoc loc :process-id instance-id)
                                         (assoc loc :persistence (services/get-service _ :PersistenceProtocol))
                                         (assoc loc :tosca-persistence (services/get-service _ :ToscaPersistenceProtocol))
                                         (core/initilize-informal-process! loc)))
  (terminate-process-execution! [_ process-id instance-id]
                                (core/terminate-process-execution!
                                 (as-> (:redo-runtime (get-config)) loc
                                   (assoc loc :redo/entity-identity process-id)
                                   (assoc loc :redo/parent-instance instance-id)
                                   (into loc {:state-description "Successfully finished"
                                              :instance-state :completed})
                                   (assoc loc :resources redo-schema/resources)
                                   (assoc-in loc [:process-id] process-id)
                                   (assoc loc :persistence (services/get-service _ :PersistenceProtocol))
                                   (assoc loc :tosca-persistence (services/get-service _ :ToscaPersistenceProtocol)))))
  (get-process-instance [_ process-id]
                        (as-> (:redo-runtime (get-config)) loc
                          (assoc-in loc [:process-id] process-id)
                          (assoc loc :persistence (services/get-service _ :PersistenceProtocol))
                          (assoc loc :tosca-persistence (services/get-service _ :ToscaPersistenceProtocol))
                          (core/get-process-instance loc)))
  (update-process-instance! [_ process-instance]
                            (as-> (:redo-runtime (get-config)) loc
                              (assoc-in loc [:entity-data] process-instance)
                              (assoc loc :persistence (services/get-service _ :PersistenceProtocol))
                              (assoc loc :tosca-persistence (services/get-service _ :ToscaPersistenceProtocol))
                              (core/update-informal-process! loc)))
  (list-processes [_]
                  (as-> (:redo-runtime (get-config))loc
                    (assoc loc :persistence (services/get-service _ :PersistenceProtocol))
                    (assoc loc :tosca-persistence (services/get-service _ :ToscaPersistenceProtocol))
                    (core/get-all-process-instances loc)))
  (list-running-processes [_]
                          (as-> (:redo-runtime (get-config)) loc
                            (assoc loc :persistence (services/get-service _ :PersistenceProtocol))
                            (assoc loc :tosca-persistence (services/get-service _ :ToscaPersistenceProtocol))
                            (core/get-all-running-process-instances loc)))
  (list-terminated-processes [_]
                             (as-> (:redo-runtime (get-config)) loc
                               (assoc loc :persistence (services/get-service _ :PersistenceProtocol))
                               (assoc loc :tosca-persistence (services/get-service _ :ToscaPersistenceProtocol))
                               (core/get-all-terminated-process-instances loc))))

(defn get-ipe-runtime-service-instance [m] (core/->IPERuntimeServiceImpl m))
