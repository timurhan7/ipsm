(ns de.uni-stuttgart.iaas.ipsm.ipe-runtime.core
  (:require
   [de.uni-stuttgart.iaas.ipsm.ipe.model.domain-language :as domain-language]
   [de.uni-stuttgart.iaas.ipsm.ipe.model.transformations :as transformations]
   [de.uni-stuttgart.iaas.ipsm.utils.communications :as communications]
   [de.uni-stuttgart.iaas.ipsm.utils.constants :as constants]
   [de.uni-stuttgart.iaas.ipsm.ipe.model.schema :as redo-schema]
   [clojure.core.async :as async]
   [de.uni-stuttgart.iaas.ipsm.utils.conversions :as conversions]
   [clojure.walk :as walk]
   [de.uni-stuttgart.iaas.ipsm.protocols.core :as protocols]
   [de.uni-stuttgart.iaas.ipsm.ipe-runtime.resource-model :as rmi]
   [clojure.java.io :as io]
   [overtone.at-at :as at]
   [taoensso.timbre :as timbre
    :refer (log  trace  debug  info  warn  error  fatal  report
                 logf tracef debugf infof warnf errorf fatalf reportf
                 spy logged-future with-log-level with-logging-config
                 sometimes)])
  (:import [de.uni_stuttgart.iaas.ipsm.executorprocess.data ObjectFactory]
           [uni_stuttgart.ipsm.protocols.services IPERuntimeService]
           [de.uni_stuttgart.iaas.ipsm.executor_process.wsdl IPEExecutorServiceService]
           [de.uni_stuttgart.iaas.ipsm.executorprocess.data TInformalProcessData]
           [javax.xml.ws Endpoint]
           [java.util.zip ZipFile ZipEntry]))

(def dipea-directory "dipeas/")
(def ^:private endpoint (atom {}))

(defn- register-execution-environement-integrator!
  "Handle a registration element of execution environement integrator. "
  [m]
  {:pre [(:persistence m)]}
  (when-let [new-ee (:register-ee (:message-payload m))] ; We are not using :pre for this val as we don't want direct fail
    (debug "The execution environment integrator will be registered" new-ee)
    (let [new-ee-with-id (assoc new-ee :id (conversions/create-unique-id-from-str (str (:type (:entity-map new-ee)) (:id (:entity-map new-ee)))))]
      (do
        (protocols/delete-entity-by-id! (:persistence m) new-ee-with-id)
        (protocols/add-entity! (:persistence m) new-ee-with-id)))))



(defn handle-execution-environment-message
  [m]
  (fn [ch {:keys [content-type delivery-tag type] :as meta} ^bytes payload]
    (debug "Message received")
                                        ;(debug "Message payload" (String. payload "UTF-8"))
    (-> m
        (communications/add-edn-message-body payload)
        register-execution-environement-integrator!)))


(defn- create-process-id
  [m]
  {:pre [(:entity-map m) (:id (:entity-map m))]}
  (conversions/create-unique-id-from-str ;; create the random variable based on the non nil values
   (apply str (remove nil? [(str (new java.util.Date))
                            (get-in m [:entity-map :id])
                            (get-in m [:entity-map :domain-uri])
                            (get-in m [:entity-map :type])]))))

(defn- add-ipe-instance
  [m]
  {:pre [(:process-id m) (:entity-data m)]}
  (assoc m :entity-data
         (assoc (:entity-data m)
                :entity-type :informal-process-instance
                :instance-descriptor
                (domain-language/instance-descriptor-type
                 {:id (:process-id m)
                  :instance-state :initializable
                  :target-namespace (:target-namespace (:entity-data m))}))))

(defn- add-resource-model-instance
  [m]
  {:pre [(:entity-data m)]}
  (-> m
      (update-in [:entity-data] #(clojure.set/rename-keys % {:resource-model :resource-model-instance}))
      (update-in [:entity-data] #(assoc-in % [:resource-model-instance :entity-type] :resource-model-instance))))


(defn- ipe-model->ipe-instance
  [m]
  {:pre [(:entity-data m) (:process-id m)]}
  (debug "Converting ipe model into ipe instance with id" (:process-id m))
  (-> m
      add-resource-model-instance
      add-ipe-instance))

(defn- persist-ipe-instance!
  [m]
  {:pre [(:persistence m) (:entity-data m)]}
  (debug "Persisting ipe model instance"
         (:id (:entity-data m))
         (:instance-state (:instance-descriptor (:entity-data m))))
  (protocols/delete-entity-by-id! (:persistence m) (:entity-data m))
  (protocols/add-entity! (:persistence m) (:entity-data m)))

(defn- get-wsdl-service-client
  []
  (.getIPEExecutorServicePort (IPEExecutorServiceService.)))


(defn- create-persistent-ipe-instance!
  [m]
  (doto (ipe-model->ipe-instance m)
    persist-ipe-instance!))




(def handler-path "/informal-processes/")


(defn- add-process-definition
  [m]
  {:pre [(:content-type m)]}
  (assoc m :process-definition
         (case (:content-type m)
           :edn (transformations/get-informal-process-definition-in-xml m)
           :xml (:entity-data m))))

(defn- generate-informal-process-data
  [m]
  {:pre [(:entity-data m)
         (:process-id m)
         (:process-definition m)]}
  (debug "Setting Informal Process Data for the following id" (:process-id m) (:instance-state m))
  (let [object-factory (ObjectFactory.)]
    (doto (.createTInformalProcessData object-factory)
      (.setModel (:process-definition m))
      (.setState (if (:instance-state m) (str (:instance-state m)) ":unknown"))
      (.setStateDescription (if (:state-description m) (:state-description m) "NOT AVAILABLE"))
      (.setProcessId  (:process-id m)))))

(defn- add-informal-process-data
  [m]
  {:post [(:informal-process-data %)]}
  (assoc m :informal-process-data (generate-informal-process-data m)))


(defn- execute-succesful-engagement!
  [m]
  {:pre [(:executor-wsdl-client m) (:informal-process-data m)]}
  (debug "Engagement succesful message will be sent")
  (.engagementSuccessful (:executor-wsdl-client m) (:informal-process-data m)))

(defn- add-executor-wsdl-client
  [m]
  {:pre [(:executor-wsdl-client-url m)]
   :post [(:executor-wsdl-client %)]}
  (debug "Adding executor client")
  (assoc m :executor-wsdl-client (.getIPEExecutorServicePort (IPEExecutorServiceService. (:executor-wsdl-client-url m)))))


(defn- add-process-content-type
  [m]
  (cond
    (map? (:entity-data m))
    (assoc m :content-type :edn)

    (string? (:entity-data m))
    (assoc m :content-type :xml)

    :default
    (assoc m :content-type :edn)))

(defn- add-process-instance-location
  [m]
  {:pre [(:uri m)
         (:process-id m)]}
  (assoc m :instance-location (if (.endsWith (str (:uri m)) "/")
                                (str (.removeLast (str (:uri m))) handler-path (:process-id m))
                                (str (str (:uri m)) handler-path (:process-id m)))))

(defn- resource-initalization-succ-handler!
  [m]
  (debug "Resource initialization was successful!")
  (cond-> m
    (:process-initialized? m) (assoc :new-state :initialized)
    (:process-initialized? m) domain-language/update-informal-process-instance-state
    :true (#(do (debug "Final resource-driven process is:" (:entity-data (:graph-data %))) %))
    :true persist-ipe-instance!))

(defn- execute-terminate-with-error!
  [m]
  {:pre [(:executor-wsdl-client m) (:informal-process-data m)]}
  (debug "Terminate with error message will be sent")
  (.terminateExecutionWithError (:executor-wsdl-client m) (:informal-process-data m)))

(defn- execute-terminate-successully!
  [m]
  {:pre [(:executor-wsdl-client m) (:informal-process-data m)]}
  (debug "Terminate successfully will be sent")
  (.terminateSuccessfully (:executor-wsdl-client m) (:informal-process-data m)))



;; (def app (user/go))
;; (def test-data (slurp "dev-resources/compiler-output-example.xml"))
;; (print (init-ipe-model! {:ipe-model-xml test-data :persistence (puppetlabs.trapperkeeper.app/get-service app :PersistenceProtocol)}))



(defn initialize-services!
  [initialization-data]
  (debug "WS and REST services will be initialized..."))


(defn init-service!
  [initialization-data]
  (debug "Consumer queue is prepared...")
  (doto (communications/add-connection-data initialization-data)
    communications/create-exchanges!
    communications/create-queues-and-bind-them-to-exchanges!)
  (debug "Resource announcer service is initialized!"))

(defn start-service!
  [initialization-data]
  (doto (communications/add-connection-data initialization-data)
    initialize-services!)
  (info "IPE initializer service has been started!"))


(defn stop-services
  [initialization-data]
  (when (:rest-service @endpoint)
    (.stop (:rest-service @endpoint)))
  (when (:ws @endpoint)
    (.stop (:ws @endpoint))))

(defn- add-ipe-model-map
  [m]
  {:pre [(:entity-data m)]}
  (debug "Adding IPE model from entity data...")
  (assoc m :entity-data
         (transformations/get-map-of-xml-element-str (:entity-data m))))

(defn- add-ipe-model-instance-map
  [m]
  {:pre [(:entity-data m)]}
  (debug "IPE model instance map will be added" )
  (assoc m :entity-data
         (transformations/get-map-of-xml-element-str (:entity-data m))))

(defn- add-process-id
  [m]
  {:pre [(:entity-data m) (map? (:entity-data m)) (:id (:instance-descriptor (:entity-data m)))]}
  (debug "IPE model instance id field will be added")
  (if-not (:process-id m)
    (assoc m :process-id (:id (:instance-descriptor (:entity-data m))))
    m))

(defn- resource-model->resource-model-instance
  [m]
  (update-in [:entity-data]
             #(clojure.set/rename-keys % {:resource-model :resource-model-instance})))

(defn create-model-instance!
  [m]
  {:pre [(:entity-data m) (:process-id m)]}
  (debug "Creating model instance for")
  (->> m
       add-ipe-model-map
       ipe-model->ipe-instance
       domain-language/resources->resource-instances
       :entity-data
       domain-language/resource-driven-process-definition
       (assoc m :entity-data)
       transformations/get-informal-process-definition-in-xml))

(defn- add-entity-id
  [m]
  (when-not (:id (:instance-descriptor (:entity-data m)))
    (error "Process instance id was not provided!!!!!"))
  (assoc-in m [:entity-data :id] (:id (:instance-descriptor (:entity-data m)))))

(defn store-ipe-model-instance!
  [m]
  {:pre [(:persistence m) (:entity-data m)]}
  (-> m
      add-ipe-model-instance-map
      add-entity-id
      persist-ipe-instance!))

(defn- allocate-resource-model!
  [m]
  {:pre [(:persistence m) (:entity-data m) (:uri m)]}
  (debug "Resource allocation is started")
  (future (-> m
              rmi/initialize-resource-model!)))

(defn engage-resources-for-ipe-goals!
  [m]
  {:pre [(:persistence m) (:entity-data m) (:uri m)]}
  (debug "Resources of the given informal process will be engaged")
  (future (-> m
              add-ipe-model-instance-map
              add-process-id
              rmi/initialize-resource-model!))
  true)


(defn- add-process-instances-from-persistence
  [m]
  {:pre [(:persistence m)]}
  (assoc m
         :entity-data
         (protocols/get-all-entities
          (:persistence m)
          (-> m
              (assoc-in [:entity-data :entity-type] :informal-process-instance)
              :entity-data))))


(defn- add-process-instance-from-persistence
  [m]
  {:pre [(:process-id m) (:persistence m)]}
  (assoc m
         :entity-data
         (protocols/get-entity (:persistence m)
                               (-> m
                                   (assoc-in [:entity-data :id] (:process-id m))
                                   (assoc-in [:entity-data :entity-type] :informal-process-instance)
                                   :entity-data))))

(defn- delete-process-instance-from-persistence!
  [m]
  {:pre [(:process-id m) (:persistence m)]}
  (protocols/delete-entity-by-id! (:persistence m)
                                  (-> m
                                      (assoc-in [:entity-data :id] (:process-id m))
                                      (assoc-in [:entity-data :entity-type] :informal-process-instance)
                                      :entity-data)))

(defn- add-resource-initialization-success-handler
  [m]
  (assoc m :resource-model-init-succ-fn! resource-initalization-succ-handler!))






(defn- release-resources-if-needed!
  [m]
  {:pre [(:instance-state m) (:target-state m)]}
  (debug "Releasing resources if needed" (:instance-state m) (:target-state m))
  (when-not (= (:instance-state m) (:target-state m))
    (rmi/release-resources-of-an-informal-process-instance! m)))

(defn- update-and-return-process-model
  [m]
  {:pre [(:target-state m)]}
  (debug "Updating and returning process model!!" (:target-state m))
  (-> m
      (assoc :new-state (:target-state m))
      domain-language/update-informal-process-instance-state
      domain-language/update-informal-process-resources-state))

(defn- execute-process-termination!
  [m]
  (release-resources-if-needed! m)
  (debug (update-and-return-process-model m))
  (doto (-> m
            update-and-return-process-model
            :entity-data)
    (#(protocols/delete-entity-by-id! (:persistence m) %))
    (#(protocols/add-entity! (:persistence m) %))))


(defn- add-process-instance-state
  [m]
  {:pre [(:redo/entity-identity m)]}
  (->> (assoc m
              :field-key :redo/instance-descriptors
              :redo/entity-identity (:redo/parent-instance m))
       redo-schema/add-field-data-with-entity-identiy
       :field-data
       :redo/instance-state
       (assoc m :instance-state)))



(defn- resource-initalization-fail-handler!
  [m]
  (debug "The process initialization has been delegated to the core process with error...")
  (-> m
      (assoc :target-state :failed)
      add-process-instance-state
      execute-process-termination!))

(defn- add-resource-initialization-fail-handler
  [m]
  (assoc m :resource-model-init-fail-fn! resource-initalization-fail-handler!))

(defn- handle-process-termination!
  [m]
  (-> m
      add-ipe-model-instance-map
      add-process-id
      add-process-instance-state
      execute-process-termination!))


(defn- add-informal-process-from-persistence
  [m]
  {:pre [(:persistence m) (:entity-data m)]}
  (assoc m :entity-data (protocols/get-entity (:persistence m) (:entity-data m))))

(defn- add-process-instance-id
  [m]
  {:post [(:process-id %)]}
  (assoc m :process-id {:redo/name (str (:redo/name (:redo/entity-identity m)) "-" (str (new java.util.Date)) "-" (conversions/create-unique-id))
                        :redo/target-namespace (:instance-descriptor-uri m)}))

(defn- instantiate-initalization-process!
  [m]
  {:pre [(:entity-data m)]}
  (debug "Resources will be allocated" (:entity-data m))
  (do
    (protocols/delete-entity-by-id! (:persistence m) (:entity-data m))
    (protocols/add-entity! (:persistence m) (:entity-data m)) ;; save new instace
    (rmi/initialize-resource-model! m)))

(defn- update-process-instance!
  [m]
  {:pre [(:executor-wsdl-client m) (:informal-process-data m)]}
  (debug "Instantiating process with informal procss data" (:informal-process-data m))
  (.updateIPEProcess (:executor-wsdl-client m) (:informal-process-data m)))


;; a simple assumption should be further checked

(defn- add-process-content-if-not-there
  [m]
  (if-not (:content-type m)
    (add-process-content-type m)))

(defn- add-new-instance-descriptor
  [m]
  {:pre [(:entity-data m)
         (:uri m)
         (:redo/entity-identity m)
         (:process-id m)
         (:importance m)
         (:instance-state m)
         (:instance-descriptor-uri m)]}

  (assoc m :instance-descriptor (let [start-time (str (new java.util.Date))]
                                  (domain-language/instance-descriptor-type
                                   {:redo/identifiable-entity-definition {:redo/entity-identity (:process-id m)}
                                    :redo/instance-state (:instance-state m)
                                    :redo/source-model (:redo/entity-identity m)
                                    :redo/start-time (str start-time)
                                    :redo/instance-uri (str (:uri m)
                                                            "/redo-editor"
                                                            (-> m (assoc :redo/entity-type (:redo/entity-type (:entity-data m))) redo-schema/add-resolved-path-of-entity-type :path)
                                                            "/"
                                                            (conversions/url-encode (-> m redo-schema/add-entity-identity-of-entity-data :redo/entity-identity :redo/target-namespace))

                                                            "/"
                                                            (conversions/url-encode (-> m redo-schema/add-entity-identity-of-entity-data :redo/entity-identity :redo/name))
                                                            "/instance-descriptors/"
                                                            (conversions/get-unique-id-from-entity-identity (:process-id m))) ;; must be updated
                                    :redo/importance (:importance m)
                                    :redo/propogate-success false
                                    :redo/propogate-failure true}))))

(defn remove-invalid-map-entries
  [m]
  (let [f (fn [[k v]] (when-not (re-find #".*(:|/| ).*" (if (keyword? k) (name k) k)) [k v]))]
    (walk/postwalk (fn [x] (if (map? x) (into {} (map f x)) x)) m)))





(defn add-resolved-resource-model-if-not-there
  [m]
  {:pre [(:entity-data m)
         (= (:redo/entity-type (:entity-data m)) :redo/resource-driven-process-definition)]}
  (debug "Adding resolved resource model if it is not there using tosca persistence")
  (if-not (and (:redo/resource-model (:entity-data m))
               (:redo/model-content (:redo/resource-model (:entity-data m))))
    (if (:redo/model-uri (:redo/resource-model (:entity-data m)))
      (->> (assoc m :service-template-uri (:redo/model-uri (:redo/resource-model (:entity-data m))))
           (#(protocols/get-service-template-on-location (:tosca-persistence m) %))
           :defs
           remove-invalid-map-entries
           (assoc-in m [:entity-data :redo/resource-model :redo/model-content]))
      (error "Model cannot be resolved as the model-uri is missing!"))
    m)) ;; content is already there



(defn- get-process-model-from-persistence-if-not-provided
  [m]
  (debug "Getting the process model" (:entity-data m)
         (:process-model-should-be-added? m)
         (:redo/entity-identity m)
         (-> m
             (assoc :resources redo-schema/resources)
             (assoc-in [:entity-data :redo/entity-type] :redo/resource-driven-process-definition)
             (assoc :new-data (:redo/entity-identity m))
             (assoc :field-key :redo/entity-identity)
             redo-schema/update-field-data
             :entity-data
             (#(protocols/get-entity (:persistence m) %))))
  (or (:entity-data m)
      (and
       (:process-model-should-be-added? m)
       (:redo/entity-identity m)
       (-> m
           (assoc :resources redo-schema/resources)
           (assoc-in [:entity-data :redo/entity-type] :redo/resource-driven-process-definition)
           (assoc :new-data (:redo/entity-identity m))
           (assoc :field-key :redo/entity-identity)
           redo-schema/update-field-data
           :entity-data
           (#(protocols/get-entity (:persistence m) %))))))


(defn- add-actual-target-intentions
  [m]
  (some->> m
           :entity-data
           :redo/target-intentions
           (mapv #(-> m
                      (assoc-in [:entity-data :redo/entity-type] :redo/intention-definition)
                      (assoc :new-data %)
                      (assoc :field-key :redo/entity-identity)
                      redo-schema/update-field-data
                      :entity-data
                      ((fn [entity-data] (protocols/get-entity (:persistence m) entity-data)))))
           (assoc m :actual-target-intentions)))

(defn- add-target-intention-instance-descriptors
  [m]
  {:pre [(:instance-descriptor m)
         (:instance-descriptor-uri m)]}
  (some->> m
           :entity-data
           :redo/target-intentions

           (mapv #(let [id (conversions/create-unique-id)]
                    (domain-language/instance-descriptor-type
                     {:redo/identifiable-entity-definition {:redo/entity-identity {:redo/name (str (:redo/name %) "-" (:redo/start-time (:instance-descriptor m)) "-" id)
                                                                                   :redo/target-namespace (:instance-descriptor-uri m)}}
                      :redo/instance-state :in-progress
                      :redo/source-model %
                      :redo/start-time (:redo/start-time (:instance-descriptor m))
                      :redo/instance-uri (str (:uri m) "/instance-descriptors/" (conversions/url-encode (str (:redo/name %) "-" (:redo/start-time (:instance-descriptor m)) "-" id))) ;; must be updated
                      :redo/importance (:importance m)

                      :redo/propogate-success false
                      :redo/propogate-failure true})))

           (mapv #(vector %1 %2) (:actual-target-intentions m))

           (assoc m :target-intention-instances)))


(defn- update-target-intentions-with-new-instance-descriptors
  [m]
  (some->> m
           :target-intention-instances
           (mapv #(-> m
                      (assoc :entity-data (first %))
                      (assoc :instance-descriptor (second %))
                      domain-language/add-instance-descriptor-into-definition-model
                      :entity-data))
           (assoc m :updated-intention-definitions)))



(defn- update-target-intentions!
  [m]
  (debug "Updating target intentions")
  (when (:redo/target-intentions (:entity-data m))
    (some->> m
             add-actual-target-intentions
             add-target-intention-instance-descriptors
             update-target-intentions-with-new-instance-descriptors
             :updated-intention-definitions
             (mapv #(do (protocols/delete-entity-by-id! (:persistence m) %)
                        (protocols/add-entity! (:persistence m) %)))))
  nil)


(defn initilize-informal-process!
  [m]
  (debug "Initializing the process with the following map" (:redo/entity-identity m))
  (locking (if (:redo/entity-identity m)
             (rmi/get-process-lock m)
             (-> m
                 (assoc :resources redo-schema/resources)
                 redo-schema/add-entity-identity-of-entity-data
                 rmi/get-process-lock))
      (let [process-model (get-process-model-from-persistence-if-not-provided m)]
        (debug "Initializing process for" process-model)
        (when process-model
          (:process-id (doto (-> m
                                 (assoc :entity-data process-model)
                                 (assoc :resources redo-schema/resources)
                                 redo-schema/add-entity-identity-of-entity-data
                                 add-process-instance-id
                                 redo-schema/add-entity-identity-of-entity-data
                                 add-process-content-if-not-there
                                 (assoc :instance-state "initializing")
                                 (assoc :importance 5)
                                 (assoc :state-description "Process is initialized!!!!")
                                 add-new-instance-descriptor
                                 domain-language/add-instance-descriptor-into-definition-model
                                 add-resolved-resource-model-if-not-there
                                 add-resource-initialization-fail-handler
                                 add-resource-initialization-success-handler)
                         update-target-intentions!
                         persist-ipe-instance!
                         instantiate-initalization-process!))))))


(defn update-informal-process!
  [m]
  (debug "Updating the process with the following map" m)
  (:process-id (doto (-> m
                         add-executor-wsdl-client
                         add-ipe-model-instance-map
                         add-process-id
                         add-process-content-if-not-there
                         (assoc :instance-state :updating)
                         (assoc :state-description "Process is being updated!!!!")
                         add-process-definition
                         add-informal-process-data)
                 update-process-instance!)))


(defn- terminate-process-execution-based-on-instance-state!
  [m]
  {:pre [(:instance-state m)]}
  (case (:instance-state m)
    :terminated (execute-terminate-with-error! m)
    :completed (execute-terminate-successully! m)))

(defn- add-process-data-using-empty-entity-identity
  [m]
  {:pre [(:redo/entity-identity m)]}
  (->> (assoc m
              :entity-data {:redo/entity-type :redo/resource-driven-process-definition}
              :new-data (:redo/entity-identity m)
              :field-key :redo/entity-identity)
       redo-schema/update-field-data))




(defn- add-default-target-state
  [m]
  (if-not (:target-state m)
    (assoc m :target-state :completed)
    m))


(defn terminate-process-execution!
  [m]
  (->> m
       add-process-data-using-empty-entity-identity
       add-informal-process-from-persistence
       add-process-instance-state
       add-default-target-state
       execute-process-termination!))

(defn get-process-instance              ;
  [m]
  (-> m
      add-process-instance-from-persistence
      transformations/get-informal-process-definition-in-xml))

(defn get-all-process-instances
  [m]
  (->> m
       add-process-instances-from-persistence
       :entity-data
       (mapv :id)))

(defn get-all-running-process-instances
  [m]
  (->> m
       add-process-instances-from-persistence
       :entity-data
       (filterv #(do

                   (= (add-process-instance-state %) :initialized)))
       (mapv :id)))

(defn get-all-terminated-process-instances
  [m]
  (->> m
       add-process-instances-from-persistence
       :entity-data
       (filterv #(= (add-process-instance-state m) :terminated))
       (mapv :id)))


(defn update-resources-for-ipe-goals!
  [m]
  {:pre [(:persistence m) (:entity-data m) (:uri m)]}
  (debug "Resources of the given informal process will be engaged")
  (future (-> m
              add-ipe-model-instance-map
              add-process-id
              rmi/initialize-resource-model!))
  true)


(deftype ^{WebService {:endpointInterface
                       "uni_stuttgart.ipsm.protocols.services.IPERuntimeService"}}
    IPERuntimeServiceImpl [m]
  IPERuntimeService
  (createIPEInstance [_ ipe-model process-id]
    (info "IPE Model Instance will be created for the model with the identifier" process-id)
    (debug "Process model is" ipe-model)
    (create-model-instance! {:entity-data ipe-model :process-id process-id}))
  (storeIPEModelInstance [_ ipe-model-instance]
    (info "IPE Model Instance will be stored and persisted" ipe-model-instance)
    (store-ipe-model-instance! (assoc m :entity-data ipe-model-instance))
    (debug "Returning true for store response model!")
    true)
  (updateProcessInstance [_ new-ipe-model-instance]
    (info "IPE Model Instance will be updated")
    (update-resources-for-ipe-goals! (assoc m :entity-data new-ipe-model-instance)))
  (engageResources [_ ipe-model-instance]
    (info "Resources of the given IPE model instance will be initialized!" m)
    (engage-resources-for-ipe-goals! (assoc m :entity-data ipe-model-instance)))
  (terminateInstanceWithError [_ ipe-model-instance error-def]
    (info "Terminating instance with an error!!")
    (handle-process-termination! (into m {:entity-data ipe-model-instance :state-description error-def :target-state :terminated})))
  (terminateInstanceSuccessfully [_ ipe-model-instance]
    (info "Terminating ninstance successfully!!")
    (handle-process-termination! (into m {:entity-data ipe-model-instance :state-description "Succesfully finished" :target-state :completed}))))


(defn- handle-resource-model-allocation!
  [m])

(defn- init-resource-driven-process
  [m]
  (doto (-> m
            redo-schema/add-entity-identity-of-entity-data
            add-new-instance-descriptor
            ;; add-new-instance-descriptor-into-resource-driven-process
            )
    ;; persist-ipe-instance!
    ;; allocate-resource-model!
    ))

(defn init-ws-service!
  [m]
  {:pre [(m :ws-host)
         (m :ws-port)]}
  (when-not (nil? (:ws @endpoint))
    (.stop (:ws @endpoint)))
  (swap! endpoint assoc
         :ws (Endpoint/publish (str "http://"
                                    (m :ws-host)
                                    ":"
                                    (m :ws-port)
                                    "/runtime-service")
                               (IPERuntimeServiceImpl. (-> m
                                                           add-resource-initialization-success-handler
                                                           add-resource-initialization-fail-handler))))
  nil)



;; (def operation-schema ^:private
;;   {:operation-type s/Str
;;    :parameters s/Any})

(defn- return-process-fn
  "Returns available process instances"
  [m]
  (fn [ctx]
    (let [mt (get-in ctx [:representation :media-type])
          id (get-in ctx [:request :params :id])]
      (cond->> (assoc m :process-id id)
        :true add-process-instance-from-persistence
        (= mt "application/xml") transformations/get-informal-process-definition-in-xml
        (or (= mt "application/edn")(= mt "application/json")) :entity-data
        (= mt "application/edn") str
        (= mt "application/json") clojure.data.json/write-str))))



(defmulti handle-post-operation! :operation-type)

(defmethod handle-post-operation! (constants/property :release-resource-operation-name)
  [m]
  {:pre [(:operation-body m)]}
  (doto (-> m
            (assoc :instance-state
                   (or (get-in m [:operation-body :instance-state])
                       :completed))
            (assoc :state-description
                   (or (get-in m [:operation-body :instance-descriptor])
                       "Process has been completed succesfully via a REST client...")))
    terminate-process-execution!)
  {::response-map "Release has been executed!"})

(defmethod handle-post-operation! (constants/property :acquire-resource-operation-name)
  [m]
  {:pre [(:operation-body m)]}
  {::response-map (->> m
                       :operation-body
                       conversions/entity-type-str>entity-type-keyword ;; fix keywords
                       domain-language/resource-driven-process-definition
                       (assoc m :entity-data)
                       redo-schema/add-entity-identity-of-entity-data
                       initilize-informal-process!)})

(defmethod handle-post-operation! :default
  [m]
  {:pre [(:operation-body m)]}
  (cond
    ((:process-id m) (:resource-id (:operation-body m)))
    (do
      (debug "Resource specific operation will be executed!!"))

    (:process-id m)
    (do
      (debug "Process specific operation will be executed!!"))

    :else
    (do
      (debug "An unknown operation without resource or process ID has been sent!"))))

(defn- delete-process!-fn
  "Returns available process instances"
  [m]
  (fn [ctx]
    (doto (-> m
              (assoc :process-id (get-in ctx [:request :params :id]))
              (assoc :instance-state
                     (or (get-in ctx :request [:params :operation-body :instance-state])
                         :completed))
              (assoc :state-description
                     (or (get-in ctx [:request :params :operation-body :instance-descriptor])
                         "Process has been completed succesfully via a REST client...")))
      delete-process-instance-from-persistence!)))

(defn- return-processes-fn
  [m]
  (fn [ctx]
    (debug "Returning process instances")
    (let [mt (get-in ctx [:representation :media-type])]
      (cond->> m
        :true add-process-instances-from-persistence
        :true :entity-data
        :true (mapv #(select-keys % [:id :name :target-namespace :instance-descriptor]))
        (= mt "application/edn") str
        (= mt "application/json") clojure.data.json/write-str))))

;; (s/defschema OperationsOnProcesses
;;   {(s/required-key :operation-type) s/Str
;;    (s/optional-key :process-id) s/Str
;;    (s/optional-key :operation-body) s/Any})


(defn- post-processes!-fn
  "Returns available process instances"
  [m]
  (fn [ctx]
    (debug "Post process request has been recieved" (:request ctx))
    ;; (s/validate OperationsOnProcesses (get-in ctx [:request :params]))
    (-> m
        (assoc :process-id (or (get-in ctx [:request :params :process-id])
                               (get-in ctx [:request :params :id])))
        (assoc :operation-type (get-in ctx [:request :params :operation-type]))
        (assoc :operation-body (get-in ctx [:request :params :operation-body]))
        handle-post-operation!)))

(defn- handle-resource-creation
  "Handle created resource"
  [m]
  (fn [ctx]
    (if (::response-map ctx)
      (str (::response-map ctx))
      (str "Process has been created!!"))))


(defn route-configs
  [m]
  [{:resource-path (str handler-path ":id")
    :allowed-methods [:get :delete :post]
    :available-media-types ["application/edn"
                            "application/xml"
                            "application/json"]
    :post! (post-processes!-fn m)
    :delete! (delete-process!-fn m)
    :handle-ok (return-process-fn m)}
   {:resource-path (str handler-path)
    :allowed-methods [:get :post]
    :media-type ["application/json"]
    :available-media-types ["application/edn"
                            "application/json"]
    :handle-created (handle-resource-creation m)
    :handle-ok (return-processes-fn m)
    :post! (post-processes!-fn m)}])



(defn- rest-handler-config
  [m]
  (into m {:resource-config
           {:route-configs
            (into (rmi/get-route-configs m)
                  (route-configs m))}}))


(defn add-rest-resource-routes
  [m]
  (debug "Adding routes in the path" (:context-path m))
  (-> m
      add-resource-initialization-success-handler
      add-resource-initialization-fail-handler
      rest-handler-config
      (assoc :host (.getHost (:uri m)))
      (assoc :port (.getPort (:uri m)))
      communications/add-routes))

(defn init-rest-service!
  [m]
  (->> m
       add-rest-resource-routes
       communications/run-server-detached!
       (swap! endpoint assoc :rest-service))
  nil)
