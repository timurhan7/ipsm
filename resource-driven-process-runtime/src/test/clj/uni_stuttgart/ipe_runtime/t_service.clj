(ns uni-stuttgart.ipe-runtime.t-service
  (:require [taoensso.timbre :as ti]
            [clojure.test :as clj-test]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.domain-language :as domain-language]
            [de.uni-stuttgart.iaas.ipsm.protocols.core :as protocols]
            [de.uni-stuttgart.iaas.ipsm.ipe.model.schema :as sc]
            [de.uni-stuttgart.iaas.ipsm.utils.conversions :as ipsm-conversions]))

(def test-data {:redo/resource-model {:redo/model-uri "http://localhost:8080/winery-ipsm/servicetemplates/http%253A%252F%252Fwww.example.org%252FTOSCA%252FPHPApp/Bug_Fixing_Process/"}, :redo/initializable-entity-definition {:redo/identifiable-entity-definition {:redo/entity-identity {:redo/name "Bug Fixing Process", :redo/target-namespace "http://www.example.org/TOSCA/PHPApp"}}}, :redo/entity-type :redo/resource-driven-process-Definition})

(def entity-identity {:redo/name "Bug fixing process with human resources", :redo/target-namespace "http://www.uni-stuttgart.de/iaas/resource-driven-processes"})

(defonce app (user/go))
(def persistence (puppetlabs.trapperkeeper.app/get-service app :PersistenceProtocol))
(def ipe-runtime (puppetlabs.trapperkeeper.app/get-service app :RedoRuntime))


(def temp-store (atom []))


;; (protocols/init-resource-driven-process-using-entity-identity! ipe-runtime entity-identity)

(clj-test/deftest test-get-multiple
  (clj-test/is
   (domain-language/resource-driven-process-definition test-data))
  (clj-test/is
   (protocols/init-resource-driven-process-model! ipe-runtime test-data))
  (clj-test/is
   (swap! temp-store conj (protocols/init-resource-driven-process-using-entity-identity! ipe-runtime entity-identity))))

                                        ; (protocols/terminate-process-execution! ipe-runtime entity-identity (first @temp-store))

(clj-test/run-tests 'uni-stuttgart.ipe-runtime.t-service)

;; FIX instance state is added into a map into a vector after a failure must be corrected {0 {:instance state}}
