# README #

Root repository containing the prototypes regarding resource-driven processes
Currently tools are named using informal processes this will be changed in the future

Resource-driven Process Modeling and Support tool (aka. Informal Process Modeling and Support tool)

### How do I get set up? ###
* First install Leiningen from http://leiningen.org/
* Install winery from https://github.com/eclipse/winery switch to the ipsm branch
* Fetch submodules such as protocols and co-act
* run lein modules install to build the whole project


### Contribution guidelines ###
Make self-descriptive commit messages
* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
